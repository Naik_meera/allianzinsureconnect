package com.metamorphosys.insureconnect.transferobjects;

public class TokenValidationResponseTO {
	
	public static final String VALID="VALID";
	public static final String INVALID="INVALID";
	private String responseCode;
	private String responseDescription;
	private String user;
	private String issuer;
	private String audience;
	private String hostName;
	
	private String createdDate;
	private String userGroups;
	private String partnerId;
	private String token;
	private String tokenStatus;
	private String userGroupSeparator;
	private String technicalUserId;
	
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseDescription() {
		return responseDescription;
	}
	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getIssuer() {
		return issuer;
	}
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	public String getAudience() {
		return audience;
	}
	public void setAudience(String audience) {
		this.audience = audience;
	}
	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getUserGroups() {
		return userGroups;
	}
	public void setUserGroups(String userGroups) {
		this.userGroups = userGroups;
	}
	public String getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getTokenStatus() {
		return tokenStatus;
	}
	public void setTokenStatus(String tokenStatus) {
		this.tokenStatus = tokenStatus;
	}
	public String getUserGroupSeparator() {
		return userGroupSeparator;
	}
	public void setUserGroupSeparator(String userGroupSeparator) {
		this.userGroupSeparator = userGroupSeparator;
	}
	public String getTechnicalUserId() {
		return technicalUserId;
	}
	public void setTechnicalUserId(String technicalUserId) {
		this.technicalUserId = technicalUserId;
	}
	
	
}
