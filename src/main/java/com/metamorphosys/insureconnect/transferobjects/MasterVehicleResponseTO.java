package com.metamorphosys.insureconnect.transferobjects;


import java.util.List;

import com.metamorphosys.insureconnect.dataobjects.master.VehicleMasterDumpDO;

public class MasterVehicleResponseTO {
	
	private String statusCode;
	private List<VehicleMasterDumpDO> replyMessageList;
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public List<VehicleMasterDumpDO> getReplyMessageList() {
		return replyMessageList;
	}
	public void setReplyMessageList(List<VehicleMasterDumpDO> replyMessageList) {
		this.replyMessageList = replyMessageList;
	}
	
}
