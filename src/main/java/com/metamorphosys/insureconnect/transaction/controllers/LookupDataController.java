package com.metamorphosys.insureconnect.transaction.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metamorphosys.insureconnect.controllers.WebExecuteController;
import com.metamorphosys.insureconnect.controllers.WebGetController;
import com.metamorphosys.insureconnect.controllers.WebSaveController;
import com.metamorphosys.insureconnect.dataobjects.master.LookupDataDO;
import com.metamorphosys.insureconnect.jpa.master.LookupDataRepository;
import com.metamorphosys.insureconnect.jpa.master.MetadataRepository;

@RestController
@RequestMapping("/lookupdata")
public class LookupDataController {

	private static final Logger LOGGER =  LoggerFactory.getLogger(LookupDataController.class);

	@Autowired
	WebExecuteController webExecuteController;
	
	@Autowired 
	WebGetController webGetController;
	
	@Autowired
	MetadataRepository metadataRepository;
	
	@Autowired 
	WebSaveController webSaveController;

	@Autowired
	 LookupDataRepository lookupDataRepository;
	
	@RequestMapping(value = "/fetchAll" , method = RequestMethod.GET)
	ResponseEntity fetchAll() {
		
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		LOGGER.info("Fetch SearchLookupDataController");
		
		List<LookupDataDO> lookupDataDOs =(List<LookupDataDO>) lookupDataRepository.findAll();
		
				
		return new ResponseEntity(lookupDataDOs, httpHeaders, httpStatus);	
	}
	

}
