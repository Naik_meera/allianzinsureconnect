package com.metamorphosys.insureconnect.transaction.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metamorphosys.insureconnect.common.interfaceobjects.RequestDataIO;
import com.metamorphosys.insureconnect.controllers.WebExecuteController;
import com.metamorphosys.insureconnect.controllers.WebGetController;
import com.metamorphosys.insureconnect.controllers.WebSaveController;
import com.metamorphosys.insureconnect.dataobjects.master.MetadataDO;
import com.metamorphosys.insureconnect.dataobjects.master.PreferenceDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.ServiceDO;
import com.metamorphosys.insureconnect.jpa.master.MetadataRepository;
import com.metamorphosys.insureconnect.ruleengine.RuleFunction;
import com.metamorphosys.insureconnect.utilities.SerializationUtility;

@RestController
@RequestMapping("/metadataController")
public class MetadataController {
	
	private static final Logger LOGGER =  LoggerFactory.getLogger(MetadataController.class);

	@Autowired
	WebExecuteController webExecuteController;
	
	@Autowired 
	WebGetController webGetController;
	
	
	@Autowired 
	WebSaveController webSaveController;
	
	@Autowired
	MetadataRepository metadataRepository;
	
	@Autowired
	ApplicationContext applicationContext;
	
	@CrossOrigin
	@RequestMapping(value = "/fetch/{metadatacd}" , method = RequestMethod.GET)
	ResponseEntity fetchByrulenameandservice(@PathVariable("metadatacd") String metadatacd) {
		
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
//		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		
		LOGGER.info("Fetch SearchMetaDataController..neha");
		
		MetadataDO metadataDO =(MetadataDO) metadataRepository.findByMetadataCd(metadatacd)	;		
		return new ResponseEntity(metadataDO, httpHeaders, httpStatus);	
	}		
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity save(@RequestBody @Valid List<MetadataDO> metadataDOList) {
		
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
//		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		LOGGER.info("save MetadataController");
		List<String> data = new ArrayList<String>();
			try {
				for(int i=0;i<metadataDOList.size();i++){
					data.add(webSaveController.process(convertTojson(metadataDOList.get(i),null, null,metadataDOList.get(i).getObjectName())));
				}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();

		}
		return new ResponseEntity(data, httpHeaders, httpStatus);		
	}
	
	public String convertTojson(Object object, ServiceDO serviceDO, PreferenceDO preferenceDO,String objectName) {
		List<RequestDataIO> requestDataIOs = new ArrayList<RequestDataIO>();

		if(object!=null){
			RequestDataIO requestDataIO = new RequestDataIO();
			requestDataIO.setObjectName(objectName);
			requestDataIO.setObject(SerializationUtility.getInstance().toJson(object));
			requestDataIOs.add(requestDataIO);
		}

		if(serviceDO!=null){
			RequestDataIO requestDataIO1 = new RequestDataIO();
			requestDataIO1.setObjectName(serviceDO.getObjectName());
			requestDataIO1.setObject(SerializationUtility.getInstance().toJson(serviceDO));
			requestDataIOs.add(requestDataIO1);
		}
		
		if(preferenceDO!=null){
			RequestDataIO requestDataIO2 = new RequestDataIO();
			requestDataIO2.setObjectName(preferenceDO.getObjectName());
			requestDataIO2.setObject(SerializationUtility.getInstance().toJson(preferenceDO));
			requestDataIOs.add(requestDataIO2);
		}

		String serialize = SerializationUtility.getInstance().toJson(requestDataIOs);
		return serialize;
	}
	
	@CrossOrigin
	@RequestMapping(value = "/loadMetaData/{metadatacd}" , method = RequestMethod.GET)
	ResponseEntity loadMetaData(@PathVariable("metadatacd") String metadatacd) {
		
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
//		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		Object returnObject = null;
		try
		{
			Object appContextObject = applicationContext.getBean("loadMetadata"); 
			if(null != appContextObject)
			{
				RuleFunction ruleFunction = (RuleFunction)appContextObject;
				
				Object[] functionParameters = new Object[1];
				for (int i = 0; i < functionParameters.length; i++) 
				{
					functionParameters[i] = metadatacd.trim();
				}
				returnObject = ruleFunction.execute(functionParameters);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return new ResponseEntity(returnObject, httpHeaders,httpStatus);
	}

}
