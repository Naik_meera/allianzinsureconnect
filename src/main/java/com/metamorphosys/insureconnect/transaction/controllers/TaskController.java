package com.metamorphosys.insureconnect.transaction.controllers;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metamorphosys.insureconnect.controllers.WebExecuteController;
import com.metamorphosys.insureconnect.controllers.WebGetController;
import com.metamorphosys.insureconnect.controllers.WebSaveController;
import com.metamorphosys.insureconnect.dataobjects.master.TaskDO;
import com.metamorphosys.insureconnect.jpa.master.TaskRepository;
import com.metamorphosys.insureconnect.utilities.SerializationUtility;

@RestController
@RequestMapping("/task")
public class TaskController {
	private static final Logger LOGGER =  LoggerFactory.getLogger(TaskController.class);

	@Autowired
	WebExecuteController webExecuteController;
	
	@Autowired 
	WebGetController webGetController;
	
	@Autowired
	TaskRepository taskRepository;
	
	@Autowired 
	WebSaveController webSaveController;

	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity save(@RequestBody @Valid String json){
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		TaskDO taskDO = (TaskDO) SerializationUtility.getInstance().fromJson(json, TaskDO.class);
		taskRepository.save(taskDO);		
		LOGGER.info("save RuleEngineController");
		
		return new ResponseEntity(taskDO, httpHeaders, httpStatus);	
	}
	
	
	
	
	@RequestMapping(value = "/fetchAll" , method = RequestMethod.GET)
	ResponseEntity fetchAll() {
		
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		LOGGER.info("Fetch SearchRuleController");
		
		List<TaskDO> ruleList =(List<TaskDO>) taskRepository.findAll();
//		List<TaskDO> ruleList =(List<TaskDO>) ruleRepository.findByRuleNameAndServiceAndServiceTypeAndServiceTask("Flood-Water-Hammer-RateTable","Product","ProductCalculation","FWHREGULARPREMIUM");
//		for(TaskDO taskDO:ruleList){
//			taskDO.setRuleDataList(null);
//		}
		return new ResponseEntity(ruleList, httpHeaders, httpStatus);	
	}
	
	@RequestMapping(value = "/fetchByService/{service}" , method = RequestMethod.GET)
	ResponseEntity fetchByService(@PathVariable("service") String service) {
		
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		LOGGER.info("Fetch SearchRuleController");
		
		List<TaskDO> ruleList =(List<TaskDO>) taskRepository.findByService(service);
//		List<TaskDO> ruleList =(List<TaskDO>) ruleRepository.findByRuleNameAndServiceAndServiceTypeAndServiceTask("Flood-Water-Hammer-RateTable","Product","ProductCalculation","FWHREGULARPREMIUM");
//		for(TaskDO taskDO:ruleList){
//			taskDO.setRuleDataList(null);
//		}
		return new ResponseEntity(ruleList, httpHeaders, httpStatus);	
	}
	
	@RequestMapping(value = "fetchById/{id}", method = RequestMethod.GET)
	ResponseEntity fetchById(@PathVariable("id") Long id) {
	
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
				
		List<TaskDO> ruleList =(List<TaskDO>) taskRepository.findById(id);
		return new ResponseEntity(ruleList, httpHeaders, httpStatus);	
	}
	
}
