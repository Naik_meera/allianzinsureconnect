package com.metamorphosys.insureconnect.transaction.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metamorphosys.insureconnect.dataobjects.master.LookupDataDO;
import com.metamorphosys.insureconnect.dataobjects.master.LookupMasterDO;
import com.metamorphosys.insureconnect.jpa.master.LookupMasterRepository;

@RestController
@RequestMapping("/lookupMaster")
public class LookupMasterController {

	@Autowired
	LookupMasterRepository lookupMasterRepository;
	
	@Autowired
	@Qualifier("masterDB")
	JdbcTemplate jdbcTemplate;
	
	/*@Autowired
	public LookupMasterController(LookupMasterRepository lookupMasterRepository, JdbcTemplate jdbcTemplate) {
		super();
		this.lookupMasterRepository = lookupMasterRepository;
		this.jdbcTemplate = jdbcTemplate;
	}*/
	
	@RequestMapping(value="{lookupCd}", method = RequestMethod.GET)
	public List<LookupDataDO> getLookupData(@PathVariable String lookupCd){ 
		
		LookupMasterDO lookupMasterDO = lookupMasterRepository.findByLookupCd(lookupCd);
		
		if(lookupMasterDO!=null){
			try{
				List<LookupDataDO> lookupDataDOs = getDataFromLookupMaster(lookupMasterDO);
				
				return lookupDataDOs;
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		return null;
	}

	private List<LookupDataDO> getLookupDataList(List<Map<String, Object>> list,LookupMasterDO lookupMasterDO) {
		List<LookupDataDO> lookupDataDOs = new ArrayList<LookupDataDO>();
		for (Map<String, Object> row : list) {
			LookupDataDO lookupDataDO = new LookupDataDO();
			lookupDataDO.setLookupCd(lookupMasterDO.getLookupCd());
			lookupDataDO.setLookupKey((String) row.get(lookupMasterDO.getLookupDataCd()));
			lookupDataDO.setLookupValue((String) row.get(lookupMasterDO.getLookupDataValue()));
			lookupDataDOs.add(lookupDataDO);
		}
		return lookupDataDOs;
	}

	private List<LookupDataDO> getDataFromLookupMaster(LookupMasterDO lookupMasterDO) {
		if(lookupMasterDO.getLookupQueryTable()!=null){
			String sql="select ";
			if(lookupMasterDO.getLookupDataCd()!=null){
				sql = sql + lookupMasterDO.getLookupDataCd();
			}
			
			if(lookupMasterDO.getLookupDataValue()!=null){
				sql = sql + " , " + lookupMasterDO.getLookupDataValue(); 
			} 
			
			sql = sql + " from " + lookupMasterDO.getLookupQueryTable();
			
			if(lookupMasterDO.getLookupCriteria()!=null){
				sql = sql + " where " + lookupMasterDO.getLookupCriteria();
			}
			
			if(lookupMasterDO.getLookupPostQuery()!=null){
				sql = sql + " " + lookupMasterDO.getLookupPostQuery(); 
			}
			
			List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);

			if(list!=null && list.size()>0){
				List<LookupDataDO> lookupDataDOs = getLookupDataList(list,lookupMasterDO);
				
				return lookupDataDOs;
			}
			
			
		}
		return null;
	}
}
