package com.metamorphosys.insureconnect.transaction.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metamorphosys.insureconnect.common.interfaceobjects.BaseIO;
import com.metamorphosys.insureconnect.common.interfaceobjects.RequestDataIO;
import com.metamorphosys.insureconnect.common.interfaceobjects.RuleIO;
import com.metamorphosys.insureconnect.controllers.WebExecuteController;
import com.metamorphosys.insureconnect.controllers.WebGetController;
import com.metamorphosys.insureconnect.controllers.WebSaveController;
import com.metamorphosys.insureconnect.dataobjects.master.PreferenceDO;
import com.metamorphosys.insureconnect.dataobjects.master.RuleActionDO;
import com.metamorphosys.insureconnect.dataobjects.master.RuleDO;
import com.metamorphosys.insureconnect.dataobjects.master.RuleDataDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.ServiceDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.ServiceTaskDO;
import com.metamorphosys.insureconnect.jpa.master.RuleRepository;
import com.metamorphosys.insureconnect.utilities.SerializationUtility;

@RestController
@RequestMapping("/ruleEngineController")
public class RuleEngineController {
	
	private static final Logger LOGGER =  LoggerFactory.getLogger(RuleEngineController.class);
	
	@Autowired
	WebSaveController webSaveController;

	@Autowired
	WebGetController webGetController;
	
	@Autowired
	WebExecuteController webExecuteController;

	@Autowired
	RuleRepository ruleRepository;
	
	List<RuleDataDO> ruleDataMemory = new ArrayList<RuleDataDO>();
	
	@RequestMapping(method = RequestMethod.GET)
	ResponseEntity init(){
		LOGGER.info("init RuleEngineController");
		
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		//httpHeaders.setAccessControlAllowMethods();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		String data = null;
		try {
			RuleDO ruleDO = new RuleDO();
			RuleIO io = new RuleIO();
			List<RuleDO> ruleDOList = new ArrayList<RuleDO>();
			ruleDOList.add(ruleDO);
			io.setRuleDOList(ruleDOList);
			io.setServiceDO(getService());
			LOGGER.info(SerializationUtility.getInstance().toJson(io));
			
			RuleActionDO actionDO = new RuleActionDO();
			actionDO.setAction("Pappu");
			ArrayList<RuleActionDO> ruleActionList = new ArrayList<RuleActionDO>();
			ruleDO.setRuleActionList(ruleActionList);
			ruleDO.setRuleName("Raju");
			ruleRepository.save(ruleDO);
	
			
			data = webExecuteController.process(convertTojson(ruleDO, getService(),null,ruleDO.getObjectName()));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ResponseEntity(data, httpHeaders, httpStatus);
	}
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity save(@RequestBody @Valid String json){
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		//httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		RuleIO io = (RuleIO) SerializationUtility.getInstance().fromJson(json, RuleIO.class);
		
		LOGGER.info("save RuleEngineController");
		String data=null;
		for(RuleDO ruleDO : io.getRuleDOList()){
			try {
				if(ruleDO.getId()==null){
					ruleDataMemory = null;
				}
				List<RuleDataDO> ruleDataDOs = new ArrayList<RuleDataDO>();
				int i=0;
				if(ruleDataMemory!=null && ruleDataMemory.size()>0){
					for(RuleDataDO dataDO:ruleDataMemory){
						for(RuleDataDO ruleDataDO: ruleDO.getRuleDataList()){
							Long id=ruleDataDO.getId();
							if(i==0 && ruleDataDO.getId()==null){
								ruleDataDOs.add(ruleDataDO);
							}else if(id!=null && dataDO.getId().equals(id)){
								ruleDataMemory.set(i, ruleDataDO);
							}
						}
						i++;
					}
					if(ruleDataDOs.size()>0){
						ruleDataMemory.addAll(ruleDataDOs);
					}
					ruleDO.setRuleDataList(ruleDataMemory);
				}
				data = webSaveController.process(convertTojson(ruleDO, (ServiceDO)io.getServiceDO(),null, ruleDO.getObjectName()));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return new ResponseEntity(data, httpHeaders, httpStatus);	
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	ResponseEntity fetch(@PathVariable("id") Long id) {
		
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		RuleDO ruleDO = new RuleDO();
		ruleDO.setId(id);
		LOGGER.info("fetch RuleEngineController");
		HashMap<String, String> hashMap = new HashMap<String, String>();
		String data = null;
		try {
			/*data = webGetController.process(convertTojson(ruleDO,null,null,ruleDO.getObjectName()));*/
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		RuleDO rule=ruleRepository.findById(id);
		/*BaseIO baseIO = webGetController.convertToIO(data);*/
		//for (RuleDO object : rule) {
						//if ("RuleDO".equals(object.getClass().getSimpleName())) {

						//	rule = (RuleDO) object;
							
							ruleDataMemory= rule.getRuleDataList();
							hashMap.put("ruleDataLength", String.valueOf(rule.getRuleDataList().size()));
							hashMap.put("ruleDO", SerializationUtility.getInstance().toJson(rule));
							rule.setRuleDataList(null);
						//}
		//}
		
		/*hashMap.put("ruleDataLength", String.valueOf(rule.getRuleDataList().size()));
		rule.setRuleDataList(null);
		hashMap.put("ruleDO", convertTojson(rule, null, null, rule.getObjectName()));*/
		
		
		return new ResponseEntity(hashMap, httpHeaders, httpStatus);	
		
	}
	
	@RequestMapping(value = "/fetchRuleData/{pageNumber}", method = RequestMethod.GET)
	ResponseEntity fetchRuleData(@PathVariable("pageNumber") int pageNumber) {
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		int record= 50;       
		int lastRecord =record * pageNumber;  
		int firstRecord = lastRecord - (record);
		List<RuleDataDO> list=null;
		if(ruleDataMemory.size()>=lastRecord){
			list = ruleDataMemory.subList(firstRecord, lastRecord);
		}else if(ruleDataMemory.size()>0){
			list = ruleDataMemory.subList(firstRecord, ruleDataMemory.size());
		}
		
		return new ResponseEntity(list, httpHeaders, httpStatus);
	}
	
//	@RequestMapping(value = "{id}", method = RequestMethod.GET)
//	ResponseEntity fetchRuleData(@PathVariable("id") Long id) {
//		
//		HttpStatus httpStatus = HttpStatus.OK;
//		HttpHeaders httpHeaders = new HttpHeaders();
//		httpHeaders.setAccessControlAllowOrigin("*");
//		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
//		RuleDO ruleDO = new RuleDO();
//		RuleDataDO ruleDataDO = new RuleDataDO();
//		ruleDO.setId(id);
//		LOGGER.info("fetch RuleEngineController");
//		
//		String data = null;
//		String ruleDataString = null;
//		try {
//			data = webGetController.process(convertTojson(ruleDO,null,null,ruleDO.getObjectName()));
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}		
//		
//		
//		RuleDO rule=null;
//		BaseIO baseIO = webGetController.convertToIO(data);
//		for (Object object : baseIO.getBaseDOList()) {
//						if ("RuleDO".equals(object.getClass().getSimpleName())) {
//
//							rule = (RuleDO) object;
////							try{
////								ruleDataString = webGetController.process(convertTojson(rule.getRuleDataList(),null,null,ruleDataDO.getObjectName()));
////							}catch (Exception e){
////								
////							}
//							
//							RuleDO r = new RuleDO();
//							int RuleDataListSize=rule.getRuleDataList().size();
//							r.setRuleDataList(rule.getRuleDataList().subList(0, RuleDataListSize));
//							try{
//								ruleDataString =  webGetController.process(convertTojson(r,null,null,r.getObjectName()));
//							} catch (Exception e){
//								e.printStackTrace();
//							}
//						}
//		}
//		return new ResponseEntity(ruleDataString, httpHeaders, httpStatus);	
//		
//	}
	
//	@RequestMapping(value = "/allDataOfOneRule/{id}", method = RequestMethod.GET)
//	ResponseEntity fetchRuleData(@PathVariable("id") Long id) {
//		
//		HttpStatus httpStatus = HttpStatus.OK;
//		HttpHeaders httpHeaders = new HttpHeaders();
//		httpHeaders.setAccessControlAllowOrigin("*");
//		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
//		RuleDO ruleDO = new RuleDO();
//		RuleDataDO ruleDataDO = new RuleDataDO();
//		ruleDO.setId(id);
//		LOGGER.info("fetch RuleEngineController");
//		
//		String data = null;
//		String ruleDataString = null;
//		try {
//			data = webGetController.process(convertTojson(ruleDO,null,null,ruleDO.getObjectName()));
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}		
//		
//		
//		RuleDO rule=null;
//		BaseIO baseIO = webGetController.convertToIO(data);
//		for (Object object : baseIO.getBaseDOList()) {
//						if ("RuleDO".equals(object.getClass().getSimpleName())) {
//
//							rule = (RuleDO) object;
////							try{
////								ruleDataString = webGetController.process(convertTojson(rule.getRuleDataList(),null,null,ruleDataDO.getObjectName()));
////							}catch (Exception e){
////								
////							}
//							
//							RuleDO r = new RuleDO();
//							int RuleDataListSize=rule.getRuleDataList().size();
//							r.setRuleDataList(rule.getRuleDataList().subList(0, RuleDataListSize));
//							try{
//								ruleDataString =  webGetController.process(convertTojson(r,null,null,r.getObjectName()));
//							} catch (Exception e){
//								e.printStackTrace();
//							}
//						}
//		}
//		return new ResponseEntity(ruleDataString, httpHeaders, httpStatus);	
//		
//	}

	private ServiceDO getService() {
		ServiceDO serviceDO = new ServiceDO();
		serviceDO.setService("RULE");
		serviceDO.setServiceType("WORKFLOW");
		ServiceTaskDO task = new ServiceTaskDO();
		task.setServiceTask("init");
		serviceDO.getServiceTaskList().add(task);
		return serviceDO;
	}
	
	public String convertTojson(Object object, ServiceDO serviceDO, PreferenceDO preferenceDO,String objectName) {
		List<RequestDataIO> requestDataIOs = new ArrayList<RequestDataIO>();

		if(object!=null){
			RequestDataIO requestDataIO = new RequestDataIO();
			requestDataIO.setObjectName(objectName);
			requestDataIO.setObject(SerializationUtility.getInstance().toJson(object));
			requestDataIOs.add(requestDataIO);
		}

		if(serviceDO!=null){
			RequestDataIO requestDataIO1 = new RequestDataIO();
			requestDataIO1.setObjectName(serviceDO.getObjectName());
			requestDataIO1.setObject(SerializationUtility.getInstance().toJson(serviceDO));
			requestDataIOs.add(requestDataIO1);
		}
		
		if(preferenceDO!=null){
			RequestDataIO requestDataIO2 = new RequestDataIO();
			requestDataIO2.setObjectName(preferenceDO.getObjectName());
			requestDataIO2.setObject(SerializationUtility.getInstance().toJson(preferenceDO));
			requestDataIOs.add(requestDataIO2);
		}

		String serialize = SerializationUtility.getInstance().toJson(requestDataIOs);
		return serialize;
	}
	
	@RequestMapping(value = "/fetchAll" , method = RequestMethod.GET)
	ResponseEntity fetchAll() throws ParseException {
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-mm-dd"); 
		 Date parsed = (Date) sdf1.parse("2016-06-08");
		List<RuleDO> r = (List<RuleDO>) ruleRepository.findByServiceAndServiceTypeAndServiceTaskAndFromtoDt("CREATEPROPOSAL",
				"Workflow","init",new java.sql.Date(parsed.getTime()));
		
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		LOGGER.info("Fetch SearchRuleController");
		
		List<RuleDO> ruleList = (List<RuleDO>) ruleRepository.findAll();
//		List<RuleDO> ruleList =(List<RuleDO>) ruleRepository.findByRuleNameAndServiceAndServiceTypeAndServiceTask("Flood-Water-Hammer-RateTable","Product","ProductCalculation","FWHREGULARPREMIUM");
		for(RuleDO ruleDO:ruleList){
			ruleDO.setRuleDataList(null);
		}
		return new ResponseEntity(ruleList, httpHeaders, httpStatus);	
	}


		@RequestMapping(value = "/fetchByRuleType/{ruleType}", method = RequestMethod.GET)
		ResponseEntity fetchByRuleType(@PathVariable("ruleType") String ruleType) {
		
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		LOGGER.info("Fetch SearchRuleController");
		
		List<RuleDO> ruleList =(List<RuleDO>) ruleRepository.findByRuleType(ruleType);
//		List<RuleDO> ruleList =(List<RuleDO>) ruleRepository.findByRuleNameAndServiceAndServiceTypeAndServiceTask("Flood-Water-Hammer-RateTable","Product","ProductCalculation","FWHREGULARPREMIUM");
		for(RuleDO ruleDO:ruleList){
			ruleDO.setRuleDataList(null);
		}
		return new ResponseEntity(ruleList, httpHeaders, httpStatus);	
	}

		@RequestMapping(value = "/fetchByServiceServiceType/{serviceServiceType}", method = RequestMethod.GET)
		ResponseEntity fetchByServiceServiceType(@PathVariable("serviceServiceType") List<String> serviceServiceType) {
			
			HttpStatus httpStatus = HttpStatus.OK;
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setAccessControlAllowOrigin("*");
			httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
			
			String service=serviceServiceType.get(0);
			String serviceType= serviceServiceType.get(1);
			//LOGGER.info("Fetch Service ServiceType");
			List<RuleDO> ruleList =(List<RuleDO>) ruleRepository.findByServiceAndServiceTypeOrderBySortOrderAsc(service, serviceType);		
			return new ResponseEntity(ruleList, httpHeaders, httpStatus);	
		}
		
		@RequestMapping(value = "/fetch/{rule_attributes}" , method = RequestMethod.GET)
		ResponseEntity fetchByrulenameandservice(@PathVariable("rule_attributes") List<String> rule_attributes) {
			
			HttpStatus httpStatus = HttpStatus.OK;
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setAccessControlAllowOrigin("*");
			httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
			
			String rulename=rule_attributes.get(0);
			String service_sent= rule_attributes.get(1);
			String servicetype_sent= rule_attributes.get(2);
			String servicetask_sent=rule_attributes.get(3);
			
			List<RuleDO> ruleList =(List<RuleDO>) ruleRepository.findByRuleNameAndServiceAndServiceTypeAndServiceTask(rulename, service_sent,servicetype_sent, servicetask_sent);		
			
			return new ResponseEntity(ruleList, httpHeaders, httpStatus);	
		}

	

	
}
