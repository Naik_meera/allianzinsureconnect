package com.metamorphosys.insureconnect.transaction.controllers;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metamorphosys.insureconnect.dataobjects.transaction.FolderDO;
import com.metamorphosys.insureconnect.inbox.dao.InboxDao;
import com.metamorphosys.insureconnect.jpa.transaction.FolderRepository;
import com.metamorphosys.insureconnect.utilities.SerializationUtility;

@RestController
@RequestMapping("/inbox")
public class InboxController {

	@Autowired
	InboxDao inboxDao;
	
	@Autowired
	FolderRepository folderRepository;
	
	@CrossOrigin
	@RequestMapping(value = "{id}/{criteria}", method = RequestMethod.GET)
	ResponseEntity getQueryResult(@PathVariable("id") Long id,@PathVariable("criteria")String criteria) {
		HashMap<String, String> criteriaList=null;
		if(criteria!=null && !criteria.equals("")){
			criteriaList=(HashMap<String, String>) SerializationUtility.getInstance().fromJson(criteria, HashMap.class);
		}
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
//		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		HashMap<String,String> resultMap = inboxDao.inboxQuery(id, criteriaList);
		return new ResponseEntity(resultMap, httpHeaders, httpStatus);	
	}
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET)
	ResponseEntity getFolder() {
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		//httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		Iterable<FolderDO> folderList = folderRepository.findAll();
		return new ResponseEntity(folderList, httpHeaders, httpStatus);	
	}

}
