package com.metamorphosys.insureconnect.transaction.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import javax.validation.Valid;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metamorphosys.insureconnect.common.interfaceobjects.BaseIO;
import com.metamorphosys.insureconnect.common.interfaceobjects.RequestDataIO;
import com.metamorphosys.insureconnect.common.interfaceobjects.VocabularyIO;
import com.metamorphosys.insureconnect.controllers.WebExecuteController;
import com.metamorphosys.insureconnect.controllers.WebGetController;
import com.metamorphosys.insureconnect.controllers.WebSaveController;
import com.metamorphosys.insureconnect.dataobjects.master.MetadataDO;
import com.metamorphosys.insureconnect.dataobjects.master.PreferenceDO;
import com.metamorphosys.insureconnect.dataobjects.master.UIMetadataDO;
import com.metamorphosys.insureconnect.dataobjects.master.VocabularyDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.ServiceDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.ServiceTaskDO;
import com.metamorphosys.insureconnect.jpa.master.MetadataRepository;
import com.metamorphosys.insureconnect.jpa.master.VocabularyRepository;
import com.metamorphosys.insureconnect.utilities.InsureConnectConstants;
import com.metamorphosys.insureconnect.utilities.SerializationUtility;


@RestController
@RequestMapping("/vocabulary")
public class VocabularyController {
	
	private static final Logger LOGGER =  LoggerFactory.getLogger(VocabularyController.class);

	@Autowired
	WebExecuteController webExecuteController;
	
	@Autowired 
	WebGetController webGetController;
	
	@Autowired
	MetadataRepository metadataRepository;
	
	@Autowired 
	WebSaveController webSaveController;

	@Autowired
	VocabularyRepository vocabularyRepository;
	
	

	@RequestMapping(value = "/fetchAll" , method = RequestMethod.GET)
	ResponseEntity fetchAll() {
		
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		LOGGER.info("Fetch SearchVocabularyController");
		
		List<VocabularyDO> vocabularyList =(List<VocabularyDO>) vocabularyRepository.findAll();
		
		List<VocabularyIO> vocabularyIOs = new ArrayList<VocabularyIO>();
		for(VocabularyDO vocabularyDO:vocabularyList){
			VocabularyIO vocabularyIO=new VocabularyIO();
			MetadataDO metadataDO =null;
			HashMap<String, HashMap<String, String>> uIMetadataMap = new HashMap<String, HashMap<String, String>>();
/*
			if(vocabularyDO.getSubObjectDO()!=null){
				metadataDO = metadataRepository.findByMetadataCd(vocabularyDO.getSubObjectDO().toUpperCase()+"BASIC");
				
				HashMap<String, String> hashMap = new HashMap<>();
				for(UIMetadataDO uiMetadataDO: metadataDO.getUiMetadataList()){
					hashMap.put(uiMetadataDO.getUiProperty(), uiMetadataDO.getUiValue());
				}
				uIMetadataMap.put(vocabularyDO.getSubObjectDO(), hashMap);
				
			}else*/ if(vocabularyDO.getParentObjectDO()!=null){
				metadataDO = metadataRepository.findByMetadataCd(vocabularyDO.getParentObjectDO().toUpperCase()+"BASIC");
				
				HashMap<String, String> hashMap = new HashMap<String, String>();
				if(metadataDO!=null){
					for(UIMetadataDO uiMetadataDO: metadataDO.getUiMetadataList()){
						if(vocabularyDO.getVocabularyCd().equals(uiMetadataDO.getAttributeCd()))
							hashMap.put(uiMetadataDO.getUiProperty(), uiMetadataDO.getUiValue());
					}
				}
				uIMetadataMap.put(vocabularyDO.getParentObjectDO(), hashMap);
			}
			vocabularyIO.setuIMetadataMap(uIMetadataMap);
			vocabularyIO.setVocabulary(vocabularyDO);
			vocabularyIOs.add(vocabularyIO);
			
		}
			
		return new ResponseEntity(vocabularyIOs, httpHeaders, httpStatus);	
	}
	
	

	
	@RequestMapping(method = RequestMethod.GET)
	ResponseEntity init(){
		LOGGER.info("init VocabularyController");
		
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		//httpHeaders.setAccessControlAllowMethods();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		String data = null;
		try {
			VocabularyDO vocabularyDO = new VocabularyDO();
			data = webExecuteController.process(convertTojson(vocabularyDO, getService(),null,vocabularyDO.getObjectName()));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return new ResponseEntity(data, httpHeaders, httpStatus);		
	}
	
	
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	ResponseEntity fetch(@PathVariable("id") Long id) {
		
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		VocabularyDO vocabularyDO = new VocabularyDO();
		vocabularyDO.setId(id);
		LOGGER.info("fetch VocabularyController");
		
		VocabularyIO  vocabularyIO = new VocabularyIO();
		/*vocabularyDO.setVocabularyCd("sumInsured");
		vocabularyDO.setVocabularyType("SYSTEM");
		vocabularyIO.setVocabulary(vocabularyDO);
		vocabularyIO.setServiceDO(getService());
		HashMap<String, String> data = new HashMap<>();
		data.put("LABEL", "Sum Insured");
		data.put("INPUTTYPE", "TEXT");
		
		HashMap<String,HashMap<String, String>> hashMap = new HashMap<>();
		hashMap.put("PolicyDO", data);
		hashMap.put("CaseDO", data);
		vocabularyIO.setuIMetadataMap(hashMap);
		
		String Json = SerializationUtility.getInstance().toJson(vocabularyIO);
		LOGGER.info(Json);*/
		String data = null;
		try {
			data = webGetController.process(convertTojson(vocabularyDO,null,null,vocabularyDO.getObjectName()));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		BaseIO baseIO = webGetController.convertToIO(data);
		vocabularyIO = convertToObject(baseIO,vocabularyIO);
		MetadataDO metadataDO =null;
		HashMap<String, HashMap<String, String>> uIMetadataMap = new HashMap<String, HashMap<String, String>>();

		if(vocabularyIO.getVocabulary().getSubObjectDO()!=null){
			metadataDO = metadataRepository.findByMetadataCd(vocabularyIO.getVocabulary().getSubObjectDO().toUpperCase()+"BASIC");
			
			HashMap<String, String> hashMap = new HashMap<String, String>();
			for(UIMetadataDO uiMetadataDO: metadataDO.getUiMetadataList()){
				hashMap.put(uiMetadataDO.getUiProperty(), uiMetadataDO.getUiValue());
			}
			uIMetadataMap.put(vocabularyIO.getVocabulary().getSubObjectDO(), hashMap);
			
		}else if(vocabularyIO.getVocabulary().getParentObjectDO()!=null){
			metadataDO = metadataRepository.findByMetadataCd(vocabularyIO.getVocabulary().getParentObjectDO().toUpperCase()+"BASIC");
			
			HashMap<String, String> hashMap = new HashMap<String, String>();
			for(UIMetadataDO uiMetadataDO: metadataDO.getUiMetadataList()){
				hashMap.put(uiMetadataDO.getUiProperty(), uiMetadataDO.getUiValue());
			}
			uIMetadataMap.put(vocabularyIO.getVocabulary().getParentObjectDO(), hashMap);
		}
		vocabularyIO.setuIMetadataMap(uIMetadataMap);
		
		
		return new ResponseEntity(vocabularyIO, httpHeaders, httpStatus);	
		
	}
	
	private VocabularyIO convertToObject(BaseIO baseIO, VocabularyIO vocabularyIO) {
		
			for (Object object : baseIO.getBaseDOList()) {
				if (InsureConnectConstants.DataObjects.VOCABULARYDO.equals(object.getClass().getSimpleName())) {

					vocabularyIO.setVocabulary((VocabularyDO) object);
				}
				if (InsureConnectConstants.DataObjects.SERVICEDO.equals(object.getClass().getSimpleName())) {
					vocabularyIO.setServiceDO((ServiceDO) object);
				}
			}
			return vocabularyIO;
	}

	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity save(@RequestBody @Valid VocabularyIO io){
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
//		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		LOGGER.info("save VocabularyController");
		String data = null;
		try {
			data = webSaveController.process(convertTojson(io.getVocabulary(), io.getServiceDO(), null, io.getVocabulary().getObjectName()));
			if(io.getuIMetadataMap()!=null){
			for (Entry<String, HashMap<String, String>> metadataMap : io.getuIMetadataMap().entrySet()) {
				//MetadataDO metadataDO = new MetadataDO();
				//metadataDO.setMetadataCd(metadataMap.getKey().toUpperCase() +"BASIC");
				MetadataDO metadataDO = metadataRepository.findByMetadataCd(metadataMap.getKey().toUpperCase() +"BASIC");
				if(metadataDO==null){
					metadataDO = new MetadataDO();
					metadataDO.setMetadataCd(metadataMap.getKey().toUpperCase() +"BASIC");
				}
				
				
				if(metadataMap.getValue()!=null && !metadataMap.getValue().equals("")){
					List<UIMetadataDO> uiMetadataDOs = new ArrayList<UIMetadataDO>();
					
					HashMap<String, String> uiMetadataMap = (HashMap<String, String>) metadataMap.getValue();
					
					if(metadataDO.getUiMetadataList()!=null)
						uiMetadataDOs.addAll(metadataDO.getUiMetadataList());
					
					for(Entry<String, String> uiMetadataIterator : uiMetadataMap.entrySet()){
						UIMetadataDO uiMetadataDO = new UIMetadataDO();
						if(io.getVocabulary().getSubObjectDO()!=null){
							uiMetadataDO.setObjectCd(io.getVocabulary().getSubObjectDO());
						}else{
							uiMetadataDO.setObjectCd(metadataMap.getKey());
						}
						if("id".equals(uiMetadataIterator.getKey())){
							uiMetadataDO.setId(Long.parseLong(uiMetadataIterator.getValue()));
						}else{
							uiMetadataDO.setAttributeCd(io.getVocabulary().getVocabularyCd());
							uiMetadataDO.setUiProperty(uiMetadataIterator.getKey());
							uiMetadataDO.setUiValue(uiMetadataIterator.getValue());
						}
						
						uiMetadataDOs.add(uiMetadataDO);
					}
					metadataDO.setUiMetadataList(uiMetadataDOs);
					data = webSaveController.process(convertTojson(metadataDO,getUIService(), null,metadataDO.getObjectName()));
				}
			}
		}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ResponseEntity(data, httpHeaders, httpStatus);			
	}
	
		
	@CrossOrigin
	@RequestMapping(value = "/uploadFromExcel", method = RequestMethod.GET)
	ResponseEntity uploadFromExcel() throws IOException {
		LOGGER.info("uploadFromExcel");

		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		// httpHeaders.setAccessControlAllowMethods();
		// httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		List<VocabularyDO> vocabularyDOs = new ArrayList<VocabularyDO>();
		List<VocabularyIO> vocabularyIOs=new ArrayList<VocabularyIO>();
		String excelFilePath = "C:\\Users\\User\\Downloads\\ExcelFileReaderExamples\\Books.xlsx";
		FileInputStream inputStream = new FileInputStream(new File(excelFilePath));

		Workbook workbook = new XSSFWorkbook(inputStream);
		Sheet firstSheet = workbook.getSheetAt(0);
		Iterator<Row> iterator = firstSheet.iterator();

		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			if(nextRow.getRowNum()!=0){

			Iterator<Cell> cellIterator = nextRow.cellIterator();
			VocabularyDO voDo = new VocabularyDO();
			VocabularyIO vocabularyIO= new VocabularyIO();
			HashMap<String, String> uimetadata= new HashMap<String, String>();
			HashMap<String, HashMap<String, String>> uiHashMap=new HashMap<String, HashMap<String,String>>();
			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				int columnIndex = cell.getColumnIndex();

				switch (columnIndex) {
				case 1:
					if (!cell.getStringCellValue().equals(null)) {
						uimetadata.put("INPUTTYPE", cell.getStringCellValue());
					}
					break;
				case 2:
					if (!cell.getStringCellValue().equals(null)) {
						uimetadata.put("LABEL", cell.getStringCellValue());
					}
					break;
				case 3:
					if (!cell.getStringCellValue().equals(null)) {
						uimetadata.put("LABEL2", cell.getStringCellValue());
					}
					break;
				case 4:
					if (!cell.getStringCellValue().equals(null)) {
						uimetadata.put("LOOKUP", cell.getStringCellValue());
					}
					break;
				case 5:
					if (!cell.getStringCellValue().equals(null)) {
						voDo.setVocabularyCd(cell.getStringCellValue());
					}
					break;
				case 6:
					if (!cell.getStringCellValue().equals(null)) {
						voDo.setVocabularyName(cell.getStringCellValue());
					}
					break;
				case 7:
					if (!cell.getStringCellValue().equals(null)) {
						voDo.setDescription(cell.getStringCellValue());
					}
					break;
				case 8:					
					voDo.setParentObjectDO(cell.getStringCellValue());
					uiHashMap.put(cell.getStringCellValue(), uimetadata);
					vocabularyIO.setuIMetadataMap(uiHashMap);
					break;
				case 9:
					voDo.setSubObjectDO(cell.getStringCellValue());
					if (!cell.getStringCellValue().equals(null)) {
						vocabularyIO.getuIMetadataMap().clear();
						uiHashMap.put(cell.getStringCellValue(), uimetadata);
						vocabularyIO.setuIMetadataMap(uiHashMap);
					}
					break;
				case 10:
					voDo.setVocabularyType(cell.getStringCellValue());
					break;				
				}
			}
			vocabularyIO.setVocabulary(voDo);
//			vocabularyDOs.add(voDo);
			vocabularyIOs.add(vocabularyIO);
			}
		}
		workbook.close();
		inputStream.close();
		
		for (VocabularyIO io : vocabularyIOs) {
			save(io);
		}
	
//	 LOGGER.info(SerializationUtility.getInstance().toJson(io));
	 return new ResponseEntity(vocabularyIOs, httpHeaders, httpStatus);
}


	private ServiceDO getService() {
		ServiceDO serviceDO = new ServiceDO();
		serviceDO.setService("VOCABULARY");
		serviceDO.setServiceType("WORKFLOW");
		ServiceTaskDO task = new ServiceTaskDO();
		task.setServiceTask("init");
		serviceDO.getServiceTaskList().add(task);
		return serviceDO;
	}
	
	private ServiceDO getUIService() {
		ServiceDO serviceDO = new ServiceDO();
		serviceDO.setService("METADATA");
		serviceDO.setServiceType("Utility");
		ServiceTaskDO task = new ServiceTaskDO();
		task.setServiceTask("EntityPersistance");
		serviceDO.getServiceTaskList().add(task);
		return serviceDO;
	}
	
	public String convertTojson(Object object, ServiceDO serviceDO, PreferenceDO preferenceDO,String objectName) {
		List<RequestDataIO> requestDataIOs = new ArrayList<RequestDataIO>();

		if(object!=null){
			RequestDataIO requestDataIO = new RequestDataIO();
			requestDataIO.setObjectName(objectName);
			requestDataIO.setObject(SerializationUtility.getInstance().toJson(object));
			requestDataIOs.add(requestDataIO);
		}

		if(serviceDO!=null){
			RequestDataIO requestDataIO1 = new RequestDataIO();
			requestDataIO1.setObjectName(serviceDO.getObjectName());
			requestDataIO1.setObject(SerializationUtility.getInstance().toJson(serviceDO));
			requestDataIOs.add(requestDataIO1);
		}
		
		if(preferenceDO!=null){
			RequestDataIO requestDataIO2 = new RequestDataIO();
			requestDataIO2.setObjectName(preferenceDO.getObjectName());
			requestDataIO2.setObject(SerializationUtility.getInstance().toJson(preferenceDO));
			requestDataIOs.add(requestDataIO2);
		}

		String serialize = SerializationUtility.getInstance().toJson(requestDataIOs);
		return serialize;
	}
	
	
}
