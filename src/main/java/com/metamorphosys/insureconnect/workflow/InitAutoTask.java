package com.metamorphosys.insureconnect.workflow;

import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.metamorphosys.insureconnect.common.interfaceobjects.WorkflowIO;
import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.WorkflowServiceDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.WorkflowServiceTaskDO;
import com.metamorphosys.insureconnect.utilities.GuidGenerator;

public class InitAutoTask extends AutoTask {
	
	private static final Logger log = LoggerFactory.getLogger(InitAutoTask.class);
	
	@Override
	public void execute(WorkflowIO baseIO) 
	{
		log.info("Execute InitAutoTask: "+serviceTask);
		
		try
		{
			BaseDO baseDO = initBaseDO();
			
			baseIO.setBaseDO(baseDO);
			
			WorkflowServiceDO workflowService = initWorkflowService(baseDO);
			
			baseIO.setServiceDO(workflowService);			
			
		}
		catch(Exception e)
		{
			log.error("Error initializing base object ["+baseObjectClass+"]: "+e.getMessage());
			e.printStackTrace();
		}
	}
	
	protected BaseDO initBaseDO() throws Exception
	{
		Class klass = Class.forName(this.baseObjectClass);
		BaseDO baseDO = (BaseDO)klass.newInstance();
		baseDO.setGuid(GuidGenerator.generate());
		
		return baseDO;
	}

	protected WorkflowServiceDO initWorkflowService(BaseDO baseDO)
	{		
		// init service
		WorkflowServiceDO workflowService = null;
		try
		{
			WorkflowServiceDO taskService = (WorkflowServiceDO) getServiceDO();
			
			workflowService = new WorkflowServiceDO();
			workflowService.setService(taskService.getService());
			workflowService.setServiceType(taskService.getServiceType());
			
			workflowService.setEntityGuid(baseDO.getGuid());
			workflowService.setStartDt(new Timestamp(System.currentTimeMillis()));
			workflowService.setStatus("PENDING");
			workflowService.setGuid(GuidGenerator.generate());
			
			// init service task
			WorkflowServiceTaskDO initServiceTask = new WorkflowServiceTaskDO();
			initServiceTask.setServiceTask(serviceTask);
			initServiceTask.setStatus("PENDING");
			initServiceTask.setTaskUser("");
			initServiceTask.setStartDt(new Timestamp(System.currentTimeMillis()));
			initServiceTask.setGuid(GuidGenerator.generate());
			
			workflowService.addWorkflowServiceTask(initServiceTask);
		}
		catch(Exception e)
		{
			log.error("Error initializing workflow service ["+serviceTask+"]: "+e.getMessage());
			e.printStackTrace();
		}
		return workflowService;
	}
}
