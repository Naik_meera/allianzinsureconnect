package com.metamorphosys.insureconnect.workflow;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.metamorphosys.insureconnect.common.interfaceobjects.WorkflowIO;
import com.metamorphosys.insureconnect.dataobjects.master.RuleDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.WorkflowServiceDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.WorkflowServiceTaskDO;
import com.metamorphosys.insureconnect.jpa.master.RuleRepository;
import com.metamorphosys.insureconnect.jpa.transaction.WorkflowServiceMemoryRepository;

public class AutoTask extends Task {
	
	private static final Logger log = LoggerFactory.getLogger(AutoTask.class);
	
	@Autowired
	private RuleRepository ruleRepository;

	@Override
	public void init(WorkflowIO baseIO) 
	{
		log.info("Init AutoTask: "+serviceTask);
		
		if(null != baseIO && null != baseIO.getBaseDO())
		{
			WorkflowServiceDO workflowService = WorkflowServiceMemoryRepository.fetchService(baseIO.getBaseDO());
			
			if(null != workflowService)
			{
				completePreviousTask(workflowService);
				baseIO.setServiceDO(workflowService);
			}
		}
	}
	
	@Override
	public void execute(WorkflowIO baseIO) 
	{
		log.info("Execute AutoTask: "+serviceTask);
		
	}

	@Override
	public void postExecute(WorkflowIO baseIO){		
		super.postExecute(baseIO);
		
		List<RuleDO> ruleList = null;
		
		WorkflowServiceDO workflowService = (WorkflowServiceDO)baseIO.getServiceDO();		
		if(null != workflowService)
		{
			WorkflowServiceTaskDO pendingServiceTask = workflowService.fetchPendingWorkflowServiceTask();
			if(null != pendingServiceTask)
			{
				ruleList = ruleRepository.findByServiceAndServiceTypeAndServiceTaskOrderBySortOrderAsc(workflowService.getService(), workflowService.getServiceType(), pendingServiceTask.getServiceTask());
			}
		}
		
		baseIO.setNextTaskList(ruleList);

	}

	@Override
	protected void saveService(WorkflowServiceDO workflowServiceDO)
	{
		WorkflowServiceMemoryRepository.storeService(workflowServiceDO);
	}
}
