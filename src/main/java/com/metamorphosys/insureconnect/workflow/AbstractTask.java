package com.metamorphosys.insureconnect.workflow;

import com.metamorphosys.insureconnect.common.interfaceobjects.WorkflowIO;
import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.WorkflowServiceDO;

public abstract class AbstractTask<I extends WorkflowIO, B extends BaseDO> {
	
	protected String serviceTask;
	protected String baseObjectClass;

	protected WorkflowServiceDO serviceDO;
	
	public abstract void init(I workflowIO);

	public abstract void execute(I workflowIO);

	public abstract void load(I workflowIO);
	
	public abstract void run(I workflowIO);

	public abstract void postExecute(I workflowIO);
	
	public abstract void setServiceTask(String serviceTask);
	
	public abstract String getServiceTask();
	
	public abstract void setBaseObjectClass(String baseObjectClass);
	
	public abstract String getBaseObjectClass();

	public abstract WorkflowServiceDO getServiceDO() ;

	public abstract void setServiceDO(WorkflowServiceDO serviceDO) ;
	
	protected abstract void saveService(WorkflowServiceDO workflowServiceDO);

}
