package com.metamorphosys.insureconnect.workflow;

import java.sql.Timestamp;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.metamorphosys.insureconnect.common.interfaceobjects.WorkflowIO;
import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;
import com.metamorphosys.insureconnect.dataobjects.master.RuleDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.WorkflowServiceDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.WorkflowServiceTaskDO;
import com.metamorphosys.insureconnect.jpa.master.RuleRepository;
import com.metamorphosys.insureconnect.jpa.transaction.WorkflowServiceMemoryRepository;
import com.metamorphosys.insureconnect.jpa.transaction.WorkflowServiceRepository;
import com.metamorphosys.insureconnect.utilities.GuidGenerator;

public class ManualTask extends Task {
	
	@Autowired
	private WorkflowServiceRepository workflowServiceRepository;
	
	@Autowired
	private RuleRepository ruleRepository;
	
	private static final Logger log = LoggerFactory.getLogger(ManualTask.class);

	private String screen;
	
	@Override
	public void init(WorkflowIO baseIO) 
	{
		log.info("Init ManualTask: "+serviceTask);
		
		if(null != baseIO && null != baseIO.getBaseDO())
		{
			WorkflowServiceDO workflowService = WorkflowServiceMemoryRepository.fetchService(baseIO.getBaseDO());
			
			if(null != workflowService)
			{
				completePreviousTask(workflowService);
				baseIO.setServiceDO(workflowService);
			}
		}
	}
	
	@Override
	public void run(WorkflowIO baseIO){		
		log.info("run ManualTask: "+serviceTask);
		
		WorkflowServiceDO workflowService = (WorkflowServiceDO)baseIO.getServiceDO();
		if(null != screen)
		{
			List<RuleDO> ruleList = ruleRepository.findByServiceAndServiceTypeAndServiceTaskOrderBySortOrderAsc(workflowService.getService(), "UI", screen);
			
			baseIO.setNextTaskList(ruleList);
		}
	}

	@Override
	public void execute(WorkflowIO baseIO) 
	{
		log.info("Execute ManualTask: "+serviceTask);
		
		WorkflowServiceDO workflowService = (WorkflowServiceDO)baseIO.getServiceDO();
		workflowService.addWorkflowServiceTask(initWorkflowServiceTask(baseIO.getBaseDO()));
		
		if(null != screen)
		{
			List<RuleDO> ruleList = ruleRepository.findByServiceAndServiceTypeAndServiceTaskOrderBySortOrderAsc(workflowService.getService(), "UI", screen);
			
			baseIO.setNextTaskList(ruleList);
		}
	}

	@Override
	public void postExecute(WorkflowIO baseIO){
		super.postExecute(baseIO);
		
		log.info("postExecute ManualTask: "+serviceTask);
	}

	@Override
	protected void saveService(WorkflowServiceDO workflowServiceDO)
	{
		workflowServiceRepository.save(workflowServiceDO);
		WorkflowServiceMemoryRepository.clearMemoryCache(workflowServiceDO);
	}
	
	protected WorkflowServiceTaskDO initWorkflowServiceTask(BaseDO baseDO)
	{
		WorkflowServiceTaskDO workflowServiceTask = new WorkflowServiceTaskDO();
		workflowServiceTask.setServiceTask(serviceTask);
		workflowServiceTask.setStatus("PENDING");
		workflowServiceTask.setTaskUser("");
		workflowServiceTask.setStartDt(new Timestamp(System.currentTimeMillis()));
		workflowServiceTask.setGuid(GuidGenerator.generate());

		return workflowServiceTask;
	}

	public String getScreen() {
		return screen;
	}

	public void setScreen(String screen) {
		this.screen = screen;
	}

}
