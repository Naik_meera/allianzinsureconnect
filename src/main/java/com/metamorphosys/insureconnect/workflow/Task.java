package com.metamorphosys.insureconnect.workflow;

import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.metamorphosys.insureconnect.common.interfaceobjects.WorkflowIO;
import com.metamorphosys.insureconnect.dataobjects.transaction.WorkflowServiceDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.WorkflowServiceTaskDO;
import com.metamorphosys.insureconnect.jpa.transaction.WorkflowServiceMemoryRepository;

public abstract class Task extends AbstractTask {
	
	private static final Logger log = LoggerFactory.getLogger(Task.class);

	@Override
	public void load(WorkflowIO baseIO){		
		log.info("load Task: "+serviceTask);
		
		if(null != baseIO && null != baseIO.getBaseDO())
		{
			WorkflowServiceDO workflowService = WorkflowServiceMemoryRepository.fetchService(baseIO.getBaseDO());
			
			if(null != workflowService)
			{
				baseIO.setServiceDO(workflowService);
			}
		}

	}

	@Override
	public void run(WorkflowIO baseIO){		
		log.info("run Task: "+serviceTask);
	}

	@Override
	public void postExecute(WorkflowIO baseIO){		
		saveService((WorkflowServiceDO)baseIO.getServiceDO());
	}
	
	@Override
	public void setServiceTask(String serviceTask)
	{
		this.serviceTask = serviceTask;
	}
	
	@Override
	public String getServiceTask()
	{
		return this.serviceTask;
	}

	@Override
	public void setBaseObjectClass(String baseObjectClass) 
	{
		this.baseObjectClass = baseObjectClass;
	}

	@Override
	public String getBaseObjectClass() 
	{
		return this.baseObjectClass;
	}

	@Override
	public WorkflowServiceDO getServiceDO() {
		return this.serviceDO;
	}

	@Override
	public void setServiceDO(WorkflowServiceDO serviceDO) {
		this.serviceDO = serviceDO;
	}

	protected void completePreviousTask(WorkflowServiceDO workflowService)
	{
		if(null != workflowService)
		{
			WorkflowServiceTaskDO workflowServiceTask = workflowService.fetchPendingWorkflowServiceTask();
			
			if(null != workflowServiceTask)
			{
				workflowServiceTask.setStatus("COMPLETED");
				workflowServiceTask.setEndDt(new Timestamp(System.currentTimeMillis()));
			}
		}
	}
}
