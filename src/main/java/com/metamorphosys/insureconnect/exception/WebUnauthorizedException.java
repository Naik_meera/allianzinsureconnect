package com.metamorphosys.insureconnect.exception;

public class WebUnauthorizedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7045126137059658613L;

	public WebUnauthorizedException()
	{
		super();
	}

	public WebUnauthorizedException(String message)
	{
		super(message);
	}

	public WebUnauthorizedException(Throwable error)
	{
		super(error);
	}

	public WebUnauthorizedException(String message, Throwable error)
	{
		super(message, error);
	}
}
