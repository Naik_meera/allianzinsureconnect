package com.metamorphosys.insureconnect.exception;

public class WebBadRequestException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7045126137059658613L;

	public WebBadRequestException()
	{
		super();
	}

	public WebBadRequestException(String message)
	{
		super(message);
	}

	public WebBadRequestException(Throwable error)
	{
		super(error);
	}

	public WebBadRequestException(String message, Throwable error)
	{
		super(message, error);
	}
}
