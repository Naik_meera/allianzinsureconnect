package com.metamorphosys.insureconnect.inbox.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.metamorphosys.insureconnect.dataobjects.transaction.InboxCriteriaDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.InboxQueryDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.InboxResultDO;
import com.metamorphosys.insureconnect.jpa.transaction.InboxQueryRepository;
import com.metamorphosys.insureconnect.utilities.SerializationUtility;

@Component
public class InboxDao {

	@Autowired
	@Qualifier("transactionDB")
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	InboxQueryRepository inboxQueryRepository;
	
	public HashMap inboxQuery(Long id,HashMap<String,String> criteriaList){
		HashMap<String,String> resultMap = new HashMap<String, String>();
		InboxQueryDO inboxQuery = inboxQueryRepository.findOne(id);
		String sqlCriteria = null,sqlResult=null,sql=null;
		if(inboxQuery!=null){
			if(inboxQuery.getInboxResultDOList()!=null && inboxQuery.getInboxResultDOList().size()>0){
				for(InboxResultDO inboxResultDO:inboxQuery.getInboxResultDOList()){
					if(sqlResult==null){
						sqlResult =inboxResultDO.getResultfield();
					}else{
						sqlResult =sqlResult + "," + inboxResultDO.getResultfield();
					}
				}
				resultMap.put("resultField", sqlResult);
			}
			
			
			if(inboxQuery.getInboxCriteriaDOList()!=null && inboxQuery.getInboxCriteriaDOList().size()>0){
				if(!criteriaList.isEmpty()){
					for (String key : criteriaList.keySet()){
						for(InboxCriteriaDO inboxCriteriaDO:inboxQuery.getInboxCriteriaDOList()){
							String criteria=null;
							if(key.contains(".")){
								criteria = inboxCriteriaDO.getCriteriafield().substring(inboxCriteriaDO.getCriteriafield().lastIndexOf(".")+1);
							}else{
								criteria=inboxCriteriaDO.getCriteriafield();
							}
							if(key.equals(criteria)){
								if(sqlCriteria==null){
									sqlCriteria = inboxCriteriaDO.getCriteriafield() + " = \"" + criteriaList.get(key) +  "\"";
								}else{
									sqlCriteria = sqlCriteria + " and " + inboxCriteriaDO.getCriteriafield() + " = \"" + criteriaList.get(key) +  "\"";
								}
							}
						}
					}
				}
			}
			if(inboxQuery.getQuery()!=null){
				sql = "select " + sqlResult + " from " + inboxQuery.getQuery();
				if(sqlCriteria!=null){
					if(sql.contains(" where ")){
						sql = sql + " and  " + sqlCriteria;
					}else {
						sql = sql + " where " + sqlCriteria;
					}
				}
				if(inboxQuery.getPostquery()!=null){
					sql = sql + " " + inboxQuery.getPostquery();
				}
			}
			List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
			resultMap.put("QueryResultList", SerializationUtility.getInstance().toJson(list));
		}
		return resultMap;	
	}
}
