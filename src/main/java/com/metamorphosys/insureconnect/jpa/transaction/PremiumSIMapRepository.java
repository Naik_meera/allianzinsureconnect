package com.metamorphosys.insureconnect.jpa.transaction;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.transaction.LeadDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PremiumSIMappingTableDO;

public interface PremiumSIMapRepository extends CrudRepository<PremiumSIMappingTableDO, Long> {

	List<PremiumSIMappingTableDO> findByProductCd(String productCd);
}
