package com.metamorphosys.insureconnect.jpa.transaction;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.transaction.QuotationContainerDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.QuotationDO;

public interface QuotationContainerRepository extends CrudRepository<QuotationContainerDO, Long>{

	QuotationContainerDO findByQuotationRefNum(String quotationRefNum);

	List<QuotationContainerDO> findBybaseAgentCdAndOnlineUpdatedDtAfter(String baseAgentCd,Timestamp downloadDt);

}
