package com.metamorphosys.insureconnect.jpa.transaction;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.transaction.UsersDO;

public interface UserRepository extends CrudRepository<UsersDO, Long> {
	
	UsersDO findByUserIdAndChannel(String userid,String channel);
	
	UsersDO findByUserIdAndPassword(String userId, String password);
	
	List<UsersDO> findByUserType(String userType);

	UsersDO findByUserId(String name);

}
