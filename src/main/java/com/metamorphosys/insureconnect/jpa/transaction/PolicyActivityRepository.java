package com.metamorphosys.insureconnect.jpa.transaction;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyActivityDO;

public interface PolicyActivityRepository extends CrudRepository<PolicyActivityDO, Long>{

	PolicyActivityDO findByActivityKey(String proposalRefNum);
}
