package com.metamorphosys.insureconnect.jpa.transaction;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.transaction.WorkflowServiceDO;

public interface WorkflowServiceRepository extends CrudRepository<WorkflowServiceDO, Long> {

	List<WorkflowServiceDO> findByService(String service);
	
	List<WorkflowServiceDO> findByServiceType(String serviceType);
	
	List<WorkflowServiceDO> findByServiceAndServiceType(String service, String serviceType);
	
	List<WorkflowServiceDO> findByEntityGuid(String entityGuid);
	
}