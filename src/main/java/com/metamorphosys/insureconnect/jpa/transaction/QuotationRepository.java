package com.metamorphosys.insureconnect.jpa.transaction;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.transaction.QuotationDO;

public interface QuotationRepository extends CrudRepository<QuotationDO, Long> {

	QuotationDO findById(Long id);

	List<QuotationDO> findByBaseAgentCdAndQuotationDtIsNullOrQuotationDtAfter(String baseAgentCd, Date lastSynDt);
	
	QuotationDO findByQuotationRefNum(String quotationrefnum);
	
	List<QuotationDO> findBySystemUpdatedDtAfter(Timestamp systemupdateddt);

}