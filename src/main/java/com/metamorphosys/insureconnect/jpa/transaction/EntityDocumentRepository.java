package com.metamorphosys.insureconnect.jpa.transaction;

import java.util.HashSet;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;

import com.metamorphosys.insureconnect.dataobjects.transaction.EntityDocumentDO;

public interface EntityDocumentRepository extends CrudRepository<EntityDocumentDO, String>{

	List<EntityDocumentDO> findByEntityGuidAndUploadStatus(String guid,String uploadStatus);
	//void save(Iterable<EntityServiceDocumentDO> iterable);
	
	/*@Override
	default <S extends EntityServiceDocumentDO> Iterable<S> save(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}*/
	
	List<EntityDocumentDO> findByEntityIDAndDocumentTypeCd(String entityID,String documentTypeCd);
	List<EntityDocumentDO> findByEntityIDAndDocumentSubTypeCd(String entityID,String documentSubTypeCd);
}
