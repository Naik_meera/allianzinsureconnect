package com.metamorphosys.insureconnect.jpa.transaction;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.transaction.DeviceDO;

public interface DeviceRepository extends CrudRepository<DeviceDO, Long> {
	
	DeviceDO findByDeviceId(String deviceId);

}
