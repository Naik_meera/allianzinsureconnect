package com.metamorphosys.insureconnect.jpa.transaction;

import org.springframework.data.repository.Repository;

import com.metamorphosys.insureconnect.dataobjects.transaction.SequneceGeneratorDO;

public interface SequenceGenenratorRepository extends Repository<SequneceGeneratorDO, Long> {

	SequneceGeneratorDO findByServiceName(String servicename);
	
	SequneceGeneratorDO save(SequneceGeneratorDO sequenceGeneratorDO);
}
