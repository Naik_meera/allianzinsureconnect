package com.metamorphosys.insureconnect.jpa.transaction;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.transaction.CaseDO;

public interface CaseRepository extends CrudRepository<CaseDO, Long> {

	CaseDO findById(Long id);

}
