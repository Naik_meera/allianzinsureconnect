package com.metamorphosys.insureconnect.jpa.transaction;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.transaction.HashMapEntityDO;
import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

public interface HashMapEntityRepository extends CrudRepository<HashMapEntityDO, Long> {

	List<HashMapEntityDO> findByEntityGuid(String guid);

}