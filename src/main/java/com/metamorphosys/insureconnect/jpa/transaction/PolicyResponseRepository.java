package com.metamorphosys.insureconnect.jpa.transaction;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyResponseDO;

public interface PolicyResponseRepository extends CrudRepository<PolicyResponseDO, Long>{

}
