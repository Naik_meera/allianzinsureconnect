package com.metamorphosys.insureconnect.jpa.transaction;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.transaction.AgentDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.AgentDeviceMappingDO;

public interface AgentRepository extends CrudRepository<AgentDO, Long> {
	
	AgentDO findByAgentIdAndPassword(String agentId,String password);
	
	AgentDeviceMappingDO findByAgentIdAndAgentDeviceMappingDOListDeviceId(String agentId,String deviceId);

	AgentDO findByUserIdAndChannel(String userId,String channel);

	AgentDO findByAgentId(String agentId);

}
