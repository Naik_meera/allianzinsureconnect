package com.metamorphosys.insureconnect.jpa.transaction;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.transaction.ActivityDO;

public interface ActivityRepository extends CrudRepository<ActivityDO, Long> {

}
