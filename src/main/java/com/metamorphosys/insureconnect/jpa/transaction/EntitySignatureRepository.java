package com.metamorphosys.insureconnect.jpa.transaction;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.transaction.EntitySignatureDO;

public interface EntitySignatureRepository extends CrudRepository<EntitySignatureDO, Long> {

	List<EntitySignatureDO> findByEntityGuidAndUploadStatus(String guid, String pending);

}
