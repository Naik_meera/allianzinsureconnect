package com.metamorphosys.insureconnect.jpa.transaction;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.transaction.FolderDO;

public interface FolderRepository extends CrudRepository<FolderDO,Long>{

	
}
