package com.metamorphosys.insureconnect.jpa.transaction;

import java.util.HashMap;
import java.util.List;

import com.metamorphosys.insureconnect.InsureConnectApplication;
import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.WorkflowServiceDO;

public class WorkflowServiceMemoryRepository {
	
	private static WorkflowServiceRepository workflowServicePersistedRepository;
	
	private static HashMap<String, WorkflowServiceDO> workflowServiceRepository = new HashMap<String, WorkflowServiceDO>();
	
	private static WorkflowServiceMemoryRepository workflowServiceMemoryRepository = null;
	
	private WorkflowServiceMemoryRepository()
	{
		
	}
	
	public static void storeService(WorkflowServiceDO workflowServiceDO)
	{
		if(null != workflowServiceDO && null != workflowServiceDO.getEntityGuid())
		{
			workflowServiceRepository.put(workflowServiceDO.getEntityGuid(), workflowServiceDO);
		}
	}

	public static WorkflowServiceDO fetchService(BaseDO baseDO)
	{
		return fetchService(baseDO.getGuid());
	}
	
	public static WorkflowServiceDO fetchService(String guid)
	{
		WorkflowServiceDO workflowServiceDO = workflowServiceRepository.get(guid);
		if(null == workflowServiceDO)
		{
			if(null == workflowServicePersistedRepository)
			{
				workflowServicePersistedRepository = (WorkflowServiceRepository)InsureConnectApplication.getApplicationContext().getBean("workflowServiceRepository");
			}
			List<WorkflowServiceDO> workflowList = workflowServicePersistedRepository.findByEntityGuid(guid);
			
			if(null != workflowList && workflowList.size() >0)
			{
				workflowServiceDO = workflowList.get(0);
			}
		}
		return workflowServiceDO;
	}

	public static void clearMemoryCache(WorkflowServiceDO workflowServiceDO)
	{
		clearMemoryCache(workflowServiceDO.getEntityGuid());
	}

	public static void clearMemoryCache(BaseDO baseDO)
	{
		clearMemoryCache(baseDO.getGuid());
	}

	public static void clearMemoryCache(String guid)
	{
		workflowServiceRepository.remove(guid);
	}
}