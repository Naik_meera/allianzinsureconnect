package com.metamorphosys.insureconnect.jpa.transaction;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.transaction.InboxQueryDO;

public interface InboxQueryRepository extends CrudRepository<InboxQueryDO,Long>{

	
}
