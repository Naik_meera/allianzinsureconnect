package com.metamorphosys.insureconnect.jpa.transaction;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.transaction.LeadDO;

public interface LeadRepository extends CrudRepository<LeadDO, Long> {
	
	LeadDO findByLeadID(String leadId);

	List<LeadDO> findBybaseAgentCdAndOnlineUpdatedDtAfter(String baseAgentCd,Timestamp lastDownloadDt);
}
