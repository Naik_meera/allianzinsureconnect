package com.metamorphosys.insureconnect.jpa.transaction;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyDO;

public interface PolicyRepository extends CrudRepository<PolicyDO, Long> {
	
	PolicyDO findByPolicyNum(String policyNum);
	PolicyDO findByProposalRefNum(String proposalRefNum);
}