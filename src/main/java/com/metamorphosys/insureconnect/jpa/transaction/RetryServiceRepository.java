package com.metamorphosys.insureconnect.jpa.transaction;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.transaction.DeviceDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.RetryServiceDO;
import java.lang.String;
import java.util.List;

public interface RetryServiceRepository extends CrudRepository<RetryServiceDO, Long> {

	List<RetryServiceDO> findByServiceNameAndStatusCd(String servicename,String statusCd);
	RetryServiceDO findByServiceId(String serviceid);
}
