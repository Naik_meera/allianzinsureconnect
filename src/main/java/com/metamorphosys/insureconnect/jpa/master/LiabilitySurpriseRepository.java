package com.metamorphosys.insureconnect.jpa.master;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.master.LiabilitySurprise;

public interface LiabilitySurpriseRepository extends CrudRepository<LiabilitySurprise,Long>{
	
	List<LiabilitySurprise> findAll();
	LiabilitySurprise findByAgentId(String agentId);
}
