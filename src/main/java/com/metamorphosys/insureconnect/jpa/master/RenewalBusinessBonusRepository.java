package com.metamorphosys.insureconnect.jpa.master;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.master.RenewalBusinessBonus;

public interface RenewalBusinessBonusRepository extends  CrudRepository<RenewalBusinessBonus,Long>{
	
	List<RenewalBusinessBonus> findAll();
	RenewalBusinessBonus findByAgentId(String agentId);
}
