package com.metamorphosys.insureconnect.jpa.master;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.master.ProductDO;

public interface ProductRepository extends CrudRepository<ProductDO, Long> {

	ProductDO findByProductId(String productId);
	
	List<ProductDO> findByPlantype(String planType);
	
	List<ProductDO> findByProductNameOrProductFamilyCd(String productName,String familyCd);

	List<ProductDO> findByProductNameOrProductFamilyCdOrPlantypeOrProductStatus(String productName, String productFamilyCd,
			String plantype, String status);

	ProductDO findById(Long id);
	
}