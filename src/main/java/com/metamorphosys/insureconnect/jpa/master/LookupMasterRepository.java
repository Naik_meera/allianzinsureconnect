package com.metamorphosys.insureconnect.jpa.master;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.master.LookupMasterDO;

public interface LookupMasterRepository extends CrudRepository<LookupMasterDO, Long> {
	
	LookupMasterDO findByLookupCd(String lookupCd);
}