package com.metamorphosys.insureconnect.jpa.master;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.master.VocabularyDO;

public interface VocabularyRepository extends CrudRepository<VocabularyDO, Long> {

}
