package com.metamorphosys.insureconnect.jpa.master;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.master.TravelClub;

public interface TravelClubRepository extends  CrudRepository<TravelClub,Long>{
	
	List<TravelClub> findAll();
	TravelClub findByAgentId(String agentId);
}
