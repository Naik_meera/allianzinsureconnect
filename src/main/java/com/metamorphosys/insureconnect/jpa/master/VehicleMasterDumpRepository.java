package com.metamorphosys.insureconnect.jpa.master;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.metamorphosys.insureconnect.dataobjects.master.VehicleMasterDumpDO;

public interface VehicleMasterDumpRepository extends JpaRepository<VehicleMasterDumpDO, String> {
	
	List<VehicleMasterDumpDO> findByUpdatedDateGreaterThan(Timestamp date);
	
	VehicleMasterDumpDO findByVehicleCodeAndYear(String vehicleCode,String year);

}
