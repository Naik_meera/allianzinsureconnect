package com.metamorphosys.insureconnect.jpa.master;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.metamorphosys.insureconnect.dataobjects.master.ModelVariantDO;

public interface ModelVariantRepository extends PagingAndSortingRepository<ModelVariantDO, String> {
	
	ModelVariantDO findByModelCdAndSeatingCapacityAndVariantCd(String modelCd,String seatingCapacity,String variantCd);
	
	List<ModelVariantDO> findByUpdatedDateGreaterThanAndStatusCd(Timestamp date,long statusCd);
	
}
