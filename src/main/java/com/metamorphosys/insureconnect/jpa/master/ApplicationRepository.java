package com.metamorphosys.insureconnect.jpa.master;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.master.ApplicationDO;
import java.lang.String;

public interface ApplicationRepository extends CrudRepository<ApplicationDO,Long> {

	List<ApplicationDO> findByApplicationType(String string);
	
	ApplicationDO findByKey(String key);

}
