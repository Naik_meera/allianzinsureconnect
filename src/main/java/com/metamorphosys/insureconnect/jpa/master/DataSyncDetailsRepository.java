package com.metamorphosys.insureconnect.jpa.master;

import org.springframework.data.jpa.repository.JpaRepository;

import com.metamorphosys.insureconnect.dataobjects.master.DataSyncDetailsDO;

public interface DataSyncDetailsRepository extends JpaRepository<DataSyncDetailsDO, Long> {
	
//	List<DataSyncDetailsDO> findAll();
}
