package com.metamorphosys.insureconnect.jpa.master;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.master.CountryDO;

public interface CountryRepository extends CrudRepository<CountryDO, Long> {

	CountryDO findByCountryCd(String countryCd);
}
