package com.metamorphosys.insureconnect.jpa.master;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.metamorphosys.insureconnect.dataobjects.master.MakeDO;

public interface MakeRepository extends PagingAndSortingRepository<MakeDO, String> {
	
	MakeDO findByMakeCd(String guid);
	
	List<MakeDO> findByUpdatedDateGreaterThan(Timestamp date);
}