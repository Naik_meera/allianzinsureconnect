package com.metamorphosys.insureconnect.jpa.master;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.master.IncomeChart;

public interface IncomeChartRepository extends CrudRepository<IncomeChart,Long>{
	
	IncomeChart findByAgentId(String agentId);
	
}
