package com.metamorphosys.insureconnect.jpa.master;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.master.ProvinceDO;

public interface ProvinceRepository extends CrudRepository<ProvinceDO, Long> {

	ProvinceDO findByProvinceCd(String provinceCd);
}
