package com.metamorphosys.insureconnect.jpa.master;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.master.OfficeAllowance;

public interface OfficeAllowanceRepository extends CrudRepository<OfficeAllowance,Long>{
	
	OfficeAllowance findByAgentId(String agentId);
	
}
