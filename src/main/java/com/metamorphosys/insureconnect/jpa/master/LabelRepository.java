package com.metamorphosys.insureconnect.jpa.master;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.master.LabelDO;
import com.metamorphosys.insureconnect.dataobjects.master.ProductDO;

public interface LabelRepository extends CrudRepository<LabelDO, Long> {

	List<LabelDO> findAllByLabelKey(String labelKey);
	
	List<LabelDO> findAllByLanguage(String language);
	
	LabelDO findByLabelKeyAndLanguage(String labelKey, String language);
}