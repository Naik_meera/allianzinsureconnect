package com.metamorphosys.insureconnect.jpa.master;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.metamorphosys.insureconnect.dataobjects.master.RuleDO;

public interface RuleRepository extends CrudRepository<RuleDO, Long> {

	@Query("select r from RuleDO r where r.ruleName= ?1 and r.fromDate<=?2 and (r.toDate is null or r.toDate>=?2)")
//	List<RuleDO> findByRuleNameAndDate(String ruleName, java.util.Date date);
	
	List<RuleDO> findByRuleNameAndDate(String ruleName, java.util.Date date);
	
	List<RuleDO> findByService(String service);
	
	@Query("select r from RuleDO r where r.service= :service and r.serviceType=:serviceType and r.fromDate<=:date and (r.toDate is null or r.toDate>=:date) order by sortorder Asc")
	List<RuleDO> findByServiceAndServiceTypeAndDateOrderBySortOrderAsc(@Param("service") String service,@Param("serviceType") String serviceType,@Param("date") Date date);
	
	List<RuleDO> findByServiceAndServiceTypeOrderBySortOrderAsc(String service, String serviceType);
	
	List<RuleDO> findByServiceAndServiceTypeAndServiceTaskOrderBySortOrderAsc(String service, String serviceType, String serviceTask);

	@Query("select r from RuleDO r where r.service=:service and r.serviceType=:serviceType and r.serviceTask=:serviceTask and r.fromDate<=:date and (r.toDate is null or r.toDate>=:date)")
	List<RuleDO> findByServiceAndServiceTypeAndServiceTaskAndFromtoDt(@Param("service") String service,@Param("serviceType") String serviceType, @Param("serviceTask") String serviceTask,@Param("date") Date date);

	List<RuleDO> findByRuleNameAndServiceAndServiceTypeAndServiceTask(String rulename, String service_sent,
			String servicetype_sent, String servicetask_sent);

	List<RuleDO> findByRuleType(String ruleType);

	RuleDO findById(Long id);

	RuleDO findByRuleName(String ruleName);
	
//	List<RuleDataDO> findByParentObjectIdAndCondtion3AndCondition4AndCondition5AndCondition6AndCondition7AndCondition8(int parentId, String zoneCd, entryAgeFrom, entryAgeTo, vehicleMakeCd, vehicleModelCd, coverageTypeCd);
//	@Query("select rd.condition1, rd.condition2, rd.action1 from RuleData rd where rd.parentObject_id is in (Select r.id from Rule where rulename =?1)")
//	RuleDataDO findByRuleNameAndRuleDataListCondition3AndRuleDataListCondition6AndRuleDataListCondition7AndRuleDataListCondition8(
//			String string, String zoneCode, String vehicleMakeCd, String vehicleModelCd, String coverageType);
//	
//	RuleDO findByRuleNameAndRuleDataListCondition3AndRuleDataListCondition6AndRuleDataListCondition7AndRuleDataListCondition8(String ruleName,String Condition3,String condition6,String condition7, String condition8);

}