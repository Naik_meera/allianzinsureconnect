package com.metamorphosys.insureconnect.jpa.master;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.metamorphosys.insureconnect.dataobjects.master.SiRegtYearDO;

public interface SiRegtYearRepository extends PagingAndSortingRepository<SiRegtYearDO, String> {
	
	SiRegtYearDO findByModelCdAndVariantCdAndRegtYear(String modelCd,String variantCd,String regtYear);
	
	List<SiRegtYearDO> findByUpdatedDateGreaterThanAndStatusCd(Timestamp date,long statusCd);
	
}
