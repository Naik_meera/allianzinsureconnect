package com.metamorphosys.insureconnect.jpa.master;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.master.CityDO;

public interface CityRepository extends CrudRepository<CityDO, Long> {

	CityDO findByCityCd(String cityCd);
	
	CityDO findByCityCdAndProvinceCd(String cityCd, String provinceCd);
}
