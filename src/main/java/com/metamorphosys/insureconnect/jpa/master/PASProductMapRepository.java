package com.metamorphosys.insureconnect.jpa.master;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.master.PASProductMapDO;

public interface PASProductMapRepository extends CrudRepository<PASProductMapDO, Long>{

	List<PASProductMapDO> findByProductCd(String productCd);
	
	List<PASProductMapDO> findByProductCdAndBasePlanCd(String productCd, String basePlanCd);
}
