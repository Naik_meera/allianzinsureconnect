package com.metamorphosys.insureconnect.jpa.master;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.master.GwpGraph;

public interface GwpGraphRepository extends CrudRepository<GwpGraph,Long>{
	
	GwpGraph findByAgentId(String agentId);
	
}
