package com.metamorphosys.insureconnect.jpa.master;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.master.LoBSummary;

public interface LoBSummaryRepository extends CrudRepository<LoBSummary,Long>{
	
	List<LoBSummary> findAll();
	LoBSummary findByAgentId(String agentId);
}
