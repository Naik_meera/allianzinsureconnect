package com.metamorphosys.insureconnect.jpa.master;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.master.CommercialClub;

public interface CommercialClubRepository extends CrudRepository<CommercialClub,Long>{
	
	List<CommercialClub> findAll();
	CommercialClub findByAgentId(String agentId);
}
