package com.metamorphosys.insureconnect.jpa.master;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.master.LookupDataDO;

public interface LookupDataRepository extends CrudRepository<LookupDataDO, Long> {

	List<LookupDataDO> findAllByLookupCdOrderBySortOrderAsc(String lookupCd);
	LookupDataDO findByLookupCdAndLookupKey(String lookupCd, String lookupKey);
	
}