package com.metamorphosys.insureconnect.jpa.master;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.master.EntityConvertorDO;
import com.metamorphosys.insureconnect.dataobjects.master.LabelDO;
import com.metamorphosys.insureconnect.dataobjects.master.ProductDO;

public interface EntityConvertorRepository extends CrudRepository<EntityConvertorDO, Long> {

	EntityConvertorDO findOneBySourceClassAndDestinationClass(String sourceClass, String destinationClass);
	
	EntityConvertorDO findOneByConversionName(String conversionName);
}