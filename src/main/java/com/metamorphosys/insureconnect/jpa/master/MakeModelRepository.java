package com.metamorphosys.insureconnect.jpa.master;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.metamorphosys.insureconnect.dataobjects.master.MakeModelDO;

public interface MakeModelRepository extends PagingAndSortingRepository<MakeModelDO, String> {
	
	MakeModelDO findByModelCdAndSeatingCapacity(String modelCd,String seatingCapacity);
	
	List<MakeModelDO> findByUpdatedDateGreaterThanAndStatusCd(Timestamp date,long statusCd);
	
}