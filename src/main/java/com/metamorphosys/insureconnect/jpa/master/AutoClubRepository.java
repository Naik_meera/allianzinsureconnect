package com.metamorphosys.insureconnect.jpa.master;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.master.AutoClub;

public interface AutoClubRepository extends CrudRepository<AutoClub,Long>{
	
	List<AutoClub> findAll();
	AutoClub findByAgentId(String agentId);
}
