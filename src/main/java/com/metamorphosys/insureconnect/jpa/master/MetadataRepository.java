package com.metamorphosys.insureconnect.jpa.master;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.master.MetadataDO;

public interface MetadataRepository extends CrudRepository<MetadataDO, Long> {

	MetadataDO findByMetadataCd(String metadataCd);
	
	List<MetadataDO> findByMetadataCdIn(List<String> metadataCdList);
	
}