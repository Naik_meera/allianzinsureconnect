package com.metamorphosys.insureconnect.jpa.master;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.master.RolePrivilegeMapDO;

public interface RolePrivilegeMapRepository extends CrudRepository<RolePrivilegeMapDO, Long> {

	List<RolePrivilegeMapDO> findByRoleIdAndPrivilegeTypeOrderBySortOrderAsc(String roleId,String privilegeType);
	
	List<RolePrivilegeMapDO> findByRoleIdAndPrivilegeType(String roleId,String privilegeType);
}
