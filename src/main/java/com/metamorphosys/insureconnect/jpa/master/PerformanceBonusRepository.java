package com.metamorphosys.insureconnect.jpa.master;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.master.PerformanceBonus;

public interface PerformanceBonusRepository extends CrudRepository<PerformanceBonus,Long> {
	
	PerformanceBonus findByAgentId(String agentId);
	
}
