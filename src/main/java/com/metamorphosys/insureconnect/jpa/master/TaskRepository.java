package com.metamorphosys.insureconnect.jpa.master;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.master.TaskDO;
import java.lang.Long;

public interface TaskRepository extends CrudRepository<TaskDO, Long> {
	List<TaskDO> findByService(String service);
	List<TaskDO> findById(Long id);
}
