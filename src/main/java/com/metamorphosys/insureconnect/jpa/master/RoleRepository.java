package com.metamorphosys.insureconnect.jpa.master;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.master.RoleDO;

public interface RoleRepository extends CrudRepository<RoleDO, Long> {

}
