package com.metamorphosys.insureconnect.jpa.master;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.master.NewBusinessBonus;

public interface NewBusinessBonusRepository extends CrudRepository<NewBusinessBonus,Long>{
	
	NewBusinessBonus findByAgentId(String agentId);
	
}
