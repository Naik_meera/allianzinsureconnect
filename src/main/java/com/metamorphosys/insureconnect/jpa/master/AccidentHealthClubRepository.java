package com.metamorphosys.insureconnect.jpa.master;

import org.springframework.data.repository.CrudRepository;

import com.metamorphosys.insureconnect.dataobjects.master.AccidentHealthClub;

public interface AccidentHealthClubRepository extends CrudRepository<AccidentHealthClub,Long>{
	
	AccidentHealthClub findByAgentId(String agentId);
	
}
