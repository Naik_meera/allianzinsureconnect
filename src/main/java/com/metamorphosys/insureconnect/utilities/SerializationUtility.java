package com.metamorphosys.insureconnect.utilities;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.LinkedTreeMap;
import com.metamorphosys.insureconnect.InsureConnectApplication;

@Service
@Configurable
public class SerializationUtility {

	private static final Logger log = LoggerFactory.getLogger(SerializationUtility.class);
	
	private static SerializationUtility utility;
	
	private Gson serializer;
	private Gson deserializer;
	
	@Autowired
	private ApplicationContext applicationContext;
	
	public SerializationUtility()
	{
		log.info("App context: "+applicationContext);
	}
	
	private SerializationUtility(Boolean init)
	{		
		HashMap<String, Boolean> excludedClasses = null;
		HashMap<String, Boolean> excludedAttributes = null;
		HashMap<String, HashMap<String, Boolean>> excludedClassAttributes = null;

		if(null == applicationContext)
		{
			applicationContext = InsureConnectApplication.getApplicationContext();
		}
		
		try{
			excludedClasses = (HashMap<String, Boolean>)applicationContext.getBean("excludedClasses");
		}
		catch(Exception e)
		{
			//log.error("Error fetchingbean excludedClasses: "+e.getMessage());			
		}

		try{
			excludedAttributes = (HashMap<String, Boolean>)applicationContext.getBean("excludedAttributes");
		}
		catch(Exception e)
		{
			//log.error("Error fetchingbean excludedAttributes: "+e.getMessage());			
		}

		try{
			excludedClassAttributes = (HashMap<String, HashMap<String, Boolean>>)applicationContext.getBean("excludedClassAttributes");
		}
		catch(Exception e)
		{
			//log.error("Error fetchingbean excludedClassAttributes: "+e.getMessage());			
		}

		serializer = new GsonBuilder()
							.setExclusionStrategies(new FrameworkExclusionStrategy(excludedClasses, excludedAttributes, excludedClassAttributes))
							.registerTypeAdapter(java.util.Date.class, new DateAdapter())
							.registerTypeAdapter(java.sql.Date.class, new DateAdapter())
							.registerTypeAdapter(java.sql.Timestamp.class, new DateAdapter())
							.create();

		deserializer = new GsonBuilder()
							.registerTypeAdapter(java.util.Date.class, new DateAdapter())
							.registerTypeAdapter(java.sql.Date.class, new DateAdapter())
							.registerTypeAdapter(java.sql.Timestamp.class, new DateAdapter())
							.registerTypeAdapter(Short.class, new NumberAdapter())
							.registerTypeAdapter(Integer.class, new NumberAdapter())
							.registerTypeAdapter(Long.class, new NumberAdapter())
							.registerTypeAdapter(Float.class, new NumberAdapter())
							.registerTypeAdapter(Double.class, new NumberAdapter())
							.create();

	}
	
	public static SerializationUtility getInstance()
	{
		if(null == utility)
		{
			utility = new SerializationUtility(true);
		}
		
		return utility;
	}
	
	public String toJson(Object object)
	{
		return this.serializer.toJson(object);
	}

	public LinkedTreeMap fromJson(String json)
	{
		LinkedTreeMap returnObject = this.deserializer.fromJson(json, LinkedTreeMap.class);
		
		return returnObject;
	}

	public Object fromJson(String json, Class objectClass)
	{
		Object returnObject = this.deserializer.fromJson(json, objectClass);
		
		return returnObject;
	}

}
