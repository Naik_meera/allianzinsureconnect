package com.metamorphosys.insureconnect.utilities;

import java.util.HashMap;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

public class FrameworkExclusionStrategy implements ExclusionStrategy {

	HashMap<String, Boolean> excludedClasses = null;
	HashMap<String, Boolean> excludedAttributes = null;
	HashMap<String, HashMap<String, Boolean>> excludedClassAttributes = null;
	
	public FrameworkExclusionStrategy()
	{
		
	}

	public FrameworkExclusionStrategy(
			HashMap<String, Boolean> excludedClasses,
			HashMap<String, Boolean> excludedAttributes,
			HashMap<String, HashMap<String, Boolean>> excludedClassAttributes)
	{
		this.excludedClasses = excludedClasses;
		this.excludedAttributes = excludedAttributes;
		this.excludedClassAttributes = excludedClassAttributes;
	}

	public boolean shouldSkipClass(Class<?> klass) 
	{
		boolean shouldSkip = false;
		
		if(null != excludedClasses)
		{
			Boolean excludedClass = excludedClasses.get(klass.getSimpleName());
			if(null != excludedClass)
			{
				shouldSkip = excludedClass.booleanValue();
			}
		}
		
		return shouldSkip;
	}

	public boolean shouldSkipField(FieldAttributes f) 
	{
		boolean shouldSkip = false;

		if(null != excludedAttributes)
		{
			Boolean excludedattribute = excludedAttributes.get(f.getName());
			if(null != excludedattribute)
			{
				shouldSkip = excludedattribute.booleanValue();
			}
		}
		
		if(null != excludedClassAttributes)
		{
			HashMap<String, Boolean> excludedClass = excludedClassAttributes.get(f.getDeclaringClass().getSimpleName());
			if(null != excludedClass)
			{
				Boolean excludedAttribute = excludedClass.get(f.getName());
				if(null != excludedAttribute)
				{
					shouldSkip = excludedAttribute.booleanValue();
				}
			}
		}
		
		return shouldSkip;
	}

}