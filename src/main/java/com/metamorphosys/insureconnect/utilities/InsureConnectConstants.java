package com.metamorphosys.insureconnect.utilities;

public final  class InsureConnectConstants {

	
	public static final String QUOTATIONCONTAINER = "QuotationContainer";
	public static final String LEAD = "Lead";
	public static final String RULELENGTH = "ruleDataLength";
	public static final String STATUS = "status";
	public static final String OFFLINE = "OFFLINE";
	public static final Object UWDECISIONCD = "uwDecisionCd";
	
	public final class DataObjects {
		public static final String VOCABULARYDO = "VocabularyDO";
		public static final String SERVICEDO = "ServiceDO";
		public static final String POLICYDO = "PolicyDO";
		public static final String RESULTDO = "ResultDO";
		public static final String RULEDO = "RuleDO";
		public static final String PRODUCTDO = "ProductDO";
		public static final String WORKFLOWSERVICEDO = "WorkflowServiceDO";
		public static final String QUOTATIONDO = "QUOTATIONDO";
		public static final String CASEDO = "CaseDO";
		public static final String ENTITYDOCUMMENTDO = "EntityDocumentDO";
		public static final String ENTITYSIGNATUREDO = "EntitySignatureDO";
	}
	
	public final class Status {
		public static final String PENDING = "PENDING";
		public static final String SUCCESS = "SUCCESS";
		public static final String FAILED = "FAILED";
	}
	
	public final class Method {
		public static final String EXECUTE = "EXECUTE";
		public static final String GET = "GET";
	}
	
	public final class AttributeType {
		public static final String PRODUCTCALCULATION = "ProductCalculation";
		public static final String PRODUCTVALIDATION = "ProductValidation";
	}
	
	public final class Channel {
		public static final String AGENCY = "AGENCY";
		public static final String BANCA = "BANCA";
	}
	
	public final class Source {
		public static final String MOBILE = "MOBILE";
	}
	
	public final class Interface {
		public static final String AUTHPATH = "AUTHPATH";
		public static final String UPLOADPATH = "UPLOADPATH";
	}
	
	public final class Path {
		public static final String ENTITYDOCPATH = "ENTITYDOCPATH";
		public static final String AUTHINTERFACEPATH = "AUTHINTERFACEPATH";
		public static final String ENTITYINTERFACEPATH = "ENTITYINTERFACEPATH";
		public static final String POLICYINTERFACEPATH = "POLICYINTERFACEPATH";
		public static final String POLICYBINDINGDPATH = "POLICYBINDINGDPATH";
		public static final String ALIPOLICYINTERFACEPATH = "ALIPOLICYINTERFACEPATH";
		public static final String ALIPOLICYBINDINGDPATH = "ALIPOLICYBINDINGDPATH";
		public static final String OFFLINEDBPATH = "OFFLINEDBPATH";
		public static final String AUTHPATH = "AUTHPATH";
		public static final String POLICYBINDINGINTERFACEPATHDPATH = "POLICYBINDINGINTERFACEPATHDPATH";
		public static final String VEHICLESYNCINTERFACEPATH = "VEHICLESYNCINTERFACEPATH";
		public static final String VEHICLESYNCPATH = "VEHICLESYNCPATH";
		public static final String DASHBOARDFILEPATH = "DASHBOARDFILEPATH";
		public static final String TOKENVALIDATIONPATH = "TOKENVALIDATIONPATH";
		public static final String TOKENVALIDATIONINTERFAEPATH = "TOKENVALIDATIONINTERFAEPATH";
		public static final String TOKENINVALIDATIONPATH = "TOKENINVALIDATIONPATH";
		public static final String TOKENINVALIDATIONINTERFAEPATH = "TOKENINVALIDATIONINTERFAEPATH";
	}
	
	public final class SystemUserCredentials {
		public static final String SYSTEMUSERID = "SYSTEMUSERID";
		public static final String SYSTEMUSERPWD = "SYSTEMUSERPWD";
	}
	public final class InterfaceStatusCd {
		public static final String SUCCESS = "AU000";
		public static final String ERR_INVALIDUSER = "AU100";
		public static final String ERR_ACCOUNTEXPIRE = "AU101";
		public static final String ERR_CREDENTIALEXPIRE = "AU102";
		public static final String ERR_APPPERMISSION = "AU103";
		public static final String ERR_AUTH = "AU104";
		public static final String ERR_CROWDCONN = "AU105";
		public static final String ERR_SERVICE = "AU106";
		public static final String ERR_LOCKED = "AU111";
		
	}
	
	public final class EXCEPTION {
		public static final String SUCCESS = "SUCCESS";
		public static final String ERR_INVALIDUSER = "ERR_INVALIDUSER";
		public static final String ERR_ACCOUNTEXPIRE = "ERR_ACCOUNTEXPIRE";
		public static final String ERR_CREDENTIALEXPIRE = "ERR_CREDENTIALEXPIRE";
		public static final String ERR_APPPERMISSION = "ERR_APPPERMISSION";
		public static final String ERR_AUTH = "ERR_AUTH";
		public static final String ERR_CROWDCONN = "ERR_CROWDCONN";
		public static final String ERR_SERVICE = "ERR_SERVICE";
		public static final String ERR_UNKNOWN = "ERR_UNKNOWN";
		public static final String ERR_LOCKED = "ERR_LOCKED";
	}
	
	public final class PLANTYPE {
		public static final String RIDER = "RIDER";
		public static final String BASEPLAN = "RIDER";
	}
	
	public final class ROLECD {
		public static final String PROPOSER = "PROPOSER";
	}
	
	public final class CONTACTTYPE {
		public static final String MOBILE = "mobile";
		public static final String HOME = "HOME";
		public static final String EMAIL = "email";
		public static final String OFFICE = "office";
	}
	
	public final class uwDecision {
		public static final String ACCEPTED = "ACCEPTED";
	}
	
	public final class RetryService{
		public static final String BINDING = "BINDING";
	}
	
	public final class DOCUMENTYPECD{
		public static final String VEHICLE="VEHICLE";
		public static final String IDPROOF = "IDPROOF";
		public static final String ACCESSORYIMAGE = "ACCESSORYIMAGE";
	}
	
	public final class CLIENTTYPE{
		public static final String CLIENTCD = "clientTypeCd";
	}
	
	public final class ZONECD{
		public static final String ZONECD = "zoneCd";
	}
	
	public final class DOCSUBTYPECD{
		public static final String FRONTLEFT = "VEHICLE_FRONT_LEFT";
		public static final String FRONTRIGHT = "VEHICLE_FRONT_RIGHT";
		public static final String CHASIS = "VEHICLE_CHASSIS_NO";
		public static final String BACKLEFT = "VEHICLE_BACK_LEFT";
		public static final String BACKRIGHT = "VEHICLE_BACK_RIGHT";
		public static final String FRONT = "FRONT";
		public static final String RIGHT = "RIGHT";
		public static final String LEFT = "LEFT";
		public static final String BACK = "BACK";
		public static final String IDENTITY = "IDENTITY";
	}
	
	public final class PASCONSTANTS{
		public static final String BASICCOVERAGETYPE = "basicCoverageType";
		public static final String PRODUCTID = "productId";
		public static final String PRODUCTNAME = "productName";
		public static final String PRODUCTTYPE = "productType";
		public static final String CURRENCY = "currency";
		public static final String COVERAGETYPE = "coverageType";
		public static final String COVERAGECODE = "coverageCode";
		public static final String COVERAGENAME = "coverageName";
		public static final String ISEDITABLE = "isEditable";
		public static final String COVERAGEGROUP = "coverageGroup";
		public static final String VALUETYPE = "valueType";
		public static final String EDITABLE = "editable";
		public static final String DISPLAYFL = "display";
		public static final String VALUELIST = "valueList";	
	}
	
	public final class VALUETYPE{
		public static final String SELECTION = "SELECTION";
	}
	
	public final class BOOLEAN{
		public static final String TRUE = "true";
		public static final String FALSE = "false";
	}

	public final class EDITABLEVALUE{
		public static final String INSUREDVALUE = "INSUREDVALUE";
		public static final String PREMIUMRATE = "PREMIUMRATE";
		public static final String PREMIUMVALUE = "PREMIUMVALUE";
		public static final String DEDUCTIBLE = "DEDUCTIBLE";
	}
	
	public final class LOOKUPCD{

		public static final String ACCESSORY = "VEHICLEACCESSORY";
		public static final String OCCUPATION = "OCCUPATION";
		public static final String IDENTITYTYPE = "IDTYPE";
		
	}
	public final class GENDERCD{
		public static final String GENDERCD = "genderCd";
	}
	
	public final class IMAGEMIMETYPE{
		public static final String PNG = "PNG";
		public static final String JPEG = "JPEG";
	}
	
	public final class UWscore{
		public static final String UWSCORE = "UWscore";
		public static final String ACCEPTEDMIN = "AcceptedMin";
		public static final String ACCEPTEDMAX = "AcceptedMax";
		public static final String REFERMIN = "ReferMin";
		public static final String REFERMAX = "ReferMax";
		public static final String REJECTEDMIN = "RejectedMin";
		public static final String REJECTEDMAX = "RejectedMax";
	}
}
