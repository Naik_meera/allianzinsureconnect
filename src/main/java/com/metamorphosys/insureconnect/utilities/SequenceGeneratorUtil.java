package com.metamorphosys.insureconnect.utilities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.metamorphosys.insureconnect.dataobjects.transaction.SequneceGeneratorDO;
import com.metamorphosys.insureconnect.jpa.transaction.SequenceGenenratorRepository;

@Component("SeqGeneratorService")
public class SequenceGeneratorUtil {
	
	@Autowired
	SequenceGenenratorRepository sequenceGenenratorRepository;
	
	public String generateNextSeq(String serviceName){
		
		SequneceGeneratorDO sequenceGeneratorDO=sequenceGenenratorRepository.findByServiceName(serviceName);
		if(sequenceGeneratorDO!=null){
			String seqNum;
			if(sequenceGeneratorDO.getCurrentNum()!=null){
				seqNum=String.valueOf(Long.parseLong(sequenceGeneratorDO.getCurrentNum())+1);
				seqNum = org.apache.commons.lang.StringUtils.leftPad(seqNum, sequenceGeneratorDO.getLength(), '0');
			}else{
				seqNum = sequenceGeneratorDO.getStartsWith();
			}
			sequenceGeneratorDO.setCurrentNum(seqNum);
			sequenceGenenratorRepository.save(sequenceGeneratorDO);
			return seqNum;
		}
		return null;		
	}
}
