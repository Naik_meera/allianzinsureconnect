package com.metamorphosys.insureconnect.utilities;

import java.util.Date;

public class DateUtil {
	
	private static DateUtil utility;

	public static DateUtil getInstance()
	{
		if(null == utility)
		{
			utility = new DateUtil();
		}
		
		return utility;
	}
	
	public Date systemDate(){
		Date date = new Date();
		return date;	
	}
}
