package com.metamorphosys.insureconnect.utilities;

import java.lang.reflect.Type;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class NumberAdapter implements JsonDeserializer<Number>, JsonSerializer<Number> {

	private static final Logger log = LoggerFactory.getLogger(NumberAdapter.class);

	public JsonElement serialize(Number arg0, Type arg1, JsonSerializationContext arg2) {
		// TODO Auto-generated method stub
		return null;
	}

	public Number deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException 
	{	
		Number returnValue = null;
		try
		{
			if ("class java.lang.Short".equals(type.toString())) {
				returnValue = Short.parseShort(json.getAsString());
			} else if ("class java.lang.Integer".equals(type.toString())) {
				returnValue = Integer.parseInt(json.getAsString());
			} else if ("class java.lang.Long".equals(type.toString())) {
				returnValue = Long.parseLong(json.getAsString());
			} else if ("class java.lang.Float".equals(type.toString())) {
				returnValue = Float.parseFloat(json.getAsString());
			} else if ("class java.lang.Double".equals(type.toString())) {
				returnValue = Double.parseDouble(json.getAsString());
			} else {
				log.info("Unsupported object type [" + type.getTypeName() + "] ");
				returnValue = json.getAsNumber();
			}
		}catch(Exception e)
		{
			log.info("Cannot convert ["+type.getTypeName()+"] to number: "+json.toString());
		}
        
        return returnValue;
	}
}
