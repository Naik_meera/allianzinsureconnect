package com.metamorphosys.insureconnect.utilities;

import java.util.UUID;

public class GuidGenerator {

	public static synchronized String generate()
	{
		String guid = UUID.randomUUID().toString();
		return guid;
	}
}
