package com.metamorphosys.insureconnect.ruleengine;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.metamorphosys.insureconnect.dataobjects.master.ProductAttributeDO;
import com.metamorphosys.insureconnect.dataobjects.master.ProductDO;
import com.metamorphosys.insureconnect.jpa.master.ProductRepository;

@Component("fetchProductAttribute")
public class FetchProductAttribute extends BaseRuleFunction implements RuleFunction{

	@Autowired
	private ProductRepository productRepository;
	
	public Object execute(Object[] args) 
	{
		ProductAttributeDO productAttribute = null;
		
		if(args.length == 2)
		{
			String productId = (String)args[0];
			String attributeId = (String)args[1];
			
			ProductDO productDO = productRepository.findByProductId(productId);
			productAttribute = productDO.findProductAttribute(attributeId);
		}
		
		return productAttribute;
	}

}
