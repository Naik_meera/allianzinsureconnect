package com.metamorphosys.insureconnect.ruleengine;

import java.sql.Timestamp;

import org.springframework.stereotype.Component;

@Component("businessDate")
public class BusinessDate extends BaseRuleFunction implements RuleFunction{

	public Object execute(Object[] args) 
	{
		return new Timestamp(System.currentTimeMillis());
	}

}
