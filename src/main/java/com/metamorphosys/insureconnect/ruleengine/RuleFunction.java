package com.metamorphosys.insureconnect.ruleengine;

import java.util.HashMap;

public interface RuleFunction {

	public void setPreferences(HashMap<String, String> preferencesMap);
	
	public Object execute(Object[] args);
	
}
