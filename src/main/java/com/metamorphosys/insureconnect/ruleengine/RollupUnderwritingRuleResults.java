package com.metamorphosys.insureconnect.ruleengine;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.metamorphosys.insureconnect.dataobjects.common.ResultDO;
import com.metamorphosys.insureconnect.dataobjects.master.LookupDataDO;
import com.metamorphosys.insureconnect.jpa.master.LookupDataRepository;
import com.metamorphosys.insureconnect.utilities.InsureConnectConstants;

@Component("rollupUnderwritingRuleResults")
public class RollupUnderwritingRuleResults extends BaseRuleFunction implements RuleFunction {

	@Autowired
	LookupDataRepository lookupDataRepository;
	public Object execute(Object[] args) {
		String result = "ACCEPT";
		int resultPriority = 1;
		int sumResult = 0;
		if (args.length == 1) {
			if (args[0] != null && args[0] instanceof List) {
				List rules = (List) args[0];
				for (Object object : rules) {
					ResultDO resultDO = (ResultDO) object;
					if (null != resultDO && null != resultDO.getResultMap()
							&& null != resultDO.getResultMap().get("uw_result")) {
						//Commented for allianz
						/*int rulePriority = getPriority(resultDO.getResultMap().get("uw_result"));
						if (rulePriority > resultPriority) {
							resultPriority = rulePriority;
							result = resultDO.getResultMap().get("uw_result").toString();
						}*/
						
						//RollUp Logic for allianz
						sumResult =sumResult+ Integer.parseInt((String) resultDO.getResultMap().get("uw_result"));
					}
					result=getUWDescision(sumResult);
				}
			}
		}

		return result;
	}

	private String getUWDescision(int sumResult) {
		
		LookupDataDO acceptedMin=lookupDataRepository.findByLookupCdAndLookupKey(InsureConnectConstants.UWscore.UWSCORE, InsureConnectConstants.UWscore.ACCEPTEDMIN);
		Integer acceptedMinValue=acceptedMin!=null?Integer.parseInt(acceptedMin.getLookupValue()):null;
		LookupDataDO acceptedMax=lookupDataRepository.findByLookupCdAndLookupKey(InsureConnectConstants.UWscore.UWSCORE, InsureConnectConstants.UWscore.ACCEPTEDMAX);
		Integer acceptedMaxValue=acceptedMax!=null?Integer.parseInt(acceptedMax.getLookupValue()):null;
		
		LookupDataDO referMin=lookupDataRepository.findByLookupCdAndLookupKey(InsureConnectConstants.UWscore.UWSCORE, InsureConnectConstants.UWscore.REFERMIN);
		Integer referMinValue=referMin!=null?Integer.parseInt(referMin.getLookupValue()):null;
		LookupDataDO referMax=lookupDataRepository.findByLookupCdAndLookupKey(InsureConnectConstants.UWscore.UWSCORE, InsureConnectConstants.UWscore.REFERMAX);
		Integer referMaxValue=referMax!=null?Integer.parseInt(referMax.getLookupValue()):null;
		
		LookupDataDO rejectedMin=lookupDataRepository.findByLookupCdAndLookupKey(InsureConnectConstants.UWscore.UWSCORE, InsureConnectConstants.UWscore.REJECTEDMIN);
		Integer rejectedMinValue=rejectedMin!=null?Integer.parseInt(rejectedMin.getLookupValue()):null;
		LookupDataDO rejectedMax=lookupDataRepository.findByLookupCdAndLookupKey(InsureConnectConstants.UWscore.UWSCORE, InsureConnectConstants.UWscore.REJECTEDMAX);
		Integer rejectedMaxValue=rejectedMax!=null?Integer.parseInt(rejectedMax.getLookupValue()):null;
		
		if(sumResult>=acceptedMinValue && sumResult<=acceptedMaxValue){
			return "ACCEPTED";
		}else if(sumResult>=referMinValue && sumResult<referMaxValue){
			return "REFER";
		}else if(sumResult>=rejectedMinValue && sumResult<rejectedMaxValue){
			return "REJECTED";
		}
		return "REFER";
	}

	private int getPriority(Object result) {
		int priority = -1;
		if (null != result) {
			/*
			 * switch(result.toString()) { case "ACCEPT": priority = 1; break;
			 * case "REFER": priority = 2; break; case "REJECT": priority = 3;
			 * break; default: priority = 0; }
			 */
			if ("ACCEPT".equals(result.toString())) {
				priority = 1;
			} else if ("REFER".equals(result.toString())) {
				priority = 2;
			} else if ("REJECT".equals(result.toString())) {
				priority = 3;
			} else {
				priority = 0;
			}
		}

		return priority;
	}
}
