package com.metamorphosys.insureconnect.ruleengine;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Component;

import com.metamorphosys.insureconnect.dataobjects.common.ResultDO;

@Component("fetchRiders")
public class FetchRiders extends BaseRuleFunction implements RuleFunction {

	public Object execute(Object[] args) {
		if(args.length == 3)
		{
			HashMap<String,String> riderList = new HashMap<String,String>();
			String ridername=(String) args[2];
			String validation=(String) args[1];
			if(args[0] != null && args[0] instanceof List)
			{
				List rules = (List)args[0];
				if(rules.size()>0){
					for(Object object : rules)
					{	
						ResultDO resultDO=(ResultDO) object;
						
						
						if(null != resultDO && null != resultDO.getResultMap() && null != resultDO.getResultMap().get("rider_result"))
						{
							riderList = (HashMap) resultDO.getResultMap().get("rider_result");
							
						}
						riderList.put(ridername, validation);
						return riderList;
					}
				}else{
					riderList.put(ridername, validation);
					return riderList;
				}
			}
		}
		return null;
	}

}
