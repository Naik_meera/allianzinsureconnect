package com.metamorphosys.insureconnect.ruleengine;

import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;
import com.metamorphosys.insureconnect.dataobjects.common.TransactionDO;
import com.metamorphosys.insureconnect.utilities.GuidGenerator;

@Component("persistEntity")
public class PersistEntity extends BaseRuleFunction implements RuleFunction{

	private static final Logger log = LoggerFactory.getLogger(PersistEntity.class);
	
	@Autowired
	private ApplicationContext applicationContext;
	
	public Object execute(Object[] args) 
	{
		BaseDO entity = null;
		if(args.length == 2)
		{
			String repositoryName = (String)args[0];
			entity = (BaseDO)args[1];

			try
			{
				Object repositoryBean = applicationContext.getBean(repositoryName);
	
				if(null != repositoryBean)
				{
					CrudRepository<BaseDO, Long> crudRepository = (CrudRepository)repositoryBean;
					prepareEntity(entity);
					entity = crudRepository.save(entity);
				}
				else
				{
					log.error(repositoryName+" bean does not exist!!");
				}
			}
			catch(Exception e)
			{
				log.error("Error persisting entity using repository: "+repositoryName+"; error: "+e.getMessage() );
				e.printStackTrace();
			}
		}
		
		return entity;
	}

	private void prepareEntity(BaseDO entity) {		
		
		// guid
		if(null == entity.getGuid())
		{
			entity.setGuid(GuidGenerator.generate());
		}
		
		// version id
		if(null == entity.getVersionId())
		{
			entity.setVersionId(1);
		}
		else
		{
			entity.setVersionId(entity.getVersionId() + 1);
		}
		
		// systemcreateddt & systemupdateddt
		if( entity instanceof TransactionDO)
		{
			TransactionDO transactionDO = (TransactionDO)entity;
			// guid
			if(null == transactionDO.getSystemCreatedDt())
			{
				transactionDO.setSystemCreatedDt(new Timestamp(System.currentTimeMillis()));
			}
			transactionDO.setSystemUpdatedDt(new Timestamp(System.currentTimeMillis()));
		}
		
		Method[] methods = entity.getClass().getMethods();
		
		for(Method method: methods)
		{
			if(method.getName().startsWith("get") && method.getName().endsWith("List"))
			{
				try
				{
					List<BaseDO> list= (List<BaseDO>)method.invoke(entity);
					if(null != list)
					{
						for(BaseDO childEntity : list)
						{
							if(null != childEntity)
							{
								childEntity.setParentObject(entity);
								prepareEntity(childEntity);
							}
						}
					}
				}
				catch(Exception e)
				{
					log.error("Error invoking method: ["+method.getName()+"] on object: "+entity.getObjectName());
				}
			}
		}
	}

}
