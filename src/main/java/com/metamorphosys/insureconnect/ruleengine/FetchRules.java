package com.metamorphosys.insureconnect.ruleengine;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.metamorphosys.insureconnect.dataobjects.master.RuleDO;
import com.metamorphosys.insureconnect.jpa.master.RuleRepository;

@Component("fetchRules")
public class FetchRules extends BaseRuleFunction implements RuleFunction{

	@Autowired
	private RuleRepository ruleRepository;
	
	public Object execute(Object[] args) 
	{
		List<RuleDO> ruleList = null;
		
		if(args.length == 3)
		//if(args.length == 2)
		{
			String service = (String)args[0];
			String serviceType = (String)args[1];
			Date date = (Date)args[2];
			
			ruleList = ruleRepository.findByServiceAndServiceTypeAndDateOrderBySortOrderAsc(service, serviceType,date);
			//ruleList = ruleRepository.findByServiceAndServiceTypeOrderBySortOrderAsc(service, serviceType);
		
		}
		
		return ruleList;
	}

}
