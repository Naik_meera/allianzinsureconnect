package com.metamorphosys.insureconnect.ruleengine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.metamorphosys.insureconnect.dataobjects.master.LabelDO;
import com.metamorphosys.insureconnect.dataobjects.master.LookupDataDO;
import com.metamorphosys.insureconnect.dataobjects.master.MetadataDO;
import com.metamorphosys.insureconnect.dataobjects.master.UIMetadataDO;
import com.metamorphosys.insureconnect.jpa.master.LabelRepository;
import com.metamorphosys.insureconnect.jpa.master.MetadataRepository;
import com.metamorphosys.insureconnect.transaction.controllers.LookupMasterController;
import com.metamorphosys.insureconnect.utilities.SerializationUtility;

@Component("loadMetadata")
public class LoadMetadata extends BaseRuleFunction implements RuleFunction{

	@Autowired
	private MetadataRepository metadataRepository;

	@Autowired
	private LabelRepository labelRepository;

	@Autowired
	private LookupMasterController lookupMasterController;
	
	public Object execute(Object[] args) 
	{
		HashMap<String, HashMap<String, HashMap<String, String>>> metadataMap = new HashMap<String, HashMap<String, HashMap<String, String>>>();
		
		if(args.length == 1)
		{
			String contexts = (String)args[0];
			List<String> contextList = Arrays.asList(contexts.split(","));
			
			List<MetadataDO> metadataList = metadataRepository.findByMetadataCdIn(contextList);
			
			if(null != metadataList && metadataList.size() > 0)
			{
				for (MetadataDO metadataDO : metadataList) 
				{
					if(null != metadataDO && null != metadataDO.getUiMetadataList() && metadataDO.getUiMetadataList().size() > 0)
					{
						for (UIMetadataDO uiMetadataDO : metadataDO.getUiMetadataList())
						{
							String objectCd = uiMetadataDO.getObjectCd();
							String attributeCd = uiMetadataDO.getAttributeCd();
							String uiProperty = uiMetadataDO.getUiProperty();
							String uiValue = uiMetadataDO.getUiValue();
							
							HashMap<String, HashMap<String, String>> objectMap = metadataMap.get(objectCd);
							if(null == objectMap)
							{
								objectMap = new HashMap<String, HashMap<String, String>>();
								metadataMap.put(objectCd, objectMap);
							}
							
							HashMap<String, String> attributeMap = objectMap.get(attributeCd);
							if(null == attributeMap)
							{
								attributeMap = new HashMap<String, String>();
								objectMap.put(attributeCd, attributeMap);
							}
							
							// preprocess property and value combonation
							if("LABEL".equals(uiProperty))
							{
								String labelKey = uiValue;
								LabelDO label = labelRepository.findByLabelKeyAndLanguage(labelKey, preferencesMap.get("languageCd"));
								if(null != label && null != label.getLabelValue())
								{
									uiValue = label.getLabelValue();
								}
							}
							else if ("LOOKUP".equals(uiProperty))
							{
								String lookupCd = uiValue;
								List<LookupDataDO> lookupDataList = lookupMasterController.getLookupData(lookupCd);
								List<HashMap<String, String>> lookupOutputList = new ArrayList<HashMap<String, String>>();
								if(null != lookupDataList && lookupDataList.size() > 0)
								{
									for (LookupDataDO lookupDataDO : lookupDataList) 
									{
										if(null != lookupDataDO && null != lookupDataDO.getLookupKey() && null != lookupDataDO.getLookupValue())
										{
											String lookupValue = lookupDataDO.getLookupValue(); 
											LabelDO label = labelRepository.findByLabelKeyAndLanguage(lookupDataDO.getLookupValue(), preferencesMap.get("languageCd"));
											if(null != label && null != label.getLabelValue())
											{
												lookupValue = label.getLabelValue();
											}
											HashMap<String, String> lookup = new HashMap<String, String>();
											lookup.put("lookupKey", lookupDataDO.getLookupKey());
											lookup.put("lookupValue", lookupValue);
											lookupOutputList.add(lookup);
										}
										
									}
								}
								uiValue = SerializationUtility.getInstance().toJson(lookupOutputList);
							}
							
							// insert combination into map
							attributeMap.put(uiProperty, uiValue);
						}
					}
				}
			}
		}
		
		return metadataMap;
	}

}
