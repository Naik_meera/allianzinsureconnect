package com.metamorphosys.insureconnect.ruleengine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.metamorphosys.insureconnect.common.interfaceobjects.WorkflowIO;
import com.metamorphosys.insureconnect.dataobjects.master.PreferenceDO;
import com.metamorphosys.insureconnect.workflow.Task;

@Component("executeTask")
public class ExecuteTask extends BaseRuleFunction implements RuleFunction{

	private static final Logger log = LoggerFactory.getLogger(ExecuteTask.class);
	
	@Autowired
	private ApplicationContext applicationContext;
	
	public Object execute(Object[] args) 
	{
		WorkflowIO baseIO = null;
		
		if(args.length > 0)
		{
			String serviceTask = args[0].toString();
			
			
			Task task = null;
			try
			{
				task = (Task)applicationContext.getBean(serviceTask);
				
				baseIO = new WorkflowIO();
				
				if(args.length > 1)
				{
					PreferenceDO baseDO = new PreferenceDO();
					baseDO.setGuid(args[1].toString());
					baseIO.setBaseDO(baseDO);
				}
				
				task.init(baseIO);
				
				task.execute(baseIO);
				
				task.postExecute(baseIO);
				
			}
			catch(Exception e)
			{
				log.error("Error fetching task bean ["+serviceTask+"]: "+e.getMessage());
				e.printStackTrace();
			}
		}
		
		return baseIO;
	}

}
