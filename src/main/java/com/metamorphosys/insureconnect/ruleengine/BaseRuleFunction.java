package com.metamorphosys.insureconnect.ruleengine;

import java.util.HashMap;

public abstract class BaseRuleFunction implements RuleFunction{

	HashMap<String, String> preferencesMap = new HashMap<String, String>();
	
	public void setPreferences(HashMap<String, String> preferencesMap)
	{
		this.preferencesMap = preferencesMap;
	}

}
