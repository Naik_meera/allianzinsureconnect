package com.metamorphosys.insureconnect.ruleengine;

import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;
import com.metamorphosys.insureconnect.dataobjects.common.TransactionDO;
import com.metamorphosys.insureconnect.dataobjects.master.EntityConvertorDO;
import com.metamorphosys.insureconnect.dataobjects.master.EntityConvertorMappingDO;
import com.metamorphosys.insureconnect.jpa.master.EntityConvertorRepository;
import com.metamorphosys.insureconnect.utilities.GuidGenerator;

@Component("entityConvertor")
public class EntityConvertor extends BaseRuleFunction implements RuleFunction{

	private static final Logger log = LoggerFactory.getLogger(EntityConvertor.class);
	
	@Autowired
	private EntityConvertorRepository entityConvertorRepository;
	
	public Object execute(Object[] args) 
	{
		Object destinationObject = null;
		if(args.length == 2)
		{
			Object sourceObject = args[0];
			String destinationClassName = (String)args[1];
			EntityConvertorDO entityConvertor = entityConvertorRepository.findOneBySourceClassAndDestinationClass(
					sourceObject.getClass().getSimpleName(), destinationClassName);

			destinationObject = convertObject(sourceObject, entityConvertor);
		}
		
		return destinationObject;
	}

	private Object convertObject(Object sourceObject, EntityConvertorDO entityConvertor)
	{
		Object destinationObject = null;
		Class destinationClass = null;
		
		if(null != entityConvertor && null != sourceObject)
		{
			Class sourceClass = sourceObject.getClass();
			//log.info("Converting from ["+sourceObject.getClass().getSimpleName()+"] to ["+entityConvertor.getDestinationClass()+"]");
			
			/*
			 * load entity convertor
			 */
			
			if(null != entityConvertor)
			{
				// initialize the destination class
				try
				{
					destinationClass = Class.forName(entityConvertor.getDestinationClassName());
					destinationObject = destinationClass.newInstance();
				}
				catch(Exception e)
				{
					log.error("Cannot create class of type: "+entityConvertor.getDestinationClassName());
					e.printStackTrace();
				}
				
				if(null != destinationObject)
				{
					// copy attributes
					for(EntityConvertorMappingDO mapping : entityConvertor.getEntityConvertorMappingList())
					{
						if(null != mapping && null != mapping.getSourceField() && null != mapping.getDestinationField())
						{
							Method sourceMethod = null;
							Method destinationMethod = null;
							Object sourceAttributeValue = null;
							try
							{
								// call getter of source object
								sourceMethod = sourceClass.getMethod("get"+initCaps(mapping.getSourceField()), null);
								sourceAttributeValue = sourceMethod.invoke(sourceObject);
							}
							catch(Exception e)
							{
								log.error("error invoking [set"+initCaps(mapping.getSourceField())+"]. Does not exist");
								e.printStackTrace();
							}
							
							if("listTransformer".equals(mapping.getTransformationType()))
							{
								List destinationList = new ArrayList();
								if(sourceAttributeValue instanceof ArrayList)
								{
									EntityConvertorDO subConvertor = entityConvertorRepository.findOneByConversionName(mapping.getTransformationFunction());
									for(Object sourceListItem : (ArrayList)sourceAttributeValue)
									{										
										sourceListItem = convertObject(sourceListItem, subConvertor);
										destinationList.add(sourceListItem);
									}
								}
								sourceAttributeValue = destinationList;
							}
							
							try
							{
								// find setter of destination object
								Method[] destinationMethods = destinationClass.getMethods();
								for(Method method : destinationMethods)
								{
									if(method.getName().startsWith("set") && method.getName().contains(initCaps(mapping.getDestinationField())))
									{
										// call setter of destination object											
										method.invoke(destinationObject, sourceAttributeValue);
									}
								}
							}
							catch(Exception e)
							{
								log.error("Method [set"+initCaps(mapping.getSourceField())+"] does not exist in source object");
								e.printStackTrace();
							}
						}
					}
				}
			}
		}

		return destinationObject;
	}
	private void prepareEntity(BaseDO entity) {		
		
		// guid
		if(null == entity.getGuid())
		{
			entity.setGuid(GuidGenerator.generate());
		}
		
		// version id
		if(null == entity.getVersionId())
		{
			entity.setVersionId(1);
		}
		else
		{
			entity.setVersionId(entity.getVersionId() + 1);
		}
		
		// systemcreateddt & systemupdateddt
		if( entity instanceof TransactionDO)
		{
			TransactionDO transactionDO = (TransactionDO)entity;
			// guid
			if(null == transactionDO.getSystemCreatedDt())
			{
				transactionDO.setSystemCreatedDt(new Timestamp(System.currentTimeMillis()));
			}
			transactionDO.setSystemUpdatedDt(new Timestamp(System.currentTimeMillis()));
		}
		
		Method[] methods = entity.getClass().getMethods();
		
		for(Method method: methods)
		{
			if(method.getName().startsWith("get") && method.getName().endsWith("List"))
			{
				try
				{
					List<BaseDO> list= (List<BaseDO>)method.invoke(entity);
					if(null != list)
					{
						for(BaseDO childEntity : list)
						{
							if(null != childEntity)
							{
								childEntity.setParentObject(entity);
								prepareEntity(childEntity);
							}
						}
					}
				}
				catch(Exception e)
				{
					log.error("Error invoking method: ["+method.getName()+"] on object: "+entity.getObjectName());
				}
			}
		}
	}
	
	private String initCaps(String string)
	{
		return string.substring(0, 1).toUpperCase() + string.substring(1);
	}

}
