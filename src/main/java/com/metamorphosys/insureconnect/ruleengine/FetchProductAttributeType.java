package com.metamorphosys.insureconnect.ruleengine;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.metamorphosys.insureconnect.dataobjects.master.ProductAttributeDO;
import com.metamorphosys.insureconnect.dataobjects.master.ProductDO;
import com.metamorphosys.insureconnect.jpa.master.ProductRepository;

@Component("fetchProductAttributeType")
public class FetchProductAttributeType extends BaseRuleFunction implements RuleFunction{

	@Autowired
	private ProductRepository productRepository;
	
	public Object execute(Object[] args) 
	{
		List<ProductAttributeDO> productAttributeList = null;
		
		if(args.length == 2)
		{
			String productId = (String)args[0];
			String attributeTypeCd = (String)args[1];
			
			ProductDO productDO = productRepository.findByProductId(productId);
			productAttributeList = productDO.findProductAttributeTypeList(attributeTypeCd);
		}
		
		return productAttributeList;
	}

}
