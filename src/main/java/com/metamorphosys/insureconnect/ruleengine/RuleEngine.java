package com.metamorphosys.insureconnect.ruleengine;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.mvel2.MVEL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.metamorphosys.insureconnect.common.interfaceobjects.RuleIO;
import com.metamorphosys.insureconnect.common.interfaceobjects.WorkflowIO;
import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;
import com.metamorphosys.insureconnect.dataobjects.common.ExceptionDO;
import com.metamorphosys.insureconnect.dataobjects.common.ResultDO;
import com.metamorphosys.insureconnect.dataobjects.master.PreferenceAdditionalFieldDO;
import com.metamorphosys.insureconnect.dataobjects.master.PreferenceDO;
import com.metamorphosys.insureconnect.dataobjects.master.ProductAttributeDO;
import com.metamorphosys.insureconnect.dataobjects.master.ProductDO;
import com.metamorphosys.insureconnect.dataobjects.master.RuleActionDO;
import com.metamorphosys.insureconnect.dataobjects.master.RuleConditionDO;
import com.metamorphosys.insureconnect.dataobjects.master.RuleDO;
import com.metamorphosys.insureconnect.dataobjects.master.RuleDataDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.ServiceDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.ServiceTaskDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.WorkflowServiceDO;
import com.metamorphosys.insureconnect.jpa.master.RuleRepository;

@Component
public class RuleEngine {

	private static final Logger log = LoggerFactory.getLogger(RuleEngine.class);

	@Autowired
	private RuleRepository ruleRepository;

	@Autowired
	private ApplicationContext applicationContext;
	
	private HashMap<String, String> preferencesMap = new HashMap<String, String>();
	
	public RuleRepository getRuleRepository()
	{
		return ruleRepository;
	}
		
	public RuleIO execute(RuleIO ruleIO)
	{
		if(null != ruleIO)
		{

			/*
			 *  populate the rule list
			 */
			ArrayList<RuleDO> ruleList = new ArrayList<RuleDO>();
			
			if(null != ruleIO.getServiceDO())
			{
				ServiceDO serviceDO = (ServiceDO)ruleIO.getServiceDO();
				for(ServiceTaskDO serviceTask: serviceDO.getServiceTaskList())
				{
					List tempList = ruleRepository
										.findByServiceAndServiceTypeAndServiceTaskAndFromtoDt(
											serviceDO.getService(), 
											serviceDO.getServiceType(), 
											serviceTask.getServiceTask(),
											serviceDO.getServiceDt()
										);
					
					ruleList.addAll(tempList);
				}
				ruleIO.setRuleDOList(ruleList);
			}

			/*
			 * Load preferences 
			 */
			loadPreferences(ruleIO.getPreferenceDO());
			
			
			/* 
			 *	form transaction hash map
			 */
			HashMap<String, Object> transactionHashMap = populateTransactionHashMap(ruleIO.getTransactionDOList());
			
			/*
			 * 	Evaluate all rules
			 */
			Object actionResult = null;
			String actionKey = null;
			Object conditionFinalResult = null;
			String conditionKey = null;
			
			HashMap<String, Object> outputMap = new HashMap<String, Object>();
			List<ResultDO> resultDOList = new ArrayList<ResultDO>();
			ResultDO ruleResult = new ResultDO();

			/*
			 * 	Using this construct of the "for loop" because the rule engine may inject new rules in the rule list during processing
			 *	When this happens, the loop should be able to pick up and process the new rule too!!!
			 */			
			for(int index = 0; index < ruleIO.getRuleDOList().size(); index++)
			{
				RuleDO ruleDO = ruleIO.getRuleDOList().get(index);
				/*
				 * 	Evaluate all conditions
				 */
				
				List<RuleDataDO> selectedData = new ArrayList<RuleDataDO>();

				if(null != ruleDO.getRuleConditionList() && ruleDO.getRuleConditionList().size() > 0)
				{
					for(RuleDataDO ruleDataDO: ruleDO.getRuleDataList())
					{
						HashMap<String, Object> conditionHashMap = new HashMap<String, Object>();
						for (RuleConditionDO ruleConditionDO : ruleDO.getRuleConditionList()) 
						{
							conditionKey = ruleConditionDO.getCondition();
							Object conditionResult = evaluateCondition(transactionHashMap, conditionHashMap, ruleConditionDO, ruleDataDO);//true or false in condition
							conditionHashMap.put(conditionKey, conditionResult);
							
							if(null != conditionResult)
							{
								conditionFinalResult = conditionResult;
								if(conditionResult.equals(Boolean.FALSE))
								{
									break;
								}
							}
						}
						
						if(null != conditionFinalResult && conditionFinalResult.equals(Boolean.TRUE))
						{
							selectedData.add(ruleDataDO);
							
							// [SINGLE / LIST]
							if(!"LIST".equalsIgnoreCase(ruleDO.fetchAdditionalField("RETURN_AMOUNT")))
							{
								break;
							}
						}
					}
				}
				else // if no conditions are defined, select all data for action processing
				{
					selectedData = ruleDO.getRuleDataList();
				}
				
				
				/*
				 * 	Evaluate all actions on all shortlisted data here
				 */
				for(RuleDataDO selectedRuleDataDO: selectedData)
				{
					HashMap<String, Object> actionHashMap = new HashMap<String, Object>();
					
					// output elements are persisted for the entire rule session in the current action hash map
					actionHashMap.putAll(outputMap);
					
					for (RuleActionDO ruleActionDO : ruleDO.getRuleActionList()) 
					{
						actionKey = ruleActionDO.getAction();
						actionResult = evaluateAction(transactionHashMap, actionHashMap, ruleActionDO, selectedRuleDataDO);
						actionHashMap.put(actionKey, actionResult);
						if("IO".equalsIgnoreCase(ruleActionDO.getActionIoType()) || "O".equalsIgnoreCase(ruleActionDO.getActionIoType()))
						{
							if(ruleDO.getParentRuleName()!=null){
								outputMap.put("$"+ruleDO.getParentRuleName(), actionResult);
							}
							outputMap.put(actionKey, actionResult);
						}
						
						if("rule_flow".equals(ruleActionDO.getActionType()) && null != actionResult)
						{
							List additionalRuleList = (List)actionResult;
							if(additionalRuleList.size()>0){
								for(int count=0;count<additionalRuleList.size();count++){
									((RuleDO) additionalRuleList.get(count)).setParentRuleName(ruleDO.getRuleName());
									ruleIO.getRuleDOList().add(index+1+count, (RuleDO) additionalRuleList.get(count));
								}
							}
						}
						else if("work_flow".equals(ruleActionDO.getActionType()) && null != actionResult)
						{
							WorkflowIO workflowIO = (WorkflowIO)actionResult;
							outputMap.put(((WorkflowServiceDO)workflowIO.getServiceDO()).getService(), workflowIO.getBaseDO());
							outputMap.put(workflowIO.getServiceDO().getClass().getSimpleName(), workflowIO.getServiceDO());
							HashMap<String, Object> newHashMap = populateTransactionHashMap(Arrays.asList(workflowIO.getBaseDO(), workflowIO.getServiceDO()));
							transactionHashMap.putAll(newHashMap);
							if(null != workflowIO.getNextTaskList())
							{
								ruleIO.getRuleDOList().addAll(workflowIO.getNextTaskList());
							}
						}
						else if("exception".equals(ruleActionDO.getActionType()) && null != actionResult)
						{
							ruleResult.getExceptionList().add((ExceptionDO)actionResult);
							outputMap.put("exceptionList", ruleResult.getExceptionList());
						}
						else if("uw_rule".equals(ruleActionDO.getActionType()) && null != actionResult)
						{
							actionHashMap.put("uw_result", actionResult);
							actionHashMap.put("rule_name", actionKey);
							ResultDO uwResult = new ResultDO();
							uwResult.setResultMap(actionHashMap);
							resultDOList.add(uwResult);
						}
						else if("uw_result_rollup".equals(ruleActionDO.getActionType()) && null != actionResult)
						{
							actionHashMap.put(actionKey, resultDOList);
						}else if("rider_rule".equals(ruleActionDO.getActionType()) && null != actionResult)
						{
							actionHashMap.put("rider_result", actionHashMap.get(actionResult));
							ResultDO riderResult = new ResultDO();
							riderResult.setResultMap(actionHashMap);
							resultDOList.add(riderResult);
						}else if("rider_result_rollup".equals(ruleActionDO.getActionType()) && null != actionResult)
						{
							actionHashMap.put(actionKey, resultDOList);
						}
							
					}
					
					//ruleResult.getResultMap().put(actionKey, actionResult);
				}
			}

			ruleResult.setResultMap(outputMap);
			resultDOList.add(ruleResult);
			ruleIO.setResultDOList(resultDOList);
			
			writeResults(ruleIO.getTransactionDOList(), outputMap);
			
			/*
			 * 	Return the rule IO
			 */
			return ruleIO;
		}
		else
		{
			return null;
		}
	}

	private void loadPreferences(BaseDO baseDO) 
	{
		preferencesMap.clear();
		// Load defaults
		preferencesMap.put("languageCd", "EN");
		
		if(null != baseDO)
		{
			PreferenceDO preferenceDO = (PreferenceDO)baseDO;
			if(null != preferenceDO.getPreferenceAdditionalFieldList())
			{
				for (PreferenceAdditionalFieldDO pafDO : preferenceDO.getPreferenceAdditionalFieldList()) 
				{
					if(null != pafDO && null != pafDO.getFieldKey() && null != pafDO.getFieldValue())
					{
						preferencesMap.put(pafDO.getFieldKey(), pafDO.getFieldValue());
					}
				}
			}
		}
	}

	private Object evaluateCondition(HashMap<String, Object> transactionHashMap, HashMap<String, Object> conditionHashMap, RuleConditionDO ruleConditionDO, RuleDataDO ruleDataDO) 
	{
		Object returnObject = null;
		
		if("data_variable".equals(ruleConditionDO.getConditionType()))
		{
			Object lhs = transactionHashMap.get(ruleConditionDO.getCondition());
			Object rhs = ruleDataDO.getCondition(ruleConditionDO.getConditionOrder());
			if(null != rhs && "NULL".equals(rhs))
			{
				returnObject = new Boolean(null == lhs);
			}
			else if(null != rhs && "EMPTY".equals(rhs))
			{
				returnObject = new Boolean(lhs.equals(""));
			}
			else if(null != rhs && ("NULLEMPTY".equals(rhs) || "EMPTYNULL".equals(rhs)))
			{
				returnObject = new Boolean(null == lhs || lhs.equals(""));
			}
			else
			{
				if(null != lhs && null != rhs)
				{
					returnObject = new Boolean(lhs.toString().equals(rhs.toString()));
				}
			}
		}
		else if("data_variable_from".equals(ruleConditionDO.getConditionType()))
		{
			Object lhs = transactionHashMap.get(ruleConditionDO.getCondition());
			Object rhs = ruleDataDO.getCondition(ruleConditionDO.getConditionOrder());
			
			if( lhs!= null && rhs != null)
			{
				returnObject = (Double.valueOf(lhs.toString()).compareTo(Double.valueOf(rhs.toString())) >= 0 ? Boolean.TRUE : Boolean.FALSE);
			}
		}
		else if("data_variable_to".equals(ruleConditionDO.getConditionType()))
		{
			Object lhs = transactionHashMap.get(ruleConditionDO.getCondition());
			Object rhs = ruleDataDO.getCondition(ruleConditionDO.getConditionOrder());
			
			if( lhs!= null && rhs != null)
			{
				returnObject = (Double.valueOf(lhs.toString()).compareTo(Double.valueOf(rhs.toString())) <= 0 ? Boolean.TRUE : Boolean.FALSE);
			}
		}
		if("calc_variable".equals(ruleConditionDO.getConditionType()))
		{
			Object lhs = conditionHashMap.get(ruleConditionDO.getCondition());
			Object rhs = ruleDataDO.getCondition(ruleConditionDO.getConditionOrder());
			if(null != rhs && "NULL".equals(rhs))
			{
				returnObject = new Boolean(null == lhs);
			}
			else if(null != rhs && "EMPTY".equals(rhs))
			{
				returnObject = new Boolean(lhs.equals(""));
			}
			else if(null != rhs && ("NULLEMPTY".equals(rhs) || "EMPTYNULL".equals(rhs)))
			{
				returnObject = new Boolean(null == lhs || lhs.equals(""));
			}
			else
			{
				returnObject = new Boolean(lhs.equals(rhs));
			}
		}
		else if("calc_variable_from".equals(ruleConditionDO.getConditionType()))
		{
			Object lhs = conditionHashMap.get(ruleConditionDO.getCondition());
			Object rhs = ruleDataDO.getCondition(ruleConditionDO.getConditionOrder());
			
			if( lhs!= null && rhs != null)
			{
				returnObject = (Double.valueOf(lhs.toString()).compareTo(Double.valueOf(rhs.toString())) >= 0 ? Boolean.TRUE : Boolean.FALSE);
			}
		}
		else if("calc_variable_to".equals(ruleConditionDO.getConditionType()))
		{
			Object lhs = conditionHashMap.get(ruleConditionDO.getCondition());
			Object rhs = ruleDataDO.getCondition(ruleConditionDO.getConditionOrder());
			
			if( lhs!= null && rhs != null)
			{
				returnObject = (Double.valueOf(lhs.toString()).compareTo(Double.valueOf(rhs.toString())) <= 0 ? Boolean.TRUE : Boolean.FALSE);
			}
		}
		else if("constant".equals(ruleConditionDO.getConditionType()))
		{
			//TODO: write implementation
			
		}
		else if("function".equals(ruleConditionDO.getConditionType()))
		{
			//TODO: write implementation
			
			/*
			String functionCall = ruleDataDO.getCondition(ruleConditionDO.getConditionOrder());
			
			String functionName = functionCall;
			String functionParameterString = "";
			if(functionCall.indexOf("(")>-1)
			{
				functionName = functionCall.substring(0, functionCall.indexOf("("));
				functionParameterString = functionCall.substring(functionCall.indexOf("(")+1, functionCall.indexOf(")"));
			}
			String functionParameterNames[] = functionParameterString.split(",");
			
			try
			{
				Object appContextObject = applicationContext.getBean(functionName); 
				if(null != appContextObject)
				{
					RuleFunction ruleFunction = (RuleFunction)appContextObject;
					
					Object[] functionParameters = new Object[functionParameterNames.length];
					for (int i = 0; i < functionParameters.length; i++) 
					{
						functionParameters[i] = conditionHashMap.get(functionParameterNames[i].trim());
					}
					returnObject = ruleFunction.execute(functionParameters);
				}
			}
			catch(Exception e)
			{
				log.error("Error invoking function ["+functionCall+"]: "+e.getMessage());
				e.printStackTrace();
			}
			*/
		}
		
		return returnObject;
	}

	private Object evaluateAction(HashMap<String, Object> transactionHashMap, HashMap<String, Object> actionHashMap, RuleActionDO ruleActionDO, RuleDataDO ruleDataDO) 
	{
		Object returnObject = null;
		HashMap<String, Object> mergedHasmap = null;
		if("data_variable".equals(ruleActionDO.getActionType()))
		{
			returnObject = transactionHashMap.get(ruleDataDO.getAction(ruleActionDO.getActionOrder()));
		}
		else if("calc_variable".equals(ruleActionDO.getActionType()))
		{
			returnObject = actionHashMap.get(ruleDataDO.getAction(ruleActionDO.getActionOrder()));
		}
		else if("constant".equals(ruleActionDO.getActionType()))
		{
			returnObject = ruleDataDO.getAction(ruleActionDO.getActionOrder());
			
			try
			{
				if(null != returnObject)
				{
					returnObject = Double.parseDouble(returnObject.toString());
				}
			}catch(Exception e)
			{
				//log.info("Returning String. Tried converting constant to Double. Failed for "+returnObject);
			}
		}
		else if("rule_flow".equals(ruleActionDO.getActionType()))
		{
			Object rule = actionHashMap.get(ruleDataDO.getAction(ruleActionDO.getActionOrder()));
			
			if(rule instanceof ProductAttributeDO)
			{
				ProductAttributeDO productAttribute = (ProductAttributeDO)rule;
				ProductDO product = (ProductDO)productAttribute.getParentObject();
				returnObject = ruleRepository.findByServiceAndServiceTypeAndServiceTaskOrderBySortOrderAsc("Product", productAttribute.getAttributeTypeCd(), product.getProductId()+productAttribute.getAttributeId());
			}
			else if(rule instanceof List)
			{
				List list = (List)rule;
				List<RuleDO> ruleList = new ArrayList<RuleDO>();
				for(Object listItem: list)
				{
					if(listItem instanceof ProductAttributeDO)
					{
						ProductAttributeDO productAttribute = (ProductAttributeDO)listItem;
						ProductDO product = (ProductDO)productAttribute.getParentObject();
						List<RuleDO> intermediateRuleList = ruleRepository.findByServiceAndServiceTypeAndServiceTaskOrderBySortOrderAsc("Product", productAttribute.getAttributeTypeCd(), product.getProductId()+productAttribute.getAttributeId());
						ruleList.addAll(intermediateRuleList);
					}
					else if(listItem instanceof RuleDO)
					{
						RuleDO ruleDO = (RuleDO)listItem;
						ruleList.add(ruleDO);
					}
				}
				returnObject = ruleList;
			}
		}
		else if("work_flow".equals(ruleActionDO.getActionType()))
		{
			returnObject = actionHashMap.get(ruleDataDO.getAction(ruleActionDO.getActionOrder()));
		}
		else if("exception".equals(ruleActionDO.getActionType()))
		{
			Object exception = actionHashMap.get(ruleDataDO.getAction(ruleActionDO.getActionOrder()));
			
			if(exception instanceof ExceptionDO)
			{
				returnObject = exception;
			}
		}
		else if("expression".equals(ruleActionDO.getActionType()))
		{
			String expression = ruleActionDO.getAction()+" = ";
			expression += ruleDataDO.getAction(ruleActionDO.getActionOrder());
			mergedHasmap.putAll(transactionHashMap);
			mergedHasmap.putAll(actionHashMap);
//			returnObject = MVEL.eval(expression, actionHashMap);
			returnObject = MVEL.eval(expression, mergedHasmap);
		}
		else if("function".equals(ruleActionDO.getActionType()))
		{
			String functionCall = ruleDataDO.getAction(ruleActionDO.getActionOrder());
			
			String functionName = functionCall;
			String functionParameterString = "";
			if(functionCall.indexOf("(")>-1)
			{
				functionName = functionCall.substring(0, functionCall.indexOf("("));
				
				functionParameterString = functionCall.substring(functionCall.indexOf("(")+1, functionCall.indexOf(")"));
			}
			if(functionName.equals("fetchRuleByName") || functionName.equals("fetchRules")){
				actionHashMap.put("$ruleEffectiveDate", transactionHashMap.get("PolicyDO-RuleEffectiveDate"));
				functionParameterString = functionParameterString + ","+"$ruleEffectiveDate";
			}
			String functionParameterNames[] = functionParameterString.split(",");

			try
			{
				Object appContextObject = applicationContext.getBean(functionName); 
				if(null != appContextObject)
				{
					RuleFunction ruleFunction = (RuleFunction)appContextObject;
					
					Object[] functionParameters = new Object[functionParameterNames.length];
					for (int i = 0; i < functionParameters.length; i++) 
					{
						functionParameters[i] = actionHashMap.get(functionParameterNames[i].trim());
					}
					ruleFunction.setPreferences(preferencesMap);
					returnObject = ruleFunction.execute(functionParameters);
				}
			}
			catch(Exception e)
			{
				log.error("Error invoking function ["+functionCall+"]: "+e.getMessage());
				e.printStackTrace();
			}
			
		}
		else if("uw_rule".equals(ruleActionDO.getActionType()))
		{
			returnObject = ruleDataDO.getAction(ruleActionDO.getActionOrder());
		}
		else if("uw_result_rollup".equals(ruleActionDO.getActionType()))
		{
			returnObject = ruleDataDO.getAction(ruleActionDO.getActionOrder());
		}
		else if("rider_rule".equals(ruleActionDO.getActionType()))
		{
			returnObject = ruleDataDO.getAction(ruleActionDO.getActionOrder());
		}else if("rider_result_rollup".equals(ruleActionDO.getActionType()))
		{
			returnObject = ruleDataDO.getAction(ruleActionDO.getActionOrder());
		}
		
		return returnObject;
	}

	private HashMap<String, Object> populateTransactionHashMap(List<BaseDO> list) 
	{
		HashMap<String, Object> transactionHashMap = new HashMap<String, Object>();
		try{
		// use a counter for list of transaction DOs ??
			//int index = 0;
			
			// iterate transaction DO list
			for (BaseDO transactionDO : list) 
			{
				String objectName = transactionDO.getObjectName();
				transactionHashMap.put(objectName, transactionDO);
				
				Method[] transactionDOMethods = transactionDO.getClass().getMethods();
				for(Method method: transactionDOMethods)
				{
					if(method.getName().startsWith("get"))
					{	
						String attributeName = method.getName().replaceFirst("get", "");
						
						String key = objectName + ("FieldValue".equalsIgnoreCase(attributeName)? "": "-" + attributeName);
						Object value = null; 
						try 
						{	
							value = method.invoke(transactionDO, null);
							
						} catch (IllegalAccessException e) {
							log.error("IllegalAccessException: "+e);
							e.printStackTrace();
						} catch (IllegalArgumentException e) {
							log.error("IllegalArgumentException: "+e);
							e.printStackTrace();
						} catch (InvocationTargetException e) {
							log.error("InvocationTargetException: "+e);
							
							e.printStackTrace();
						}
						
						if(value instanceof List) // handle list
						{
							HashMap<String, Object> childHashMap = populateTransactionHashMap((List)value);
							if(null != childHashMap)
							{
								Set<String> childHashMapKeys = childHashMap.keySet();
								for (String childHashMapKey : childHashMapKeys) 
								{
									key = objectName + "-" + childHashMapKey;
									transactionHashMap.put(key, childHashMap.get(childHashMapKey));
								}
							}
						}
						else // handle single value
						{
							transactionHashMap.put(key, value);
						}
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
			
		return transactionHashMap;
	}
	
	private void writeResults(List<BaseDO> list, HashMap<String, Object> outputHashMap)
	{
		writeResults(list, outputHashMap, "");
	}
	
	private void writeResults(List<BaseDO> list, HashMap<String, Object> outputHashMap, String prefix) 
	{
	//	HashMap<String, Object> transactionHashMap = new HashMap<String, Object>();
		
		// use a counter for list of transaction DOs ??
	//	int index = 0;
		
		// iterate transaction DO list
		for (BaseDO transactionDO : list) 
		{
			String objectName = transactionDO.getObjectName();
			
			Method[] transactionDOMethods = transactionDO.getClass().getMethods();
			for(Method method: transactionDOMethods)
			{
				if(method.getName().startsWith("set"))
				{	
					String attributeName = method.getName().replaceFirst("set", "");
					
					String key = prefix + objectName + "-" + attributeName;

					Object value = outputHashMap.get(key);
					
					if(null != value)
					{
						try
						{
							// call setter
							method.invoke(transactionDO, value);
						} catch (IllegalAccessException e) {
							e.printStackTrace();
						} catch (IllegalArgumentException e) {
							e.printStackTrace();
						} catch (InvocationTargetException e) {
							e.printStackTrace();
						}
					}
					
					if(null != method.getParameterTypes() && method.getParameterTypes().length > 0 && null != method.getParameterTypes()[0])
					{
						if("java.util.List".equals(method.getParameterTypes()[0].getName()))
						{
							String getter = "get"+attributeName;
							try 
							{
								Method getterMethod = transactionDO.getClass().getMethod(getter);
								if(null != getterMethod)
								{
									value = getterMethod.invoke(transactionDO, null);
									if(null != value && value instanceof List)
									{
										writeResults((List)value, outputHashMap, prefix + objectName + "-");
									}
								}
							} catch (NoSuchMethodException e) {
								e.printStackTrace();
							} catch (IllegalAccessException e) {
								e.printStackTrace();
							} catch (IllegalArgumentException e) {
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								e.printStackTrace();
							}catch (SecurityException e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
		}
	}

}
