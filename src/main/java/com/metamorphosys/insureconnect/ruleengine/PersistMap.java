package com.metamorphosys.insureconnect.ruleengine;

import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;
import com.metamorphosys.insureconnect.dataobjects.common.ExceptionDO;
import com.metamorphosys.insureconnect.dataobjects.common.ResultDO;
import com.metamorphosys.insureconnect.dataobjects.common.TransactionDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.HashMapDataDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.HashMapEntityDO;
import com.metamorphosys.insureconnect.jpa.transaction.HashMapEntityRepository;
import com.metamorphosys.insureconnect.utilities.GuidGenerator;

@Component("persistMap")
public class PersistMap extends BaseRuleFunction implements RuleFunction{

	private static final Logger log = LoggerFactory.getLogger(PersistMap.class);
	
	@Autowired
	private HashMapEntityRepository hashMapEntityRepository;

	public Object execute(Object[] args) 
	{
		BaseDO entity = null;
		if(args.length == 3)
		{
			String entityName = (String)args[0];
			String entityGuid = (String)args[1];
			
			List maps = null;
			if(args[2] instanceof List)
			{
				maps = (List)args[2];
			}
			else
			{
				log.error("args[2] of instance type is not supported: "+args[2]);
				maps = new ArrayList();
			}

			if(null != maps)
			{
				for(Object object : maps)
				{
					HashMapEntityDO hashMapEntity = new HashMapEntityDO();
					hashMapEntity.setEntityName(entityName);
					hashMapEntity.setEntityGuid(entityGuid);
					hashMapEntity.setGuid(GuidGenerator.generate());
					hashMapEntity.setVersionId(1);
					hashMapEntity.setSystemCreatedDt(new Timestamp(System.currentTimeMillis()));
					hashMapEntity.setSystemUpdatedDt(new Timestamp(System.currentTimeMillis()));
					
					List<HashMapDataDO> dataList = new ArrayList<HashMapDataDO>();
					
					if(null != object && object instanceof ResultDO)
					{
						HashMap map = ((ResultDO)object).getResultMap();
						Set<String> keySet = map.keySet();
						
						for(String key : keySet)
						{
							HashMapDataDO hashMapData = new HashMapDataDO();
							hashMapData.setParentObject(hashMapEntity);
							hashMapData.setGuid(GuidGenerator.generate());
							hashMapData.setFieldKey(key);
							hashMapData.setFieldValue(map.get(key).toString());
							dataList.add(hashMapData);
						}
					}
					else if(null != object && object instanceof ExceptionDO)
					{
						ExceptionDO exception = (ExceptionDO)object;
						
						HashMapDataDO exceptionCd = new HashMapDataDO();
						exceptionCd.setParentObject(hashMapEntity);
						exceptionCd.setGuid(GuidGenerator.generate());
						exceptionCd.setFieldKey("exceptionCd");
						exceptionCd.setFieldValue(exception.getExceptionCd());
						dataList.add(exceptionCd);

						HashMapDataDO exceptionMessage = new HashMapDataDO();
						exceptionMessage.setParentObject(hashMapEntity);
						exceptionMessage.setGuid(GuidGenerator.generate());
						exceptionMessage.setFieldKey("exceptionMessage");
						exceptionMessage.setFieldValue(exception.getExceptionMessage());
						dataList.add(exceptionMessage);

					}
					hashMapEntity.setHashMapDataList(dataList);
					
					hashMapEntityRepository.save(hashMapEntity);
				}
			}
		}
		
		return entity;
	}

	private void prepareEntity(BaseDO entity) {		
		
		// guid
		if(null == entity.getGuid())
		{
			entity.setGuid(GuidGenerator.generate());
		}
		
		// version id
		if(null == entity.getVersionId())
		{
			entity.setVersionId(1);
		}
		else
		{
			entity.setVersionId(entity.getVersionId() + 1);
		}
		
		// systemcreateddt & systemupdateddt
		if( entity instanceof TransactionDO)
		{
			TransactionDO transactionDO = (TransactionDO)entity;
			// guid
			if(null == transactionDO.getSystemCreatedDt())
			{
				transactionDO.setSystemCreatedDt(new Timestamp(System.currentTimeMillis()));
			}
			transactionDO.setSystemUpdatedDt(new Timestamp(System.currentTimeMillis()));
		}
		
		Method[] methods = entity.getClass().getMethods();
		
		for(Method method: methods)
		{
			if(method.getName().startsWith("get") && method.getName().endsWith("List"))
			{
				try
				{
					List<BaseDO> list= (List<BaseDO>)method.invoke(entity);
					if(null != list)
					{
						for(BaseDO childEntity : list)
						{
							if(null != childEntity)
							{
								childEntity.setParentObject(entity);
								prepareEntity(childEntity);
							}
						}
					}
				}
				catch(Exception e)
				{
					log.error("Error invoking method: ["+method.getName()+"] on object: "+entity.getObjectName());
				}
			}
		}
	}

}
