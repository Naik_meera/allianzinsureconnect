package com.metamorphosys.insureconnect.ruleengine;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.metamorphosys.insureconnect.dataobjects.common.ExceptionDO;
import com.metamorphosys.insureconnect.dataobjects.master.LabelDO;
import com.metamorphosys.insureconnect.jpa.master.LabelRepository;

@Component("raiseException")
public class RaiseException extends BaseRuleFunction implements RuleFunction{

	@Autowired
	private LabelRepository labelRepository;
	
	public Object execute(Object[] args) 
	{
		ExceptionDO exceptionDO = null;
		
		if(args.length >= 1)
		{
			String labelKey = (String)args[0];
			
			LabelDO labelDO = labelRepository.findByLabelKeyAndLanguage(labelKey, preferencesMap.get("languageCd"));
			
			if(null != labelDO)
			{
				String labelValue = labelDO.getLabelValue();
				
				for(int i = 1; i < args.length; i++)
				{
					if(null != args[i])
					{
						labelValue = labelValue.replace("{"+i+"}", args[i].toString());
					}
				}
				
				exceptionDO = new ExceptionDO();
				exceptionDO.setExceptionCd(labelKey);
				exceptionDO.setExceptionMessage(labelValue);
			}
		}
		
		return exceptionDO;
	}

}
