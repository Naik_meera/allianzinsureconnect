package com.metamorphosys.insureconnect.ruleengine;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;

import org.springframework.stereotype.Component;

@Component("calculateAge")
public class CalculateAge extends BaseRuleFunction implements RuleFunction{

	public Object execute(Object[] args) 
	{
		Integer age = null;
		
		if(args.length == 2)
		{
			Timestamp currentDate = (Timestamp)args[1];
			Timestamp birthDate = (Timestamp)args[0];
			
			Period p = Period.between(birthDate.toLocalDateTime().toLocalDate(), currentDate.toLocalDateTime().toLocalDate());
			age = p.getYears();
		}
		
		return age;
	}

}
