package com.metamorphosys.insureconnect.ruleengine;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.metamorphosys.insureconnect.dataobjects.master.RuleDO;
import com.metamorphosys.insureconnect.jpa.master.RuleRepository;

@Component("fetchRuleByName")
public class FetchRuleByName extends BaseRuleFunction implements RuleFunction{
	
	@Autowired
	RuleRepository ruleRepository;
	
	public Object execute(Object[] args) 
	{
		List<RuleDO> ruleList = null;
		
		if(args.length == 2)
		//if(args.length == 1)
		{
			String ruleName = (String)args[0];
			Date date = (Date)args[1];
			
			ruleList = ruleRepository.findByRuleNameAndDate(ruleName,date);
			//ruleList = ruleRepository.findByRuleName(ruleName);
		}
		
		return ruleList;
	}


}
