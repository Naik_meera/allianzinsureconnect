package com.metamorphosys.insureconnect.controllers;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metamorphosys.insureconnect.common.interfaceobjects.BaseIO;
import com.metamorphosys.insureconnect.common.interfaceobjects.ResponseDataIO;
import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;
import com.metamorphosys.insureconnect.exception.WebBadRequestException;
import com.metamorphosys.insureconnect.utilities.SerializationUtility;

@RestController
@RequestMapping("/save")
public class WebSaveController extends WebCrudController{

	private static final Logger log = LoggerFactory.getLogger(WebSaveController.class);

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity save() {

		log.info("Web Crud Controller [Get] :: Save");

		String response = "SUCCESS";
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		

	    return new ResponseEntity(response, httpHeaders, httpStatus);
	}

	@RequestMapping(method = RequestMethod.POST, consumes= "application/json")
	public ResponseEntity saveJson(@RequestBody String json) {

		log.info("saveJson");
				
	    return execute(json);
	}
	
	public String process(String data) throws Exception
	{
		BaseIO crudIO = new BaseIO();
		ArrayList<ResponseDataIO> list = new ArrayList<ResponseDataIO>();
		
		if(null != data)
		{
			crudIO = convertToIO(data);	
			
			if(null != crudIO && null != crudIO.getBaseDOList())
			{
				for(BaseDO baseDO: crudIO.getBaseDOList())
				{
					//log.info(baseDO.getObjectName());
					//log.info(baseDO.toString());
					CrudRepository repository = (CrudRepository)getObjectMapParameter(baseDO.getClass().getSimpleName(), ObjectMapKeys.REPOSITORY);
					repository.save(baseDO);
					
					if(null != baseDO)
					{
						ResponseDataIO dataIO = new ResponseDataIO();
						dataIO.setObject(SerializationUtility.getInstance().toJson(baseDO));
						dataIO.setObjectName(baseDO.getClass().getSimpleName());
						list.add(dataIO);
					}

				}
			}
			else
			{
				throw new WebBadRequestException("Bad request");
			}
		}
		else
		{
			throw new WebBadRequestException("Bad request");
		}

		return SerializationUtility.getInstance().toJson(list);
	}
}
