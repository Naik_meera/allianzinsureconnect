package com.metamorphosys.insureconnect.controllers;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.internal.LinkedTreeMap;
import com.metamorphosys.insureconnect.controllers.WebCrudController.ObjectMapKeys;
import com.metamorphosys.insureconnect.dataobjects.master.MakeDO;
import com.metamorphosys.insureconnect.dataobjects.master.MakeModelDO;
import com.metamorphosys.insureconnect.dataobjects.master.ModelVariantDO;
import com.metamorphosys.insureconnect.dataobjects.master.SiRegtYearDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.ActivityDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.AgentDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.AgentDeviceMappingDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.LeadDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.QuotationContainerDO;
import com.metamorphosys.insureconnect.jpa.master.MakeModelRepository;
import com.metamorphosys.insureconnect.jpa.master.MakeRepository;
import com.metamorphosys.insureconnect.jpa.master.ModelVariantRepository;
import com.metamorphosys.insureconnect.jpa.master.SiRegtYearRepository;
import com.metamorphosys.insureconnect.jpa.transaction.ActivityRepository;
import com.metamorphosys.insureconnect.jpa.transaction.AgentRepository;
import com.metamorphosys.insureconnect.jpa.transaction.LeadRepository;
import com.metamorphosys.insureconnect.jpa.transaction.QuotationContainerRepository;
import com.metamorphosys.insureconnect.utilities.InsureConnectConstants;
import com.metamorphosys.insureconnect.utilities.SerializationUtility;

@RestController
@CrossOrigin
@RequestMapping("/activity")
public class ActivityController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ActivityController.class);
	
	@Autowired
	ActivityRepository activityRepository;

	@Autowired
	WebCrudController webCrudController;

	@Autowired
	QuotationContainerRepository quotationContainerRepository;
	
	@Autowired
	AgentRepository agentRepository;
	
	@Autowired
	MakeRepository makeRepository;
	
	@Autowired
	MakeModelRepository makeModelRepository;
	
	@Autowired
	ModelVariantRepository modelVariantRepository;
	
	@Autowired
	SiRegtYearRepository siRegtYearRepository;

	@RequestMapping(method = RequestMethod.POST, value = "/upload")
	ResponseEntity uploadActivity(@RequestBody @Valid String json) {
		LOGGER.info("Uploading data");
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		// httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		try {
			ActivityDO activityDO = new ActivityDO();
			activityDO.setActivityValue(json);
			activityDO.setActivityStatus(InsureConnectConstants.Status.PENDING);
			activityRepository.save(activityDO);
			processActivity(activityDO);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}
		LOGGER.info(""+new ResponseEntity(httpStatus));
		return new ResponseEntity(httpStatus);

	}

	@Async
	private void processActivity(ActivityDO activityDO) {
		try {
			LinkedTreeMap<String, ArrayList> map = SerializationUtility.getInstance().fromJson(activityDO.getActivityValue());
			checkUploadData(map);
			activityDO.setActivityStatus(InsureConnectConstants.Status.SUCCESS);
			activityRepository.save(activityDO);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void checkUploadData(LinkedTreeMap map) {
		if (map.containsKey(InsureConnectConstants.QUOTATIONCONTAINER)) {
			ArrayList<String> requestDataList = (ArrayList<String>) map.get(InsureConnectConstants.QUOTATIONCONTAINER);
			for (String requestDataIO : requestDataList) {
				QuotationContainerDO quotationContainerDO = (QuotationContainerDO) SerializationUtility.getInstance()
						.fromJson(requestDataIO.toString(), QuotationContainerDO.class);

				quotationContainerDO.setOnlineUpdatedDt(new Timestamp(System.currentTimeMillis()));
				quotationContainerRepository.save(quotationContainerDO);
			}
		} 
		if (map.containsKey(InsureConnectConstants.LEAD)) {
			ArrayList<String> requestDataList = (ArrayList<String>) map.get(InsureConnectConstants.LEAD);
			for (String requestDataIO : requestDataList) {
				LeadDO leadDO = (LeadDO) SerializationUtility.getInstance().fromJson(requestDataIO.toString(),
						LeadDO.class);
				CrudRepository repository = (CrudRepository) webCrudController
						.getObjectMapParameter(leadDO.getObjectName(), ObjectMapKeys.REPOSITORY);
				leadDO.setOnlineUpdatedDt(new Timestamp(System.currentTimeMillis()));
				repository.save(leadDO);
			}
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/download")
	ResponseEntity downloadActivity(@RequestParam("entityList") String json,
			@RequestParam("lastDownloadDt") String lastSyncDt, HttpServletRequest request) {
		LOGGER.info("Downloading data");
		String responseJson = null;
		AgentDO agent =agentRepository.findByAgentId(request.getHeader("agentId"));
		Long deviceSeq=Long.parseLong(request.getHeader("agentDeviceSeq"));
		
		AgentDeviceMappingDO currentDevice = null;
		String lastSyncDtFromOnline = null;
		
		for(AgentDeviceMappingDO agentDeviceMappingDO:agent.getAgentDeviceMappingDOList()){
			if(deviceSeq.longValue()==agentDeviceMappingDO.getId().longValue()){
//				currentDevice = agentDeviceMappingDO;
				if(Long.parseLong(lastSyncDt) == 0){
					lastSyncDtFromOnline = String.valueOf(0);
				}else{
//					lastSyncDtFromOnline =String.valueOf(currentDevice.getLastDownloadDt().getTime()) ;
//					if(lastSyncDtFromOnline == null){
//						if(lastSyncDt != null || lastSyncDt.equals("")){
//							lastSyncDtFromOnline = lastSyncDt;
//						}else{
//							lastSyncDtFromOnline = String.valueOf(0);
//						}
//						
//					}
					lastSyncDtFromOnline =String.valueOf(agentDeviceMappingDO.getLastDownloadDt().getTime()) ;
				}
				
				agentDeviceMappingDO.setLastDownloadDt(new Timestamp(System.currentTimeMillis()));
				break;
			}else{
				lastSyncDtFromOnline=String.valueOf(0);;
			}
		}
		agentRepository.save(agent);
		try {
			HashMap<String, ArrayList> hashMap = new HashMap<String, ArrayList>();
			List<String> objectNames = Arrays.asList(json.split(","));
			for (String objectName : objectNames) {
				if (InsureConnectConstants.QUOTATIONCONTAINER.equals(objectName)) {
					List<QuotationContainerDO> quotationContainerDOs = quotationContainerRepository
							.findBybaseAgentCdAndOnlineUpdatedDtAfter((String) request.getHeader("agentId"),
									new Timestamp(Long.parseLong(lastSyncDtFromOnline)));
					hashMap.put(objectName, (ArrayList) quotationContainerDOs);
				} else if (InsureConnectConstants.LEAD.equals(objectName)) {
					CrudRepository repository = (CrudRepository) webCrudController.getObjectMapParameter("LeadDO",
							ObjectMapKeys.REPOSITORY);
					List<LeadDO> leadDOs = ((LeadRepository) repository)
							.findBybaseAgentCdAndOnlineUpdatedDtAfter((String) request.getHeader("agentId"),new Timestamp(Long.parseLong(lastSyncDtFromOnline)));
					hashMap.put(objectName, (ArrayList) leadDOs);
				}
			}
//			currentDevice.setLastDownloadDt(new Timestamp(System.currentTimeMillis()));
//			agentRepository.save(agent);
			LOGGER.info(responseJson);
			responseJson = SerializationUtility.getInstance().toJson(hashMap);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity(responseJson, new HttpHeaders(), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/syncVehicles/{lastSyncDate}")
	ResponseEntity syncVehiclesActivity(@PathVariable String lastSyncDate, HttpServletRequest request) {
		LOGGER.info("Synchronising vehicles");
		List<MakeDO> makes=new ArrayList<MakeDO>();
		List<MakeModelDO> makemodels=new ArrayList<MakeModelDO>();
		List<ModelVariantDO> modelVariants=new ArrayList<ModelVariantDO>();
		List<SiRegtYearDO> siRegtYears=new ArrayList<SiRegtYearDO>();
		
		try {
			Date lastyDt=new Date(Long.parseLong(lastSyncDate));
			//System.out.println(lastyDt);
			Timestamp time = new Timestamp(lastyDt.getTime());
			// dateFormat.format(policyDO.getPolicyExpiryDt().getTime()
			makes = makeRepository.findByUpdatedDateGreaterThan(time);
			//PageRequest prequest=new PageRequest(1,100);
			makemodels = makeModelRepository.findByUpdatedDateGreaterThanAndStatusCd(time,MakeModelDO.ACTIVE);
			modelVariants = modelVariantRepository.findByUpdatedDateGreaterThanAndStatusCd(time,MakeModelDO.ACTIVE);
			siRegtYears = siRegtYearRepository.findByUpdatedDateGreaterThanAndStatusCd(time,SiRegtYearDO.ACTIVE);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		LOGGER.info("Total MAKES :"+makes.size()+"Total MAKEMODEL :"+makemodels.size()+"Total MODELVARIANT :"+modelVariants.size()+"Total SI_REGTYEAR :"+siRegtYears.size());
		if(makes!=null && makes.size()!=0) {
			hashMap.put("MAKE", SerializationUtility.getInstance().toJson(makes));
		}
		if(makemodels!=null && makemodels.size()!=0) {
			hashMap.put("MAKEMODEL", SerializationUtility.getInstance().toJson(makemodels));
		}
		if(modelVariants!=null && modelVariants.size()!=0) {
			hashMap.put("MODELVARIANT",SerializationUtility.getInstance().toJson(modelVariants));
		}
		if(siRegtYears!=null && siRegtYears.size()!=0) {
			hashMap.put("SI_REGTYEAR",SerializationUtility.getInstance().toJson(siRegtYears));
		}
		String jsonResult = SerializationUtility.getInstance().toJson(hashMap);
		return new ResponseEntity(jsonResult, HttpStatus.OK);
	}
	
}
