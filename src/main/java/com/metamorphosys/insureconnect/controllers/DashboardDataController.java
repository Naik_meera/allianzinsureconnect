package com.metamorphosys.insureconnect.controllers;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metamorphosys.insureconnect.dataobjects.master.AccidentHealthClub;
import com.metamorphosys.insureconnect.dataobjects.master.AutoClub;
import com.metamorphosys.insureconnect.dataobjects.master.CommercialClub;
import com.metamorphosys.insureconnect.dataobjects.master.GwpGraph;
import com.metamorphosys.insureconnect.dataobjects.master.IncomeChart;
import com.metamorphosys.insureconnect.dataobjects.master.LiabilitySurprise;
import com.metamorphosys.insureconnect.dataobjects.master.LoBSummary;
import com.metamorphosys.insureconnect.dataobjects.master.NewBusinessBonus;
import com.metamorphosys.insureconnect.dataobjects.master.OfficeAllowance;
import com.metamorphosys.insureconnect.dataobjects.master.PerformanceBonus;
import com.metamorphosys.insureconnect.dataobjects.master.RenewalBusinessBonus;
import com.metamorphosys.insureconnect.dataobjects.master.TravelClub;
import com.metamorphosys.insureconnect.jpa.master.AccidentHealthClubRepository;
import com.metamorphosys.insureconnect.jpa.master.AutoClubRepository;
import com.metamorphosys.insureconnect.jpa.master.CommercialClubRepository;
import com.metamorphosys.insureconnect.jpa.master.GwpGraphRepository;
import com.metamorphosys.insureconnect.jpa.master.IncomeChartRepository;
import com.metamorphosys.insureconnect.jpa.master.LiabilitySurpriseRepository;
import com.metamorphosys.insureconnect.jpa.master.LoBSummaryRepository;
import com.metamorphosys.insureconnect.jpa.master.NewBusinessBonusRepository;
import com.metamorphosys.insureconnect.jpa.master.OfficeAllowanceRepository;
import com.metamorphosys.insureconnect.jpa.master.PerformanceBonusRepository;
import com.metamorphosys.insureconnect.jpa.master.RenewalBusinessBonusRepository;
import com.metamorphosys.insureconnect.jpa.master.TravelClubRepository;
import com.metamorphosys.insureconnect.utilities.SerializationUtility;

@RestController
@CrossOrigin
@RequestMapping("/dashboard")
public class DashboardDataController {
	
	@Autowired
	GwpGraphRepository gWPGraphRepository;
	@Autowired
	IncomeChartRepository incomeChartRepository;
	@Autowired
	NewBusinessBonusRepository newBusinessBonusRepository;
	@Autowired
	PerformanceBonusRepository performanceBonusRepository;
	@Autowired
	OfficeAllowanceRepository officeAllowanceRepository;
	@Autowired
	RenewalBusinessBonusRepository renewalBusinessBonusRepository;
	@Autowired
	AutoClubRepository autoClubRepository;
	@Autowired
	CommercialClubRepository commercialClubRepository;
	@Autowired
	LiabilitySurpriseRepository liabilitySurpriseRepository;
	@Autowired
	AccidentHealthClubRepository accidentHealthClubRepository;
	@Autowired
	TravelClubRepository travelClubRepository;
	@Autowired
	LoBSummaryRepository loBSummaryRepository;
	
	@RequestMapping(method = RequestMethod.GET, value = "/getDashboardData//{agentId}")
	ResponseEntity getDashboardData(@PathVariable String agentId) {
		//HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		// httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		try {
			System.out.println("retrieving dashboard data"+agentId);
			IncomeChart incomeChart=incomeChartRepository.findByAgentId(agentId);
			NewBusinessBonus newBusinessBonus=newBusinessBonusRepository.findByAgentId(agentId);
			PerformanceBonus performanceBonus=performanceBonusRepository.findByAgentId(agentId);
			OfficeAllowance officeAllowance=officeAllowanceRepository.findByAgentId(agentId);
			RenewalBusinessBonus renewalBusinessBonus=renewalBusinessBonusRepository.findByAgentId(agentId);
			AutoClub autoClub=autoClubRepository.findByAgentId(agentId);
			CommercialClub commercialClub=commercialClubRepository.findByAgentId(agentId);
			LiabilitySurprise liabilitySurprise=liabilitySurpriseRepository.findByAgentId(agentId);
			AccidentHealthClub accidentHealthClub=accidentHealthClubRepository.findByAgentId(agentId);
			TravelClub travelClub=travelClubRepository.findByAgentId(agentId);
			LoBSummary loBSummary=loBSummaryRepository.findByAgentId(agentId);
			GwpGraph gwpGraph=gWPGraphRepository.findByAgentId(agentId);
			
			HashMap<String, Object> hashMap = new HashMap<String, Object>();
			if(gwpGraph!=null) {
				hashMap.put("GWPGraph", SerializationUtility.getInstance().toJson(gwpGraph));
			}
			if(incomeChart!=null) {
				hashMap.put("IncomeChart", SerializationUtility.getInstance().toJson(incomeChart));
			}
			if(officeAllowance!=null) {
				hashMap.put("OfficeAllowance",SerializationUtility.getInstance().toJson(officeAllowance));
			}
			if(renewalBusinessBonus!=null) {
				hashMap.put("RenewalBusinessBonus",SerializationUtility.getInstance().toJson(renewalBusinessBonus) );
			}
			if(newBusinessBonus!=null) {
				hashMap.put("NewBusinessBonus", SerializationUtility.getInstance().toJson(newBusinessBonus));
			}
			if(performanceBonus!=null) {
				hashMap.put("PerformanceBonus", SerializationUtility.getInstance().toJson(performanceBonus));
			}
			if(autoClub!=null) {
				hashMap.put("AutoClub",SerializationUtility.getInstance().toJson(autoClub));
			}
			if(commercialClub!=null) {
				hashMap.put("CommercialClub",SerializationUtility.getInstance().toJson(commercialClub) );
			}
			if(liabilitySurprise!=null) {
				hashMap.put("LiabilitySurprise", SerializationUtility.getInstance().toJson(liabilitySurprise));
			}
			if(accidentHealthClub!=null) {
				hashMap.put("AccidentHealthClub", SerializationUtility.getInstance().toJson(accidentHealthClub));
			}
			if(travelClub!=null) {
				hashMap.put("TravelClub",SerializationUtility.getInstance().toJson(travelClub));
			}
			if(loBSummary!=null) {
				hashMap.put("LoBSummary",SerializationUtility.getInstance().toJson(loBSummary) );
			}
			String jsonResult = SerializationUtility.getInstance().toJson(hashMap);
			return new ResponseEntity(jsonResult, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}

	}
}
