package com.metamorphosys.insureconnect.controllers;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metamorphosys.insureconnect.common.interfaceobjects.BaseIO;
import com.metamorphosys.insureconnect.dataobjects.master.RuleDO;
import com.metamorphosys.insureconnect.exception.WebBadRequestException;
import com.metamorphosys.insureconnect.jpa.master.RuleRepository;
import com.metamorphosys.insureconnect.utilities.SerializationUtility;

@RestController
@RequestMapping("/ruleSync")
public class WebRuleSyncController extends WebCrudController{

	private static final Logger log = LoggerFactory.getLogger(WebRuleSyncController.class);
	
	@Autowired
	RuleRepository ruleRepository;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity get() {

		//log.info("Web Crud Controller [Get] :: RuleSync");

		String response = "SUCCESS";
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		

	    return new ResponseEntity(response, httpHeaders, httpStatus);
	}

	@RequestMapping(method = RequestMethod.POST, consumes= "application/json")
	public ResponseEntity getJson(@RequestBody String json) {

		//log.info("getJson");
				
	    return execute(json);
	}
	
	public String process(String data) throws Exception
	{
		BaseIO crudIO = new BaseIO();
		ArrayList<RuleDO> list = new ArrayList<RuleDO>();
		
		if(null != data)
		{
			list = (ArrayList)ruleRepository.findAll();
		}
		else
		{
			throw new WebBadRequestException("Bad request");
		}

		return SerializationUtility.getInstance().toJson(list);
	}
}
