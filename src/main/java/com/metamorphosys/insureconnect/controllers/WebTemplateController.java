package com.metamorphosys.insureconnect.controllers;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metamorphosys.insureconnect.common.interfaceobjects.BaseIO;
import com.metamorphosys.insureconnect.common.interfaceobjects.ResponseDataIO;
import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;
import com.metamorphosys.insureconnect.exception.WebBadRequestException;
import com.metamorphosys.insureconnect.utilities.SerializationUtility;

@RestController
@RequestMapping("/template")
public class WebTemplateController extends WebCrudController{

	private static final Logger log = LoggerFactory.getLogger(WebTemplateController.class);

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity template() {

		log.info("Web Crud Controller [Get] :: TEMPLATE");

		String response = "SUCCESS";
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		

	    return new ResponseEntity(response, httpHeaders, httpStatus);
	}

	@RequestMapping(method = RequestMethod.POST, consumes= "application/json")
	public ResponseEntity templateJson(@RequestBody String json) {

		log.info("templateJson");
				
	    return execute(json);
	}
	
	public String process(String data) throws Exception
	{
		BaseIO crudIO = new BaseIO();
		ArrayList<ResponseDataIO> list = new ArrayList<ResponseDataIO>();
		
		if(null != data)
		{
			crudIO = convertToIO(data);	
			
			if(null != crudIO && null != crudIO.getBaseDOList())
			{
				for(BaseDO baseDO: crudIO.getBaseDOList())
				{
					log.info(baseDO.getObjectName());
					log.info(baseDO.toString());
					
					baseDO = (BaseDO)populateTemplate(baseDO);
					
					if(null != baseDO)
					{
						ResponseDataIO dataIO = new ResponseDataIO();
						dataIO.setObject(SerializationUtility.getInstance().toJson(baseDO));
						dataIO.setObjectName(baseDO.getClass().getSimpleName());
						list.add(dataIO);
					}
				}
				
				BaseDO serviceDO = crudIO.getServiceDO();
				if(null != serviceDO)
				{
					serviceDO = (BaseDO)populateTemplate(serviceDO);
					
					if(null != serviceDO)
					{
						ResponseDataIO dataIO = new ResponseDataIO();
						dataIO.setObject(SerializationUtility.getInstance().toJson(serviceDO));
						dataIO.setObjectName(serviceDO.getClass().getSimpleName());
						list.add(dataIO);
					}
				}
			}
			else
			{
				throw new WebBadRequestException("Bad request");
			}
		}
		else
		{
			throw new WebBadRequestException("Bad request");
		}

		return SerializationUtility.getInstance().toJson(list);
	}

	private Object populateTemplate(Object baseDO) {
		
		Method[] objectMethods = baseDO.getClass().getMethods();
		
		for(Method method : objectMethods)
		{
			try
			{
				// invoke all setters
				if(method.getName().startsWith("set"))
				{
					Class[] attributes = method.getParameterTypes();
					
					Object[] objects = new Object[attributes.length];
					int parameterCounter = 0;
					for(Class attributeClass: attributes)
					{
						if(attributeClass.getName().contains("String"))
						{
							objects[parameterCounter] = new String();
						}
						else if(attributeClass.getName().contains("Integer"))
						{
							objects[parameterCounter] = new Integer(0);
						}
						else if(attributeClass.getName().contains("Double"))
						{
							objects[parameterCounter] = new Double(0);
						}
						else if(attributeClass.getName().contains("Timestamp"))
						{
							objects[parameterCounter] = new Timestamp(System.currentTimeMillis());
						}
						else if(attributeClass.getName().contains("java.util.List"))
						{
							Type listType = method.getGenericParameterTypes()[parameterCounter];
							Type elementType = null;
							Object listObject = null;
							if (listType instanceof ParameterizedType) {
							    elementType = ((ParameterizedType) listType).getActualTypeArguments()[0];

							    log.info("Method: "+method.getName());
							    log.info("Method parameter Type: "+elementType.getTypeName());

								// create Object
								Class listClass = Class.forName(elementType.getTypeName());
								listObject = listClass.newInstance();
								
								listObject = populateTemplate(listObject);

							}
	
							List list = new ArrayList();
							list.add(listObject);
							objects[parameterCounter] = list;
						}
						else
						{
							log.info("Unknown attribute: "+attributeClass.getName());
						}
						
						parameterCounter++;
					}
					
					method.invoke(baseDO, objects);
				}
			}
			catch(Exception e)
			{
				log.error("Error invoking method: "+method.getName()+" on object: "+baseDO.getClass().getName());
			}
		}
		return baseDO;
	}
}
