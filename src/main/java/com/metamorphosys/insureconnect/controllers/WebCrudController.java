package com.metamorphosys.insureconnect.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.internal.LinkedTreeMap;
import com.metamorphosys.insureconnect.common.interfaceobjects.BaseIO;
import com.metamorphosys.insureconnect.common.interfaceobjects.FrameworkResponse;
import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;
import com.metamorphosys.insureconnect.exception.WebBadRequestException;
import com.metamorphosys.insureconnect.exception.WebUnauthorizedException;
import com.metamorphosys.insureconnect.utilities.SerializationUtility;

@RestController
@ImportResource({"classpath*:config/CrudBeans.xml"})
public class WebCrudController extends BaseWebController{

	private static final Logger log = LoggerFactory.getLogger(WebCrudController.class);
	
	public static class RequestKeys{
		public static final String TOKEN_KEY = "token";
		public static final String DATA_KEY = "data";
	}

	public static class ObjectMapKeys{
		public static final String CLASS = "class";
		public static final String REPOSITORY = "repository";
	}

	private static HashMap<String, HashMap<String, Object>> objectMap = new HashMap<String, HashMap<String, Object>>();
	
	@PostConstruct
	public void init() {
		log.info("init WebCrudController");
	}

	/*
	@Autowired
	private ServiceRepository serviceRepository;

	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Autowired
	private QuotationRepository quotationRepository;
	*/
	
	@Bean
	public HashMap initObjectMap() {
		/*
		// Service
		log.info("Calling bean serviceRepository: "+serviceRepository);
		HashMap<String, Object> serviceMap = new HashMap<String, Object>();
		serviceMap.put(ObjectMapKeys.CLASS, new ServiceDO());
		serviceMap.put(ObjectMapKeys.REPOSITORY, serviceRepository);
		objectMap.put("ServiceDO", serviceMap);
		
		// Employee
		log.info("Calling bean employeeRepository: "+employeeRepository);
		HashMap<String, Object> employeeMap = new HashMap<String, Object>();
		employeeMap.put(ObjectMapKeys.CLASS, new EmployeeDO());
		employeeMap.put(ObjectMapKeys.REPOSITORY, employeeRepository);
		objectMap.put("EmployeeDO", employeeMap);
		
		// Quotation
		log.info("Calling bean quotationRepository: "+quotationRepository);
		HashMap<String, Object> quotationMap = new HashMap<String, Object>();
		quotationMap.put(ObjectMapKeys.CLASS, new QuotationDO());
		quotationMap.put(ObjectMapKeys.REPOSITORY, quotationRepository);
		objectMap.put("QuotationDO", quotationMap);

		// Preference
		log.info("Calling bean preferenceRepository: ");
		HashMap<String, Object> preferenceMap = new HashMap<String, Object>();
		preferenceMap.put(ObjectMapKeys.CLASS, new PreferenceDO());
		preferenceMap.put(ObjectMapKeys.REPOSITORY, null);
		objectMap.put("PreferenceDO", preferenceMap);
		 */
		return objectMap;
	}
	
	public ResponseEntity execute(String json)
	{
		//log.info("Web Crud Controller");
		//log.info("RequestBody: "+json);

		String response = null;
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		FrameworkResponse frameworkResponse = new FrameworkResponse();
		
		String data = null;
		BaseIO crudIO = null;
		try
		{
			data = authorize(json);

			preProcess();
			response = process(data);
			
			postProcess();
			
			if(null != response)
			{
				frameworkResponse.put(FrameworkResponse.DATA_KEY, response);
			}
		}
		catch(WebBadRequestException bre)
		{
			httpStatus = HttpStatus.BAD_REQUEST;
			//log.error("Bad request: "+bre.getMessage(), bre);
			frameworkResponse.put(FrameworkResponse.ERRORCD_KEY, "BAD_REQUEST");
			frameworkResponse.put(FrameworkResponse.ERRORMSG_KEY, "Bad request: "+bre.getMessage());
		}
		catch(WebUnauthorizedException ue)
		{
			httpStatus = HttpStatus.UNAUTHORIZED;
			//log.error("Error authorizing request: "+ue.getMessage(), ue);
			frameworkResponse.put(FrameworkResponse.ERRORCD_KEY, "UNAUTHORIZED");
			frameworkResponse.put(FrameworkResponse.ERRORMSG_KEY, "Error authorizing request: "+ue.getMessage());
		}
		catch(Exception e)
		{
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			//log.error("Error occured: "+e.getMessage(), e);
			frameworkResponse.put(FrameworkResponse.ERRORCD_KEY, "INTERNAL_SERVER_ERROR");
			frameworkResponse.put(FrameworkResponse.ERRORMSG_KEY, "Error occured: "+e.getMessage());
		}
		
		String finalResponse = SerializationUtility.getInstance().toJson(frameworkResponse);
		return new ResponseEntity(finalResponse, httpHeaders, httpStatus);
	}
	
	public void preProcess() throws Exception {}
	public void postProcess() throws Exception {}
	
	public String process(String data) throws Exception{return null;}
	
	
	public String authorize(String json)
	{
		String data = null;
		
		LinkedTreeMap map = SerializationUtility.getInstance().fromJson(json);
		
		if(null != map && map.containsKey(RequestKeys.DATA_KEY))
		{
			data = map.get(RequestKeys.DATA_KEY).toString();
		}
		
		return data;
	}
	
	public BaseIO convertToIO(String json)
	{
		BaseIO baseIO = new BaseIO();
		if(null != json)
		{
			ArrayList<LinkedTreeMap> requestDataList = (ArrayList<LinkedTreeMap>)SerializationUtility.getInstance().fromJson(json, ArrayList.class);
			
			if(null != requestDataList)
			{
				ArrayList<BaseDO> objectList = new ArrayList<BaseDO>();
				for (LinkedTreeMap requestDataIO : requestDataList) 
				{
					if(objectMap.containsKey(requestDataIO.get("objectName")))
					{						
						Object object = getObjectMapParameter(requestDataIO.get("objectName").toString(), ObjectMapKeys.CLASS);
						
						BaseDO baseDO = (BaseDO)SerializationUtility.getInstance().fromJson(requestDataIO.get("object").toString(), object.getClass());
						
						if("ServiceDO".equals(requestDataIO.get("objectName").toString()))
						{
							// add service DO for service evaluation and to transaction list for workflow evaluation
							objectList.add(baseDO);
							baseIO.setServiceDO(baseDO);
						}
						else if("PreferenceDO".equals(requestDataIO.get("objectName").toString()))
						{
							// add preference DO for preference setup and to transaction list for workflow evaluation
							System.out.println("PreferenceDO"+requestDataIO);
							objectList.add(baseDO);
							baseIO.setPreferenceDO(baseDO);
						}
						else
						{
							objectList.add(baseDO);
						}
					}
					else
					{
						log.error("Undefined object: "+requestDataIO.get("objectName"));
					}
				}
				baseIO.setBaseDOList(objectList);
			}
		}
		return baseIO;
	}
	
	public Object getObjectMapParameter(String objectName, String parameter)
	{
		Object returnObject = null;
		HashMap<String, Object> objectMapParameterSet = objectMap.get(objectName);
		
		if(null != objectMapParameterSet)
		{
			returnObject = objectMapParameterSet.get(parameter);
		}
		
		return returnObject;
	}
	
	public void setObjectMap(HashMap<String, HashMap<String, Object>> objectMap)
	{
		WebCrudController.objectMap = objectMap;
	}
	
}