package com.metamorphosys.insureconnect.controllers;

import org.springframework.web.bind.annotation.RestController;

@RestController
public abstract class BaseWebController {

	public abstract String process(String data) throws Exception;
}
