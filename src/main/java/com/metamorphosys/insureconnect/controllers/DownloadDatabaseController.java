package com.metamorphosys.insureconnect.controllers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metamorphosys.insureconnect.dataobjects.master.ApplicationDO;
import com.metamorphosys.insureconnect.jpa.master.ApplicationRepository;
import com.metamorphosys.insureconnect.utilities.InsureConnectConstants;

@RestController
@RequestMapping("/downloadDB")
public class DownloadDatabaseController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DownloadDatabaseController.class);
	
	@Autowired
	Environment env;
	
	@Autowired
	ApplicationRepository applicationRepository;
	
	@RequestMapping(value = "/sqlite", method = RequestMethod.GET)
    public void getFile(
        HttpServletResponse response) throws IOException {
		
            ApplicationDO applicationDO= applicationRepository.findByKey(InsureConnectConstants.Path.OFFLINEDBPATH);
            InputStream is = new FileInputStream(applicationDO.getValue());
            IOUtils.copy(is, response.getOutputStream());
            response.flushBuffer();
    }
	@RequestMapping(method = RequestMethod.GET,value="/sqlite/usahaku")
	public void authenticate(
	        HttpServletResponse response){
		InputStream is;
		try {
			LOGGER.info("Downloading Usahaku DB");
			is = new FileInputStream(env.getProperty("db.url"));
			IOUtils.copy(is, response.getOutputStream());
	        response.flushBuffer();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
        
	}
}
