package com.metamorphosys.insureconnect.dataobjects.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.SynchronizableDO;

@Entity
@Table(name = "TASK")
public class TaskDO extends SynchronizableDO {

	private @Id @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "taskId_seq")
	@SequenceGenerator(name = "taskId_seq", sequenceName = "TASKID_SEQ", allocationSize = 1) Long id;
	
	@Column(name = "taskName")
	private String taskName;
	
	@Column(name = "service")
	private String service;
	
	@Column(name = "taskType")
	private String taskType;
	
	@Column(name = "assignmentType")
	private String assignmentType;
	
	@Column(name = "assignmentAlgorithm")
	private String assignmentAlgorithm;
	
	@Column(name = "assignmentRule")
	private String assignmentRule;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getTaskType() {
		return taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	public String getAssignmentType() {
		return assignmentType;
	}

	public void setAssignmentType(String assignmentType) {
		this.assignmentType = assignmentType;
	}

	public String getAssignmentAlgorithm() {
		return assignmentAlgorithm;
	}

	public void setAssignmentAlgorithm(String assignmentAlgorithm) {
		this.assignmentAlgorithm = assignmentAlgorithm;
	}

	public String getAssignmentRule() {
		return assignmentRule;
	}

	public void setAssignmentRule(String assignmentRule) {
		this.assignmentRule = assignmentRule;
	}

	@Override
	public String getObjectName() {
		return TaskDO.class.getSimpleName();
	}

}
