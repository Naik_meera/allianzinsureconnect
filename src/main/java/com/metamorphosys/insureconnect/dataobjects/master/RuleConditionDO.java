package com.metamorphosys.insureconnect.dataobjects.master;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "RULECONDITION")
public class RuleConditionDO extends BaseDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="ruleConditionId_seq") 
	@SequenceGenerator(name="ruleConditionId_seq",sequenceName="RULECONDITIONID_SEQ",allocationSize=1) Long id;

	@Column(name = "conditionType")
    private String conditionType;
	
	@Column(name = "condition")
    private String condition;
	
	@Column(name = "conditionOrder")
    private Integer conditionOrder;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public String getObjectName() {		
		return RuleConditionDO.class.getSimpleName();
	}

	public Integer getConditionOrder() {
		return conditionOrder;
	}

	public void setConditionOrder(Integer conditionOrder) {
		this.conditionOrder = conditionOrder;
	}

	public String getConditionType() {
		return conditionType;
	}

	public void setConditionType(String conditionType) {
		this.conditionType = conditionType;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	
}
