package com.metamorphosys.insureconnect.dataobjects.master;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.SynchronizableDO;

@Entity
@Table(name = "METADATA")
public class MetadataDO extends SynchronizableDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="metadataId_seq") 
	@SequenceGenerator(name="metadataId_seq",sequenceName="METADATAID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "metadataCd")
	private String metadataCd;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<UIMetadataDO> uiMetadataList;

	@Override
	public String getObjectName() {
		return MetadataDO.class.getSimpleName();
	}

	@Override
	public Long getId() {
		return id;
	}

	public String getMetadataCd() {
		return metadataCd;
	}

	public void setMetadataCd(String metadataCd) {
		this.metadataCd = metadataCd;
	}

	public List<UIMetadataDO> getUiMetadataList() {
		return uiMetadataList;
	}

	public void setUiMetadataList(List<UIMetadataDO> uiMetadataList) {
		this.uiMetadataList = uiMetadataList;
	}

}
