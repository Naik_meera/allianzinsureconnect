package com.metamorphosys.insureconnect.dataobjects.master;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "PERFORMANCEBONUS")
public class PerformanceBonus {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "performanceBonusId_seq")
	@SequenceGenerator(name = "performanceBonusId_seq", sequenceName = "PERFORMANCEBONUSID_SEQ", allocationSize = 1)
	private Long id;

	private String agentId;
	private String agentName;
	private String currYrGWP;

	private String currYrGrowth;
	private String currYrLossRatio;
	private String currYrEstBonus;

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCurrYrGWP() {
		return currYrGWP;
	}

	public void setCurrYrGWP(String currYrGWP) {
		this.currYrGWP = currYrGWP;
	}

	public String getCurrYrGrowth() {
		return currYrGrowth;
	}

	public void setCurrYrGrowth(String currYrGrowth) {
		this.currYrGrowth = currYrGrowth;
	}

	public String getCurrYrLossRatio() {
		return currYrLossRatio;
	}

	public void setCurrYrLossRatio(String currYrLossRatio) {
		this.currYrLossRatio = currYrLossRatio;
	}

	public String getCurrYrEstBonus() {
		return currYrEstBonus;
	}

	public void setCurrYrEstBonus(String currYrEstBonus) {
		this.currYrEstBonus = currYrEstBonus;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
}
