package com.metamorphosys.insureconnect.dataobjects.master;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@Entity
@Table(name = "AUTOCLUB")
public class AutoClub {
	
	@Id 
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="autoClubId_seq") 
	@SequenceGenerator(name="autoClubId_seq",sequenceName="AUTOCLUBID_SEQ",allocationSize=1) 
	private Long id;
	
	private String currYrAutoGWPJan;
	
	private String currYrAutoGWPFeb;
	
	private String currYrAutoGWPMar;
	
	private String currYrAutoGWPApr;
	
	private String currYrAutoGWPMay;
	
	private String currYrAutoGWPJun;
	
	private String currYrAutoGWPJul;
	
	private String currYrAutoGWPAug;
	
	private String currYrAutoGWPSep;
	
	private String currYrAutoGWPOct;
	
	private String currYrAutoGWPNov;
	
	private String currYrAutoGWPDec;
	
	private String currYrEstBonus;
	
	private String agentId;
	private String agentName;
	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCurrYrAutoGWPJan() {
		return currYrAutoGWPJan;
	}

	public void setCurrYrAutoGWPJan(String currYrAutoGWPJan) {
		this.currYrAutoGWPJan = currYrAutoGWPJan;
	}

	public String getCurrYrAutoGWPFeb() {
		return currYrAutoGWPFeb;
	}

	public void setCurrYrAutoGWPFeb(String currYrAutoGWPFeb) {
		this.currYrAutoGWPFeb = currYrAutoGWPFeb;
	}

	public String getCurrYrAutoGWPMar() {
		return currYrAutoGWPMar;
	}

	public void setCurrYrAutoGWPMar(String currYrAutoGWPMar) {
		this.currYrAutoGWPMar = currYrAutoGWPMar;
	}

	public String getCurrYrAutoGWPApr() {
		return currYrAutoGWPApr;
	}

	public void setCurrYrAutoGWPApr(String currYrAutoGWPApr) {
		this.currYrAutoGWPApr = currYrAutoGWPApr;
	}

	public String getCurrYrAutoGWPMay() {
		return currYrAutoGWPMay;
	}

	public void setCurrYrAutoGWPMay(String currYrAutoGWPMay) {
		this.currYrAutoGWPMay = currYrAutoGWPMay;
	}

	public String getCurrYrAutoGWPJun() {
		return currYrAutoGWPJun;
	}

	public void setCurrYrAutoGWPJun(String currYrAutoGWPJun) {
		this.currYrAutoGWPJun = currYrAutoGWPJun;
	}

	public String getCurrYrAutoGWPJul() {
		return currYrAutoGWPJul;
	}

	public void setCurrYrAutoGWPJul(String currYrAutoGWPJul) {
		this.currYrAutoGWPJul = currYrAutoGWPJul;
	}

	public String getCurrYrAutoGWPAug() {
		return currYrAutoGWPAug;
	}

	public void setCurrYrAutoGWPAug(String currYrAutoGWPAug) {
		this.currYrAutoGWPAug = currYrAutoGWPAug;
	}

	public String getCurrYrAutoGWPSep() {
		return currYrAutoGWPSep;
	}

	public void setCurrYrAutoGWPSep(String currYrAutoGWPSep) {
		this.currYrAutoGWPSep = currYrAutoGWPSep;
	}

	public String getCurrYrAutoGWPOct() {
		return currYrAutoGWPOct;
	}

	public void setCurrYrAutoGWPOct(String currYrAutoGWPOct) {
		this.currYrAutoGWPOct = currYrAutoGWPOct;
	}

	public String getCurrYrAutoGWPNov() {
		return currYrAutoGWPNov;
	}

	public void setCurrYrAutoGWPNov(String currYrAutoGWPNov) {
		this.currYrAutoGWPNov = currYrAutoGWPNov;
	}

	public String getCurrYrAutoGWPDec() {
		return currYrAutoGWPDec;
	}

	public void setCurrYrAutoGWPDec(String currYrAutoGWPDec) {
		this.currYrAutoGWPDec = currYrAutoGWPDec;
	}

	public String getCurrYrEstBonus() {
		return currYrEstBonus;
	}

	public void setCurrYrEstBonus(String currYrEstBonus) {
		this.currYrEstBonus = currYrEstBonus;
	}
	
}
