package com.metamorphosys.insureconnect.dataobjects.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.SynchronizableDO;

@Entity
@Table(name = "LABEL")
public class LabelDO extends SynchronizableDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="labelId_seq") 
	@SequenceGenerator(name="labelId_seq",sequenceName="LABELID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "labelKey")
	private String labelKey;
	
	@Column(name = "language")
	private String language;
	
	@Column(name = "labelValue")
	private String labelValue;

	@Override
	public String getObjectName() {
		return LabelDO.class.getSimpleName();
	}

	@Override
	public Long getId() {
		return id;
	}

	public String getLabelKey() {
		return labelKey;
	}

	public void setLabelKey(String labelKey) {
		this.labelKey = labelKey;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getLabelValue() {
		return labelValue;
	}

	public void setLabelValue(String labelValue) {
		this.labelValue = labelValue;
	}

}
