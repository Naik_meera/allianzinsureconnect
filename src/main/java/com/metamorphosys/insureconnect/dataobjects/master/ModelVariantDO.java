package com.metamorphosys.insureconnect.dataobjects.master;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "MODELVARIANT")
public class ModelVariantDO {
	 
	public final static int ACTIVE=1;
	public final static int INACTIVE=0;
	
	 @Id 
	 @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="makeVariantId_seq") 
	 @SequenceGenerator(name="makeVariantId_seq",sequenceName="MAKEVARIANTID_SEQ",allocationSize=1) 
	 private Long id;
	
	 @Column(name = "versionId")
	 private long versionId;
	 
	 @Column(name = "makeCd")
	 private String makeCd;
	 
	 @Column(name = "makeName")
	 private String makeName;
	 
	 @Column(name = "modelCd")
	 private String modelCd;
	 
	 @Column(name = "modelName")
	 private String modelName;
	 
	 @Column(name = "seatingCapacity")
	 private String seatingCapacity;
	 
	 @Column(name = "variantCd")
	 private String variantCd;
	 
	 @Column(name = "variantName")
	 private String variantName;
	 
	 @Column(name = "vehiclekey")
	 private String vehiclekey;
	 
	 @Column(name = "guid")
	 private String guid;
	 
	 @Column(name = "statusCd")
	 private long statusCd;
	 
	@Column(name = "updatedDate")
	private Timestamp updatedDate;

	public Timestamp getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}
	 
	 public long getStatusCd() {
		return statusCd;
	}

	public void setStatusCd(long statusCd) {
		this.statusCd = statusCd;
	}

	public String getGuid() {
		 return guid;
	 }

	 public void setGuid(String guid) {
		 this.guid = guid;
	 }
	 
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public long getVersionId() {
		return versionId;
	}

	public void setVersionId(long versionId) {
		this.versionId = versionId;
	}

	public String getMakeCd() {
		return makeCd;
	}

	public void setMakeCd(String makeCd) {
		this.makeCd = makeCd;
	}

	public String getMakeName() {
		return makeName;
	}

	public void setMakeName(String makeName) {
		this.makeName = makeName;
	}

	public String getModelCd() {
		return modelCd;
	}

	public void setModelCd(String modelCd) {
		this.modelCd = modelCd;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getSeatingCapacity() {
		return seatingCapacity;
	}

	public void setSeatingCapacity(String seatingCapacity) {
		this.seatingCapacity = seatingCapacity;
	}

	public String getVariantCd() {
		return variantCd;
	}

	public void setVariantCd(String string) {
		this.variantCd = string;
	}

	public String getVariantName() {
		return variantName;
	}

	public void setVariantName(String variantName) {
		this.variantName = variantName;
	}

	public String getVehiclekey() {
		return vehiclekey;
	}

	public void setVehiclekey(String vehiclekey) {
		this.vehiclekey = vehiclekey;
	}
}