package com.metamorphosys.insureconnect.dataobjects.master;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="COUNTRY")
public class CountryDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="countryId_seq") 
	@SequenceGenerator(name="countryId_seq",sequenceName="COUNTRYID_SEQ",allocationSize=1) Long id;
	private String countryCd;
	private String countryDesc;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCountryCd() {
		return countryCd;
	}
	public void setCountryCd(String countryCd) {
		this.countryCd = countryCd;
	}
	public String getCountryDesc() {
		return countryDesc;
	}
	public void setCountryDesc(String countryDesc) {
		this.countryDesc = countryDesc;
	}
	
	
}
