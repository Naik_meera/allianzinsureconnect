package com.metamorphosys.insureconnect.dataobjects.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "PRODUCTRIDERATTRTYPE")
public class ProductRiderAttributeTypeDO  extends BaseDO{
	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="productRiderAttrTypeId_seq") 
	@SequenceGenerator(name="productRiderAttrTypeId_seq",sequenceName="PRODUCTRIDERATTRID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "attributeTypeCd")
	private String attributeTypeCd;
	
	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return id;
	}
	public String getAttributeTypeCd() {
		return attributeTypeCd;
	}
	public void setAttributeTypeCd(String attributeTypeCd) {
		this.attributeTypeCd = attributeTypeCd;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return ProductRiderAttributeTypeDO.class.getSimpleName();
	}

}
