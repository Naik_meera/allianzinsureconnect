package com.metamorphosys.insureconnect.dataobjects.master;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;
import com.metamorphosys.insureconnect.dataobjects.common.SynchronizableDO;

public class PreferenceDO extends BaseDO {

	private  Long id;

	private List<PreferenceAdditionalFieldDO> preferenceAdditionalFieldList;

	@Override
	public String getObjectName() {
		return PreferenceDO.class.getSimpleName();
	}

	@Override
	public Long getId() {
		return id;
	}

	public List<PreferenceAdditionalFieldDO> getPreferenceAdditionalFieldList() {
		return preferenceAdditionalFieldList;
	}

	public void setPreferenceAdditionalFieldList(List<PreferenceAdditionalFieldDO> preferenceAdditionalFieldList) {
		this.preferenceAdditionalFieldList = preferenceAdditionalFieldList;
	}

	public String fetchAdditionalField(String key)
	{
		String value = null;
		for (PreferenceAdditionalFieldDO preferenceAdditionalFieldDO : preferenceAdditionalFieldList) 
		{
			if(	null != preferenceAdditionalFieldDO && 
				null != preferenceAdditionalFieldDO.getFieldKey() && 
				preferenceAdditionalFieldDO.getFieldKey().equalsIgnoreCase(key))
			{
				value = preferenceAdditionalFieldDO.getFieldValue();
				break;
			}
		}
		return value;
	}

}
