package com.metamorphosys.insureconnect.dataobjects.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.TransactionDO;

@Entity
@Table(name = "ROLE")
public class RoleDO extends TransactionDO{

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="roleId_seq")
	@SequenceGenerator(name="roleId_seq",sequenceName="ROLEID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "roleCd")
	private String roleCd;
	
	@Column(name = "roleDesc")
	private String roleDesc;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRoleCd() {
		return roleCd;
	}

	public void setRoleCd(String roleCd) {
		this.roleCd = roleCd;
	}

	public String getRoleDesc() {
		return roleDesc;
	}

	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}

	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return RoleDO.class.getSimpleName();
	}

}