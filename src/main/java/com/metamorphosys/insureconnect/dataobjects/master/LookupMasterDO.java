package com.metamorphosys.insureconnect.dataobjects.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.SynchronizableDO;

@Entity
@Table(name = "LOOKUPMASTER")
public class LookupMasterDO extends SynchronizableDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="lookupMasterId_seq") 
	@SequenceGenerator(name="lookupMasterId_seq",sequenceName="LOOKUPMASTERID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "lookupCd")
	private String lookupCd;
	
	@Column(name = "lookupCriteria")
	private String lookupCriteria;
	
	@Column(name = "lookupPostQuery")
	private String lookupPostQuery;
	
	@Column(name = "lookupDataCd")
	private String lookupDataCd;
	
	@Column(name = "lookupDataValue")
	private String lookupDataValue;
	
	@Column(name = "lookupQueryTable")
	private String lookupQueryTable;
	
	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return LookupMasterDO.class.getSimpleName();
	}

	public String getLookupCd() {
		return lookupCd;
	}

	public void setLookupCd(String lookupCd) {
		this.lookupCd = lookupCd;
	}

	public String getLookupCriteria() {
		return lookupCriteria;
	}

	public void setLookupCriteria(String lookupCriteria) {
		this.lookupCriteria = lookupCriteria;
	}

	public String getLookupPostQuery() {
		return lookupPostQuery;
	}

	public void setLookupPostQuery(String lookupPostQuery) {
		this.lookupPostQuery = lookupPostQuery;
	}

	public String getLookupDataCd() {
		return lookupDataCd;
	}

	public void setLookupDataCd(String lookupDataCd) {
		this.lookupDataCd = lookupDataCd;
	}

	public String getLookupDataValue() {
		return lookupDataValue;
	}

	public void setLookupDataValue(String lookupDataValue) {
		this.lookupDataValue = lookupDataValue;
	}

	public String getLookupQueryTable() {
		return lookupQueryTable;
	}

	public void setLookupQueryTable(String lookupQueryTable) {
		this.lookupQueryTable = lookupQueryTable;
	}
	
}
