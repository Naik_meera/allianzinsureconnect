package com.metamorphosys.insureconnect.dataobjects.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.SynchronizableDO;

@Entity
@Table(name = "VOCABULARY")
public class VocabularyDO extends SynchronizableDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="vocabularyId_seq") 
	@SequenceGenerator(name="vocabularyId_seq",sequenceName="VOCABULARYID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "vocabularyCd")
	private String vocabularyCd;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "parentObjectDO")
	private String parentObjectDO;
	
	@Column(name = "subObjectDO")
	private String subObjectDO;
	
	@Column(name = "childObjectDO")
	private String childObjectDO;
	
	@Column(name = "vocabularyType")
	private String vocabularyType;
	
	@Column(name = "vocabularyName")
	private String vocabularyName;
	
	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getVocabularyCd() {
		return vocabularyCd;
	}

	public void setVocabularyCd(String vocabularyCd) {
		this.vocabularyCd = vocabularyCd;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getParentObjectDO() {
		return parentObjectDO;
	}

	public void setParentObjectDO(String parentObjectDO) {
		this.parentObjectDO = parentObjectDO;
	}

	public String getSubObjectDO() {
		return subObjectDO;
	}

	public void setSubObjectDO(String subObjectDO) {
		this.subObjectDO = subObjectDO;
	}

	public String getVocabularyType() {
		return vocabularyType;
	}

	public void setVocabularyType(String vocabularyType) {
		this.vocabularyType = vocabularyType;
	}

	public String getVocabularyName() {
		return vocabularyName;
	}

	public void setVocabularyName(String vocabularyName) {
		this.vocabularyName = vocabularyName;
	}

	public String getChildObjectDO() {
		return childObjectDO;
	}

	public void setChildObjectDO(String childObjectDO) {
		this.childObjectDO = childObjectDO;
	}

	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return VocabularyDO.class.getSimpleName();
	}

}
