package com.metamorphosys.insureconnect.dataobjects.master;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.SynchronizableDO;

@Entity
@Table(name = "PRODUCT")
public class ProductDO extends SynchronizableDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="productId_seq") 
	@SequenceGenerator(name="productId_seq",sequenceName="PRODUCTID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "productId")
	private String productId;
	
	@Column(name = "productName")
	private String productName;
	
	@Column(name = "productTypeCd")
	private String productTypeCd;
	
	@Column(name = "productFamilyCd")
	private String productFamilyCd;
	
	@Column(name = "launchDt")
	private Timestamp launchDt;
	
	@Column(name = "withdrawDt")
	private Timestamp withdrawDt;
	
	@Column(name = "plantype")
	private String plantype;
	
	@Column(name = "companyCd")
	private String companyCd;
	
	@Column(name = "passProductCd")
	private String passProductCd;
	
	@Column(name = "productMarketingName")
	private String productMarketingName;
	
	@Column(name = "productStatus")
	private String productStatus;

	
	public String getProductMarketingName() {
		return productMarketingName;
	}

	public void setProductMarketingName(String productMarketingName) {
		this.productMarketingName = productMarketingName;
	}

	public String getProductStatus() {
		return productStatus;
	}

	public void setProductStatus(String productStatus) {
		this.productStatus = productStatus;
	}

	@OneToMany(cascade = CascadeType.ALL,fetch=FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<ProductAttributeDO> attributeList;

	public List<ProductRiderAssociationDO> getProductRiderAssociationList() {
		return productRiderAssociationList;
	}

	public void setProductRiderAssociationList(List<ProductRiderAssociationDO> productRiderAssociationList) {
		this.productRiderAssociationList = productRiderAssociationList;
	}

	@OneToMany(cascade = CascadeType.ALL,fetch=FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<ProductAdditionalFieldDO> productAdditionalFieldList;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<ProductRiderAssociationDO> productRiderAssociationList;

	@Override
	public String getObjectName() {
		return ProductDO.class.getSimpleName();
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductTypeCd() {
		return productTypeCd;
	}

	public void setProductTypeCd(String productTypeCd) {
		this.productTypeCd = productTypeCd;
	}

	public String getProductFamilyCd() {
		return productFamilyCd;
	}

	public void setProductFamilyCd(String productFamilyCd) {
		this.productFamilyCd = productFamilyCd;
	}

	public Timestamp getLaunchDt() {
		return launchDt;
	}

	public void setLaunchDt(Timestamp launchDt) {
		this.launchDt = launchDt;
	}

	public Timestamp getWithdrawDt() {
		return withdrawDt;
	}

	public void setWithdrawDt(Timestamp withdrawDt) {
		this.withdrawDt = withdrawDt;
	}

	public List<ProductAttributeDO> getAttributeList() {
		return attributeList;
	}

	public void setAttributeList(List<ProductAttributeDO> attributeList) {
		this.attributeList = attributeList;
	}

	public List<ProductAdditionalFieldDO> getProductAdditionalFieldList() {
		return productAdditionalFieldList;
	}

	public void setProductAdditionalFieldList(List<ProductAdditionalFieldDO> productAdditionalFieldList) {
		this.productAdditionalFieldList = productAdditionalFieldList;
	}

	public String getPlantype() {
		return plantype;
	}

	public void setPlantype(String plantype) {
		this.plantype = plantype;
	}

	public String getCompanyCd() {
		return companyCd;
	}

	public void setCompanyCd(String companyCd) {
		this.companyCd = companyCd;
	}

	public String getPassProductCd() {
		return passProductCd;
	}

	public void setPassProductCd(String passProductCd) {
		this.passProductCd = passProductCd;
	}

	// additional methods
	public ProductAttributeDO findProductAttribute(String attributeId)
	{
		ProductAttributeDO returnAttribute = null;
		
		for(ProductAttributeDO productAttribute : this.getAttributeList())
		{
			if(null != productAttribute && attributeId.equals(productAttribute.getAttributeId()))
			{
				returnAttribute = productAttribute;
				//returnAttribute.setParentObject(this);
				break;
			}
		}
		return returnAttribute;
	}
	
	public List<ProductAttributeDO> findProductAttributeTypeList(String attributeTypeCd)
	{
		List<ProductAttributeDO> returnAttributeList = new ArrayList<ProductAttributeDO>();
		
		for(ProductAttributeDO productAttribute : this.getAttributeList())
		{
			if(null != productAttribute && attributeTypeCd.equals(productAttribute.getAttributeTypeCd()))
			{
				//productAttribute.setParentObject(this);
				returnAttributeList.add(productAttribute);
			}
		}
		return returnAttributeList;
	}

	// end additional methods
}
