package com.metamorphosys.insureconnect.dataobjects.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "VOCABULARYMAPPING")
public class VocabularyMappingDO extends BaseDO{

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="vocabularyMappingId_seq") 
	@SequenceGenerator(name="vocabularyMappingId_seq",sequenceName="VOCABULARYMAPPINGID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "parentObjectDO")
	private String parentObjectDO;
	
	@Column(name = "subObjectDO")
	private String subObjectDO;

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return id;
	}
	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return VocabularyMappingDO.class.getSimpleName();
	}
	public String getParentObjectDO() {
		return parentObjectDO;
	}
	public void setParentObjectDO(String parentObjectDO) {
		this.parentObjectDO = parentObjectDO;
	}
	public String getSubObjectDO() {
		return subObjectDO;
	}
	public void setSubObjectDO(String subObjectDO) {
		this.subObjectDO = subObjectDO;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
}
