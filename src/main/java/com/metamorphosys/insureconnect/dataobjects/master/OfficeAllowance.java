package com.metamorphosys.insureconnect.dataobjects.master;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@Entity
@Table(name = "OFFICEALLOWANCE")
public class OfficeAllowance {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "officeAllowanceId_seq")
	@SequenceGenerator(name = "officeAllowanceId_seq", sequenceName = "OFFICEALLOWANCEID_SEQ", allocationSize = 1)
	private Long id;

	private String currYrGWP;

	private String currYrGrowth;
	
	private String currYrEstBonus;
	
	private String agentId;
	private String agentName;

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCurrYrGWP() {
		return currYrGWP;
	}

	public void setCurrYrGWP(String currYrGWP) {
		this.currYrGWP = currYrGWP;
	}

	public String getCurrYrGrowth() {
		return currYrGrowth;
	}

	public void setCurrYrGrowth(String currYrGrowth) {
		this.currYrGrowth = currYrGrowth;
	}

	public String getCurrYrEstBonus() {
		return currYrEstBonus;
	}

	public void setCurrYrEstBonus(String currYrEstBonus) {
		this.currYrEstBonus = currYrEstBonus;
	}
	
}
