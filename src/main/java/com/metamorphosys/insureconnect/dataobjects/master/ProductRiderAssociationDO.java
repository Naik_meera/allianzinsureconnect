package com.metamorphosys.insureconnect.dataobjects.master;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "PRODUCTRIDERASSOC")
public class ProductRiderAssociationDO extends BaseDO{

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="productRiderAssocId_seq") 
	@SequenceGenerator(name="productRiderAssocId_seq",sequenceName="PRODUCTRIDERASSOCID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "riderid")
	private int riderid;	
	
	@Column(name = "ridercd")
	private String ridercd;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<ProductRiderAttributeDO> productRiderAttributelist;
	
	public String getRidercd() {
		return ridercd;
	}

	public void setRidercd(String ridercd) {
		this.ridercd = ridercd;
	}
	
	public int getRiderid() {
		return riderid;
	}

	public void setRiderid(int riderid) {
		this.riderid = riderid;
	}

	public List<ProductRiderAttributeDO> getProductRiderAttributelist() {
		return productRiderAttributelist;
	}

	public void setProductRiderAttributelist(List<ProductRiderAttributeDO> productRiderAttributelist) {
		this.productRiderAttributelist = productRiderAttributelist;
	}


	

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return ProductRiderAssociationDO.class.getSimpleName();
	}

}
