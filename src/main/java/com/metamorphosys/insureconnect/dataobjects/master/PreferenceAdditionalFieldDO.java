package com.metamorphosys.insureconnect.dataobjects.master;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.AdditionalFieldDO;
import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;
import com.metamorphosys.insureconnect.dataobjects.common.SynchronizableDO;

public class PreferenceAdditionalFieldDO extends AdditionalFieldDO {

	private Long id;

	@Override
	public Long getId() {
		return id;
	}

}
