package com.metamorphosys.insureconnect.dataobjects.master;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.AdditionalFieldDO;

@Entity
@Table(name = "RULEADDFLD")
public class RuleAdditionalFieldDO extends AdditionalFieldDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="ruleAddFldId_seq") 
	@SequenceGenerator(name="ruleAddFldId_seq",sequenceName="RULEADDFLDID_SEQ",allocationSize=1) Long id;

	@Override
	public Long getId() {
		return id;
	}

}
