package com.metamorphosys.insureconnect.dataobjects.master;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@Entity
@Table(name = "COMMERCIALCLUB")
public class CommercialClub {
	
	@Id 
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="commercialClubId_seq") 
	@SequenceGenerator(name="commercialClubId_seq",sequenceName="COMMERCIALCLUBID_SEQ",allocationSize=1) 
	private Long id;
	
	private String currYrCommGWPJan;
	
	private String currYrCommGWPFeb;
	
	private String currYrCommGWPMar;
	
	private String currYrCommGWPApr;
	
	private String currYrCommGWPMay;
	
	private String currYrCommGWPJun;
	
	private String currYrCommGWPJul;
	
	private String currYrCommGWPAug;
	
	private String currYrCommGWPSep;
	
	private String currYrCommGWPOct;
	
	private String currYrCommGWPNov;
	
	private String currYrCommGWPDec;
	
	private String currYrEstBonus;
	private String agentId;
	private String agentName;

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCurrYrCommGWPJan() {
		return currYrCommGWPJan;
	}

	public void setCurrYrCommGWPJan(String currYrCommGWPJan) {
		this.currYrCommGWPJan = currYrCommGWPJan;
	}

	public String getCurrYrCommGWPFeb() {
		return currYrCommGWPFeb;
	}

	public void setCurrYrCommGWPFeb(String currYrCommGWPFeb) {
		this.currYrCommGWPFeb = currYrCommGWPFeb;
	}

	public String getCurrYrCommGWPMar() {
		return currYrCommGWPMar;
	}

	public void setCurrYrCommGWPMar(String currYrCommGWPMar) {
		this.currYrCommGWPMar = currYrCommGWPMar;
	}

	public String getCurrYrCommGWPApr() {
		return currYrCommGWPApr;
	}

	public void setCurrYrCommGWPApr(String currYrCommGWPApr) {
		this.currYrCommGWPApr = currYrCommGWPApr;
	}

	public String getCurrYrCommGWPMay() {
		return currYrCommGWPMay;
	}

	public void setCurrYrCommGWPMay(String currYrCommGWPMay) {
		this.currYrCommGWPMay = currYrCommGWPMay;
	}

	public String getCurrYrCommGWPJun() {
		return currYrCommGWPJun;
	}

	public void setCurrYrCommGWPJun(String currYrCommGWPJun) {
		this.currYrCommGWPJun = currYrCommGWPJun;
	}

	public String getCurrYrCommGWPJul() {
		return currYrCommGWPJul;
	}

	public void setCurrYrCommGWPJul(String currYrCommGWPJul) {
		this.currYrCommGWPJul = currYrCommGWPJul;
	}

	public String getCurrYrCommGWPAug() {
		return currYrCommGWPAug;
	}

	public void setCurrYrCommGWPAug(String currYrCommGWPAug) {
		this.currYrCommGWPAug = currYrCommGWPAug;
	}

	public String getCurrYrCommGWPSep() {
		return currYrCommGWPSep;
	}

	public void setCurrYrCommGWPSep(String currYrCommGWPSep) {
		this.currYrCommGWPSep = currYrCommGWPSep;
	}

	public String getCurrYrCommGWPOct() {
		return currYrCommGWPOct;
	}

	public void setCurrYrCommGWPOct(String currYrCommGWPOct) {
		this.currYrCommGWPOct = currYrCommGWPOct;
	}

	public String getCurrYrCommGWPNov() {
		return currYrCommGWPNov;
	}

	public void setCurrYrCommGWPNov(String currYrCommGWPNov) {
		this.currYrCommGWPNov = currYrCommGWPNov;
	}

	public String getCurrYrCommGWPDec() {
		return currYrCommGWPDec;
	}

	public void setCurrYrCommGWPDec(String currYrCommGWPDec) {
		this.currYrCommGWPDec = currYrCommGWPDec;
	}

	public String getCurrYrEstBonus() {
		return currYrEstBonus;
	}

	public void setCurrYrEstBonus(String currYrEstBonus) {
		this.currYrEstBonus = currYrEstBonus;
	}
}
