package com.metamorphosys.insureconnect.dataobjects.master;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.metamorphosys.insureconnect.dataobjects.common.SynchronizableDO;

@Entity
@Table(name = "RULE")
public class RuleDO extends SynchronizableDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="ruleId_seq") 
	@SequenceGenerator(name="ruleId_seq",sequenceName="RULEID_SEQ",allocationSize=1) Long id;

	@Column(name = "ruleName")
	private String ruleName;
	
	@Column(name = "service")	
	private String service;
	
	@Column(name = "serviceType")
	private String serviceType;
	
	@Column(name = "serviceTask")
	private String serviceTask;
	
	@Column(name = "sortOrder")
	private Integer sortOrder;
	
	@Column(name = "ruleType")
	private String ruleType;
	
	@Column(name = "ruleSubType")
	private String ruleSubType;
	
	@Column(name = "toDate")
	private Date toDate;
	
	@Column(name = "fromDate")
	private Date fromDate;
	
	@Column(name = "ruleDesc")
	private String ruleDesc;
	
	@Transient
	private String parentRuleName;
	
	
	public String getParentRuleName() {
		return parentRuleName;
	}

	public void setParentRuleName(String parentRuleName) {
		this.parentRuleName = parentRuleName;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	@OrderBy("conditionOrder")
	private List<RuleConditionDO> ruleConditionList = new ArrayList<RuleConditionDO>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	@OrderBy("actionOrder")
	private List<RuleActionDO> ruleActionList = new ArrayList<RuleActionDO>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	@OrderBy("id")
	private List<RuleDataDO> ruleDataList = new ArrayList<RuleDataDO>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<RuleAdditionalFieldDO> ruleAdditionalFieldList;

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String getObjectName() {
		return RuleDO.class.getSimpleName();
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getServiceTask() {
		return serviceTask;
	}

	public void setServiceTask(String serviceTask) {
		this.serviceTask = serviceTask;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public List<RuleConditionDO> getRuleConditionList() {
		return ruleConditionList;
	}

	public void setRuleConditionList(List<RuleConditionDO> ruleConditionList) {
		this.ruleConditionList = ruleConditionList;
	}

	public List<RuleActionDO> getRuleActionList() {
		return ruleActionList;
	}

	public void setRuleActionList(List<RuleActionDO> ruleActionList) {
		this.ruleActionList = ruleActionList;
	}

	public List<RuleDataDO> getRuleDataList() {
		return ruleDataList;
	}

	public void setRuleDataList(List<RuleDataDO> ruleDataList) {
		this.ruleDataList = ruleDataList;
	}

	public List<RuleAdditionalFieldDO> getRuleAdditionalFieldList() {
		return ruleAdditionalFieldList;
	}

	public void setRuleAdditionalFieldList(List<RuleAdditionalFieldDO> ruleAdditionalFieldList) {
		this.ruleAdditionalFieldList = ruleAdditionalFieldList;
	}

	public String getRuleType() {
		return ruleType;
	}

	public void setRuleType(String ruleType) {
		this.ruleType = ruleType;
	}

	public String getRuleSubType() {
		return ruleSubType;
	}

	public void setRuleSubType(String ruleSubType) {
		this.ruleSubType = ruleSubType;
	}
	
	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	
	public String getRuleDesc() {
		return ruleDesc;
	}

	public void setRuleDesc(String ruleDesc) {
		this.ruleDesc = ruleDesc;
	}

	public String fetchAdditionalField(String key)
	{
		String value = null;
		for (RuleAdditionalFieldDO ruleAdditionalFieldDO : ruleAdditionalFieldList) 
		{
			if(	null != ruleAdditionalFieldDO && 
				null != ruleAdditionalFieldDO.getFieldKey() && 
				ruleAdditionalFieldDO.getFieldKey().equalsIgnoreCase(key))
			{
				value = ruleAdditionalFieldDO.getFieldValue();
				break;
			}
		}
		return value;
	}
}
