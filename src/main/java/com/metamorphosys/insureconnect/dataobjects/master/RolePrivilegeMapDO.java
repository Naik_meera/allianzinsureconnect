package com.metamorphosys.insureconnect.dataobjects.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.TransactionDO;

@Entity
@Table(name = "roleprivilegemap")
public class RolePrivilegeMapDO extends TransactionDO	 {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="roleprivilegemapId_seq") 
	@SequenceGenerator(name="roleprivilegemapId_seq",sequenceName="ROLEPRIVILEGEMAPID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "parentId")
	private Long parentId;
	
	@Column(name = "roleId")
	private String roleId;
	
	@Column(name = "userId")
	private String userId;
	
	@Column(name = "privilegeCd")
	private String privilegeCd;
	
	@Column(name = "privilegeType")
	private String privilegeType;
	
	@Column(name = "url")
	private String url;
	
	@Column(name = "icon")
	private String icon;
	
	@Column(name = "sortOrder")
	private Integer sortOrder;
	
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPrivilegeCd() {
		return privilegeCd;
	}
	public void setPrivilegeCd(String privilegeCd) {
		this.privilegeCd = privilegeCd;
	}
	public String getPrivilegeType() {
		return privilegeType;
	}
	public void setPrivilegeType(String privilegeType) {
		this.privilegeType = privilegeType;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Integer getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}
	public Long getId() {
		return id;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return RolePrivilegeMapDO.class.getSimpleName();
	}
	
}
