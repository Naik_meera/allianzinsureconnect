package com.metamorphosys.insureconnect.dataobjects.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="APPLICATION")
public class ApplicationDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="ApplicationId_seq") 
	@SequenceGenerator(name="ApplicationId_seq",sequenceName="APPLICATIONID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "key")
	private String key;
	
	@Column(name = "value")
	private String value;
	
	@Column(name = "applicationType")
	private String applicationType;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getApplicationType() {
		return applicationType;
	}
	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}
	
}
