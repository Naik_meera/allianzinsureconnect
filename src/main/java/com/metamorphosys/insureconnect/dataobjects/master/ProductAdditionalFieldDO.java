package com.metamorphosys.insureconnect.dataobjects.master;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.AdditionalFieldDO;

@Entity
@Table(name = "PRODUCTADDFLD")
public class ProductAdditionalFieldDO extends AdditionalFieldDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="productAddFldId_seq") 
	@SequenceGenerator(name="productAddFldId_seq",sequenceName="PRODUCTADDFLDID_SEQ",allocationSize=1) Long id;

	@Override
	public Long getId() {
		return id;
	}

}
