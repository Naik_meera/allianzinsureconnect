package com.metamorphosys.insureconnect.dataobjects.master;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="PROVINCE")
public class ProvinceDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="provinceId_seq") 
	@SequenceGenerator(name="provinceId_seq",sequenceName="PROVINCEID_SEQ",allocationSize=1) Long id;
	private String provinceCd;
	private String provinceName;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getProvinceCd() {
		return provinceCd;
	}
	public void setProvinceCd(String provinceCd) {
		this.provinceCd = provinceCd;
	}
	public String getProvinceName() {
		return provinceName;
	}
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
}
