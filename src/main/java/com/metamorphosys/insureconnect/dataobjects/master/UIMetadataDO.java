package com.metamorphosys.insureconnect.dataobjects.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "UIMETADATA")
public class UIMetadataDO extends BaseDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="uiMetadataId_seq") 
	@SequenceGenerator(name="uiMetadataId_seq",sequenceName="UIMETADATAID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "objectCd")
	private String objectCd;

	@Column(name = "attributeCd")
	private String attributeCd;
	
	@Column(name = "uiProperty")
	private String uiProperty;
	
	@Column(name = "uiValue")
	private String uiValue;

	@Override
	public String getObjectName() {
		return UIMetadataDO.class.getSimpleName();
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAttributeCd() {
		return attributeCd;
	}

	public void setAttributeCd(String attributeCd) {
		this.attributeCd = attributeCd;
	}

	public String getUiProperty() {
		return uiProperty;
	}

	public void setUiProperty(String uiProperty) {
		this.uiProperty = uiProperty;
	}

	public String getUiValue() {
		return uiValue;
	}

	public void setUiValue(String uiValue) {
		this.uiValue = uiValue;
	}

	public String getObjectCd() {
		return objectCd;
	}

	public void setObjectCd(String objectCd) {
		this.objectCd = objectCd;
	}

}
