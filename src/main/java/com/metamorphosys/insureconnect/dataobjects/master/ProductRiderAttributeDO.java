package com.metamorphosys.insureconnect.dataobjects.master;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "PRODUCTRIDERATTR")
public class ProductRiderAttributeDO extends BaseDO{

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="productRiderAttrId_seq") 
	@SequenceGenerator(name="productRiderAttrId_seq",sequenceName="PRODUCTRIDERATTR_SEQ",allocationSize=1) Long id;
	
	@Column(name = "attributeId")
	private String attributeId;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<ProductRiderAttributeTypeDO> productriderAttributeTypeDOlist;
	
	

	

	public List<ProductRiderAttributeTypeDO> getProductriderAttributeTypeDOlist() {
		return productriderAttributeTypeDOlist;
	}

	public void setProductriderAttributeTypeDOlist(List<ProductRiderAttributeTypeDO> productriderAttributeTypeDOlist) {
		this.productriderAttributeTypeDOlist = productriderAttributeTypeDOlist;
	}

	public String getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(String attributeId) {
		this.attributeId = attributeId;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return ProductRiderAttributeDO.class.getSimpleName();
	}

}
