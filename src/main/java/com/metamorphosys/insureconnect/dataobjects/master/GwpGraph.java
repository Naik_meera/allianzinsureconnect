package com.metamorphosys.insureconnect.dataobjects.master;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "GWPGRAPH")
public class GwpGraph {
	
	@Id 
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="gWPGraphId_seq") 
	@SequenceGenerator(name="gWPGraphId_seq",sequenceName="GWPGRAPHID_SEQ",allocationSize=1) 
	private Long id;
	
	private String agentId;
	
	private String agentName;
	
	private String currYrGWP;
	
	private String prevYrGWP;
	
	private String currYrRenewalGWP;
	
	private String prevYrRenewalGWP;
	
	private String currYrNewBizGWP;
	
	private String prevYrNewBizGWP;
	
	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCurrYrGWP() {
		return currYrGWP;
	}

	public void setCurrYrGWP(String currYrGWP) {
		this.currYrGWP = currYrGWP;
	}

	public String getPrevYrGWP() {
		return prevYrGWP;
	}

	public void setPrevYrGWP(String prevYrGWP) {
		this.prevYrGWP = prevYrGWP;
	}

	public String getCurrYrRenewalGWP() {
		return currYrRenewalGWP;
	}

	public void setCurrYrRenewalGWP(String currYrRenewalGWP) {
		this.currYrRenewalGWP = currYrRenewalGWP;
	}

	public String getPrevYrRenewalGWP() {
		return prevYrRenewalGWP;
	}

	public void setPrevYrRenewalGWP(String prevYrRenewalGWP) {
		this.prevYrRenewalGWP = prevYrRenewalGWP;
	}

	public String getCurrYrNewBizGWP() {
		return currYrNewBizGWP;
	}

	public void setCurrYrNewBizGWP(String currYrNewBizGWP) {
		this.currYrNewBizGWP = currYrNewBizGWP;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getPrevYrNewBizGWP() {
		return prevYrNewBizGWP;
	}

	public void setPrevYrNewBizGWP(String prevYrNewBizGWP) {
		this.prevYrNewBizGWP = prevYrNewBizGWP;
	}
}
