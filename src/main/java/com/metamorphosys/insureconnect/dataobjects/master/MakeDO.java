package com.metamorphosys.insureconnect.dataobjects.master;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "MAKE")
public class MakeDO {
	
	 @Id 
	 @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="makeId_seq") 
	 @SequenceGenerator(name="makeId_seq",sequenceName="MAKEID_SEQ",allocationSize=1) 
	 private Long id;
	
	 @Column(name = "versionId")
	 private long versionId;
	 
	 @Column(name = "makeCd")
	 private String makeCd;
	 
	 @Column(name = "makeDesc")
	 private String makeDesc;
	 
	 @Column(name = "sortOrder")
	 private long sortOrder;
	 
	@Column(name = "updatedDate")
	private Timestamp updatedDate;
	 
	public Timestamp getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public long getVersionId() {
		return versionId;
	}

	public void setVersionId(long versionId) {
		this.versionId = versionId;
	}

	public String getMakeCd() {
		return makeCd;
	}

	public void setMakeCd(String makeCd) {
		this.makeCd = makeCd;
	}

	public String getMakeDesc() {
		return makeDesc;
	}

	public void setMakeDesc(String makeDesc) {
		this.makeDesc = makeDesc;
	}

	public long getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(long sortOrder) {
		this.sortOrder = sortOrder;
	}

}