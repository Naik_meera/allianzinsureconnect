package com.metamorphosys.insureconnect.dataobjects.master;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CITY")
public class CityDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="cityId_seq") 
	@SequenceGenerator(name="cityId_seq",sequenceName="CITYID_SEQ",allocationSize=1) Long id;
	private String cityCd;
	private String cityName;
	private String provinceCd;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCityCd() {
		return cityCd;
	}
	public void setCityCd(String cityCd) {
		this.cityCd = cityCd;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getProvinceCd() {
		return provinceCd;
	}
	public void setProvinceCd(String provinceCd) {
		this.provinceCd = provinceCd;
	}
	
	
}
