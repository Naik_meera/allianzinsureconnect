package com.metamorphosys.insureconnect.dataobjects.master;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.SynchronizableDO;

@Entity
@Table(name = "ENTITYCONVERTOR")
public class EntityConvertorDO extends SynchronizableDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="entityConvertorId_seq") 
	@SequenceGenerator(name="entityConvertorId_seq",sequenceName="ENTITYCONVERTORID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "conversionName")
	private String conversionName;
	
	@Column(name = "conversionType")
	private String conversionType;
	
	@Column(name = "sourceClass")
	private String sourceClass;
	
	@Column(name = "destinationClass")
	private String destinationClass;
	
	@Column(name = "destinationClassName")
	private String destinationClassName;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<EntityConvertorMappingDO> entityConvertorMappingList;

	@Override
	public String getObjectName() {
		return EntityConvertorDO.class.getSimpleName();
	}

	@Override
	public Long getId() {
		return id;
	}

	public String getConversionName() {
		return conversionName;
	}

	public void setConversionName(String conversionName) {
		this.conversionName = conversionName;
	}

	public String getConversionType() {
		return conversionType;
	}

	public void setConversionType(String conversionType) {
		this.conversionType = conversionType;
	}

	public String getSourceClass() {
		return sourceClass;
	}

	public void setSourceClass(String sourceClass) {
		this.sourceClass = sourceClass;
	}

	public String getDestinationClass() {
		return destinationClass;
	}

	public void setDestinationClass(String destinationClass) {
		this.destinationClass = destinationClass;
	}

	public String getDestinationClassName() {
		return destinationClassName;
	}

	public void setDestinationClassName(String destinationClassName) {
		this.destinationClassName = destinationClassName;
	}

	public List<EntityConvertorMappingDO> getEntityConvertorMappingList() {
		return entityConvertorMappingList;
	}

	public void setEntityConvertorMappingList(List<EntityConvertorMappingDO> entityConvertorMappingList) {
		this.entityConvertorMappingList = entityConvertorMappingList;
	}

}
