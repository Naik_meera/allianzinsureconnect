package com.metamorphosys.insureconnect.dataobjects.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="PASPRODUCTMAP")
public class PASProductMapDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="pasProductMapId_seq") 
	@SequenceGenerator(name="pasProductMapId_seq",sequenceName="PASPRODUCTMAPID_SEQ",allocationSize=1) Long id;
	
	@Column(name="productCd")
	private String productCd;
	
	@Column(name="fieldKey")
	private String fieldKey;
	
	@Column(name="systemValue")
	private String systemValue;
	
	@Column(name="externalValue")
	private String externalValue;
	
	@Column(name="basePlanCd")
	private String basePlanCd;
	
	public String getBasePlanCd() {
		return basePlanCd;
	}
	public void setBasePlanCd(String basePlanCd) {
		this.basePlanCd = basePlanCd;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getProductCd() {
		return productCd;
	}
	public void setProductCd(String productCd) {
		this.productCd = productCd;
	}
	public String getFieldKey() {
		return fieldKey;
	}
	public void setFieldKey(String fieldKey) {
		this.fieldKey = fieldKey;
	}
	public String getSystemValue() {
		return systemValue;
	}
	public void setSystemValue(String systemValue) {
		this.systemValue = systemValue;
	}
	public String getExternalValue() {
		return externalValue;
	}
	public void setExternalValue(String externalValue) {
		this.externalValue = externalValue;
	}
	
}
