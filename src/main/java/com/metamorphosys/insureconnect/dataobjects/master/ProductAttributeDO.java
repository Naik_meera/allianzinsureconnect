package com.metamorphosys.insureconnect.dataobjects.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "PRODUCTATTRIBUTE")
public class ProductAttributeDO extends BaseDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="productAttributeId_seq") 
	@SequenceGenerator(name="productAttributeId_seq",sequenceName="PRODUCTATTRIBUTEID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "attributeId")
	private String attributeId;
	
	@Column(name = "attributeTypeCd")
	private String attributeTypeCd;

	@Override
	public String getObjectName() {
		return ProductAttributeDO.class.getSimpleName();
	}

	@Override
	public Long getId() {
		return id;
	}

	public String getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(String attributeId) {
		this.attributeId = attributeId;
	}

	public String getAttributeTypeCd() {
		return attributeTypeCd;
	}

	public void setAttributeTypeCd(String attributeTypeCd) {
		this.attributeTypeCd = attributeTypeCd;
	}

}
