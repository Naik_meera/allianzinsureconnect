package com.metamorphosys.insureconnect.dataobjects.master;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "RULEDATA")
public class RuleDataDO extends BaseDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="ruleDataId_seq") 
	@SequenceGenerator(name="ruleDataId_seq",sequenceName="RULEDATAID_SEQ",allocationSize=1) Long id;

	@Column(name = "condition1")
    private String condition1;
	
	@Column(name = "condition2")
    private String condition2;
	
	@Column(name = "condition3")
    private String condition3;
	
	@Column(name = "condition4")
    private String condition4;
	
	@Column(name = "condition5")
    private String condition5;
	
	@Column(name = "condition6")
    private String condition6;
	
	@Column(name = "condition7")
    private String condition7;
	
	@Column(name = "condition8")
    private String condition8;
	
	@Column(name = "condition9")
    private String condition9;
	
	@Column(name = "condition10")
    private String condition10;
    
	@Column(name = "action1")
    private String action1;
	
	@Column(name = "action2")
    private String action2;
	
	@Column(name = "action3")
    private String action3;
	
	@Column(name = "action4")
    private String action4;
	
	@Column(name = "action5")
    private String action5;

	@Column(name = "sortOrder")
    private Integer sortOrder;
	
	@Column(name = "expressionUiAction")
    private String expressionUiAction;
	
	
    
	public String getExpressionUiAction() {
		return expressionUiAction;
	}

	public void setExpressionUiAction(String expressionUiAction) {
		this.expressionUiAction = expressionUiAction;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public String getObjectName() {		
		return RuleDataDO.class.getSimpleName();
	}

	public String getCondition1() {
		return condition1;
	}

	public void setCondition1(String condition1) {
		this.condition1 = condition1;
	}

	public String getCondition2() {
		return condition2;
	}

	public void setCondition2(String condition2) {
		this.condition2 = condition2;
	}

	public String getCondition3() {
		return condition3;
	}

	public void setCondition3(String condition3) {
		this.condition3 = condition3;
	}

	public String getCondition4() {
		return condition4;
	}

	public void setCondition4(String condition4) {
		this.condition4 = condition4;
	}

	public String getCondition5() {
		return condition5;
	}

	public void setCondition5(String condition5) {
		this.condition5 = condition5;
	}

	public String getCondition6() {
		return condition6;
	}

	public void setCondition6(String condition6) {
		this.condition6 = condition6;
	}

	public String getCondition7() {
		return condition7;
	}

	public void setCondition7(String condition7) {
		this.condition7 = condition7;
	}

	public String getCondition8() {
		return condition8;
	}

	public void setCondition8(String condition8) {
		this.condition8 = condition8;
	}

	public String getCondition9() {
		return condition9;
	}

	public void setCondition9(String condition9) {
		this.condition9 = condition9;
	}

	public String getCondition10() {
		return condition10;
	}

	public void setCondition10(String condition10) {
		this.condition10 = condition10;
	}

	public String getAction1() {
		return action1;
	}

	public void setAction1(String action1) {
		this.action1 = action1;
	}

	public String getAction2() {
		return action2;
	}

	public void setAction2(String action2) {
		this.action2 = action2;
	}

	public String getAction3() {
		return action3;
	}

	public void setAction3(String action3) {
		this.action3 = action3;
	}

	public String getAction4() {
		return action4;
	}

	public void setAction4(String action4) {
		this.action4 = action4;
	}

	public String getAction5() {
		return action5;
	}

	public void setAction5(String action5) {
		this.action5 = action5;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getCondition(int i)
	{
		String condition = null;
		switch(i)
		{
		case 1:
			condition = condition1;
			break;
		case 2:
			condition = condition2;
			break;
		case 3:
			condition = condition3;
			break;
		case 4:
			condition = condition4;
			break;
		case 5:
			condition = condition5;
			break;
		default:
		}
		
		return condition;
	}

	public String getAction(int i)
	{
		String action = null;
		switch(i)
		{
		case 1:
			action = action1;
			break;
		case 2:
			action = action2;
			break;
		case 3:
			action = action3;
			break;
		case 4:
			action = action4;
			break;
		case 5:
			action = action5;
			break;
		default:
		}
		
		return action;
	}

}
