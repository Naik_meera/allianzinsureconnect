package com.metamorphosys.insureconnect.dataobjects.master;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "RULEACTION")
public class RuleActionDO extends BaseDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="ruleActionId_seq") 
	@SequenceGenerator(name="ruleActionId_seq",sequenceName="RULEACTIONID_SEQ",allocationSize=1) Long id;

	@Column(name = "actionType")
    private String actionType;
    
	@Column(name = "action")
    private String action;
    
	@Column(name = "actionOrder")
    private Integer actionOrder;
    
	@Column(name = "actionIoType")
    private String actionIoType;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public String getObjectName() {		
		return RuleActionDO.class.getSimpleName();
	}

	public Integer getActionOrder() {
		return actionOrder;
	}

	public void setActionOrder(Integer actionOrder) {
		this.actionOrder = actionOrder;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getActionIoType() {
		return actionIoType;
	}

	public void setActionIoType(String actionIoType) {
		this.actionIoType = actionIoType;
	}
	
}
