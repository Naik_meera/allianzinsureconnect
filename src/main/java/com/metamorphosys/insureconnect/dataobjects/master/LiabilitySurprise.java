package com.metamorphosys.insureconnect.dataobjects.master;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@Entity
@Table(name = "LIABILITYSURPRISE")
public class LiabilitySurprise {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "liabilitySurpriseId_seq")
	@SequenceGenerator(name = "liabilitySurpriseId_seq", sequenceName = "LIABILTYSURPRISEID_SEQ", allocationSize = 1)
	private Long id;

	private String currYrPolicies;
	private String currYrQualifyingGWP;
	private String agentId;
	private String agentName;
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCurrYrPolicies() {
		return currYrPolicies;
	}
	public void setCurrYrPolicies(String currYrPolicies) {
		this.currYrPolicies = currYrPolicies;
	}
	public String getCurrYrQualifyingGWP() {
		return currYrQualifyingGWP;
	}
	public void setCurrYrQualifyingGWP(String currYrQualifyingGWP) {
		this.currYrQualifyingGWP = currYrQualifyingGWP;
	}
	
}
