package com.metamorphosys.insureconnect.dataobjects.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "ENTITYCONVERTORMAPPING")
public class EntityConvertorMappingDO extends BaseDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="entityConvertorMappingId_seq") 
	@SequenceGenerator(name="entityConvertorMappingId_seq",sequenceName="ENTITYCONVERTORMAPPINGID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "sourceField")
	private String sourceField;
	
	@Column(name = "destinationField")
	private String destinationField;
	
	@Column(name = "transformationType")
	private String transformationType;
	
	@Column(name = "transformationFunction")
	private String transformationFunction;

	@Override
	public String getObjectName() {
		return EntityConvertorMappingDO.class.getSimpleName();
	}

	@Override
	public Long getId() {
		return id;
	}

	public String getSourceField() {
		return sourceField;
	}

	public void setSourceField(String sourceField) {
		this.sourceField = sourceField;
	}

	public String getDestinationField() {
		return destinationField;
	}

	public void setDestinationField(String destinationField) {
		this.destinationField = destinationField;
	}

	public String getTransformationType() {
		return transformationType;
	}

	public void setTransformationType(String transformationType) {
		this.transformationType = transformationType;
	}

	public String getTransformationFunction() {
		return transformationFunction;
	}

	public void setTransformationFunction(String transformationFunction) {
		this.transformationFunction = transformationFunction;
	}

}
