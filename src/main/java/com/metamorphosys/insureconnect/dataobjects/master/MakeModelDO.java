package com.metamorphosys.insureconnect.dataobjects.master;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "MAKEMODEL")
public class MakeModelDO {//extends SynchronizableDO 
	public final static int ACTIVE=1;
	public final static int INACTIVE=0;
	 @Id 
	 @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="makeModelId_seq") 
	 @SequenceGenerator(name="makeModelId_seq",sequenceName="MAKEMODELID_SEQ",allocationSize=1) 
	 private Long id;
	
//	 @Column(name = "versionId")
//	 private Integer versionId;
	 
	 @Column(name = "makeCd")
	 private String makeCd;
	 
	 @Column(name = "makeName")
	 private String makeName;
	 
	 @Column(name = "modelCd")
	 private String modelCd;
	 
	 @Column(name = "modelName")
	 private String modelName;
	 
	 @Column(name = "seatingCapacity")
	 private String seatingCapacity;
	 
	 @Column(name = "sortOrder")
	 private long sortOrder;
	 
	 @Column(name = "statusCd")
	 private long statusCd;
	 
	@Column(name = "updatedDate")
	private Timestamp updatedDate;
	
	 @Column(name = "guid")
	 private String guid;
	

	public Timestamp getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMakeCd() {
		return makeCd;
	}

	public void setMakeCd(String makeCd) {
		this.makeCd = makeCd;
	}

	public String getMakeName() {
		return makeName;
	}

	public void setMakeName(String makeName) {
		this.makeName = makeName;
	}

	public String getModelCd() {
		return modelCd;
	}

	public long getStatusCd() {
		return statusCd;
	}

	public void setStatusCd(long statusCd) {
		this.statusCd = statusCd;
	}

	public void setModelCd(String modelCd) {
		this.modelCd = modelCd;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getSeatingCapacity() {
		return seatingCapacity;
	}

	public void setSeatingCapacity(String seatingCapacity) {
		this.seatingCapacity = seatingCapacity;
	}

	public long getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(long sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}
	 
}