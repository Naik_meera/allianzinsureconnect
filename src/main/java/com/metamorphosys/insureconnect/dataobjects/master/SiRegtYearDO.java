package com.metamorphosys.insureconnect.dataobjects.master;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "SI_REGTYEAR")
public class SiRegtYearDO {	

	public final static int ACTIVE=1;
	public final static int INACTIVE=0;
	
	 @Id 
	 @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="siRegtYearId_seq") 
	 @SequenceGenerator(name="siRegtYearId_seq",sequenceName="SIREGTYEARID_SEQ",allocationSize=1) 
	 private Long id;
	
	 @Column(name = "versionId")
	 private long versionId;
	 
	 @Column(name = "makeCd")
	 private String makeCd;
	 
	 @Column(name = "modelCd")
	 private String modelCd;
	 
	 @Column(name = "variantCd")
	 private String variantCd;
	 
	 @Column(name = "regtYear")
	 private String regtYear;
	 
	 @Column(name = "sumInsured")
	 private String sumInsured;
	 
	 @Column(name = "guid")
	 private String guid;
	 
	 @Column(name = "statusCd")
	 private long statusCd;
	
	@Column(name = "updatedDate")
	private Timestamp updatedDate;
	
	 @Column(name = "vehiclekey")
	 private String vehiclekey;

	public String getVehiclekey() {
		return vehiclekey;
	}

	public void setVehiclekey(String vehiclekey) {
		this.vehiclekey = vehiclekey;
	}

	public Timestamp getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}
	 
	 public long getStatusCd() {
		return statusCd;
	}

	public void setStatusCd(long statusCd) {
		this.statusCd = statusCd;
	}

	public String getGuid() {
		 return guid;
	 }

	 public void setGuid(String guid) {
		 this.guid = guid;
	 }
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public long getVersionId() {
		return versionId;
	}

	public void setVersionId(long versionId) {
		this.versionId = versionId;
	}

	public String getMakeCd() {
		return makeCd;
	}

	public void setMakeCd(String makeCd) {
		this.makeCd = makeCd;
	}

	public String getModelCd() {
		return modelCd;
	}

	public void setModelCd(String modelCd) {
		this.modelCd = modelCd;
	}

	public String getVariantCd() {
		return variantCd;
	}

	public void setVariantCd(String variantCd) {
		this.variantCd = variantCd;
	}

	public String getRegtYear() {
		return regtYear;
	}

	public void setRegtYear(String regtYear) {
		this.regtYear = regtYear;
	}

	public String getSumInsured() {
		return sumInsured;
	}

	public void setSumInsured(String sumInsured) {
		this.sumInsured = sumInsured;
	}
}
