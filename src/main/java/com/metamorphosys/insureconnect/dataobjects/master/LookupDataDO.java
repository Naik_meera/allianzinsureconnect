package com.metamorphosys.insureconnect.dataobjects.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.SynchronizableDO;

@Entity
@Table(name = "LOOKUPDATA")
public class LookupDataDO extends SynchronizableDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="lookupDataId_seq") 
	@SequenceGenerator(name="lookupDataId_seq",sequenceName="LOOKUPDATAID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "lookupCd")
	private String lookupCd;
	
	@Column(name = "lookupKey")
	private String lookupKey;
	
	@Column(name = "lookupValue")
	private String lookupValue;
	
	@Column(name = "sortOrder")
	private Integer sortOrder;

	@Override
	public String getObjectName() {
		return LookupDataDO.class.getSimpleName();
	}

	@Override
	public Long getId() {
		return id;
	}

	public String getLookupCd() {
		return lookupCd;
	}

	public void setLookupCd(String lookupCd) {
		this.lookupCd = lookupCd;
	}

	public String getLookupKey() {
		return lookupKey;
	}

	public void setLookupKey(String lookupKey) {
		this.lookupKey = lookupKey;
	}

	public String getLookupValue() {
		return lookupValue;
	}

	public void setLookupValue(String lookupValue) {
		this.lookupValue = lookupValue;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

}
