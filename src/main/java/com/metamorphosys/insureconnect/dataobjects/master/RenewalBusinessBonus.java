package com.metamorphosys.insureconnect.dataobjects.master;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@Entity
@Table(name = "RENEWALBUSINESSBONUS")
public class RenewalBusinessBonus {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "renewalBusinessBonusId_seq")
	@SequenceGenerator(name = "renewalBusinessBonusId_seq", sequenceName = "RENEWALBUSINESSBONUSID_SEQ", allocationSize = 1)
	private Long id;

	private String currYrGWP;
	private String currYrRenewalGWP;
	private String currYrLRMotor;
	private String currYrLRProperty;
	private String currYrLRLiability;
	private String currYrLRTravel;
	private String currYrLRPA;
	private String currYrLRMarine;
	private String currYrLREng;
	private String currYrEstBonus;
	private String agentId;
	private String agentName;
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCurrYrGWP() {
		return currYrGWP;
	}
	public void setCurrYrGWP(String currYrGWP) {
		this.currYrGWP = currYrGWP;
	}
	public String getCurrYrRenewalGWP() {
		return currYrRenewalGWP;
	}
	public void setCurrYrRenewalGWP(String currYrRenewalGWP) {
		this.currYrRenewalGWP = currYrRenewalGWP;
	}
	public String getCurrYrLRMotor() {
		return currYrLRMotor;
	}
	public void setCurrYrLRMotor(String currYrLRMotor) {
		this.currYrLRMotor = currYrLRMotor;
	}
	public String getCurrYrLRProperty() {
		return currYrLRProperty;
	}
	public void setCurrYrLRProperty(String currYrLRProperty) {
		this.currYrLRProperty = currYrLRProperty;
	}
	public String getCurrYrLRLiability() {
		return currYrLRLiability;
	}
	public void setCurrYrLRLiability(String currYrLRLiability) {
		this.currYrLRLiability = currYrLRLiability;
	}
	public String getCurrYrLRTravel() {
		return currYrLRTravel;
	}
	public void setCurrYrLRTravel(String currYrLRTravel) {
		this.currYrLRTravel = currYrLRTravel;
	}
	public String getCurrYrLRPA() {
		return currYrLRPA;
	}
	public void setCurrYrLRPA(String currYrLRPA) {
		this.currYrLRPA = currYrLRPA;
	}
	public String getCurrYrLRMarine() {
		return currYrLRMarine;
	}
	public void setCurrYrLRMarine(String currYrLRMarine) {
		this.currYrLRMarine = currYrLRMarine;
	}
	public String getCurrYrLREng() {
		return currYrLREng;
	}
	public void setCurrYrLREng(String currYrLREng) {
		this.currYrLREng = currYrLREng;
	}
	public String getCurrYrEstBonus() {
		return currYrEstBonus;
	}
	public void setCurrYrEstBonus(String currYrEstBonus) {
		this.currYrEstBonus = currYrEstBonus;
	}
}
