package com.metamorphosys.insureconnect.dataobjects.master;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "LOBSUMMARY")
public class LoBSummary {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "loBSummaryId_seq")
	@SequenceGenerator(name = "loBSummaryId_seq", sequenceName = "LOBSUMMARYID_SEQ", allocationSize = 1)
	private Long id;
	private String currYrMotorGWP;
	
	private String prevYrMotorGWP;
	
	private String currYrLRMotor;
	
	private String currYrAvgMotorCommission;
	
	private String currYrPropertyGWP;
	
	private String prevYrPropertyGWP;
	
	private String currYrLRProperty;
	
	private String currYrAvgPropertyCommission;
	
	private String currYrPAHealthGWP;
	
	private String prevYrPAHealthGWP;
	
	private String currYrLRPAHealth;
	
	private String currYrAvgPAHCommission;
	
	private String currYrTravelGWP;
	
	private String prevYrTravelGWP;
	
	private String currYrLRTravel;
	
	private String currYrAvgTravelCommission;
	
	private String currYrLiabilityGWP;
	
	private String prevYrLiabilityGWP;
	
	private String currYrLRLiability;
	
	private String currYrAvgLiabCommission;
	
	private String currYrMarineGWP;
	
	private String prevYrMarineGWP;
	
	private String currYrLRMarine;
	
	private String currYrAvgMarineCommission;
	
	private String currYrEngGWP;
	
	private String prevYrEngGWP;
	
	private String currYrLREng;
	
	private String currYrAvgEngCommission;
	
	private String agentId;
	private String agentName;
	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCurrYrMotorGWP() {
		return currYrMotorGWP;
	}

	public void setCurrYrMotorGWP(String currYrMotorGWP) {
		this.currYrMotorGWP = currYrMotorGWP;
	}

	public String getPrevYrMotorGWP() {
		return prevYrMotorGWP;
	}

	public void setPrevYrMotorGWP(String prevYrMotorGWP) {
		this.prevYrMotorGWP = prevYrMotorGWP;
	}

	public String getCurrYrLRMotor() {
		return currYrLRMotor;
	}

	public void setCurrYrLRMotor(String currYrLRMotor) {
		this.currYrLRMotor = currYrLRMotor;
	}

	public String getCurrYrAvgMotorCommission() {
		return currYrAvgMotorCommission;
	}

	public void setCurrYrAvgMotorCommission(String currYrAvgMotorCommission) {
		this.currYrAvgMotorCommission = currYrAvgMotorCommission;
	}

	public String getCurrYrPropertyGWP() {
		return currYrPropertyGWP;
	}

	public void setCurrYrPropertyGWP(String currYrPropertyGWP) {
		this.currYrPropertyGWP = currYrPropertyGWP;
	}

	public String getPrevYrPropertyGWP() {
		return prevYrPropertyGWP;
	}

	public void setPrevYrPropertyGWP(String prevYrPropertyGWP) {
		this.prevYrPropertyGWP = prevYrPropertyGWP;
	}

	public String getCurrYrLRProperty() {
		return currYrLRProperty;
	}

	public void setCurrYrLRProperty(String currYrLRProperty) {
		this.currYrLRProperty = currYrLRProperty;
	}

	public String getCurrYrAvgPropertyCommission() {
		return currYrAvgPropertyCommission;
	}

	public void setCurrYrAvgPropertyCommission(String currYrAvgPropertyCommission) {
		this.currYrAvgPropertyCommission = currYrAvgPropertyCommission;
	}

	public String getCurrYrPAHealthGWP() {
		return currYrPAHealthGWP;
	}

	public void setCurrYrPAHealthGWP(String currYrPAHealthGWP) {
		this.currYrPAHealthGWP = currYrPAHealthGWP;
	}

	public String getPrevYrPAHealthGWP() {
		return prevYrPAHealthGWP;
	}

	public void setPrevYrPAHealthGWP(String prevYrPAHealthGWP) {
		this.prevYrPAHealthGWP = prevYrPAHealthGWP;
	}

	public String getCurrYrLRPAHealth() {
		return currYrLRPAHealth;
	}

	public void setCurrYrLRPAHealth(String currYrLRPAHealth) {
		this.currYrLRPAHealth = currYrLRPAHealth;
	}

	public String getCurrYrAvgPAHCommission() {
		return currYrAvgPAHCommission;
	}

	public void setCurrYrAvgPAHCommission(String currYrAvgPAHCommission) {
		this.currYrAvgPAHCommission = currYrAvgPAHCommission;
	}

	public String getCurrYrTravelGWP() {
		return currYrTravelGWP;
	}

	public void setCurrYrTravelGWP(String currYrTravelGWP) {
		this.currYrTravelGWP = currYrTravelGWP;
	}

	public String getPrevYrTravelGWP() {
		return prevYrTravelGWP;
	}

	public void setPrevYrTravelGWP(String prevYrTravelGWP) {
		this.prevYrTravelGWP = prevYrTravelGWP;
	}

	public String getCurrYrLRTravel() {
		return currYrLRTravel;
	}

	public void setCurrYrLRTravel(String currYrLRTravel) {
		this.currYrLRTravel = currYrLRTravel;
	}

	public String getCurrYrAvgTravelCommission() {
		return currYrAvgTravelCommission;
	}

	public void setCurrYrAvgTravelCommission(String currYrAvgTravelCommission) {
		this.currYrAvgTravelCommission = currYrAvgTravelCommission;
	}

	public String getCurrYrLiabilityGWP() {
		return currYrLiabilityGWP;
	}

	public void setCurrYrLiabilityGWP(String currYrLiabilityGWP) {
		this.currYrLiabilityGWP = currYrLiabilityGWP;
	}

	public String getPrevYrLiabilityGWP() {
		return prevYrLiabilityGWP;
	}

	public void setPrevYrLiabilityGWP(String prevYrLiabilityGWP) {
		this.prevYrLiabilityGWP = prevYrLiabilityGWP;
	}

	public String getCurrYrLRLiability() {
		return currYrLRLiability;
	}

	public void setCurrYrLRLiability(String currYrLRLiability) {
		this.currYrLRLiability = currYrLRLiability;
	}

	public String getCurrYrAvgLiabCommission() {
		return currYrAvgLiabCommission;
	}

	public void setCurrYrAvgLiabCommission(String currYrAvgLiabCommission) {
		this.currYrAvgLiabCommission = currYrAvgLiabCommission;
	}

	public String getCurrYrMarineGWP() {
		return currYrMarineGWP;
	}

	public void setCurrYrMarineGWP(String currYrMarineGWP) {
		this.currYrMarineGWP = currYrMarineGWP;
	}

	public String getPrevYrMarineGWP() {
		return prevYrMarineGWP;
	}

	public void setPrevYrMarineGWP(String prevYrMarineGWP) {
		this.prevYrMarineGWP = prevYrMarineGWP;
	}

	public String getCurrYrLRMarine() {
		return currYrLRMarine;
	}

	public void setCurrYrLRMarine(String currYrLRMarine) {
		this.currYrLRMarine = currYrLRMarine;
	}

	public String getCurrYrAvgMarineCommission() {
		return currYrAvgMarineCommission;
	}

	public void setCurrYrAvgMarineCommission(String currYrAvgMarineCommission) {
		this.currYrAvgMarineCommission = currYrAvgMarineCommission;
	}

	public String getCurrYrEngGWP() {
		return currYrEngGWP;
	}

	public void setCurrYrEngGWP(String currYrEngGWP) {
		this.currYrEngGWP = currYrEngGWP;
	}

	public String getPrevYrEngGWP() {
		return prevYrEngGWP;
	}

	public void setPrevYrEngGWP(String prevYrEngGWP) {
		this.prevYrEngGWP = prevYrEngGWP;
	}

	public String getCurrYrLREng() {
		return currYrLREng;
	}

	public void setCurrYrLREng(String currYrLREng) {
		this.currYrLREng = currYrLREng;
	}

	public String getCurrYrAvgEngCommission() {
		return currYrAvgEngCommission;
	}

	public void setCurrYrAvgEngCommission(String currYrAvgEngCommission) {
		this.currYrAvgEngCommission = currYrAvgEngCommission;
	}
	
}
