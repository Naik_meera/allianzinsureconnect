package com.metamorphosys.insureconnect.dataobjects.master;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@Entity
@Table(name = "DATASYNCDETAILS")
public class DataSyncDetailsDO {
	
	 @Id 
	 @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="dataSyncDetailsId_seq") 
	 @SequenceGenerator(name="dataSyncDetailsId_seq",sequenceName="DATASYNCDETAILSID_SEQ",allocationSize=1) 
	 private Long id;
	
	@Column(name = "lastUpdatedDate")
	private Timestamp lastUpdatedDate;

	public Timestamp getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Timestamp lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	
}
