package com.metamorphosys.insureconnect.dataobjects.master;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name ="VEHICLEMASTERDUMP")
public class VehicleMasterDumpDO {
	
	public final static int ACTIVE=1;
	public final static int INACTIVE=0;
	public final static String TRUCK="TR";
	public final static String NONTRUCK="NTR";
	public final static String VEHICLECLASSSPORTS="1";
	public final static String VEHICLECLASSREGULAR="2";
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="vehicleMasterDumpId_seq") 
	@SequenceGenerator(name="vehicleMasterDumpId_seq",sequenceName="VEHICLEMASTERDUMPID_SEQ",allocationSize=1) 
	private Long id;
	
	@Column(name = "vehicleCode")
	private String vehicleCode;
	
	@Column(name = "year")
	private String year;
	
	@Column(name = "brand")
	private String brand;
	
	@Column(name = "model")
	private String model;
	
	@Column(name = "variant")
	private String variant;
	
	@Column(name = "info")
	private String info;
	
	@Column(name = "seat")
	private String seat;

	@Column(name = "sumInsured")
	private String sumInsured;
	
	@Column(name = "codeAlias")
	private String codeAlias;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "vehicleSubtype")
	private String vehicleSubtype;
	
	@Column(name = "vehicleClass")
	private String vehicleClass;
	
	@Column(name = "updatedDate")
	private Timestamp updatedDate;
	
	 @Column(name = "guid")
	 private String guid;
	
	 @Column(name = "statusCd")
	 private long statusCd;
	 
	public String getVehicleCode() {
		return vehicleCode;
	}
	public void setVehicleCode(String vehicleCode) {
		this.vehicleCode = vehicleCode;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getVariant() {
		return variant;
	}
	public void setVariant(String variant) {
		this.variant = variant;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public String getSeat() {
		return seat;
	}
	public void setSeat(String seat) {
		this.seat = seat;
	}
	public String getSumInsured() {
		return sumInsured;
	}
	public void setSumInsured(String sumInsured) {
		this.sumInsured = sumInsured;
	}
	
	public String getCodeAlias() {
		return codeAlias;
	}
	public void setCodeAlias(String codeAlias) {
		this.codeAlias = codeAlias;
	}
	public String getVehicleSubtype() {
		return vehicleSubtype;
	}
	public void setVehicleSubtype(String vehicleSubtype) {
		this.vehicleSubtype = vehicleSubtype;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public String getVehicleClass() {
		return vehicleClass;
	}
	public void setVehicleClass(String vehicleClass) {
		this.vehicleClass = vehicleClass;
	}
	public Timestamp getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}
	public long getStatusCd() {
		return statusCd;
	}
	public void setStatusCd(long statusCd) {
		this.statusCd = statusCd;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
}