package com.metamorphosys.insureconnect.dataobjects.master;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@Entity
@Table(name = "INCOMECHART")
public class IncomeChart {
	
	@Id 
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="incomeChartId_seq") 
	@SequenceGenerator(name="incomeChartId_seq",sequenceName="INCOMECHARTID_SEQ",allocationSize=1) 
	private Long id;
	
	private String agentId;
	
	private String agentName;
	
	private String currYrTotalIncome;
	
	private String prevYrTotalIncome;
	
	private String currYrCommission;
	
	private String prevYrCommission;
	
	private String currYrCashBonus;
	
	private String prevYrCashBonus;
	
	private String currYrJanIncome;
	
	private String currYrFebIncome;
	
	private String currYrMarIncome;
	
	private String currYrAprIncome;
	
	private String currYrMayIncome;
	
	private String currYrJunIncome;
	
	private String currYrJulIncome;
	
	private String currYrAugIncome;
	
	private String currYrSepIncome;
	
	private String currYrOctIncome;
	
	private String currYrNovIncome;
	
	private String currYrDecIncome;
	
	private String prevYrJanIncome;
	
	private String prevYrFebIncome;
	
	private String prevYrMarIncome;
	
	private String prevYrAprIncome;
	
	private String prevYrMayIncome;
	
	private String prevYrJunIncome;
	
	private String prevYrJulIncome;
	
	private String prevYrAugIncome;
	
	private String prevYrSepIncome;
	
	private String prevYrOctIncome;
	
	private String prevYrNovIncome;
	
	private String prevYrDecIncome;

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCurrYrTotalIncome() {
		return currYrTotalIncome;
	}

	public void setCurrYrTotalIncome(String currYrTotalIncome) {
		this.currYrTotalIncome = currYrTotalIncome;
	}

	public String getPrevYrTotalIncome() {
		return prevYrTotalIncome;
	}

	public void setPrevYrTotalIncome(String prevYrTotalIncome) {
		this.prevYrTotalIncome = prevYrTotalIncome;
	}

	public String getCurrYrCommission() {
		return currYrCommission;
	}

	public void setCurrYrCommission(String currYrCommission) {
		this.currYrCommission = currYrCommission;
	}

	public String getPrevYrCommission() {
		return prevYrCommission;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public void setPrevYrCommission(String prevYrCommission) {
		this.prevYrCommission = prevYrCommission;
	}

	public String getCurrYrCashBonus() {
		return currYrCashBonus;
	}

	public void setCurrYrCashBonus(String currYrCashBonus) {
		this.currYrCashBonus = currYrCashBonus;
	}

	public String getPrevYrCashBonus() {
		return prevYrCashBonus;
	}

	public void setPrevYrCashBonus(String prevYrCashBonus) {
		this.prevYrCashBonus = prevYrCashBonus;
	}

	public String getCurrYrJanIncome() {
		return currYrJanIncome;
	}

	public void setCurrYrJanIncome(String currYrJanIncome) {
		this.currYrJanIncome = currYrJanIncome;
	}

	public String getCurrYrFebIncome() {
		return currYrFebIncome;
	}

	public void setCurrYrFebIncome(String currYrFebIncome) {
		this.currYrFebIncome = currYrFebIncome;
	}

	public String getCurrYrMarIncome() {
		return currYrMarIncome;
	}

	public void setCurrYrMarIncome(String currYrMarIncome) {
		this.currYrMarIncome = currYrMarIncome;
	}

	public String getCurrYrAprIncome() {
		return currYrAprIncome;
	}

	public void setCurrYrAprIncome(String currYrAprIncome) {
		this.currYrAprIncome = currYrAprIncome;
	}

	public String getCurrYrMayIncome() {
		return currYrMayIncome;
	}

	public void setCurrYrMayIncome(String currYrMayIncome) {
		this.currYrMayIncome = currYrMayIncome;
	}

	public String getCurrYrJunIncome() {
		return currYrJunIncome;
	}

	public void setCurrYrJunIncome(String currYrJunIncome) {
		this.currYrJunIncome = currYrJunIncome;
	}

	public String getCurrYrJulIncome() {
		return currYrJulIncome;
	}

	public void setCurrYrJulIncome(String currYrJulIncome) {
		this.currYrJulIncome = currYrJulIncome;
	}

	public String getCurrYrAugIncome() {
		return currYrAugIncome;
	}

	public void setCurrYrAugIncome(String currYrAugIncome) {
		this.currYrAugIncome = currYrAugIncome;
	}

	public String getCurrYrSepIncome() {
		return currYrSepIncome;
	}

	public void setCurrYrSepIncome(String currYrSepIncome) {
		this.currYrSepIncome = currYrSepIncome;
	}

	public String getCurrYrOctIncome() {
		return currYrOctIncome;
	}

	public void setCurrYrOctIncome(String currYrOctIncome) {
		this.currYrOctIncome = currYrOctIncome;
	}

	public String getCurrYrNovIncome() {
		return currYrNovIncome;
	}

	public void setCurrYrNovIncome(String currYrNovIncome) {
		this.currYrNovIncome = currYrNovIncome;
	}

	public String getCurrYrDecIncome() {
		return currYrDecIncome;
	}

	public void setCurrYrDecIncome(String currYrDecIncome) {
		this.currYrDecIncome = currYrDecIncome;
	}

	public String getPrevYrJanIncome() {
		return prevYrJanIncome;
	}

	public void setPrevYrJanIncome(String prevYrJanIncome) {
		this.prevYrJanIncome = prevYrJanIncome;
	}

	public String getPrevYrFebIncome() {
		return prevYrFebIncome;
	}

	public void setPrevYrFebIncome(String prevYrFebIncome) {
		this.prevYrFebIncome = prevYrFebIncome;
	}

	public String getPrevYrMarIncome() {
		return prevYrMarIncome;
	}

	public void setPrevYrMarIncome(String prevYrMarIncome) {
		this.prevYrMarIncome = prevYrMarIncome;
	}

	public String getPrevYrAprIncome() {
		return prevYrAprIncome;
	}

	public void setPrevYrAprIncome(String prevYrAprIncome) {
		this.prevYrAprIncome = prevYrAprIncome;
	}

	public String getPrevYrMayIncome() {
		return prevYrMayIncome;
	}

	public void setPrevYrMayIncome(String prevYrMayIncome) {
		this.prevYrMayIncome = prevYrMayIncome;
	}

	public String getPrevYrJunIncome() {
		return prevYrJunIncome;
	}

	public void setPrevYrJunIncome(String prevYrJunIncome) {
		this.prevYrJunIncome = prevYrJunIncome;
	}

	public String getPrevYrJulIncome() {
		return prevYrJulIncome;
	}

	public void setPrevYrJulIncome(String prevYrJulIncome) {
		this.prevYrJulIncome = prevYrJulIncome;
	}

	public String getPrevYrAugIncome() {
		return prevYrAugIncome;
	}

	public void setPrevYrAugIncome(String prevYrAugIncome) {
		this.prevYrAugIncome = prevYrAugIncome;
	}

	public String getPrevYrSepIncome() {
		return prevYrSepIncome;
	}

	public void setPrevYrSepIncome(String prevYrSepIncome) {
		this.prevYrSepIncome = prevYrSepIncome;
	}

	public String getPrevYrOctIncome() {
		return prevYrOctIncome;
	}

	public void setPrevYrOctIncome(String prevYrOctIncome) {
		this.prevYrOctIncome = prevYrOctIncome;
	}

	public String getPrevYrNovIncome() {
		return prevYrNovIncome;
	}

	public void setPrevYrNovIncome(String prevYrNovIncome) {
		this.prevYrNovIncome = prevYrNovIncome;
	}

	public String getPrevYrDecIncome() {
		return prevYrDecIncome;
	}

	public void setPrevYrDecIncome(String prevYrDecIncome) {
		this.prevYrDecIncome = prevYrDecIncome;
	}
	
	
	
}
