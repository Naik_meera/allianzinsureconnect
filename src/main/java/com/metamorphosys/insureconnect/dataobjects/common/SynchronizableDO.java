package com.metamorphosys.insureconnect.dataobjects.common;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class SynchronizableDO extends BaseDO {

	@Column(name = "systemCreatedDt")
	public Timestamp systemCreatedDt;
	
	@Column(name = "systemUpdatedDt")
	public Timestamp systemUpdatedDt;
	
	@Column(name = "onlineCreatedDt")
	public Timestamp onlineCreatedDt;
	
	@Column(name = "onlineUpdatedDt")
	public Timestamp onlineUpdatedDt;
	
	@Column(name = "systemCreatedUser")
	public String systemCreatedUser;
	
	@Column(name = "systemUpdatedUser")
	public String systemUpdatedUser;
	
	public Timestamp getSystemCreatedDt() {
		return systemCreatedDt;
	}
	public void setSystemCreatedDt(Timestamp systemCreatedDt) {
		this.systemCreatedDt = systemCreatedDt;
	}
	public Timestamp getSystemUpdatedDt() {
		return systemUpdatedDt;
	}
	public void setSystemUpdatedDt(Timestamp systemUpdatedDt) {
		this.systemUpdatedDt = systemUpdatedDt;
	}
	public Timestamp getOnlineCreatedDt() {
		return onlineCreatedDt;
	}
	public void setOnlineCreatedDt(Timestamp onlineCreatedDt) {
		this.onlineCreatedDt = onlineCreatedDt;
	}
	public Timestamp getOnlineUpdatedDt() {
		return onlineUpdatedDt;
	}
	public void setOnlineUpdatedDt(Timestamp onlineUpdatedDt) {
		this.onlineUpdatedDt = onlineUpdatedDt;
	}
	public String getSystemCreatedUser() {
		return systemCreatedUser;
	}
	public void setSystemCreatedUser(String systemCreatedUser) {
		this.systemCreatedUser = systemCreatedUser;
	}
	public String getSystemUpdatedUser() {
		return systemUpdatedUser;
	}
	public void setSystemUpdatedUser(String systemUpdatedUser) {
		this.systemUpdatedUser = systemUpdatedUser;
	}
	
}
