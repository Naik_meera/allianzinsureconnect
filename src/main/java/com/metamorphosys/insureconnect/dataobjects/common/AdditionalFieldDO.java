package com.metamorphosys.insureconnect.dataobjects.common;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import com.metamorphosys.insureconnect.dataobjects.transaction.QuotationAdditionalFieldDO;

@MappedSuperclass
public abstract class AdditionalFieldDO extends BaseDO {

	@Column(name = "fieldKey")
	private String fieldKey;
	
	@Column(name = "fieldValue")
	private String fieldValue;

	public String getFieldKey() {
		return fieldKey;
	}

	public void setFieldKey(String fieldKey) {
		this.fieldKey = fieldKey;
	}

	public String getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}
	
	@Override
	public String getObjectName() {
		return fieldKey;
	}

}
