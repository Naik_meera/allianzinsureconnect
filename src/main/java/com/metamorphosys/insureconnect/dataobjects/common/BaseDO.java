package com.metamorphosys.insureconnect.dataobjects.common;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@MappedSuperclass
public abstract class BaseDO {

	@Transient
	private BaseDO parentObject;
	
	@Column(name = "versionId")
	public Integer versionId;
	
	@Column(name = "guid")
	public String guid;
	
	@Column(name = "offlineGuid")
	public String offlineGuid ;
	
	@Column(name = "agentDeviceSeq")
	public Integer agentDeviceSeq;
	
	public void setParentObject(BaseDO parentObject)
	{
		this.parentObject = parentObject;
	}
	
	public BaseDO getParentObject()
	{
		return this.parentObject;
	}
	
	public Integer getVersionId() {
		return versionId;
	}

	public void setVersionId(Integer versionId) {
		this.versionId = versionId;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getOfflineGuid() {
		return offlineGuid;
	}

	public void setOfflineGuid(String offlineGuid) {
		this.offlineGuid = offlineGuid;
	}

	public Integer getAgentDeviceSeq() {
		return agentDeviceSeq;
	}

	public void setAgentDeviceSeq(Integer agentDeviceSeq) {
		this.agentDeviceSeq = agentDeviceSeq;
	}

	public abstract Long getId();
	
	public abstract String getObjectName();
	
}
