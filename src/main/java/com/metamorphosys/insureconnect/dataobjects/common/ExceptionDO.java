package com.metamorphosys.insureconnect.dataobjects.common;

import java.util.HashMap;

import javax.persistence.Column;

public class ExceptionDO extends BaseDO {
	@Column(name = "exceptionCd")
	private String exceptionCd;

	@Column(name = "exceptionMessage")
	private String exceptionMessage;
	
	@Column(name = "referenceObject")
	private String referenceObject;
	
	@Column(name = "referenceAttribute")
	private String referenceAttribute;
	
	
		
	public String getExceptionCd() {
		return exceptionCd;
	}

	public void setExceptionCd(String exceptionCd) {
		this.exceptionCd = exceptionCd;
	}

	public String getExceptionMessage() {
		return exceptionMessage;
	}

	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}

	public String getReferenceObject() {
		return referenceObject;
	}

	public void setReferenceObject(String referenceObject) {
		this.referenceObject = referenceObject;
	}

	public String getReferenceAttribute() {
		return referenceAttribute;
	}

	public void setReferenceAttribute(String referenceAttribute) {
		this.referenceAttribute = referenceAttribute;
	}

	@Override
	public String getObjectName() {		
		return ExceptionDO.class.getSimpleName();
	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return null;
	}

}
