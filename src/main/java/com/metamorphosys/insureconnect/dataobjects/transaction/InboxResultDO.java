package com.metamorphosys.insureconnect.dataobjects.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "INBOXRESULT")
public class InboxResultDO extends BaseDO {
	
	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="inboxResultId_seq") 
	@SequenceGenerator(name="inboxResultId_seq",sequenceName="INBOXRESULTID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "resultfield")
	private String resultfield;
	
	@Column(name = "resultorder")
	private Integer resultorder;
	
	@Column(name = "link")
	private String link;
	
	public String getResultfield() {
		return resultfield;
	}

	public void setResultfield(String resultfield) {
		this.resultfield = resultfield;
	}

	public Integer getResultorder() {
		return resultorder;
	}

	public void setResultorder(Integer resultorder) {
		this.resultorder = resultorder;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return InboxResultDO.class.getSimpleName();
	}

}
