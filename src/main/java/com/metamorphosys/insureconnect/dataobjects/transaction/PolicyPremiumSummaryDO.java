package com.metamorphosys.insureconnect.dataobjects.transaction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "POLICYPREMIUMSUMMARY")
public class PolicyPremiumSummaryDO extends BaseDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="policyPremiumSummaryId_seq")
	@SequenceGenerator(name="policyPremiumSummaryId_seq",sequenceName="POLICYPREMIUMSUMMARYID_SEQ",allocationSize=1) Long id;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return PolicyPremiumSummaryDO.class.getSimpleName();
	}
}
