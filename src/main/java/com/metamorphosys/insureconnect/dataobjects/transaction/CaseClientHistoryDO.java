package com.metamorphosys.insureconnect.dataobjects.transaction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "CASECLIENTHISTORY")
public class CaseClientHistoryDO extends BaseDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="caseClientHistoryId_seq") 
	@SequenceGenerator(name="caseClientHistoryId_seq",sequenceName="CASECLIENTHISTORYID_SEQ",allocationSize=1) Long id;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return CaseClientHistoryDO.class.getSimpleName();
	}
	
}
