package com.metamorphosys.insureconnect.dataobjects.transaction;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

public class ServiceDO extends BaseDO {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String service;
	private String serviceType;
	private Date serviceDt;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<ServiceTaskDO> serviceTaskList = new ArrayList<ServiceTaskDO>(); //Not used

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public Date getServiceDt() {
		return serviceDt;
	}

	public void setServiceDt(Date serviceDt) {
		this.serviceDt = serviceDt;
	}

	public List<ServiceTaskDO> getServiceTaskList() {
		return serviceTaskList;
	}

	public void setServiceTaskList(List<ServiceTaskDO> serviceTaskList) {
		this.serviceTaskList = serviceTaskList;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public String getObjectName() {
		return ServiceDO.class.getSimpleName();
	}

	public void addServiceTask(ServiceTaskDO serviceTask) {
		if (null == serviceTaskList) {
			serviceTaskList = new ArrayList<ServiceTaskDO>();
		}

		// serviceTask.setParentObject(this);
		serviceTaskList.add(serviceTask);
	}
	@Override
	public String toString() {
		return "ServiceDO [id=" + id + ", service=" + service + ", serviceType=" + serviceType + ", serviceDt="
				+ serviceDt + "]";
	}

}
