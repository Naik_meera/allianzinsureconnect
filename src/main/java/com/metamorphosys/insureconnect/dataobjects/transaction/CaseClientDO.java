package com.metamorphosys.insureconnect.dataobjects.transaction;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "CASECLIENT")
public class CaseClientDO extends BaseDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="caseClientId_seq") 
	@SequenceGenerator(name="caseClientId_seq",sequenceName="CASECLIENTID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "clientID")
	private String clientID;
	
	@Column(name = "customerId")
	private String customerId;
	
	@Column(name = "clientTypeCd")
	private String clientTypeCd;
	
	@Column(name = "titleCd")
	private String titleCd;
	
	@Column(name = "firstName")
	private String firstName;
	
	@Column(name = "middleName")
	private String middleName;
	
	@Column(name = "lastName")
	private String lastName;
	
	@Column(name = "firstName1")
	private String firstName1;
	
	@Column(name = "middleName1")
	private String middleName1;
	
	@Column(name = "lastName1")
	private String lastName1;
	
	@Column(name = "companyName")
	private String companyName;
	
	@Column(name = "companyName1")
	private String companyName1;
	
	@Column(name = "genderCd")
	private String genderCd;
	
	@Column(name = "dateOfBirth")
	private Date dateOfBirth;
	
	@Column(name = "age")
	private short age;
	
	@Column(name = "maritalStatusCd")
	private String maritalStatusCd;
	
	@Column(name = "occupationCd")
	private String occupationCd;
	
	@Column(name = "industryCd")
	private String industryCd;
	
	@Column(name = "natureOfDutyCd")
	private String natureOfDutyCd;
	
	@Column(name = "occupationClassCd")
	private String occupationClassCd;
	
	@Column(name = "medicalClassCd")
	private String medicalClassCd;
	
	@Column(name = "MontlyIncome")
	private Double MontlyIncome;
	
	@Column(name = "mothersMaidenName")
	private String mothersMaidenName;
	
	@Column(name = "fathersName")
	private String fathersName;
	
	@Column(name = "givenName")
	private String givenName;
	
	@Column(name = "legalName")
	private String legalName;
	
	@Column(name = "otherName")
	private String otherName;
	
	@Column(name = "identityTypeCD")
	private String identityTypeCD;
	
	@Column(name = "identityNum")
	private String identityNum;
	
	@Column(name = "countryOfBirthCd")
	private String countryOfBirthCd;
	
	@Column(name = "citizenshipCd")
	private String citizenshipCd;
	
	@Column(name = "income")
	private Double income;
	
	@Column(name = "isPoliticalFlag")
	private boolean isPoliticalFlag;
	
	@Column(name = "isConvictedFlag")
	private boolean isConvictedFlag;
	
	@Column(name = "clientStatusCd")
	private String clientStatusCd;
	
	@Column(name = "deDupStatusCd")
	private String deDupStatusCd;
	
	@Column(name = "roleCd")
	private String roleCd;

	public String getRoleCd() {
		return roleCd;
	}

	public void setRoleCd(String roleCd) {
		this.roleCd = roleCd;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<CaseClientAddressDO> caseClientAddressList;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<CaseClientContactDetailsDO> caseClientContactDetailsList;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<CaseClientHistoryDO> caseClientHistoryList;
	
	@Override
	public Long getId() {
		return id;
	}
	
	public String getClientID() {
		return clientID;
	}
	public void setClientID(String clientID) {
		this.clientID = clientID;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getClientTypeCd() {
		return clientTypeCd;
	}
	public void setClientTypeCd(String clientTypeCd) {
		this.clientTypeCd = clientTypeCd;
	}
	public String getTitleCd() {
		return titleCd;
	}
	public void setTitleCd(String titleCd) {
		this.titleCd = titleCd;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName1() {
		return firstName1;
	}
	public void setFirstName1(String firstName1) {
		this.firstName1 = firstName1;
	}
	public String getMiddleName1() {
		return middleName1;
	}
	public void setMiddleName1(String middleName1) {
		this.middleName1 = middleName1;
	}
	public String getLastName1() {
		return lastName1;
	}
	public void setLastName1(String lastName1) {
		this.lastName1 = lastName1;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCompanyName1() {
		return companyName1;
	}
	public void setCompanyName1(String companyName1) {
		this.companyName1 = companyName1;
	}
	public String getGenderCd() {
		return genderCd;
	}
	public void setGenderCd(String genderCd) {
		this.genderCd = genderCd;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public short getAge() {
		return age;
	}
	public void setAge(short age) {
		this.age = age;
	}
	public String getMaritalStatusCd() {
		return maritalStatusCd;
	}
	public void setMaritalStatusCd(String maritalStatusCd) {
		this.maritalStatusCd = maritalStatusCd;
	}
	public String getOccupationCd() {
		return occupationCd;
	}
	public void setOccupationCd(String occupationCd) {
		this.occupationCd = occupationCd;
	}
	public String getIndustryCd() {
		return industryCd;
	}
	public void setIndustryCd(String industryCd) {
		this.industryCd = industryCd;
	}
	public String getNatureOfDutyCd() {
		return natureOfDutyCd;
	}
	public void setNatureOfDutyCd(String natureOfDutyCd) {
		this.natureOfDutyCd = natureOfDutyCd;
	}
	public String getOccupationClassCd() {
		return occupationClassCd;
	}
	public void setOccupationClassCd(String occupationClassCd) {
		this.occupationClassCd = occupationClassCd;
	}
	public String getMedicalClassCd() {
		return medicalClassCd;
	}
	public void setMedicalClassCd(String medicalClassCd) {
		this.medicalClassCd = medicalClassCd;
	}
	public Double getMontlyIncome() {
		return MontlyIncome;
	}
	public void setMontlyIncome(Double montlyIncome) {
		MontlyIncome = montlyIncome;
	}
	public String getMothersMaidenName() {
		return mothersMaidenName;
	}
	public void setMothersMaidenName(String mothersMaidenName) {
		this.mothersMaidenName = mothersMaidenName;
	}
	public String getFathersName() {
		return fathersName;
	}
	public void setFathersName(String fathersName) {
		this.fathersName = fathersName;
	}
	public String getGivenName() {
		return givenName;
	}
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	public String getLegalName() {
		return legalName;
	}
	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}
	public String getOtherName() {
		return otherName;
	}
	public void setOtherName(String otherName) {
		this.otherName = otherName;
	}
	public String getIdentityTypeCD() {
		return identityTypeCD;
	}
	public void setIdentityTypeCD(String identityTypeCD) {
		this.identityTypeCD = identityTypeCD;
	}
	public String getIdentityNum() {
		return identityNum;
	}
	public void setIdentityNum(String identityNum) {
		this.identityNum = identityNum;
	}
	public String getCountryOfBirthCd() {
		return countryOfBirthCd;
	}
	public void setCountryOfBirthCd(String countryOfBirthCd) {
		this.countryOfBirthCd = countryOfBirthCd;
	}
	public String getCitizenshipCd() {
		return citizenshipCd;
	}
	public void setCitizenshipCd(String citizenshipCd) {
		this.citizenshipCd = citizenshipCd;
	}
	public Double getIncome() {
		return income;
	}
	public void setIncome(Double income) {
		this.income = income;
	}
	public boolean isPoliticalFlag() {
		return isPoliticalFlag;
	}
	public void setPoliticalFlag(boolean isPoliticalFlag) {
		this.isPoliticalFlag = isPoliticalFlag;
	}
	public boolean isConvictedFlag() {
		return isConvictedFlag;
	}
	public void setConvictedFlag(boolean isConvictedFlag) {
		this.isConvictedFlag = isConvictedFlag;
	}
	public String getClientStatusCd() {
		return clientStatusCd;
	}
	public void setClientStatusCd(String clientStatusCd) {
		this.clientStatusCd = clientStatusCd;
	}
	public String getDeDupStatusCd() {
		return deDupStatusCd;
	}
	public void setDeDupStatusCd(String deDupStatusCd) {
		this.deDupStatusCd = deDupStatusCd;
	}
	public List<CaseClientAddressDO> getCaseClientAddressList() {
		return caseClientAddressList;
	}
	public void setCaseClientAddressList(List<CaseClientAddressDO> caseClientAddressList) {
		this.caseClientAddressList = caseClientAddressList;
	}
	public List<CaseClientContactDetailsDO> getCaseClientContactDetailsList() {
		return caseClientContactDetailsList;
	}
	public void setCaseClientContactDetailsList(List<CaseClientContactDetailsDO> caseClientContactDetailsList) {
		this.caseClientContactDetailsList = caseClientContactDetailsList;
	}
	public List<CaseClientHistoryDO> getCaseClientHistoryList() {
		return caseClientHistoryList;
	}
	public void setCaseClientHistoryList(List<CaseClientHistoryDO> caseClientHistoryList) {
		this.caseClientHistoryList = caseClientHistoryList;
	}

	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return CaseClientDO.class.getSimpleName();
	}

}
