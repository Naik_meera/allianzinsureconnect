package com.metamorphosys.insureconnect.dataobjects.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.TransactionDO;

@Entity
@Table(name="DEVICE")
public class DeviceDO extends TransactionDO {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="deviceId_seq")
	@SequenceGenerator(name="deviceId_seq",sequenceName="DEVICEID_SEQ",allocationSize=1)
	private Long id;
	
	
	@Column(name = "deviceId")
	private String deviceId;
	
	
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Override
	public Long getId() {
		return id;
	}
	@Override
	public String getObjectName() {
		return DeviceDO.class.getSimpleName();
	}
	
}
