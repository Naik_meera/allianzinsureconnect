package com.metamorphosys.insureconnect.dataobjects.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SEQUENCEGENERATOR")
//@Document(collection="SEQUENCEGENERATOR")
public class SequneceGeneratorDO {

	@Id
	private String id;
	
	@Column(name = "serviceName")
	private String serviceName;
	
	@Column(name = "startsWith")
	private String startsWith;
	
	@Column(name = "currentNum")
	private String currentNum;
	
	@Column(name = "length")
	private Integer length;
	
	public Integer getLength() {
		return length;
	}
	public void setLength(Integer length) {
		this.length = length;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getStartsWith() {
		return startsWith;
	}
	public void setStartsWith(String startsWith) {
		this.startsWith = startsWith;
	}
	public String getCurrentNum() {
		return currentNum;
	}
	public void setCurrentNum(String currentNum) {
		this.currentNum = currentNum;
	}

}
