package com.metamorphosys.insureconnect.dataobjects.transaction;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.TransactionDO;

@Entity
@Table(name = "ENTITYDOCUMENT")
public class EntityDocumentDO extends TransactionDO {
	
	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="entityDocumentId_seq") 
	@SequenceGenerator(name="entityDocumentId_seq",sequenceName="ENTITYDOCUMENTID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "agentId")
	private String agentId;
	
	@Column(name = "entityGuid")
	private String entityGuid;
	
	@Column(name = "entityDocumentID")
	private String entityDocumentID ;
	
	@Column(name = "entityServiceID")
	private String entityServiceID;
	
	@Column(name = "entityID")
	private String entityID;	
	
	@Lob
	@Column(name = "entityImage")
	private String entityImage;
	
	@Column(name = "referenceID")
	private String referenceID ;
	
	@Column(name = "referenceTypeCd")
	private String referenceTypeCd;
	
	@Column(name = "documentTypeCd")
	private String documentTypeCd ;
	
	@Column(name = "documentSubTypeCd")
	private String documentSubTypeCd;	
	
	@Column(name = "documentCd")
	private String documentCd;
	
	@Column(name = "entityImageName")
	private String entityImageName;
	
	@Column(name = "entityImageMimeType")
	private String entityImageMimeType;
	
	@Column(name = "documentReceiptStatusCd")
	private String documentReceiptStatusCd ;
	
	@Column(name = "documentRaisedBy")
	private String documentRaisedBy;
	
	@Column(name = "documentStatusCd")
	private String documentStatusCd;
	
	@Column(name = "documentRecievedDt")
	private Date documentRecievedDt;
	
	@Column(name = "purpose")
	private String purpose;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "imageURL")
	private String imageURL;
	
	@Column(name = "uploadStatus")
	private String uploadStatus;
	
	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getAgentId() {
		return agentId;
	}


	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}


	public String getEntityGuid() {
		return entityGuid;
	}


	public void setEntityGuid(String entityGuid) {
		this.entityGuid = entityGuid;
	}


	public String getEntityDocumentID() {
		return entityDocumentID;
	}


	public void setEntityDocumentID(String entityDocumentID) {
		this.entityDocumentID = entityDocumentID;
	}


	public String getEntityServiceID() {
		return entityServiceID;
	}


	public void setEntityServiceID(String entityServiceID) {
		this.entityServiceID = entityServiceID;
	}


	public String getEntityID() {
		return entityID;
	}


	public void setEntityID(String entityID) {
		this.entityID = entityID;
	}


	public String getEntityImage() {
		return entityImage;
	}


	public void setEntityImage(String entityImage) {
		this.entityImage = entityImage;
	}


	public String getReferenceID() {
		return referenceID;
	}


	public void setReferenceID(String referenceID) {
		this.referenceID = referenceID;
	}


	public String getReferenceTypeCd() {
		return referenceTypeCd;
	}


	public void setReferenceTypeCd(String referenceTypeCd) {
		this.referenceTypeCd = referenceTypeCd;
	}


	public String getDocumentTypeCd() {
		return documentTypeCd;
	}


	public void setDocumentTypeCd(String documentTypeCd) {
		this.documentTypeCd = documentTypeCd;
	}


	public String getDocumentSubTypeCd() {
		return documentSubTypeCd;
	}


	public void setDocumentSubTypeCd(String documentSubTypeCd) {
		this.documentSubTypeCd = documentSubTypeCd;
	}


	public String getDocumentCd() {
		return documentCd;
	}


	public String getEntityImageName() {
		return entityImageName;
	}


	public void setEntityImageName(String entityImageName) {
		this.entityImageName = entityImageName;
	}


	public String getEntityImageMimeType() {
		return entityImageMimeType;
	}


	public void setEntityImageMimeType(String entityImageMimeType) {
		this.entityImageMimeType = entityImageMimeType;
	}


	public void setDocumentCd(String documentCd) {
		this.documentCd = documentCd;
	}


	public String getDocumentReceiptStatusCd() {
		return documentReceiptStatusCd;
	}


	public void setDocumentReceiptStatusCd(String documentReceiptStatusCd) {
		this.documentReceiptStatusCd = documentReceiptStatusCd;
	}


	public String getDocumentRaisedBy() {
		return documentRaisedBy;
	}


	public void setDocumentRaisedBy(String documentRaisedBy) {
		this.documentRaisedBy = documentRaisedBy;
	}


	public String getDocumentStatusCd() {
		return documentStatusCd;
	}


	public void setDocumentStatusCd(String documentStatusCd) {
		this.documentStatusCd = documentStatusCd;
	}


	public Date getDocumentRecievedDt() {
		return documentRecievedDt;
	}


	public void setDocumentRecievedDt(Date documentRecievedDt) {
		this.documentRecievedDt = documentRecievedDt;
	}


	public String getPurpose() {
		return purpose;
	}


	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getImageURL() {
		return imageURL;
	}


	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}


	public String getUploadStatus() {
		return uploadStatus;
	}


	public void setUploadStatus(String uploadStatus) {
		this.uploadStatus = uploadStatus;
	}


	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return EntityDocumentDO.class.getSimpleName();
	}
}
