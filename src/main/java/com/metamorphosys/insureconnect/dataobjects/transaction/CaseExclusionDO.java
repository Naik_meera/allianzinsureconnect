package com.metamorphosys.insureconnect.dataobjects.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "CASEEXCLUSION")
public class CaseExclusionDO extends BaseDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="caseExclusionId_seq") 
	@SequenceGenerator(name="caseExclusionId_seq",sequenceName="CASEEXCLUSIONID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "exclusionCD")
	private String exclusionCD;
	
	@Column(name = "iCDCode")
	private String iCDCode;
	
	@Column(name = "exclusionReasonCd")
	private String exclusionReasonCd;
	
	@Column(name = "exclusionEventCd")
	private String exclusionEventCd;
	
	@Override
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getExclusionCD() {
		return exclusionCD;
	}
	public void setExclusionCD(String exclusionCD) {
		this.exclusionCD = exclusionCD;
	}
	public String getiCDCode() {
		return iCDCode;
	}
	public void setiCDCode(String iCDCode) {
		this.iCDCode = iCDCode;
	}
	public String getExclusionReasonCd() {
		return exclusionReasonCd;
	}
	public void setExclusionReasonCd(String exclusionReasonCd) {
		this.exclusionReasonCd = exclusionReasonCd;
	}
	public String getExclusionEventCd() {
		return exclusionEventCd;
	}
	public void setExclusionEventCd(String exclusionEventCd) {
		this.exclusionEventCd = exclusionEventCd;
	}
	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return CaseExclusionDO.class.getSimpleName();
	}
}
