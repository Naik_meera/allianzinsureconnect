package com.metamorphosys.insureconnect.dataobjects.transaction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.AdditionalFieldDO;

@Entity
@Table(name = "POLICYADDFLD")
public class PolicyAdditionalFieldDO extends AdditionalFieldDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="policyAddFldId_seq") 
	@SequenceGenerator(name="policyAddFldId_seq",sequenceName="POLICYADDFLDID_SEQ",allocationSize=1) Long id;
	
	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return id;
	}

}
