package com.metamorphosys.insureconnect.dataobjects.transaction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="RETTRYSERVICE")
public class RetryServiceDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="retryServiceId_seq") 
	@SequenceGenerator(name="retryServiceId_seq",sequenceName="RETRYSERVICEID_SEQ",allocationSize=1) Long id;
	
	private String serviceName;
	private String serviceId;
	private String serviceObject;
	private String parentServiceId;
	private String statusCd;
	private int noOfRetries;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getServiceObject() {
		return serviceObject;
	}
	public void setServiceObject(String serviceObject) {
		this.serviceObject = serviceObject;
	}
	public String getParentServiceId() {
		return parentServiceId;
	}
	public void setParentServiceId(String parentServiceId) {
		this.parentServiceId = parentServiceId;
	}
	public String getStatusCd() {
		return statusCd;
	}
	public void setStatusCd(String statusCd) {
		this.statusCd = statusCd;
	}

	public int getNoOfRetries() {
		return noOfRetries;
	}

	public void setNoOfRetries(int noOfRetries) {
		this.noOfRetries = noOfRetries;
	}
	
	
}
