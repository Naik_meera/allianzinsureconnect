package com.metamorphosys.insureconnect.dataobjects.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "POLICYCLIENTADDRESS")
public class PolicyClientAddressDO extends BaseDO {
	
	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="policyClientAddressId_seq") 
	@SequenceGenerator(name="policyClientAddressId_seq",sequenceName="POLICYCLIENTADDRESSID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "clientAddressID")
	private String clientAddressID;
	
	@Column(name = "addressTypeCd")
	private String addressTypeCd;
	
	@Column(name = "addressSubTypeCd")
	private String addressSubTypeCd;
	
	@Column(name = "addressLine1")
	private String addressLine1;
	
	@Column(name = "addressLine2")
	private String addressLine2;
	
	@Column(name = "addressLine3")
	private String addressLine3;
	
	@Column(name = "addressLine4")
	private String addressLine4;
	
	@Column(name = "cityCd")
	private String cityCd;
	
	@Column(name = "talukaCd")
	private String talukaCd;
	
	@Column(name = "villageCd")
	private String villageCd;
	
	@Column(name = "stateCd")
	private String stateCd;
	
	@Column(name = "countryCd")
	private String countryCd;
	
	@Column(name = "postCd")
	private Long postCd;
	
	@Column(name = "isCorrespondenctAddFlag")
	private boolean isCorrespondenctAddFlag;
	
	public String getClientAddressID() {
		return clientAddressID;
	}
	public void setClientAddressID(String clientAddressID) {
		this.clientAddressID = clientAddressID;
	}
	public String getAddressTypeCd() {
		return addressTypeCd;
	}
	public void setAddressTypeCd(String addressTypeCd) {
		this.addressTypeCd = addressTypeCd;
	}
	public String getAddressSubTypeCd() {
		return addressSubTypeCd;
	}
	public void setAddressSubTypeCd(String addressSubTypeCd) {
		this.addressSubTypeCd = addressSubTypeCd;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getAddressLine3() {
		return addressLine3;
	}
	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}
	public String getAddressLine4() {
		return addressLine4;
	}
	public void setAddressLine4(String addressLine4) {
		this.addressLine4 = addressLine4;
	}
	public String getCityCd() {
		return cityCd;
	}
	public void setCityCd(String cityCd) {
		this.cityCd = cityCd;
	}
	public String getTalukaCd() {
		return talukaCd;
	}
	public void setTalukaCd(String talukaCd) {
		this.talukaCd = talukaCd;
	}
	public String getVillageCd() {
		return villageCd;
	}
	public void setVillageCd(String villageCd) {
		this.villageCd = villageCd;
	}
	public String getStateCd() {
		return stateCd;
	}
	public void setStateCd(String stateCd) {
		this.stateCd = stateCd;
	}
	public String getCountryCd() {
		return countryCd;
	}
	public void setCountryCd(String countryCd) {
		this.countryCd = countryCd;
	}
	public Long getPostCd() {
		return postCd;
	}
	public void setPostCd(Long postCd) {
		this.postCd = postCd;
	}
	public boolean isCorrespondenctAddFlag() {
		return isCorrespondenctAddFlag;
	}
	public void setCorrespondenctAddFlag(boolean isCorrespondenctAddFlag) {
		this.isCorrespondenctAddFlag = isCorrespondenctAddFlag;
	}
	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return id;
	}
	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return PolicyClientAddressDO.class.getSimpleName();
	}


}
