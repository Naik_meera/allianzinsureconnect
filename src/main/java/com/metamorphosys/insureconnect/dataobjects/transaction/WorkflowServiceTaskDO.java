package com.metamorphosys.insureconnect.dataobjects.transaction;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "WORKFLOWSERVICETASK")
public class WorkflowServiceTaskDO extends BaseDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="workflowServiceTaskId_seq") 
	@SequenceGenerator(name="workflowServiceTaskId_seq",sequenceName="WORKFLOWSERVICETASKID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "serviceTask")
	private String serviceTask;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "decisionCd")
	private String decisionCd;
	
	@Column(name = "taskUser")
	private String taskUser;
	
	@Column(name = "startDt")
	private Timestamp startDt;
	
	@Column(name = "endDt")
	private Timestamp endDt;

	@Override
	public String getObjectName() {
		return WorkflowServiceTaskDO.class.getSimpleName();
	}

	@Override
	public Long getId() {
		return id;
	}

	public String getServiceTask() {
		return serviceTask;
	}

	public void setServiceTask(String serviceTask) {
		this.serviceTask = serviceTask;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDecisionCd() {
		return decisionCd;
	}

	public void setDecisionCd(String decisionCd) {
		this.decisionCd = decisionCd;
	}

	public String getTaskUser() {
		return taskUser;
	}

	public void setTaskUser(String taskUser) {
		this.taskUser = taskUser;
	}

	public Timestamp getStartDt() {
		return startDt;
	}

	public void setStartDt(Timestamp startDt) {
		this.startDt = startDt;
	}

	public Timestamp getEndDt() {
		return endDt;
	}

	public void setEndDt(Timestamp endDt) {
		this.endDt = endDt;
	}

}
