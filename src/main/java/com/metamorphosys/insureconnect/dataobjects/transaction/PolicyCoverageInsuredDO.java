package com.metamorphosys.insureconnect.dataobjects.transaction;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "POLICYCOVERAGEINSURED")
public class PolicyCoverageInsuredDO extends BaseDO implements Serializable {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="polCvrgeInsurdId_seq") 
	@SequenceGenerator(name="polCvrgeInsurdId_seq",sequenceName="POLCVRGEINSURDID_SEQ",allocationSize=1) Long id;

	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "bodyTypeCd")
	private String bodyTypeCd;
	
	
	@Column(name = "chasisNum")
	private String chasisNum;
	
//	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "clientID")
	private Long clientID;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "customerId")
	private String customerId;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "engineCapacity")
	private String engineCapacity;
	
	@Column(name = "engineNum")
	private String engineNum;
	
	@Column(name = "entryAge")
	private short entryAge;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "fuelTypeCd")
	private String fuelTypeCd;
	
	@Column(name = "manufacturingDt")
	private Integer manufacturingDt;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "medicalClassCd")
	private String medicalClassCd;
	
	@Column(name = "registrationDt")
	private Date registrationDt;
	
	@Column(name = "registrationNum")
	private String registrationNum;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "roleCd")
	private String roleCd;
	
	@Column(name = "seatingCapacity")
	private short seatingCapacity;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "transmissionType")
	private String transmissionType;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "uwClassCd")
	private String uwClassCd;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "uwDecisionCd")
	private String uwDecisionCd;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "uwDecisionValue")
	private String uwDecisionValue;
	
	//@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "vehicleModelCd")
	private String vehicleModelCd;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "vehicleTypeCd")
	private String vehicleTypeCd;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "vehicleColour")
	private String vehicleColour;
	
	//@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "vehicleMakeCd")
	private String vehicleMakeCd;
	
	//@Pattern(regexp = "^[A-Za-z0-9.-]*$")
	@Column(name = "vehicleVariant")
	private String vehicleVariant;
	
	@Column(name = "mileage")
	private Double mileage;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "usagePurpose")
	private String usagePurpose;
	
	@Column(name = "derivedMakeModel")
	private String derivedMakeModel;
	
	@Column(name = "derivedMakeVariantt")
	private String derivedMakeVariantt;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "zoneCd")
	private String zoneCd;


	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "defaultSumInsured")
	private String defaultSumInsured;
	
	@Column(name = "derivedVehicleMakeCd")
	private String derivedVehicleMakeCd;
	
	@Column(name = "derivedVehicleModelCd")
	private String derivedVehicleModelCd;
	
	@Column(name = "vehicleKey")
	private String vehicleKey;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "idType")
	private String idType;
	
	@Column(name = "purchaseDt")
	private Timestamp purchaseDt;
	
	public String getZoneCd() {
		return zoneCd;
	}
	public void setZoneCd(String zoneCd) {
		this.zoneCd = zoneCd;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<PolicyCoverageInsuredAccessoryDO> policyCoverageInsuredAccessoryList;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<PolicyCoverageInsuredLoadingDO> policyCoverageInsuredLoadingList;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<PolicyCoverageInsuredDiscountDO> policyCoverageInsuredDiscountList;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<PolicyCoverageInsuredAdditionalFieldDO> policyCoverageInsuredAdditionalFieldDOs;
	
	public Timestamp getPurchaseDt() {
		return purchaseDt;
	}
	public void setPurchaseDt(Timestamp purchaseDt) {
		this.purchaseDt = purchaseDt;
	}
	@Override
	public Long getId() {
		return id;
	}
	public String getBodyTypeCd() {
		return bodyTypeCd;
	}
	public void setBodyTypeCd(String bodyTypeCd) {
		this.bodyTypeCd = bodyTypeCd;
	}
	public String getChasisNum() {
		return chasisNum;
	}
	public void setChasisNum(String chasisNum) {
		this.chasisNum = chasisNum;
	}
	public Long getClientID() {
		return clientID;
	}
	public void setClientID(Long clientID) {
		this.clientID = clientID;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getEngineCapacity() {
		return engineCapacity;
	}
	public void setEngineCapacity(String engineCapacity) {
		this.engineCapacity = engineCapacity;
	}
	public String getEngineNum() {
		return engineNum;
	}
	public void setEngineNum(String engineNum) {
		this.engineNum = engineNum;
	}
	public short getEntryAge() {
		return entryAge;
	}
	public void setEntryAge(short entryAge) {
		this.entryAge = entryAge;
	}
	public String getFuelTypeCd() {
		return fuelTypeCd;
	}
	public void setFuelTypeCd(String fuelTypeCd) {
		this.fuelTypeCd = fuelTypeCd;
	}
	public Integer getManufacturingDt() {
		return manufacturingDt;
	}
	public void setManufacturingDt(Integer manufacturingDt) {
		this.manufacturingDt = manufacturingDt;
	}
	public String getMedicalClassCd() {
		return medicalClassCd;
	}
	public void setMedicalClassCd(String medicalClassCd) {
		this.medicalClassCd = medicalClassCd;
	}
	public Date getRegistrationDt() {
		return registrationDt;
	}
	public void setRegistrationDt(Date registrationDt) {
		this.registrationDt = registrationDt;
	}
	public String getRegistrationNum() {
		return registrationNum;
	}
	public void setRegistrationNum(String registrationNum) {
		this.registrationNum = registrationNum;
	}
	public String getRoleCd() {
		return roleCd;
	}
	public void setRoleCd(String roleCd) {
		this.roleCd = roleCd;
	}
	public short getSeatingCapacity() {
		return seatingCapacity;
	}
	public void setSeatingCapacity(short seatingCapacity) {
		this.seatingCapacity = seatingCapacity;
	}
	public String getTransmissionType() {
		return transmissionType;
	}
	public void setTransmissionType(String transmissionType) {
		this.transmissionType = transmissionType;
	}
	public String getUwClassCd() {
		return uwClassCd;
	}
	public void setUwClassCd(String uwClassCd) {
		this.uwClassCd = uwClassCd;
	}
	public String getUwDecisionCd() {
		return uwDecisionCd;
	}
	public void setUwDecisionCd(String uwDecisionCd) {
		this.uwDecisionCd = uwDecisionCd;
	}
	public String getUwDecisionValue() {
		return uwDecisionValue;
	}
	public void setUwDecisionValue(String uwDecisionValue) {
		this.uwDecisionValue = uwDecisionValue;
	}
	public String getVehicleModelCd() {
		return vehicleModelCd;
	}
	public void setVehicleModelCd(String vehicleModelCd) {
		this.vehicleModelCd = vehicleModelCd;
	}
	public String getVehicleTypeCd() {
		return vehicleTypeCd;
	}
	public void setVehicleTypeCd(String vehicelTypeCd) {
		this.vehicleTypeCd = vehicleTypeCd;
	}
	public String getVehicleColour() {
		return vehicleColour;
	}
	public void setVehicleColour(String vehicleColour) {
		this.vehicleColour = vehicleColour;
	}
	public String getVehicleMakeCd() {
		return vehicleMakeCd;
	}
	public void setVehicleMakeCd(String vehicleMakeCd) {
		this.vehicleMakeCd = vehicleMakeCd;
	}
	public String getVehicleVariant() {
		return vehicleVariant;
	}
	public void setVehicleVariant(String vehicleVariant) {
		this.vehicleVariant = vehicleVariant;
	}
	public List<PolicyCoverageInsuredLoadingDO> getPolicyCoverageInsuredLoadingList() {
		return policyCoverageInsuredLoadingList;
	}
	public void setPolicyCoverageInsuredLoadingList(List<PolicyCoverageInsuredLoadingDO> policyCoverageInsuredLoadingList) {
		this.policyCoverageInsuredLoadingList = policyCoverageInsuredLoadingList;
	}
	public List<PolicyCoverageInsuredDiscountDO> getPolicyCoverageInsuredDiscountList() {
		return policyCoverageInsuredDiscountList;
	}
	public void setPolicyCoverageInsuredDiscountList(
			List<PolicyCoverageInsuredDiscountDO> policyCoverageInsuredDiscountList) {
		this.policyCoverageInsuredDiscountList = policyCoverageInsuredDiscountList;
	}
	public List<PolicyCoverageInsuredAdditionalFieldDO> getPolicyCoverageInsuredAdditionalFieldDOs() {
		return policyCoverageInsuredAdditionalFieldDOs;
	}
	public void setPolicyCoverageInsuredAdditionalFieldDOs(
			List<PolicyCoverageInsuredAdditionalFieldDO> policyCoverageInsuredAdditionalFieldDOs) {
		this.policyCoverageInsuredAdditionalFieldDOs = policyCoverageInsuredAdditionalFieldDOs;
	}
	public Double getMileage() {
		return mileage;
	}
	public void setMileage(Double mileage) {
		this.mileage = mileage;
	}
	public String getUsagePurpose() {
		return usagePurpose;
	}
	public void setUsagePurpose(String usagePurpose) {
		this.usagePurpose = usagePurpose;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<PolicyCoverageInsuredAccessoryDO> getPolicyCoverageInsuredAccessoryList() {
		return policyCoverageInsuredAccessoryList;
	}
	public void setPolicyCoverageInsuredAccessoryList(
			List<PolicyCoverageInsuredAccessoryDO> policyCoverageInsuredAccessoryList) {
		this.policyCoverageInsuredAccessoryList = policyCoverageInsuredAccessoryList;
	}
	public String getDerivedMakeModel() {
		return derivedMakeModel;
	}
	public void setDerivedMakeModel(String derivedMakeModel) {
		this.derivedMakeModel = derivedMakeModel;
	}
	public String getDerivedMakeVariantt() {
		return derivedMakeVariantt;
	}
	public void setDerivedMakeVariantt(String derivedMakeVariantt) {
		this.derivedMakeVariantt = derivedMakeVariantt;
	}
	
	public String getDefaultSumInsured() {
		return defaultSumInsured;
	}
	public void setDefaultSumInsured(String defaultSumInsured) {
		this.defaultSumInsured = defaultSumInsured;
	}
	public String getDerivedVehicleMakeCd() {
		return derivedVehicleMakeCd;
	}
	public void setDerivedVehicleMakeCd(String derivedVehicleMakeCd) {
		this.derivedVehicleMakeCd = derivedVehicleMakeCd;
	}
	public String getDerivedVehicleModelCd() {
		return derivedVehicleModelCd;
	}
	public void setDerivedVehicleModelCd(String derivedVehicleModelCd) {
		this.derivedVehicleModelCd = derivedVehicleModelCd;
	}
	public String getVehicleKey() {
		return vehicleKey;
	}
	public void setVehicleKey(String vehicleKey) {
		this.vehicleKey = vehicleKey;
	}
	
	
	public String getIdType() {
		return idType;
	}
	public void setIdType(String idType) {
		this.idType = idType;
	}
	@Override
	public String getObjectName() {
		if (null != roleCd) {
			return roleCd;
		} else {
			return PolicyCoverageInsuredDO.class.getSimpleName();
		}
	}
}
