package com.metamorphosys.insureconnect.dataobjects.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "userrole")
public class UserRoleDO extends BaseDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="userRoleId_seq") 
	@SequenceGenerator(name="userRoleId_seq",sequenceName="USERROLEID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "roleId")
	private String roleId;
	
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public Long getId() {
		return id;
	}
	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return UserRoleDO.class.getSimpleName();
	}
	
	
}
