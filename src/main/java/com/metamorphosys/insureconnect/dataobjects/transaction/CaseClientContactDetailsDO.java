package com.metamorphosys.insureconnect.dataobjects.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "CASECLIENTCONTACTDETAILS")
public class CaseClientContactDetailsDO extends BaseDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="caseClientContctDetailId_seq") 
	@SequenceGenerator(name="caseClientContctDetailId_seq",sequenceName="CASECLIENTCONTCTDETAILID_SEQ",allocationSize=1) Long id;

	@Column(name = "contactTypeCd")
	private String contactTypeCd;

	@Column(name = "contactSubTypeCd")
	private String contactSubTypeCd;
	
	@Column(name = "ISDCd")
	private String ISDCd;
	
	@Column(name = "STDCd")
	private String STDCd;
	
	@Column(name = "contactNum")
	private String contactNum;
	
	@Column(name = "extensionNum")
	private Long extensionNum;
	
	@Column(name = "emailId")
	private String emailId;
	
	@Column(name = "isPrefferedFlag")
	private boolean isPrefferedFlag;
	
	@Override
	public Long getId() {
		return id;
	}
	
	public String getContactTypeCd() {
		return contactTypeCd;
	}
	public void setContactTypeCd(String contactTypeCd) {
		this.contactTypeCd = contactTypeCd;
	}
	public String getContactSubTypeCd() {
		return contactSubTypeCd;
	}
	public void setContactSubTypeCd(String contactSubTypeCd) {
		this.contactSubTypeCd = contactSubTypeCd;
	}
	public String getISDCd() {
		return ISDCd;
	}
	public void setISDCd(String iSDCd) {
		ISDCd = iSDCd;
	}
	public String getSTDCd() {
		return STDCd;
	}
	public void setSTDCd(String sTDCd) {
		STDCd = sTDCd;
	}
	public String getContactNum() {
		return contactNum;
	}
	public void setContactNum(String contactNum) {
		this.contactNum = contactNum;
	}
	public Long getExtensionNum() {
		return extensionNum;
	}
	public void setExtensionNum(Long extensionNum) {
		this.extensionNum = extensionNum;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public boolean isPrefferedFlag() {
		return isPrefferedFlag;
	}
	public void setPrefferedFlag(boolean isPrefferedFlag) {
		this.isPrefferedFlag = isPrefferedFlag;
	}

	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return CaseClientContactDetailsDO.class.getSimpleName();
	}

	
}
