package com.metamorphosys.insureconnect.dataobjects.transaction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="PREMIUMSIMAP")
public class PremiumSIMappingTableDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="premiumSIMapId_seq") 
	@SequenceGenerator(name="premiumSIMapId_seq",sequenceName="PREMIUMSIMAPID_SEQ",allocationSize=1) Long id;
	
	private String productCd;
	private Double SIFrom;
	private Double SITo;
	private Double premiumRate;
	public String getProductCd() {
		return productCd;
	}
	public void setProductCd(String productCd) {
		this.productCd = productCd;
	}
	public Double getSIFrom() {
		return SIFrom;
	}
	public void setSIFrom(Double sIFrom) {
		SIFrom = sIFrom;
	}
	public Double getSITo() {
		return SITo;
	}
	public void setSITo(Double sITo) {
		SITo = sITo;
	}
	public Double getPremiumRate() {
		return premiumRate;
	}
	public void setPremiumRate(Double premiumRate) {
		this.premiumRate = premiumRate;
	}
	
	
}
