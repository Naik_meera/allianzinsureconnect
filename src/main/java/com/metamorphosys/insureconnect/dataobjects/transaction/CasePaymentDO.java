package com.metamorphosys.insureconnect.dataobjects.transaction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "CASEPAYMENT")
public class CasePaymentDO extends BaseDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="casePaymentId_seq") 
	@SequenceGenerator(name="casePaymentId_seq",sequenceName="CASEPAYMENTID_SEQ",allocationSize=1) Long id;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return CasePaymentDO.class.getSimpleName();
	}
	
}
