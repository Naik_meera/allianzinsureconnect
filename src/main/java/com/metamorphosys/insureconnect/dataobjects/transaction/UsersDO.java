package com.metamorphosys.insureconnect.dataobjects.transaction;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.TransactionDO;

@Entity
@Table(name = "users")
public class UsersDO extends TransactionDO {

	
	private @Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="userId_seq")
	@SequenceGenerator(name="userId_seq",sequenceName="USERID_SEQ",allocationSize=1)
	Long id;
	
	@Column(name = "userId")
	private String userId;
    
	@Column(name = "username")
	private String username;
    
	@Column(name = "password")
	private String password;
    
	@Column(name = "userStatus")
	private String userStatus;
    
	@Column(name = "userType")
	private String userType;
    
	@Column(name = "noOfFailedAttempt")
	private Integer noOfFailedAttempt;
    
	@Column(name = "lastloginDt")
	private Timestamp lastloginDt;
    
	@Column(name = "accountEndDt")
	private Date accountEndDt;
    
	@Column(name = "mobileNumber")
	private Integer mobileNumber;
    
	@Column(name = "emailId")
	private String emailId;
    
	@Column(name = "channel")
	private String channel;
    
	@Column(name = "language")
	private String language; 
    
	
    
	@OneToMany(cascade=CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<UserRoleDO> userRoleDOList;
    
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserStatus() {
		return userStatus;
	}
	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public Integer getNoOfFailedAttempt() {
		return noOfFailedAttempt;
	}
	public void setNoOfFailedAttempt(Integer noOfFailedAttempt) {
		this.noOfFailedAttempt = noOfFailedAttempt;
	}
	public Timestamp getLastloginDt() {
		return lastloginDt;
	}
	public void setLastloginDt(Timestamp lastloginDt) {
		this.lastloginDt = lastloginDt;
	}
	@Override
	public Long getId() {
		return id;
	}
	public Date getAccountEndDt() {
		return accountEndDt;
	}
	public void setAccountEndDt(Date accountEndDt) {
		this.accountEndDt = accountEndDt;
	}
	public List<UserRoleDO> getUserRoleDOList() {
		return userRoleDOList;
	}
	public void setUserRoleDOList(List<UserRoleDO> userRoleDOList) {
		this.userRoleDOList = userRoleDOList;
	}
	public Integer getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(Integer mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return UsersDO.class.getSimpleName();
	}
	
	@Override
	public String toString() {
		return "UsersDO [id=" + id + ", userId=" + userId + ", username=" + username + ", password=" + password
				+ ", userStatus=" + userStatus + ", userType=" + userType + ", noOfFailedAttempt=" + noOfFailedAttempt
				+ ", lastloginDt=" + lastloginDt + ", accountEndDt=" + accountEndDt + ", mobileNumber=" + mobileNumber
				+ ", emailId=" + emailId + ", channel=" + channel + ", language=" + language + ", userRoleDOList="
				+ userRoleDOList + "]";
	}
    
}
