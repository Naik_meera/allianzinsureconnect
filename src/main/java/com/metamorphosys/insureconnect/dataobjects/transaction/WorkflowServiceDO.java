package com.metamorphosys.insureconnect.dataobjects.transaction;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.SynchronizableDO;

@Entity
@Table(name = "WORKFLOWSERVICE")
public class WorkflowServiceDO extends SynchronizableDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="workflowServiceId_seq") 
	@SequenceGenerator(name="workflowServiceId_seq",sequenceName="WORKFLOWSERVICEID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "service")
	private String service;
	
	@Column(name = "serviceType")
	private String serviceType;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "entityGuid")
	private String entityGuid;
	
	@Column(name = "startDt")
	private Timestamp startDt;
	
	@Column(name = "endDt")
	private Timestamp endDt;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<WorkflowServiceTaskDO> workflowServiceTaskList;

	@Override
	public String getObjectName() {
		return WorkflowServiceDO.class.getSimpleName();
	}

	@Override
	public Long getId() {
		return id;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getEntityGuid() {
		return entityGuid;
	}

	public void setEntityGuid(String entityGuid) {
		this.entityGuid = entityGuid;
	}

	public Timestamp getStartDt() {
		return startDt;
	}

	public void setStartDt(Timestamp startDt) {
		this.startDt = startDt;
	}

	public Timestamp getEndDt() {
		return endDt;
	}

	public void setEndDt(Timestamp endDt) {
		this.endDt = endDt;
	}

	public List<WorkflowServiceTaskDO> getWorkflowServiceTaskList() {
		return workflowServiceTaskList;
	}

	public void setWorkflowServiceTaskList(List<WorkflowServiceTaskDO> workflowServiceTaskList) {
		this.workflowServiceTaskList = workflowServiceTaskList;
	}

	public void addWorkflowServiceTask(WorkflowServiceTaskDO workflowServiceTask) {
		if (null == workflowServiceTaskList) {
			workflowServiceTaskList = new ArrayList<WorkflowServiceTaskDO>();
		}

		//workflowServiceTask.setParentObject(this);
		workflowServiceTaskList.add(workflowServiceTask);
	}
	
	public WorkflowServiceTaskDO fetchPendingWorkflowServiceTask()
	{
		WorkflowServiceTaskDO pendingServiceTask = null;
		for(WorkflowServiceTaskDO workflowServiceTask : workflowServiceTaskList)
		{
			if(null != workflowServiceTask && "PENDING".equals(workflowServiceTask.getStatus()))
			{
				pendingServiceTask = workflowServiceTask;
				break;
			}
		}
		return pendingServiceTask;
	}
}
