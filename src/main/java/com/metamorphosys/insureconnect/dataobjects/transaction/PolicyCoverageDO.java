package com.metamorphosys.insureconnect.dataobjects.transaction;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "POLICYCOVERAGE")
public class PolicyCoverageDO extends BaseDO implements Serializable{

	public void setId(Long id) {
		this.id = id;
	}

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="policyCoverageId_seq") 
	@SequenceGenerator(name="policyCoverageId_seq",sequenceName="POLICYCOVERAGEID_SEQ",allocationSize=1) Long id;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "productCd")
	private String productCd;
	
	@Column(name = "productVersion")
	private Long productVersion;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "planOptionCd")
	private String planOptionCd;
	
	@Column(name = "baseAnnualPremium")
	private Double baseAnnualPremium;
	
	@Column(name = "baseModalPremium")
	private Double baseModalPremium;
	
	@Column(name = "totalExtraPremium")
	private Double totalExtraPremium;
	
	@Column(name = "totalDiscount")
	private Double totalDiscount;
	
	@Column(name = "totalRiderPremium")
	private Double totalRiderPremium;
	
	@Column(name = "totalPremium")
	private Double totalPremium;
	
	@Column(name = "policyterm")
	private Integer policyterm;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "paymentMethodCd")
	private String paymentMethodCd;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "paymentFrequencyCd")
	private String paymentFrequencyCd;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "paymentTerm")
	private String paymentTerm;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "coverageType")
	private String coverageType;
	
	@Column(name = "sumInsured")
	private Double sumInsured;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "uwDecisionCd")
	private String uwDecisionCd;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "uwDecisionValue")
	private String uwDecisionValue;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "planTypeCd")
	private String planTypeCd;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "associationType")
	private String associationType;
	
//	@Pattern(regexp = "^[A-Za-z0-9\\s]*$")
	@Column(name = "productName")
	private String productName;
	
	@Column(name = "premiumRate")
	private Double premiumRate;
	
	@Column(name = "defaultSumInsured")
	private Double defaultSumInsured;
	
	@Column(name = "isSelectedFlag")
	private boolean isSelectedFlag;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "isSelected")
	private String isSelected;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "coverageUIProperty")
	private String coverageUIProperty;
	
//	@Pattern(regexp = "^[A-Za-z0-9.]*+[0-9]$")
	@Column(name = "coverageUIPropertyValue")
	private String coverageUIPropertyValue;
	
	public String getCoverageUIProperty() {
		return coverageUIProperty;
	}

	public void setCoverageUIProperty(String coverageUIProperty) {
		this.coverageUIProperty = coverageUIProperty;
	}
	public String getCoverageUIPropertyValue() {
		return coverageUIPropertyValue;
	}
	public void setCoverageUIPropertyValue(String coverageUIPropertyValue) {
		this.coverageUIPropertyValue = coverageUIPropertyValue;
	}
	public String getIsSelected() {
		return isSelected;
	}
	public void setIsSelected(String isSelected) {
		this.isSelected = isSelected;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<PolicyCoverageInsuredDO> policyCoverageInsuredList;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<PolicyCoverageAdditionalFieldDO> policyCoverageAdditionalFieldDOs;

	public Long setId() {
		return id;
	}
	public String getProductCd() {
		return productCd;
	}

	public void setProductCd(String productCd) {
		this.productCd = productCd;
	}

	public Long getProductVersion() {
		return productVersion;
	}

	public void setProductVersion(Long productVersion) {
		this.productVersion = productVersion;
	}

	public String getPlanOptionCd() {
		return planOptionCd;
	}

	public void setPlanOptionCd(String planOptionCd) {
		this.planOptionCd = planOptionCd;
	}

	public Double getBaseAnnualPremium() {
		return baseAnnualPremium;
	}

	public void setBaseAnnualPremium(Double baseAnnualPremium) {
		this.baseAnnualPremium = baseAnnualPremium;
	}

	public Double getBaseModalPremium() {
		return baseModalPremium;
	}

	public void setBaseModalPremium(Double baseModalPremium) {
		this.baseModalPremium = baseModalPremium;
	}

	public Double getTotalExtraPremium() {
		return totalExtraPremium;
	}

	public void setTotalExtraPremium(Double totalExtraPremium) {
		this.totalExtraPremium = totalExtraPremium;
	}

	public Double getTotalDiscount() {
		return totalDiscount;
	}

	public void setTotalDiscount(Double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}

	public Double getTotalRiderPremium() {
		return totalRiderPremium;
	}

	public void setTotalRiderPremium(Double totalRiderPremium) {
		this.totalRiderPremium = totalRiderPremium;
	}

	public Double getTotalPremium() {
		return totalPremium;
	}

	public void setTotalPremium(Double totalPremium) {
		this.totalPremium = totalPremium;
	}

	public Integer getPolicyterm() {
		return policyterm;
	}

	public void setPolicyterm(Integer policyterm) {
		this.policyterm = policyterm;
	}

	public String getPaymentMethodCd() {
		return paymentMethodCd;
	}

	public void setPaymentMethodCd(String paymentMethodCd) {
		this.paymentMethodCd = paymentMethodCd;
	}

	public String getPaymentFrequencyCd() {
		return paymentFrequencyCd;
	}

	public void setPaymentFrequencyCd(String paymentFrequencyCd) {
		this.paymentFrequencyCd = paymentFrequencyCd;
	}

	public String getPaymentTerm() {
		return paymentTerm;
	}

	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm;
	}

	public String getCoverageType() {
		return coverageType;
	}

	public void setCoverageType(String coverageType) {
		this.coverageType = coverageType;
	}

	public Double getSumInsured() {
		return sumInsured;
	}

	public void setSumInsured(Double sumInsured) {
		this.sumInsured = sumInsured;
	}

	public List<PolicyCoverageInsuredDO> getPolicyCoverageInsuredList() {
		return policyCoverageInsuredList;
	}

	public void setPolicyCoverageInsuredList(List<PolicyCoverageInsuredDO> policyCoverageInsuredList) {
		this.policyCoverageInsuredList = policyCoverageInsuredList;
	}

	public Long getId() {
		return id;
	}

	public String getUwDecisionCd() {
		return uwDecisionCd;
	}

	public void setUwDecisionCd(String uwDecisionCd) {
		this.uwDecisionCd = uwDecisionCd;
	}

	public String getUwDecisionValue() {
		return uwDecisionValue;
	}

	public void setUwDecisionValue(String uwDecisionValue) {
		this.uwDecisionValue = uwDecisionValue;
	}

	public List<PolicyCoverageAdditionalFieldDO> getPolicyCoverageAdditionalFieldDOs() {
		return policyCoverageAdditionalFieldDOs;
	}

	public void setPolicyCoverageAdditionalFieldDOs(
			List<PolicyCoverageAdditionalFieldDO> policyCoverageAdditionalFieldDOs) {
		this.policyCoverageAdditionalFieldDOs = policyCoverageAdditionalFieldDOs;
	}

	public String getPlanTypeCd() {
		return planTypeCd;
	}

	public void setPlanTypeCd(String planTypeCd) {
		this.planTypeCd = planTypeCd;
	}

	public String getAssociationType() {
		return associationType;
	}
	public void setAssociationType(String associationType) {
		this.associationType = associationType;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Double getPremiumRate() {
		return premiumRate;
	}
	public void setPremiumRate(Double premiumRate) {
		this.premiumRate = premiumRate;
	}
	public Double getDefaultSumInsured() {
		return defaultSumInsured;
	}
	public void setDefaultSumInsured(Double defaultSumInsured) {
		this.defaultSumInsured = defaultSumInsured;
	}
	public boolean isSelectedFlag() {
		return isSelectedFlag;
	}
	public void setSelectedFlag(boolean isSelectedFlag) {
		this.isSelectedFlag = isSelectedFlag;
	}
	@Override
	public String getObjectName() {
		if(planTypeCd.equals("BASEPLAN")){
			return planTypeCd;
		}else{
			return PolicyCoverageDO.class.getSimpleName();
		}
	}
	
	public String StringFormattedSumInsured(){
		return this.sumInsured.toString();
	}

}
