package com.metamorphosys.insureconnect.dataobjects.transaction;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import com.metamorphosys.insureconnect.dataobjects.common.TransactionDO;


@Entity
@Table(name = "policy")
public class PolicyDO extends TransactionDO {
	
	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="policyId_seq") 
	@SequenceGenerator(name="policyId_seq",sequenceName="POLICYID_SEQ",allocationSize=1) Long id;


	@Pattern(regexp = "^[A-Za-z0-9-]*$")
	@Column(name = "leadId")
	private String leadId;
	
  	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "policyNum")
	private String policyNum;
	
	@Column(name = "policyIssueDt")
	private Date policyIssueDt;
	
	@Column(name = "proposalEntryDt")
	private Date proposalEntryDt;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "proposalNum")
	private String proposalNum;
	
	@Column(name = "proposalReceivedDt")
	private Date proposalReceivedDt;
	
	@Column(name = "proposalSignedDt")
	private Date proposalSignedDt;
	
	@Column(name = "policyCommencementDt")
	private Date policyCommencementDt;
	
	@Column(name = "policyExpiryDt")
	private Date policyExpiryDt;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "purposeOfInsuranceCd")
	private String purposeOfInsuranceCd;
	
	@Pattern(regexp = "^[A-Za-z0-9-.]*$")
	@Column(name = "quotationRefNum")
	private String quotationRefNum;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "submissionModeCd")
	private String submissionModeCd;
	
	@Pattern(regexp = "^[A-Za-z0-9-.]*$")
	@Column(name = "proposalRefNum")
	private String proposalRefNum;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "agentId")
	private String agentId;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "baseProductFamilyCd")
	private String baseProductFamilyCd;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "baseProductCd")
	private String baseProductCd;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "baseProductTypeCd")
	private String baseProductTypeCd;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "productPlanOptionCd")
	private String productPlanOptionCd;
	
	@Column(name = "baseProductVersion")
	private Integer baseProductVersion;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "policyTypeCd")
	private String policyTypeCd;

	@Pattern(regexp = "^[A-Za-z0-9]*$")	
	@Column(name = "branchCd")
	private String branchCd;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "channelCd")
	private String channelCd;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "policyStatusCd")
	private String policyStatusCd;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "processStatusCd")
	private String processStatusCd;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "stageCd")
	private String stageCd;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "uwDecisionCd")
	private String uwDecisionCd;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "uwDecisionValue")
	private String uwDecisionValue;
	
	@Column(name = "uwDecisionDt")
	private Date uwDecisionDt;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "workFlowDecisionCd")
	private String workFlowDecisionCd;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "premiumStatusCd")
	private String premiumStatusCd;

	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "validationStatusCd")
	private String validationStatusCd;
	
	@Column(name = "finalSumInsured")
	private Double finalSumInsured;
	
	@Column(name = "finalPremium")
	private Double finalPremium;
	
	@Column(name = "policyterm")
	private Integer policyterm;
	
	@Column(name = "commissionRate")
	private float commissionRate;
	
	@Column(name = "discountRate")
	private float discountRate;
	
	@Column(name = "ownCarInsurance")
	private String ownCarInsurance;
	
	@Column(name = "insuranceExpiryDt")
	private Date insuranceExpiryDt ;
	
	@Column(name = "insuranceCompanyCd")
	private String insuranceCompanyCd ;
	
	@Column(name = "insuranceProtectionType")
	private String insuranceProtectionType ;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<PolicyCoverageDO> policyCoverageList;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<PolicyPremiumSummaryDO> policyPremiumSummaryList;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<PolicyAgentDO> policyAgentList;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<PolicyPaymentDO> policyPaymentList;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<PolicyClientDO> policyClientList;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<PolicyAdditionalFieldDO> policyAdditionalFieldList;
	
	@Column(name="ruleEffectiveDate")
	private Date ruleEffectiveDate;
	
	@Override
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPolicyNum() {
		return policyNum;
	}
	public void setPolicyNum(String policyNum) {
		this.policyNum = policyNum;
	}
	public Date getPolicyIssueDt() {
		return policyIssueDt;
	}
	public void setPolicyIssueDt(Date policyIssueDt) {
		this.policyIssueDt = policyIssueDt;
	}
	public Date getProposalEntryDt() {
		return proposalEntryDt;
	}
	public void setProposalEntryDt(Date proposalEntryDt) {
		this.proposalEntryDt = proposalEntryDt;
	}
	public String getProposalNum() {
		return proposalNum;
	}
	public void setProposalNum(String proposalNum) {
		this.proposalNum = proposalNum;
	}
	public Date getProposalReceivedDt() {
		return proposalReceivedDt;
	}
	public void setProposalReceivedDt(Date proposalReceivedDt) {
		this.proposalReceivedDt = proposalReceivedDt;
	}
	public Date getProposalSignedDt() {
		return proposalSignedDt;
	}
	public void setProposalSignedDt(Date proposalSignedDt) {
		this.proposalSignedDt = proposalSignedDt;
	}
	public Date getPolicyCommencementDt() {
		return policyCommencementDt;
	}
	public void setPolicyCommencementDt(Date policyCommencementDt) {
		this.policyCommencementDt = policyCommencementDt;
	}
	public Date getPolicyExpiryDt() {
		return policyExpiryDt;
	}
	public void setPolicyExpiryDt(Date policyExpiryDt) {
		this.policyExpiryDt = policyExpiryDt;
	}
	public String getPurposeOfInsuranceCd() {
		return purposeOfInsuranceCd;
	}
	public void setPurposeOfInsuranceCd(String purposeOfInsuranceCd) {
		this.purposeOfInsuranceCd = purposeOfInsuranceCd;
	}
	public String getQuotationRefNum() {
		return quotationRefNum;
	}
	public void setQuotationRefNum(String quotationRefNum) {
		this.quotationRefNum = quotationRefNum;
	}
	public String getSubmissionModeCd() {
		return submissionModeCd;
	}
	public void setSubmissionModeCd(String submissionModeCd) {
		this.submissionModeCd = submissionModeCd;
	}
	public String getProposalRefNum() {
		return proposalRefNum;
	}
	public void setProposalRefNum(String proposalRefNum) {
		this.proposalRefNum = proposalRefNum;
	}
	public String getBaseProductFamilyCd() {
		return baseProductFamilyCd;
	}
	public void setBaseProductFamilyCd(String baseProductFamilyCd) {
		this.baseProductFamilyCd = baseProductFamilyCd;
	}
	public String getBaseProductCd() {
		return baseProductCd;
	}
	public void setBaseProductCd(String baseProductCd) {
		this.baseProductCd = baseProductCd;
	}
	public String getBaseProductTypeCd() {
		return baseProductTypeCd;
	}
	public void setBaseProductTypeCd(String baseProductTypeCd) {
		this.baseProductTypeCd = baseProductTypeCd;
	}
	public String getProductPlanOptionCd() {
		return productPlanOptionCd;
	}
	public void setProductPlanOptionCd(String productPlanOptionCd) {
		this.productPlanOptionCd = productPlanOptionCd;
	}
	public String getPolicyTypeCd() {
		return policyTypeCd;
	}
	public void setPolicyTypeCd(String policyTypeCd) {
		this.policyTypeCd = policyTypeCd;
	}
	public String getBranchCd() {
		return branchCd;
	}
	public void setBranchCd(String branchCd) {
		this.branchCd = branchCd;
	}
	public String getChannelCd() {
		return channelCd;
	}
	public void setChannelCd(String channelCd) {
		this.channelCd = channelCd;
	}
	public String getPolicyStatusCd() {
		return policyStatusCd;
	}
	public void setPolicyStatusCd(String policyStatusCd) {
		this.policyStatusCd = policyStatusCd;
	}
	public String getProcessStatusCd() {
		return processStatusCd;
	}
	public void setProcessStatusCd(String processStatusCd) {
		this.processStatusCd = processStatusCd;
	}
	public String getStageCd() {
		return stageCd;
	}
	public void setStageCd(String stageCd) {
		this.stageCd = stageCd;
	}
	public String getUwDecisionCd() {
		return uwDecisionCd;
	}
	public void setUwDecisionCd(String uwDecisionCd) {
		this.uwDecisionCd = uwDecisionCd;
	}
	public String getUwDecisionValue() {
		return uwDecisionValue;
	}
	public void setUwDecisionValue(String uwDecisionValue) {
		this.uwDecisionValue = uwDecisionValue;
	}
	public Date getUwDecisionDt() {
		return uwDecisionDt;
	}
	public void setUwDecisionDt(Date uwDecisionDt) {
		this.uwDecisionDt = uwDecisionDt;
	}
	public String getWorkFlowDecisionCd() {
		return workFlowDecisionCd;
	}
	public void setWorkFlowDecisionCd(String workFlowDecisionCd) {
		this.workFlowDecisionCd = workFlowDecisionCd;
	}
	public String getPremiumStatusCd() {
		return premiumStatusCd;
	}
	public void setPremiumStatusCd(String premiumStatusCd) {
		this.premiumStatusCd = premiumStatusCd;
	}
	public String getValidationStatusCd() {
		return validationStatusCd;
	}
	public void setValidationStatusCd(String validationStatusCd) {
		this.validationStatusCd = validationStatusCd;
	}
	public List<PolicyCoverageDO> getPolicyCoverageList() {
		return policyCoverageList;
	}
	public void setPolicyCoverageList(List<PolicyCoverageDO> policyCoverageList) {
		this.policyCoverageList = policyCoverageList;
	}
	public List<PolicyPremiumSummaryDO> getPolicyPremiumSummaryList() {
		return policyPremiumSummaryList;
	}
	public void setPolicyPremiumSummaryList(List<PolicyPremiumSummaryDO> policyPremiumSummaryList) {
		this.policyPremiumSummaryList = policyPremiumSummaryList;
	}
	public List<PolicyAgentDO> getPolicyAgentList() {
		return policyAgentList;
	}
	public void setPolicyAgentList(List<PolicyAgentDO> policyAgentList) {
		this.policyAgentList = policyAgentList;
	}
	public List<PolicyPaymentDO> getPolicyPaymentList() {
		return policyPaymentList;
	}
	public void setPolicyPaymentList(List<PolicyPaymentDO> policyPaymentList) {
		this.policyPaymentList = policyPaymentList;
	}
	public List<PolicyClientDO> getPolicyClientList() {
		return policyClientList;
	}
	public void setPolicyClientList(List<PolicyClientDO> policyClientList) {
		this.policyClientList = policyClientList;
	}
	public Integer getBaseProductVersion() {
		return baseProductVersion;
	}
	public void setBaseProductVersion(Integer baseProductVersion) {
		this.baseProductVersion = baseProductVersion;
	}
	public List<PolicyAdditionalFieldDO> getPolicyAdditionalFieldList() {
		return policyAdditionalFieldList;
	}
	public void setPolicyAdditionalFieldList(List<PolicyAdditionalFieldDO> policyAdditionalFieldList) {
		this.policyAdditionalFieldList = policyAdditionalFieldList;
	}
	public String getLeadId() {
		return leadId;
	}
	public void setLeadId(String leadId) {
		this.leadId = leadId;
	}
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public Double getFinalSumInsured() {
		return finalSumInsured;
	}
	public void setFinalSumInsured(Double finalSumInsured) {
		this.finalSumInsured = finalSumInsured;
	}
	
	public Date getRuleEffectiveDate() {
		return ruleEffectiveDate;
	}
	public void setRuleEffectiveDate(Date ruleEffectiveDate) {
		this.ruleEffectiveDate = ruleEffectiveDate;
	}
	@Override
	public String getObjectName() {
		return PolicyDO.class.getSimpleName();
	}
	public float getCommissionRate() {
		return commissionRate;
	}
	public void setCommissionRate(float commissionRate) {
		this.commissionRate = commissionRate;
	}
	public float getDiscountRate() {
		return discountRate;
	}
	public void setDiscountRate(float discountRate) {
		this.discountRate = discountRate;
	}
	public Double getFinalPremium() {
		return finalPremium;
	}
	public void setFinalPremium(Double finalPremium) {
		this.finalPremium = finalPremium;
	}
	
	public Integer getPolicyterm() {
		return policyterm;
	}
	public void setPolicyterm(Integer policyterm) {
		this.policyterm = policyterm;
	}
	
	
	public String getOwnCarInsurance() {
		return ownCarInsurance;
	}
	public void setOwnCarInsurance(String ownCarInsurance) {
		this.ownCarInsurance = ownCarInsurance;
	}
	
	public Date getInsuranceExpiryDt() {
		return insuranceExpiryDt;
	}
	public void setInsuranceExpiryDt(Date insuranceExpiryDt) {
		this.insuranceExpiryDt = insuranceExpiryDt;
	}
	public String getInsuranceCompanyCd() {
		return insuranceCompanyCd;
	}
	public void setInsuranceCompanyCd(String insuranceCompanyCd) {
		this.insuranceCompanyCd = insuranceCompanyCd;
	}
	public String getInsuranceProtectionType() {
		return insuranceProtectionType;
	}
	public void setInsuranceProtectionType(String insuranceProtectionType) {
		this.insuranceProtectionType = insuranceProtectionType;
	}
	public PolicyCoverageDO getBasePlan() {
		PolicyCoverageDO coverageDO = new PolicyCoverageDO();
		if(this.getPolicyCoverageList() != null && this.getPolicyCoverageList().size() > 0){
			for(PolicyCoverageDO tempCoverageDO : this.getPolicyCoverageList()){
		//			if("BASEPLAN".equals(coverageDO.getPlanTypeCd())){
		//				return coverageDO;
		//			}
					if(tempCoverageDO.getPlanTypeCd().toString().equals("BASEPLAN")){
		//					return coverageDO;
						coverageDO = tempCoverageDO;
						return coverageDO;
					}
				}
			}
		
		return null;
	
	}

}
