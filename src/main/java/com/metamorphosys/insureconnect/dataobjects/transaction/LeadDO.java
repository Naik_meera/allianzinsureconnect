package com.metamorphosys.insureconnect.dataobjects.transaction;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.TransactionDO;

@Entity
@Table(name="LEAD")
public class LeadDO extends TransactionDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="leadId_seq") 
	@SequenceGenerator(name="leadId_seq",sequenceName="LEADID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "baseAgentCd")
	private String baseAgentCd;
	
	@Column(name = "leadID")
	private String leadID;
	
	@Column(name = "leadTitleCd")
	private String leadTitleCd;
	
	@Column(name = "fullName")
	private String fullName;
	
	@Column(name = "genderCd")
	private String genderCd;
	
	@Column(name = "dateOfBirth")
	private Date dateOfBirth;
	
	@Column(name = "age")
	private Short age;
	
	@Column(name = "lastSyncDt")
	private Date lastSyncDt;
	
	@Column(name = "leadSource")
	private String leadSource;
	
	@Column(name = "leadStatus")
	private String leadStatus;
	
	@Column(name = "leadOccupation")
	private String leadOccupation;
	
	@Column(name = "leadIncome")
	private String leadIncome ; 
	
	@Column(name = "leadEducation")
	private String leadEducation;
	
	@Column(name = "interestedProduct")
	private String interestedProduct;
	
	@Column(name="agentId")
	private String agentId;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<LeadAddressDO> leadAddressList;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<LeadContactDetailsDO> leadContactDetailsList;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<LeadActivityDO> leadActivityList;
	
	public void setId(Long id) {
		this.id = id;
	}
	public String getBaseAgentCd() {
		return baseAgentCd;
	}
	public void setBaseAgentCd(String baseAgentCd) {
		this.baseAgentCd = baseAgentCd;
	}
	public String getLeadID() {
		return leadID;
	}
	public void setLeadID(String leadID) {
		this.leadID = leadID;
	}
	public String getLeadTitleCd() {
		return leadTitleCd;
	}
	public void setLeadTitleCd(String leadTitleCd) {
		this.leadTitleCd = leadTitleCd;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getGenderCd() {
		return genderCd;
	}
	public void setGenderCd(String genderCd) {
		this.genderCd = genderCd;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public Short getAge() {
		return age;
	}
	public void setAge(Short age) {
		this.age = age;
	}
	public Date getLastSyncDt() {
		return lastSyncDt;
	}
	public void setLastSyncDt(Date lastSyncDt) {
		this.lastSyncDt = lastSyncDt;
	}
	public String getLeadSource() {
		return leadSource;
	}
	public void setLeadSource(String leadSource) {
		this.leadSource = leadSource;
	}
	public String getLeadStatus() {
		return leadStatus;
	}
	public void setLeadStatus(String leadStatus) {
		this.leadStatus = leadStatus;
	}
	public String getLeadOccupation() {
		return leadOccupation;
	}
	public void setLeadOccupation(String leadOccupation) {
		this.leadOccupation = leadOccupation;
	}
	public String getLeadIncome() {
		return leadIncome;
	}
	public void setLeadIncome(String leadIncome) {
		this.leadIncome = leadIncome;
	}
	public String getLeadEducation() {
		return leadEducation;
	}
	public void setLeadEducation(String leadEducation) {
		this.leadEducation = leadEducation;
	}
	public String getInterestedProduct() {
		return interestedProduct;
	}
	public void setInterestedProduct(String interestedProduct) {
		this.interestedProduct = interestedProduct;
	}
	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return LeadDO.class.getSimpleName();
	}
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public List<LeadAddressDO> getLeadAddressList() {
		return leadAddressList;
	}
	public void setLeadAddressList(List<LeadAddressDO> leadAddressList) {
		this.leadAddressList = leadAddressList;
	}
	public List<LeadActivityDO> getLeadActivityList() {
		return leadActivityList;
	}
	public void setLeadActivityList(List<LeadActivityDO> leadActivityList) {
		this.leadActivityList = leadActivityList;
	}
	public List<LeadContactDetailsDO> getLeadContactDetailsList() {
		return leadContactDetailsList;
	}
	public void setLeadContactDetailsList(List<LeadContactDetailsDO> leadContactDetailsList) {
		this.leadContactDetailsList = leadContactDetailsList;
	}
}
