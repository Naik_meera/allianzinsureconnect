package com.metamorphosys.insureconnect.dataobjects.transaction;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "QUOTATIONCLIENT")
public class QuotationClientDO extends BaseDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="quotationClientId_seq") 
	@SequenceGenerator(name="quotationClientId_seq",sequenceName="QUOTATIONCLIENTID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "roleCd")
	private String roleCd;
	
	@Column(name = "genderCd")
	private String genderCd;
	
	@Column(name = "dateOfBirth")
	private Timestamp dateOfBirth;
	
	@Column(name = "age")
	private Integer age;
	
	@Column(name = "occupationClass")
	private String occupationClass;
	
	@Column(name = "titleCd")
	private String titleCd;
	
	@Column(name = "firstName")
	private String firstName;
	
	@Column(name = "lastName")
	private String lastName;
	
	@Column(name = "relationCd")
	private String relationCd;
	
	@Column(name = "parent_key")
	private String parent_key;
	
	@Column(name = "agentId")
	private String agentId;
	
	@Column(name = "baseAgentCd")
	private String baseAgentCd;
	
	@Column(name = "clientID")
	private Integer clientID;
	
	@Column(name = "quotationclientID")
	private Integer quotationclientID;
	
	@Column(name = "customerId")
	private Integer customerId;
	
	@Column(name = "clientTypeCd")
	private String clientTypeCd;
	
	@Column(name = "middleName")
	private String middleName;
	
	
	@Column(name = "firstName1")
	private String firstName1;
	
	@Column(name = "middleName1")
	private String middleName1;
	
	@Column(name = "lastName1")
	private String lastName1;
	
	@Column(name = "maritalStatusCd")
	private String maritalStatusCd;
	
	@Column(name = "occupationCd")
	private String occupationCd;
	
	@Column(name = "industryCd")
	private String industryCd;
	
	@Column(name = "natureOfDutyCd")
	private String natureOfDutyCd;
	
	@Column(name = "occupationClassCd")
	private String occupationClassCd;
	
	@Column(name = "medicalClassCd")
	private String medicalClassCd;
	
	@Column(name = "monthlyIncome")
	private String monthlyIncome;
	
	@Column(name = "mothersMaidenName")
	private String mothersMaidenName;
	
	@Column(name = "fathersName")
	private String fathersName;
	
	@Column(name = "givenName")
	private String givenName;
	
	@Column(name = "legalName")
	private String legalName;
	
	@Column(name = "otherName")
	private String otherName;
	
	@Column(name = "identityTypeCD")
	private String identityTypeCD;
	
	@Column(name = "identityNum")
	private Integer identityNum;
	
	@Column(name = "countryOfBirthCd")
	private String countryOfBirthCd;
	
	@Column(name = "citizenshipCd")
	private String citizenshipCd;
	
	@Column(name = "income")
	private Integer income;
	
	@Column(name = "isPoliticalFlag")
	private String isPoliticalFlag;
	
	@Column(name = "isConvictedFlag")
	private String isConvictedFlag;
	
	@Column(name = "clientStatusCd")
	private String clientStatusCd;
	
	@Column(name = "deDupStatusCd")
	private String deDupStatusCd;
	
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<QuotationClientAdditionalFieldDO> quotationClientAdditionalFieldList;
	

	@Override
	public String getObjectName() {
		if(null != roleCd)
		{
			return roleCd;
		}
		else
		{
			return QuotationClientDO.class.getSimpleName();
		}
	}

	@Override
	public Long getId() {
		return id;
	}

	public String getRoleCd() {
		return roleCd;
	}

	public void setRoleCd(String roleCd) {
		this.roleCd = roleCd;
	}

	public String getGenderCd() {
		return genderCd;
	}

	public void setGenderCd(String genderCd) {
		this.genderCd = genderCd;
	}


	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getOccupationClass() {
		return occupationClass;
	}

	public void setOccupationClass(String occupationClass) {
		this.occupationClass = occupationClass;
	}

	public String getTitleCd() {
		return titleCd;
	}

	public void setTitleCd(String titleCd) {
		this.titleCd = titleCd;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getRelationCd() {
		return relationCd;
	}

	public void setRelationCd(String relationCd) {
		this.relationCd = relationCd;
	}

	public List<QuotationClientAdditionalFieldDO> getQuotationClientAdditionalFieldList() {
		return quotationClientAdditionalFieldList;
	}

	public void setQuotationClientAdditionalFieldList(
			List<QuotationClientAdditionalFieldDO> quotationClientAdditionalFieldList) {
		this.quotationClientAdditionalFieldList = quotationClientAdditionalFieldList;
	}

	public Timestamp getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Timestamp dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getParent_key() {
		return parent_key;
	}

	public void setParent_key(String parent_key) {
		this.parent_key = parent_key;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getBaseAgentCd() {
		return baseAgentCd;
	}

	public void setBaseAgentCd(String baseAgentCd) {
		this.baseAgentCd = baseAgentCd;
	}

	public Integer getClientID() {
		return clientID;
	}

	public void setClientID(Integer clientID) {
		this.clientID = clientID;
	}

	public Integer getQuotationclientID() {
		return quotationclientID;
	}

	public void setQuotationclientID(Integer quotationclientID) {
		this.quotationclientID = quotationclientID;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getClientTypeCd() {
		return clientTypeCd;
	}

	public void setClientTypeCd(String clientTypeCd) {
		this.clientTypeCd = clientTypeCd;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getFirstName1() {
		return firstName1;
	}

	public void setFirstName1(String firstName1) {
		this.firstName1 = firstName1;
	}

	public String getMiddleName1() {
		return middleName1;
	}

	public void setMiddleName1(String middleName1) {
		this.middleName1 = middleName1;
	}

	public String getLastName1() {
		return lastName1;
	}

	public void setLastName1(String lastName1) {
		this.lastName1 = lastName1;
	}

	public String getMaritalStatusCd() {
		return maritalStatusCd;
	}

	public void setMaritalStatusCd(String maritalStatusCd) {
		this.maritalStatusCd = maritalStatusCd;
	}

	public String getOccupationCd() {
		return occupationCd;
	}

	public void setOccupationCd(String occupationCd) {
		this.occupationCd = occupationCd;
	}

	public String getIndustryCd() {
		return industryCd;
	}

	public void setIndustryCd(String industryCd) {
		this.industryCd = industryCd;
	}

	public String getNatureOfDutyCd() {
		return natureOfDutyCd;
	}

	public void setNatureOfDutyCd(String natureOfDutyCd) {
		this.natureOfDutyCd = natureOfDutyCd;
	}

	public String getOccupationClassCd() {
		return occupationClassCd;
	}

	public void setOccupationClassCd(String occupationClassCd) {
		this.occupationClassCd = occupationClassCd;
	}

	public String getMedicalClassCd() {
		return medicalClassCd;
	}

	public void setMedicalClassCd(String medicalClassCd) {
		this.medicalClassCd = medicalClassCd;
	}

	public String getMonthlyIncome() {
		return monthlyIncome;
	}

	public void setMonthlyIncome(String monthlyIncome) {
		this.monthlyIncome = monthlyIncome;
	}

	public String getMothersMaidenName() {
		return mothersMaidenName;
	}

	public void setMothersMaidenName(String mothersMaidenName) {
		this.mothersMaidenName = mothersMaidenName;
	}

	public String getFathersName() {
		return fathersName;
	}

	public void setFathersName(String fathersName) {
		this.fathersName = fathersName;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getLegalName() {
		return legalName;
	}

	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}

	public String getOtherName() {
		return otherName;
	}

	public void setOtherName(String otherName) {
		this.otherName = otherName;
	}

	public String getIdentityTypeCD() {
		return identityTypeCD;
	}

	public void setIdentityTypeCD(String identityTypeCD) {
		this.identityTypeCD = identityTypeCD;
	}

	public Integer getIdentityNum() {
		return identityNum;
	}

	public void setIdentityNum(Integer identityNum) {
		this.identityNum = identityNum;
	}

	public String getCountryOfBirthCd() {
		return countryOfBirthCd;
	}

	public void setCountryOfBirthCd(String countryOfBirthCd) {
		this.countryOfBirthCd = countryOfBirthCd;
	}

	public String getCitizenshipCd() {
		return citizenshipCd;
	}

	public void setCitizenshipCd(String citizenshipCd) {
		this.citizenshipCd = citizenshipCd;
	}

	public Integer getIncome() {
		return income;
	}

	public void setIncome(Integer income) {
		this.income = income;
	}

	public String getIsPoliticalFlag() {
		return isPoliticalFlag;
	}

	public void setIsPoliticalFlag(String isPoliticalFlag) {
		this.isPoliticalFlag = isPoliticalFlag;
	}

	public String getIsConvictedFlag() {
		return isConvictedFlag;
	}

	public void setIsConvictedFlag(String isConvictedFlag) {
		this.isConvictedFlag = isConvictedFlag;
	}

	public String getClientStatusCd() {
		return clientStatusCd;
	}

	public void setClientStatusCd(String clientStatusCd) {
		this.clientStatusCd = clientStatusCd;
	}

	public String getDeDupStatusCd() {
		return deDupStatusCd;
	}

	public void setDeDupStatusCd(String deDupStatusCd) {
		this.deDupStatusCd = deDupStatusCd;
	}


	public void setId(Long id) {
		this.id = id;
	}

}
