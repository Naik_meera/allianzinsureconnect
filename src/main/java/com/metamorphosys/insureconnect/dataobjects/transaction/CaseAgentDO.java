package com.metamorphosys.insureconnect.dataobjects.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "CASEAGENT")
public class CaseAgentDO extends BaseDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="caseAgentId_seq") 
	@SequenceGenerator(name="caseAgentId_seq",sequenceName="CASEAGENTID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "agentId")
	private String agentId;
	
	@Column(name = "agentName")
	private String agentName;
	
	@Column(name = "channelCd")
	private String channelCd;
	
	@Column(name = "isPrimaryAgentFlag")
	private boolean isPrimaryAgentFlag;
	
	@Column(name = "unitCd")
	private String unitCd;
	
	@Column(name = "userId")
	private String userId;
	
	@Override
	public Long getId() {
		return id;
	}
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public String getChannelCd() {
		return channelCd;
	}
	public void setChannelCd(String channelCd) {
		this.channelCd = channelCd;
	}
	public boolean isPrimaryAgentFlag() {
		return isPrimaryAgentFlag;
	}
	public void setPrimaryAgentFlag(boolean isPrimaryAgentFlag) {
		this.isPrimaryAgentFlag = isPrimaryAgentFlag;
	}
	public String getUnitCd() {
		return unitCd;
	}
	public void setUnitCd(String unitCd) {
		this.unitCd = unitCd;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	@Override
	public String getObjectName() {
		return CaseAgentDO.class.getSimpleName();
	}

}
