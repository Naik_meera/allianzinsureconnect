package com.metamorphosys.insureconnect.dataobjects.transaction;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.TransactionDO;

@Entity
@Table(name = "INBOXQUERY")
public class InboxQueryDO extends TransactionDO {
	
	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="inboxQueryId_seq") 
	@SequenceGenerator(name="inboxQueryId_seq",sequenceName="INBOXQUERYID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "query")
	private String query;
	
	@Column(name = "sortorder")
	private Integer sortorder;
	
	@Column(name = "postquery")
	private String postquery;
	
	@Column(name = "link")
	private String link;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<InboxCriteriaDO> inboxCriteriaDOList;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	@OrderBy("resultOrder")
	private List<InboxResultDO> inboxResultDOList;
	
	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public Integer getSortorder() {
		return sortorder;
	}

	public void setSortorder(Integer sortorder) {
		this.sortorder = sortorder;
	}

	public String getPostquery() {
		return postquery;
	}

	public void setPostquery(String postquery) {
		this.postquery = postquery;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public List<InboxCriteriaDO> getInboxCriteriaDOList() {
		return inboxCriteriaDOList;
	}

	public void setInboxCriteriaDOList(List<InboxCriteriaDO> inboxCriteriaDOList) {
		this.inboxCriteriaDOList = inboxCriteriaDOList;
	}

	public List<InboxResultDO> getInboxResultDOList() {
		return inboxResultDOList;
	}

	public void setInboxResultDOList(List<InboxResultDO> inboxResultDOList) {
		this.inboxResultDOList = inboxResultDOList;
	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return InboxQueryDO.class.getSimpleName();
	}

}
