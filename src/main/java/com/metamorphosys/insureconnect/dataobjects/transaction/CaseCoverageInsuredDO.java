package com.metamorphosys.insureconnect.dataobjects.transaction;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "CASECOVERAGEINSURED")
public class CaseCoverageInsuredDO extends BaseDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="caseCoverageInsuredId_seq") 
	@SequenceGenerator(name="caseCoverageInsuredId_seq",sequenceName="CASECOVERAGEINSUREDID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "bodyTypeCd")
	private String bodyTypeCd;
	
	@Column(name = "chasisNum")
	private String chasisNum;
	
	@Column(name = "clientID")
	private Long clientID;
	
	@Column(name = "customerId")
	private String customerId;
	
	@Column(name = "engineCapacity")
	private String engineCapacity;
	
	@Column(name = "engineNum")
	private String engineNum;
	
	@Column(name = "entryAge")
	private short entryAge;
	
	@Column(name = "fuelTypeCd")
	private String fuelTypeCd;
	
	@Column(name = "manufacturingDt")
	private Integer manufacturingDt;
	
	@Column(name = "medicalClassCd")
	private String medicalClassCd;
	
	@Column(name = "registrationDt")
	private Date registrationDt;
	
	@Column(name = "registrationNum")
	private String registrationNum;
	
	@Column(name = "roleCd")
	private String roleCd;
	
	@Column(name = "seatingCapacity")
	private short seatingCapacity;
	
	@Column(name = "transmissionType")
	private String transmissionType;
	
	@Column(name = "uwClassCd")
	private String uwClassCd;
	
	@Column(name = "uwDecisionCd")
	private String uwDecisionCd;
	
	@Column(name = "uwDecisionValue")
	private String uwDecisionValue;
	
	@Column(name = "vehicelModelCd")
	private String vehicelModelCd;
	
	@Column(name = "vehicelTypeCd")
	private String vehicelTypeCd;
	
	@Column(name = "vehicleColour")
	private String vehicleColour;
	
	@Column(name = "vehicleMakeCd")
	private String vehicleMakeCd;
	
	@Column(name = "vehicleVariant")
	private String vehicleVariant;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<CaseCoverageInsuredLoadingDO> caseCoverageInsuredLoadingList;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<CaseCoverageInsuredDiscountDO> caseCoverageInsuredDiscountList;
	
	@Override
	public Long getId() {
		return id;
	}
	public String getBodyTypeCd() {
		return bodyTypeCd;
	}
	public void setBodyTypeCd(String bodyTypeCd) {
		this.bodyTypeCd = bodyTypeCd;
	}
	public String getChasisNum() {
		return chasisNum;
	}
	public void setChasisNum(String chasisNum) {
		this.chasisNum = chasisNum;
	}
	public Long getClientID() {
		return clientID;
	}
	public void setClientID(Long clientID) {
		this.clientID = clientID;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getEngineCapacity() {
		return engineCapacity;
	}
	public void setEngineCapacity(String engineCapacity) {
		this.engineCapacity = engineCapacity;
	}
	public String getEngineNum() {
		return engineNum;
	}
	public void setEngineNum(String engineNum) {
		this.engineNum = engineNum;
	}
	public short getEntryAge() {
		return entryAge;
	}
	public void setEntryAge(short entryAge) {
		this.entryAge = entryAge;
	}
	public String getFuelTypeCd() {
		return fuelTypeCd;
	}
	public void setFuelTypeCd(String fuelTypeCd) {
		this.fuelTypeCd = fuelTypeCd;
	}
	public Integer getManufacturingDt() {
		return manufacturingDt;
	}
	public void setManufacturingDt(Integer manufacturingDt) {
		this.manufacturingDt = manufacturingDt;
	}
	public String getMedicalClassCd() {
		return medicalClassCd;
	}
	public void setMedicalClassCd(String medicalClassCd) {
		this.medicalClassCd = medicalClassCd;
	}
	public Date getRegistrationDt() {
		return registrationDt;
	}
	public void setRegistrationDt(Date registrationDt) {
		this.registrationDt = registrationDt;
	}
	public String getRegistrationNum() {
		return registrationNum;
	}
	public void setRegistrationNum(String registrationNum) {
		this.registrationNum = registrationNum;
	}
	public String getRoleCd() {
		return roleCd;
	}
	public void setRoleCd(String roleCd) {
		this.roleCd = roleCd;
	}
	public short getSeatingCapacity() {
		return seatingCapacity;
	}
	public void setSeatingCapacity(short seatingCapacity) {
		this.seatingCapacity = seatingCapacity;
	}
	public String getTransmissionType() {
		return transmissionType;
	}
	public void setTransmissionType(String transmissionType) {
		this.transmissionType = transmissionType;
	}
	public String getUwClassCd() {
		return uwClassCd;
	}
	public void setUwClassCd(String uwClassCd) {
		this.uwClassCd = uwClassCd;
	}
	public String getUwDecisionCd() {
		return uwDecisionCd;
	}
	public void setUwDecisionCd(String uwDecisionCd) {
		this.uwDecisionCd = uwDecisionCd;
	}
	public String getUwDecisionValue() {
		return uwDecisionValue;
	}
	public void setUwDecisionValue(String uwDecisionValue) {
		this.uwDecisionValue = uwDecisionValue;
	}
	public String getVehicelModelCd() {
		return vehicelModelCd;
	}
	public void setVehicelModelCd(String vehicelModelCd) {
		this.vehicelModelCd = vehicelModelCd;
	}
	public String getVehicelTypeCd() {
		return vehicelTypeCd;
	}
	public void setVehicelTypeCd(String vehicelTypeCd) {
		this.vehicelTypeCd = vehicelTypeCd;
	}
	public String getVehicleColour() {
		return vehicleColour;
	}
	public void setVehicleColour(String vehicleColour) {
		this.vehicleColour = vehicleColour;
	}
	public String getVehicleMakeCd() {
		return vehicleMakeCd;
	}
	public void setVehicleMakeCd(String vehicleMakeCd) {
		this.vehicleMakeCd = vehicleMakeCd;
	}
	public String getVehicleVariant() {
		return vehicleVariant;
	}
	public void setVehicleVariant(String vehicleVariant) {
		this.vehicleVariant = vehicleVariant;
	}
	public List<CaseCoverageInsuredLoadingDO> getCaseCoverageInsuredLoadingList() {
		return caseCoverageInsuredLoadingList;
	}
	public void setCaseCoverageInsuredLoadingList(List<CaseCoverageInsuredLoadingDO> caseCoverageInsuredLoadingList) {
		this.caseCoverageInsuredLoadingList = caseCoverageInsuredLoadingList;
	}
	public List<CaseCoverageInsuredDiscountDO> getCaseCoverageInsuredDiscountList() {
		return caseCoverageInsuredDiscountList;
	}
	public void setPolicyCoverageInsuredDiscountList(
			List<CaseCoverageInsuredDiscountDO> caseCoverageInsuredDiscountList) {
		this.caseCoverageInsuredDiscountList = caseCoverageInsuredDiscountList;
	}
	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return CaseCoverageInsuredDO.class.getSimpleName();
	}
}
