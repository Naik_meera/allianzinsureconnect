package com.metamorphosys.insureconnect.dataobjects.transaction;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.TransactionDO;

@Entity
@Table(name = "HASHMAPENTITY")
public class HashMapEntityDO extends TransactionDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="hashmapEntityId_seq") 
	@SequenceGenerator(name="hashmapEntityId_seq",sequenceName="HASHMAPENTITYID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "entityName")
	private String entityName;
	
	@Column(name = "entityId")
	private String entityId;
	
	@Column(name = "entityGuid")
	private String entityGuid;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<HashMapDataDO> hashMapDataList;

	@Override
	public String getObjectName() {
		if(null != entityName)
		{
			return entityName;
		}
		else
		{
			return HashMapEntityDO.class.getSimpleName();
		}
	}

	@Override
	public Long getId() {
		return id;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getEntityGuid() {
		return entityGuid;
	}

	public void setEntityGuid(String entityGuid) {
		this.entityGuid = entityGuid;
	}

	public List<HashMapDataDO> getHashMapDataList() {
		return hashMapDataList;
	}

	public void setHashMapDataList(List<HashMapDataDO> hashMapDataList) {
		this.hashMapDataList = hashMapDataList;
	}

}
