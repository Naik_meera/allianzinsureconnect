package com.metamorphosys.insureconnect.dataobjects.transaction;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.metamorphosys.insureconnect.dataobjects.common.TransactionDO;

@Entity
@Table(name="AGENT")
public class AgentDO extends TransactionDO {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="agentId_seq")
	@SequenceGenerator(name="agentId_seq",sequenceName="AGENTID_SEQ",allocationSize=1)
	Long id;
	
	@Column(name = "agentId")
	private String agentId;
	
	@Column(name = "agentFirstName")
    private String agentFirstName;
	
	@Column(name = "agentLastName")
    private String agentLastName;
	
	@Column(name = "emailId")
    private String emailId;
	
	@Column(name = "password")
    private String password;
	
	@Column(name = "userId")
    private String userId;
	
	@Column(name = "channel")
    private String channel;
	
	@Column(name = "agentStatusCd")
    private String agentStatusCd;
	
	@Column(name = "subChannel")
    private String subChannel;
	
	@Column(name = "agentDesignation")
    private String agentDesignation;
	
	@Column(name = "applicableProducts")
    private String applicableProducts;
	
	@Column(name = "source")
    private String source;
	
	@Column(name = "activeDays")
    private Integer activeDays;
	
	@Column(name = "mobileNumber")
    private String mobileNumber;
	
	@Column(name = "lastloginDt")
    private Timestamp lastloginDt;
	
	@Column(name = "licenseExpiryDt")
    private Timestamp licenseExpiryDt;
	
	@Column(name = "lastSyncDt")
    private Timestamp lastSyncDt;
	
	@Column(name = "licenseIssueDt")
    private Timestamp licenseIssueDt;
	
	@Column(name = "groupList")
    private String  groupList;
	
	@Column(name="agentCategory")
	private String agentCategory;
    
    @Transient
    private String deviceUserSeq;
    @Transient
    private Timestamp lastOnlineDt;
    
    @Transient
    private Integer policyseq;
    
    @Transient
    private Integer quotationseq;
    
    @Transient
    private Timestamp lastDownloadDt;
    
    @Column(name="authToken")
	private String authToken;
    
    public String getAuthToken() {
		return authToken;
	}
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
	public Timestamp getLastDownloadDt() {
		return lastDownloadDt;
	}
	public void setLastDownloadDt(Timestamp lastDownloadDt) {
		this.lastDownloadDt = lastDownloadDt;
	}
	public Integer getPolicyseq() {
		return policyseq;
	}
	public void setPolicyseq(Integer policyseq) {
		this.policyseq = policyseq;
	}
	public Integer getQuotationseq() {
		return quotationseq;
	}
	public void setQuotationseq(Integer quotationseq) {
		this.quotationseq = quotationseq;
	}
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<AgentDeviceMappingDO> agentDeviceMappingDOList;
    
    
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public String getAgentFirstName() {
		return agentFirstName;
	}
	public void setAgentFirstName(String agentFirstName) {
		this.agentFirstName = agentFirstName;
	}
	public String getAgentLastName() {
		return agentLastName;
	}
	public void setAgentLastName(String agentLastName) {
		this.agentLastName = agentLastName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getAgentStatusCd() {
		return agentStatusCd;
	}
	public void setAgentStatusCd(String agentStatusCd) {
		this.agentStatusCd = agentStatusCd;
	}
	public String getSubChannel() {
		return subChannel;
	}
	public void setSubChannel(String subChannel) {
		this.subChannel = subChannel;
	}
	public String getAgentDesignation() {
		return agentDesignation;
	}
	public void setAgentDesignation(String agentDesignation) {
		this.agentDesignation = agentDesignation;
	}
	public String getApplicableProducts() {
		return applicableProducts;
	}
	public void setApplicableProducts(String applicableProducts) {
		this.applicableProducts = applicableProducts;
	}
	public Integer getActiveDays() {
		return activeDays;
	}
	public void setActiveDays(Integer activeDays) {
		this.activeDays = activeDays;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getAgentCategory() {
		return agentCategory;
	}
	public void setAgentCategory(String agentCategory) {
		this.agentCategory = agentCategory;
	}
	public Timestamp getLastloginDt() {
		return lastloginDt;
	}
	public void setLastloginDt(Timestamp lastloginDt) {
		this.lastloginDt = lastloginDt;
	}
	public Timestamp getLicenseExpiryDt() {
		return licenseExpiryDt;
	}
	public void setLicenseExpiryDt(Timestamp licenseExpiryDt) {
		this.licenseExpiryDt = licenseExpiryDt;
	}
	public Timestamp getLastSyncDt() {
		return lastSyncDt;
	}
	public void setLastSyncDt(Timestamp lastSyncDt) {
		this.lastSyncDt = lastSyncDt;
	}
	public Timestamp getLicenseIssueDt() {
		return licenseIssueDt;
	}
	public void setLicenseIssueDt(Timestamp licenseIssueDt) {
		this.licenseIssueDt = licenseIssueDt;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<AgentDeviceMappingDO> getAgentDeviceMappingDOList() {
		return agentDeviceMappingDOList;
	}
	public void setAgentDeviceMappingDOList(List<AgentDeviceMappingDO> agentDeviceMappingDOList) {
		this.agentDeviceMappingDOList = agentDeviceMappingDOList;
	}
	@Override
	public Long getId() {
		return id;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public Timestamp getLastOnlineDt() {
		return lastOnlineDt;
	}
	public void setLastOnlineDt(Timestamp lastOnlineDt) {
		this.lastOnlineDt = lastOnlineDt;
	}
	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return AgentDO.class.getSimpleName();
	}
	public String getDeviceUserSeq() {
		return deviceUserSeq;
	}
	public void setDeviceUserSeq(String deviceUserSeq) {
		this.deviceUserSeq = deviceUserSeq;
	}
	public String getGroupList() {
		return groupList;
	}
	public void setGroupList(String groupList) {
		this.groupList = groupList;
	}
    
}
