package com.metamorphosys.insureconnect.dataobjects.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "POLICYCOVERAGEINSUREDDSC")
public class PolicyCoverageInsuredDiscountDO extends BaseDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="polCvrgeInsurdDscId_seq") 
	@SequenceGenerator(name="polCvrgeInsurdDscId_seq",sequenceName="POLCVRGEINSURDDSCID_SEQ",allocationSize=1) Long id;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "discountReason")
	private String discountReason;
	
	@Column(name = "discountValue")
	private Double discountValue;
	
	@Column(name = "discoutPremium")
	private Double discoutPremium;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "discoutType")
	private String discoutType;
	
	@Override
	public Long getId() {
		return id;
	}
	public String getDiscountReason() {
		return discountReason;
	}
	public void setDiscountReason(String discountReason) {
		this.discountReason = discountReason;
	}
	public Double getDiscountValue() {
		return discountValue;
	}
	public void setDiscountValue(Double discountValue) {
		this.discountValue = discountValue;
	}
	public Double getDiscoutPremium() {
		return discoutPremium;
	}
	public void setDiscoutPremium(Double discoutPremium) {
		this.discoutPremium = discoutPremium;
	}
	public String getDiscoutType() {
		return discoutType;
	}
	public void setDiscoutType(String discoutType) {
		this.discoutType = discoutType;
	}
	@Override
	public String getObjectName() {
		return PolicyCoverageInsuredDiscountDO.class.getSimpleName();
	}


}
