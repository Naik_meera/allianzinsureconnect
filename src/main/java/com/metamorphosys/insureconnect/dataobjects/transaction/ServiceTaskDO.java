package com.metamorphosys.insureconnect.dataobjects.transaction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "SERVICETASK")
public class ServiceTaskDO extends BaseDO {

	private @Id @GeneratedValue(strategy = GenerationType.AUTO) Long id;
	private String serviceTask;

	public String getServiceTask() {
		return serviceTask;
	}

	public void setServiceTask(String serviceTask) {
		this.serviceTask = serviceTask;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public String getObjectName() {
		return ServiceTaskDO.class.getSimpleName();
	}

}
