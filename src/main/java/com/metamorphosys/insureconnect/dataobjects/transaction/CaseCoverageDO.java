package com.metamorphosys.insureconnect.dataobjects.transaction;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "CASECOVERAGE")
public class CaseCoverageDO extends BaseDO {

private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="caseCoverageId_seq") 
@SequenceGenerator(name="caseCoverageId_seq",sequenceName="CASECOVERAGEID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "productCd")
	private String productCd;
	
	@Column(name = "productVersion")
	private Long productVersion;

	@Column(name = "planOptionCd")
	private String planOptionCd;
	
	@Column(name = "baseAnnualPremium")
	private Double baseAnnualPremium;
	
	@Column(name = "baseModalPremium")
	private Double baseModalPremium;

	@Column(name = "totalExtraPremium")
	private Double totalExtraPremium;

	@Column(name = "totalDiscount")
	private Double totalDiscount;
	
	@Column(name = "totalRiderPremium")
	private Double totalRiderPremium;

	@Column(name = "totalPremium")
	private Double totalPremium;
	
	@Column(name = "policyterm")
	private Long policyterm;
	
	@Column(name = "paymentMethodCd")
	private String paymentMethodCd;

	@Column(name = "paymentFrequencyCd")
	private String paymentFrequencyCd;
	
	@Column(name = "paymentTerm")
	private Integer paymentTerm;
	
	@Column(name = "coverageType")
	private String coverageType;
	
	@Column(name = "sumInsured")
	private Double sumInsured;
	
	@Column(name = "uwDecisionCd")
	private String uwDecisionCd;
	
	@Column(name = "uwDecisionValue")
	private String uwDecisionValue;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	List<CaseCoverageInsuredDO> caseCoverageInsuredDOList;

	public String getProductCd() {
		return productCd;
	}

	public void setProductCd(String productCd) {
		this.productCd = productCd;
	}

	public Long getProductVersion() {
		return productVersion;
	}

	public void setProductVersion(Long productVersion) {
		this.productVersion = productVersion;
	}

	public String getPlanOptionCd() {
		return planOptionCd;
	}

	public void setPlanOptionCd(String planOptionCd) {
		this.planOptionCd = planOptionCd;
	}

	public Double getBaseAnnualPremium() {
		return baseAnnualPremium;
	}

	public void setBaseAnnualPremium(Double baseAnnualPremium) {
		this.baseAnnualPremium = baseAnnualPremium;
	}

	public Double getBaseModalPremium() {
		return baseModalPremium;
	}

	public void setBaseModalPremium(Double baseModalPremium) {
		this.baseModalPremium = baseModalPremium;
	}

	public Double getTotalExtraPremium() {
		return totalExtraPremium;
	}

	public void setTotalExtraPremium(Double totalExtraPremium) {
		this.totalExtraPremium = totalExtraPremium;
	}

	public Double getTotalDiscount() {
		return totalDiscount;
	}

	public void setTotalDiscount(Double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}

	public Double getTotalRiderPremium() {
		return totalRiderPremium;
	}

	public void setTotalRiderPremium(Double totalRiderPremium) {
		this.totalRiderPremium = totalRiderPremium;
	}

	public Double getTotalPremium() {
		return totalPremium;
	}

	public void setTotalPremium(Double totalPremium) {
		this.totalPremium = totalPremium;
	}

	public Long getPolicyterm() {
		return policyterm;
	}

	public void setPolicyterm(Long policyterm) {
		this.policyterm = policyterm;
	}

	public String getPaymentMethodCd() {
		return paymentMethodCd;
	}

	public void setPaymentMethodCd(String paymentMethodCd) {
		this.paymentMethodCd = paymentMethodCd;
	}

	public String getPaymentFrequencyCd() {
		return paymentFrequencyCd;
	}

	public void setPaymentFrequencyCd(String paymentFrequencyCd) {
		this.paymentFrequencyCd = paymentFrequencyCd;
	}

	public Integer getPaymentTerm() {
		return paymentTerm;
	}

	public void setPaymentTerm(Integer paymentTerm) {
		this.paymentTerm = paymentTerm;
	}

	public String getCoverageType() {
		return coverageType;
	}

	public void setCoverageType(String coverageType) {
		this.coverageType = coverageType;
	}

	public Double getSumInsured() {
		return sumInsured;
	}

	public void setSumInsured(Double sumInsured) {
		this.sumInsured = sumInsured;
	}

	public String getUwDecisionCd() {
		return uwDecisionCd;
	}

	public void setUwDecisionCd(String uwDecisionCd) {
		this.uwDecisionCd = uwDecisionCd;
	}

	public String getUwDecisionValue() {
		return uwDecisionValue;
	}

	public void setUwDecisionValue(String uwDecisionValue) {
		this.uwDecisionValue = uwDecisionValue;
	}

	public List<CaseCoverageInsuredDO> getCaseCoverageInsuredDOList() {
		return caseCoverageInsuredDOList;
	}

	public void setCaseCoverageInsuredDOList(List<CaseCoverageInsuredDO> caseCoverageInsuredDOList) {
		this.caseCoverageInsuredDOList = caseCoverageInsuredDOList;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return CaseCoverageDO.class.getSimpleName();
	} 

}
