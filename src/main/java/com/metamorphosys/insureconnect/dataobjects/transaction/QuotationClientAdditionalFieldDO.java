package com.metamorphosys.insureconnect.dataobjects.transaction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.AdditionalFieldDO;

@Entity
@Table(name = "QUOTATIONCLIENTADDFLD")
public class QuotationClientAdditionalFieldDO extends AdditionalFieldDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="quotationClientAddFldId_seq") 
	@SequenceGenerator(name="quotationClientAddFldId_seq",sequenceName="QUOTATIONCLIENTADDFLDID_SEQ",allocationSize=1) Long id;

	@Override
	public Long getId() {
		return id;
	}

}
