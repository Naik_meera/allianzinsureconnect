package com.metamorphosys.insureconnect.dataobjects.transaction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.AdditionalFieldDO;

@Entity
@Table(name = "HASHMAPDATA")
public class HashMapDataDO extends AdditionalFieldDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="hashMapDataId_seq") 
	@SequenceGenerator(name="hashMapDataId_seq",sequenceName="HASHMAPDATAID_SEQ",allocationSize=1) Long id;

	@Override
	public Long getId() {
		return id;
	}

}
