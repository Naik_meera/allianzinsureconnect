
package com.metamorphosys.insureconnect.dataobjects.transaction;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.TransactionDO;

@Entity
@Table(name="LEADACTIVITY")
public class LeadActivityDO  extends TransactionDO {
	
	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="leadActivityId_seq") 
	@SequenceGenerator(name="leadActivityId_seq",sequenceName="LEADACTIVITYID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "parent_key")
	private String parent_key;
	
	@Column(name = "leadID")
	private Integer leadID;
	
	@Column(name = "leadActivityID")
	private Integer leadActivityID;
	
	@Column(name = "activityTypeCd")
	private String activityTypeCd;
	
	@Column(name = "activityStatusCd")
	private String activityStatusCd;
	
	@Column(name = "activityNotes")
	private String activityNotes;
	
	@Column(name = "agentId")
	private String agentId;
	
	@Column(name = "baseAgentCd")
	private String baseAgentCd;
	
	@Column(name = "lastSyncDt")
	private Timestamp lastSyncDt;
	
	@Column(name = "scheduledDt")
	private Timestamp scheduledDt;

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getParent_key() {
		return parent_key;
	}

	public void setParent_key(String parent_key) {
		this.parent_key = parent_key;
	}


	public String getActivityTypeCd() {
		return activityTypeCd;
	}

	public void setActivityTypeCd(String activityTypeCd) {
		this.activityTypeCd = activityTypeCd;
	}

	public String getActivityStatusCd() {
		return activityStatusCd;
	}

	public void setActivityStatusCd(String activityStatusCd) {
		this.activityStatusCd = activityStatusCd;
	}

	public String getActivityNotes() {
		return activityNotes;
	}

	public void setActivityNotes(String activityNotes) {
		this.activityNotes = activityNotes;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getBaseAgentCd() {
		return baseAgentCd;
	}

	public void setBaseAgentCd(String baseAgentCd) {
		this.baseAgentCd = baseAgentCd;
	}

	public Integer getLeadID() {
		return leadID;
	}

	public void setLeadID(Integer leadID) {
		this.leadID = leadID;
	}

	public Integer getLeadActivityID() {
		return leadActivityID;
	}

	public void setLeadActivityID(Integer leadActivityID) {
		this.leadActivityID = leadActivityID;
	}

	public Timestamp getLastSyncDt() {
		return lastSyncDt;
	}

	public void setLastSyncDt(Timestamp lastSyncDt) {
		this.lastSyncDt = lastSyncDt;
	}

	public Timestamp getScheduledDt() {
		return scheduledDt;
	}

	public void setScheduledDt(Timestamp scheduledDt) {
		this.scheduledDt = scheduledDt;
	}

	public void setId(Long id) {
		this.id = id;
	}


}
