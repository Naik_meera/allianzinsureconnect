package com.metamorphosys.insureconnect.dataobjects.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "INBOXCRITERIA")
public class InboxCriteriaDO extends BaseDO {
	
	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="inboxCriteriaId_seq") 
	@SequenceGenerator(name="inboxCriteriaId_seq",sequenceName="INBOXCRITERIAID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "criteriafield")
	private String criteriafield;
	
	@Column(name = "criteriaOrder")
	private Integer criteriaOrder;
	
	public String getCriteriafield() {
		return criteriafield;
	}

	public void setCriteriafield(String criteriafield) {
		this.criteriafield = criteriafield;
	}

	public Integer getCriteriaOrder() {
		return criteriaOrder;
	}

	public void setCriteriaOrder(Integer criteriaOrder) {
		this.criteriaOrder = criteriaOrder;
	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return InboxCriteriaDO.class.getSimpleName();
	}

}
