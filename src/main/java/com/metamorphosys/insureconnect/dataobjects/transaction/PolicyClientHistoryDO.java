package com.metamorphosys.insureconnect.dataobjects.transaction;

import javax.persistence.Id;

public class PolicyClientHistoryDO {
	
	@Id
	private String clientHistoryseq;

	public String getClientHistoryseq() {
		return clientHistoryseq;
	}

	public void setClientHistoryseq(String clientHistoryseq) {
		this.clientHistoryseq = clientHistoryseq;
	}

}
