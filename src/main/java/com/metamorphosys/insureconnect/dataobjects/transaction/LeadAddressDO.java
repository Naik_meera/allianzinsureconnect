package com.metamorphosys.insureconnect.dataobjects.transaction;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.TransactionDO;
@Entity
@Table(name="LEADADDRESS")
public class LeadAddressDO  extends TransactionDO {
	
	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="leadAddressId_seq") 
	@SequenceGenerator(name="leadAddressId_seq",sequenceName="LEADADDRESSID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "parent_key")
	private String parent_key;
	
	@Column(name = "agentId")
	private String agentId;
	
	@Column(name = "baseAgentCd")
	private String baseAgentCd;
	
	@Column(name = "leadID")
	private Integer leadID;
	
	@Column(name = "leadAddressId")
	private Integer leadAddressId;
	
	@Column(name = "leadAddress")
	private String leadAddress;
	
	@Column(name = "addressTypeCd")
	private String addressTypeCd;
	
	@Column(name = "addressSubTypeCd")
	private String addressSubTypeCd;
	
	@Column(name = "addressLine1")
	private String addressLine1;
	
	@Column(name = "addressLine2")
	private String addressLine2;
	
	@Column(name = "addressLine3")
	private String addressLine3;
	
	@Column(name = "addressLine4")
	private String addressLine4;
	
	@Column(name = "cityCd")
	private String cityCd;
	
	@Column(name = "talukaCd")
	private String talukaCd;
	
	@Column(name = "villageCd")
	private String villageCd;
	
	@Column(name = "stateCd")
	private String stateCd;
	
	@Column(name = "countryCd")
	private String countryCd;
	
	@Column(name = "postCd")
	private Integer postCd;
	
	@Column(name = "lastSyncDt")
	private Timestamp lastSyncDt;
	
	

	public String getParent_key() {
		return parent_key;
	}

	public void setParent_key(String parent_key) {
		this.parent_key = parent_key;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getBaseAgentCd() {
		return baseAgentCd;
	}

	public void setBaseAgentCd(String baseAgentCd) {
		this.baseAgentCd = baseAgentCd;
	}

	public Integer getLeadID() {
		return leadID;
	}

	public void setLeadID(Integer leadID) {
		this.leadID = leadID;
	}

	public Integer getLeadAddressId() {
		return leadAddressId;
	}

	public void setLeadAddressId(Integer leadAddressId) {
		this.leadAddressId = leadAddressId;
	}

	public String getLeadAddress() {
		return leadAddress;
	}

	public void setLeadAddress(String leadAddress) {
		this.leadAddress = leadAddress;
	}

	public String getAddressTypeCd() {
		return addressTypeCd;
	}

	public void setAddressTypeCd(String addressTypeCd) {
		this.addressTypeCd = addressTypeCd;
	}

	public String getAddressSubTypeCd() {
		return addressSubTypeCd;
	}

	public void setAddressSubTypeCd(String addressSubTypeCd) {
		this.addressSubTypeCd = addressSubTypeCd;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getAddressLine3() {
		return addressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	public String getAddressLine4() {
		return addressLine4;
	}

	public void setAddressLine4(String addressLine4) {
		this.addressLine4 = addressLine4;
	}

	public String getCityCd() {
		return cityCd;
	}

	public void setCityCd(String cityCd) {
		this.cityCd = cityCd;
	}

	public String getTalukaCd() {
		return talukaCd;
	}

	public void setTalukaCd(String talukaCd) {
		this.talukaCd = talukaCd;
	}

	public String getVillageCd() {
		return villageCd;
	}

	public void setVillageCd(String villageCd) {
		this.villageCd = villageCd;
	}

	public String getStateCd() {
		return stateCd;
	}

	public void setStateCd(String stateCd) {
		this.stateCd = stateCd;
	}

	public String getCountryCd() {
		return countryCd;
	}

	public void setCountryCd(String countryCd) {
		this.countryCd = countryCd;
	}

	public Integer getPostCd() {
		return postCd;
	}

	public void setPostCd(Integer postCd) {
		this.postCd = postCd;
	}

	public Timestamp getLastSyncDt() {
		return lastSyncDt;
	}

	public void setLastSyncDt(Timestamp lastSyncDt) {
		this.lastSyncDt = lastSyncDt;
	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return null;
	}

}
