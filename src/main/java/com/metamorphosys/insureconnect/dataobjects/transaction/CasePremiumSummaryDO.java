package com.metamorphosys.insureconnect.dataobjects.transaction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "CASEPREMIUMSUMMARY")
public class CasePremiumSummaryDO extends BaseDO{

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="casePremiumSummaryId_seq") 
	@SequenceGenerator(name="casePremiumSummaryId_seq",sequenceName="CASEPREMIUMSUMMARYID_SEQ",allocationSize=1) Long id;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return CasePremiumSummaryDO.class.getSimpleName();
	}

}
