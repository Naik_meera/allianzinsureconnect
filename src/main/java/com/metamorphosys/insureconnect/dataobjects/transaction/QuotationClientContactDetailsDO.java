package com.metamorphosys.insureconnect.dataobjects.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.TransactionDO;
@Entity
@Table(name="QUOTATIONCLIENTCONTACTDETAILS")
public class QuotationClientContactDetailsDO  extends TransactionDO {
	
	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="quotationClientContactDetailsId_seq") 
	@SequenceGenerator(name="quotationClientContactDetailsId_seq",sequenceName="QUOTATIONCLIENTCONTACTDETAILSID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "quotationClientContactDetailsID")
	private Integer quotationClientContactDetailsID;
	
	@Column(name = "parent_key")
	private String parent_key;
	
	@Column(name = "agentId")
	private String agentId;
	
	@Column(name = "baseAgentCd")
	private String baseAgentCd;
	
	@Column(name = "contactTypeCd")
	private String contactTypeCd;
	
	@Column(name = "contactSubTypeCd")
	private String contactSubTypeCd;
	
	@Column(name = "ISDCd")
	private String ISDCd;
	
	@Column(name = "STDCd")
	private String STDCd;
	
	@Column(name = "contactNum")
	private String contactNum;
	
	@Column(name = "extensionNum")
	private String extensionNum;
	
	@Column(name = "emailId")
	private String emailId;
	
	@Column(name = "isPrefferedFlag")
	private String isPrefferedFlag;
	
	public Integer getQuotationClientContactDetailsID() {
		return quotationClientContactDetailsID;
	}

	public void setQuotationClientContactDetailsID(Integer quotationClientContactDetailsID) {
		this.quotationClientContactDetailsID = quotationClientContactDetailsID;
	}

	public String getParent_key() {
		return parent_key;
	}

	public void setParent_key(String parent_key) {
		this.parent_key = parent_key;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getBaseAgentCd() {
		return baseAgentCd;
	}

	public void setBaseAgentCd(String baseAgentCd) {
		this.baseAgentCd = baseAgentCd;
	}

	public String getContactTypeCd() {
		return contactTypeCd;
	}

	public void setContactTypeCd(String contactTypeCd) {
		this.contactTypeCd = contactTypeCd;
	}

	public String getContactSubTypeCd() {
		return contactSubTypeCd;
	}

	public void setContactSubTypeCd(String contactSubTypeCd) {
		this.contactSubTypeCd = contactSubTypeCd;
	}

	public String getISDCd() {
		return ISDCd;
	}

	public void setISDCd(String iSDCd) {
		ISDCd = iSDCd;
	}

	public String getSTDCd() {
		return STDCd;
	}

	public void setSTDCd(String sTDCd) {
		STDCd = sTDCd;
	}

	public String getContactNum() {
		return contactNum;
	}

	public void setContactNum(String contactNum) {
		this.contactNum = contactNum;
	}

	public String getExtensionNum() {
		return extensionNum;
	}

	public void setExtensionNum(String extensionNum) {
		this.extensionNum = extensionNum;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getIsPrefferedFlag() {
		return isPrefferedFlag;
	}

	public void setIsPrefferedFlag(String isPrefferedFlag) {
		this.isPrefferedFlag = isPrefferedFlag;
	}

	public void setId(Long id) {
		this.id = id;
	}

	

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
