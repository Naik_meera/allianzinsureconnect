package com.metamorphosys.insureconnect.dataobjects.transaction;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "QUOTATIONCOVERAGEINSURED")
public class QuotationCoverageInsuredDO extends BaseDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="quoteCvrgeInsurdId_seq") 
	@SequenceGenerator(name="quoteCvrgeInsurdId_seq",sequenceName="QUOTECVRGEINSURDID_SEQ",allocationSize=1) Long id;

	@Column(name = "bodyTypeCd")
	private String bodyTypeCd;
	
	@Column(name = "chasisNum")
	private String chasisNum;
	
	@Column(name = "clientID")
	private String clientID;
	
	@Column(name = "engineCapacity")
	private Double engineCapacity;
	
	@Column(name = "engineNum")
	private String engineNum;
	
	@Column(name = "entryAge")
	private Integer entryAge;
	
	@Column(name = "fuelTypeCd")
	private String fuelTypeCd;
	
	@Column(name = "manufacturingDt")
	private Integer manufacturingDt;
	
	@Column(name = "registrationDt")
	private Timestamp registrationDt;
	
	@Column(name = "registrationNum")
	private String registrationNum;
	
	@Column(name = "roleCd")
	private String roleCd;
	
	@Column(name = "seatingCapacity")
	private Integer seatingCapacity;
	
	@Column(name = "transmissionTypeCd")
	private String transmissionTypeCd;
	
	@Column(name = "vehicleModelCd")
	private String vehicleModelCd;
	
	@Column(name = "vehicleTypeCd")
	private String vehicleTypeCd;
	
	@Column(name = "vehicleColourCd")
	private String vehicleColourCd;
	
	@Column(name = "vehicleMakeCd")
	private String vehicleMakeCd;
	
	@Column(name = "vehicleVariantCd")
	private String vehicleVariantCd;

	@Column(name="parent_key")
	private String parent_key;
	
	@Column(name="agentId")
	private Integer agentId;
	
	@Column(name="baseAgentCd")
	private String baseAgentCd;
	
	@Column(name="quotationCoverageInsuredID")
	private Integer quotationCoverageInsuredID;
	
	@Column(name="coverageSeq")
	private Integer coverageSeq;
	
	@Column(name="insuredSeq")
	private Integer insuredSeq;
	
	@Column(name="registrationYear")
	private Integer registrationYear;
	
	@Column(name="manufacturingYear")
	private Integer manufacturingYear;
	
	@Column(name="zoneCd")
	private String zoneCd;
	
	@Column(name="transmissionType")
	private String transmissionType;
	
	@Column(name="vehicleColour")
	private String vehicleColour;
	
	@Column(name="vehicleVariant")
	private String vehicleVariant;
	
	@Column(name="coverType")
	private String coverType;
	
	@Column(name="derivedVehicleMakeCd")
	private String derivedVehicleMakeCd;
	
	@Column(name="derivedVehicleModelCd")
	private String derivedVehicleModelCd;
	
	@Column(name="vehicleKey")
	private String vehicleKey;
	
	@Column(name="age")
	private Integer age;
	
	@Column(name="usagePurpose")
	private String usagePurpose;
	
	@Column(name="defaultSumInsured")
	private Double defaultSumInsured;
	
	@Column(name="quotationNum")
	private Integer quotationNum;
	
	@Column(name="quotationSeq")
	private Integer quotationSeq;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<QuotationCoverageInsuredAdditionalFieldDO> quotationCoverageInsuredAdditionalFieldList;

	@Override
	public String getObjectName() {
		if (null != roleCd) {
			return roleCd;
		} else {
			return QuotationCoverageInsuredDO.class.getSimpleName();
		}
	}


	@Override
	public Long getId() {
		return id;
	}

	public String getBodyTypeCd() {
		return bodyTypeCd;
	}

	public void setBodyTypeCd(String bodyTypeCd) {
		this.bodyTypeCd = bodyTypeCd;
	}

	public String getChasisNum() {
		return chasisNum;
	}

	public void setChasisNum(String chasisNum) {
		this.chasisNum = chasisNum;
	}

	public String getClientID() {
		return clientID;
	}

	public void setClientID(String clientID) {
		this.clientID = clientID;
	}

	public Double getEngineCapacity() {
		return engineCapacity;
	}

	public void setEngineCapacity(Double engineCapacity) {
		this.engineCapacity = engineCapacity;
	}

	public String getEngineNum() {
		return engineNum;
	}

	public void setEngineNum(String engineNum) {
		this.engineNum = engineNum;
	}

	public Integer getEntryAge() {
		return entryAge;
	}

	public void setEntryAge(Integer entryAge) {
		this.entryAge = entryAge;
	}

	public String getFuelTypeCd() {
		return fuelTypeCd;
	}

	public void setFuelTypeCd(String fuelTypeCd) {
		this.fuelTypeCd = fuelTypeCd;
	}

	public Integer getManufacturingDt() {
		return manufacturingDt;
	}

	public void setManufacturingDt(Integer manufacturingDt) {
		this.manufacturingDt = manufacturingDt;
	}

	public Timestamp getRegistrationDt() {
		return registrationDt;
	}

	public void setRegistrationDt(Timestamp registrationDt) {
		this.registrationDt = registrationDt;
	}

	public String getRegistrationNum() {
		return registrationNum;
	}

	public void setRegistrationNum(String registrationNum) {
		this.registrationNum = registrationNum;
	}

	public String getRoleCd() {
		return roleCd;
	}

	public void setRoleCd(String roleCd) {
		this.roleCd = roleCd;
	}

	public Integer getSeatingCapacity() {
		return seatingCapacity;
	}

	public void setSeatingCapacity(Integer seatingCapacity) {
		this.seatingCapacity = seatingCapacity;
	}

	public String getTransmissionTypeCd() {
		return transmissionTypeCd;
	}

	public void setTransmissionTypeCd(String transmissionTypeCd) {
		this.transmissionTypeCd = transmissionTypeCd;
	}

	public String getVehicleModelCd() {
		return vehicleModelCd;
	}

	public void setVehicleModelCd(String vehicleModelCd) {
		this.vehicleModelCd = vehicleModelCd;
	}

	public String getVehicleTypeCd() {
		return vehicleTypeCd;
	}

	public void setVehicleTypeCd(String vehicleTypeCd) {
		this.vehicleTypeCd = vehicleTypeCd;
	}

	public String getVehicleColourCd() {
		return vehicleColourCd;
	}

	public void setVehicleColourCd(String vehicleColourCd) {
		this.vehicleColourCd = vehicleColourCd;
	}

	public String getVehicleMakeCd() {
		return vehicleMakeCd;
	}

	public void setVehicleMakeCd(String vehicleMakeCd) {
		this.vehicleMakeCd = vehicleMakeCd;
	}

	public String getVehicleVariantCd() {
		return vehicleVariantCd;
	}

	public void setVehicleVariantCd(String vehicleVariantCd) {
		this.vehicleVariantCd = vehicleVariantCd;
	}

	public List<QuotationCoverageInsuredAdditionalFieldDO> getQuotationCoverageInsuredAdditionalFieldList() {
		return quotationCoverageInsuredAdditionalFieldList;
	}

	public void setQuotationCoverageInsuredAdditionalFieldList(
			List<QuotationCoverageInsuredAdditionalFieldDO> quotationCoverageInsuredAdditionalFieldList) {
		this.quotationCoverageInsuredAdditionalFieldList = quotationCoverageInsuredAdditionalFieldList;
	}


	public String getParent_key() {
		return parent_key;
	}


	public void setParent_key(String parent_key) {
		this.parent_key = parent_key;
	}


	public Integer getAgentId() {
		return agentId;
	}


	public void setAgentId(Integer agentId) {
		this.agentId = agentId;
	}


	public String getBaseAgentCd() {
		return baseAgentCd;
	}


	public void setBaseAgentCd(String baseAgentCd) {
		this.baseAgentCd = baseAgentCd;
	}


	public Integer getQuotationCoverageInsuredID() {
		return quotationCoverageInsuredID;
	}


	public void setQuotationCoverageInsuredID(Integer quotationCoverageInsuredID) {
		this.quotationCoverageInsuredID = quotationCoverageInsuredID;
	}


	public Integer getCoverageSeq() {
		return coverageSeq;
	}


	public void setCoverageSeq(Integer coverageSeq) {
		this.coverageSeq = coverageSeq;
	}


	public Integer getInsuredSeq() {
		return insuredSeq;
	}


	public void setInsuredSeq(Integer insuredSeq) {
		this.insuredSeq = insuredSeq;
	}


	public Integer getRegistrationYear() {
		return registrationYear;
	}


	public void setRegistrationYear(Integer registrationYear) {
		this.registrationYear = registrationYear;
	}


	public Integer getManufacturingYear() {
		return manufacturingYear;
	}


	public void setManufacturingYear(Integer manufacturingYear) {
		this.manufacturingYear = manufacturingYear;
	}


	public String getZoneCd() {
		return zoneCd;
	}


	public void setZoneCd(String zoneCd) {
		this.zoneCd = zoneCd;
	}


	public String getTransmissionType() {
		return transmissionType;
	}


	public void setTransmissionType(String transmissionType) {
		this.transmissionType = transmissionType;
	}


	public String getVehicleColour() {
		return vehicleColour;
	}


	public void setVehicleColour(String vehicleColour) {
		this.vehicleColour = vehicleColour;
	}


	public String getVehicleVariant() {
		return vehicleVariant;
	}


	public void setVehicleVariant(String vehicleVariant) {
		this.vehicleVariant = vehicleVariant;
	}


	public String getCoverType() {
		return coverType;
	}


	public void setCoverType(String coverType) {
		this.coverType = coverType;
	}


	public String getDerivedVehicleMakeCd() {
		return derivedVehicleMakeCd;
	}


	public void setDerivedVehicleMakeCd(String derivedVehicleMakeCd) {
		this.derivedVehicleMakeCd = derivedVehicleMakeCd;
	}


	public String getDerivedVehicleModelCd() {
		return derivedVehicleModelCd;
	}


	public void setDerivedVehicleModelCd(String derivedVehicleModelCd) {
		this.derivedVehicleModelCd = derivedVehicleModelCd;
	}


	public String getVehicleKey() {
		return vehicleKey;
	}


	public void setVehicleKey(String vehicleKey) {
		this.vehicleKey = vehicleKey;
	}


	public Integer getAge() {
		return age;
	}


	public void setAge(Integer age) {
		this.age = age;
	}


	public String getUsagePurpose() {
		return usagePurpose;
	}


	public void setUsagePurpose(String usagePurpose) {
		this.usagePurpose = usagePurpose;
	}


	public Double getDefaultSumInsured() {
		return defaultSumInsured;
	}


	public void setDefaultSumInsured(Double defaultSumInsured) {
		this.defaultSumInsured = defaultSumInsured;
	}


	public Integer getQuotationNum() {
		return quotationNum;
	}


	public void setQuotationNum(Integer quotationNum) {
		this.quotationNum = quotationNum;
	}


	public Integer getQuotationSeq() {
		return quotationSeq;
	}


	public void setQuotationSeq(Integer quotationSeq) {
		this.quotationSeq = quotationSeq;
	}


	public void setId(Long id) {
		this.id = id;
	}

}
