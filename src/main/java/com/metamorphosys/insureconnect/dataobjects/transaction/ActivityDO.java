package com.metamorphosys.insureconnect.dataobjects.transaction;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="ACTIVITY")
public class ActivityDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="activityId_seq") 
	@SequenceGenerator(name="activityId_seq",sequenceName="ACTIVITYID_SEQ",allocationSize=1) 
	Long id;

	@Column(name = "activityType")
	private String activityType;

	@Column(name = "activityKey")
	private String activityKey;

	@Lob
	@Column(name = "activityValue")
	private String activityValue;

	@Column(name = "activityStatus")
	private String activityStatus;
	
	@Column(name = "activityProirity")
	private String activityProirity;
	
	@Column(name = "lastSyncDt")
	private Date lastSyncDt;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getActivityType() {
		return activityType;
	}
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}
	public String getActivityKey() {
		return activityKey;
	}
	public void setActivityKey(String activityKey) {
		this.activityKey = activityKey;
	}
	public String getActivityValue() {
		return activityValue;
	}
	public void setActivityValue(String activityValue) {
		this.activityValue = activityValue;
	}
	public String getActivityStatus() {
		return activityStatus;
	}
	public void setActivityStatus(String activityStatus) {
		this.activityStatus = activityStatus;
	}
	public String getActivityProirity() {
		return activityProirity;
	}
	public void setActivityProirity(String activityProirity) {
		this.activityProirity = activityProirity;
	}
	public Date getLastSyncDt() {
		return lastSyncDt;
	}
	public void setLastSyncDt(Date lastSyncDt) {
		this.lastSyncDt = lastSyncDt;
	}
	
	
}
