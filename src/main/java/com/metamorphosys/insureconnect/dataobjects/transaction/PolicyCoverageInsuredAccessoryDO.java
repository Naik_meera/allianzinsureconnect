package com.metamorphosys.insureconnect.dataobjects.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name="INSUREDACCESSORY")
public class PolicyCoverageInsuredAccessoryDO extends BaseDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="insuredAccessoryId_seq") 
	@SequenceGenerator(name="insuredAccessoryId_seq",sequenceName="INSUREDACCESSORYID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "accessoryCd")
	private String accessoryCd;
	
	//@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "description")
	private String description;
	
	@Column(name = "accessoryValue")
	private Double accessoryValue;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "selected")
	private String selected;
	
	
	public String getAccessoryCd() {
		return accessoryCd;
	}

	public void setAccessoryCd(String accessoryCd) {
		this.accessoryCd = accessoryCd;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getAccessoryValue() {
		return accessoryValue;
	}

	public void setAccessoryValue(Double accessoryValue) {
		this.accessoryValue = accessoryValue;
	}

	public String getSelected() {
		return selected;
	}

	public void setSelected(String selected) {
		this.selected = selected;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return PolicyCoverageInsuredAccessoryDO.class.getSimpleName();
	}

}
