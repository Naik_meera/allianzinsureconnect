package com.metamorphosys.insureconnect.dataobjects.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.TransactionDO;

@Entity
@Table(name = "folder")
public class FolderDO extends TransactionDO {
	
	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="folderId_seq") 
	@SequenceGenerator(name="folderId_seq",sequenceName="FOLDERID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "folderCd")
	private String folderCd;
	
	@Column(name = "folderName")
	private String folderName;
	
	@Column(name = "inboxQueryId")
	private Integer inboxQueryId;
	
	@Column(name = "parentFolderId")
	private String parentFolderId;
	
	@Column(name = "taskCd")
	private String taskCd;
	
	public String getFolderCd() {
		return folderCd;
	}

	public void setFolderCd(String folderCd) {
		this.folderCd = folderCd;
	}

	public String getFolderName() {
		return folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	public Integer getInboxQueryId() {
		return inboxQueryId;
	}

	public void setInboxQueryId(Integer inboxQueryId) {
		this.inboxQueryId = inboxQueryId;
	}

	public String getParentFolderId() {
		return parentFolderId;
	}

	public void setParentFolderId(String parentFolderId) {
		this.parentFolderId = parentFolderId;
	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return id;
	}

	public String getTaskCd() {
		return taskCd;
	}

	public void setTaskCd(String taskCd) {
		this.taskCd = taskCd;
	}

	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return FolderDO.class.getSimpleName();
	}

}
