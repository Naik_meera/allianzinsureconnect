package com.metamorphosys.insureconnect.dataobjects.transaction;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name="AGENTDEVICEMAP")
public class AgentDeviceMappingDO extends BaseDO {

	private @Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="agentDeviceMapId_seq")
	@SequenceGenerator(name="agentDeviceMapId_seq",sequenceName="AGENTDEVICEMAPID_SEQ",allocationSize=1) Long id;

	@Column(name = "agentId")
	private String agentId;
	
	@Column(name = "deviceId")
	private String deviceId;
	
	@Column(name = "policyseq")
	private Integer policyseq;
    
	@Column(name = "quotationseq")
	private Integer quotationseq;
    
	@Column(name = "lastSynchDt")
	private Timestamp lastSynchDt;
    
	@Column(name = "lastUploadedDt")
	private Timestamp lastUploadedDt;
    
	@Column(name = "lastDownloadDt")
	private Timestamp lastDownloadDt;
    
	@Column(name = "lastOnlineDt")
	private Timestamp lastOnlineDt;
	
	
	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getPolicyseq() {
		return policyseq;
	}

	public void setPolicyseq(Integer policyseq) {
		this.policyseq = policyseq;
	}

	public Integer getQuotationseq() {
		return quotationseq;
	}

	public void setQuotationseq(Integer quotationseq) {
		this.quotationseq = quotationseq;
	}

	public Timestamp getLastSynchDt() {
		return lastSynchDt;
	}

	public void setLastSynchDt(Timestamp lastSynchDt) {
		this.lastSynchDt = lastSynchDt;
	}

	public Timestamp getLastUploadedDt() {
		return lastUploadedDt;
	}

	public void setLastUploadedDt(Timestamp lastUploadedDt) {
		this.lastUploadedDt = lastUploadedDt;
	}

	public Timestamp getLastDownloadDt() {
		return lastDownloadDt;
	}

	public void setLastDownloadDt(Timestamp lastDownloadDt) {
		this.lastDownloadDt = lastDownloadDt;
	}

	public Timestamp getLastOnlineDt() {
		return lastOnlineDt;
	}

	public void setLastOnlineDt(Timestamp lastOnlineDt) {
		this.lastOnlineDt = lastOnlineDt;
	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return AgentDeviceMappingDO.class.getSimpleName();
	}

}
