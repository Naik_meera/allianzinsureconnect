package com.metamorphosys.insureconnect.dataobjects.transaction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.AdditionalFieldDO;

@Entity
@Table(name = "QUOTATIONCOVERAGEADDFLD")
public class QuotationCoverageAdditionalFieldDO extends AdditionalFieldDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="quotationCoverageAddFldId_seq") 
	@SequenceGenerator(name="quotationCoverageAddFldId_seq",sequenceName="QUOTATIONCOVERAGEADDFLDID_SEQ",allocationSize=1) Long id;

	@Override
	public Long getId() {
		return id;
	}

}
