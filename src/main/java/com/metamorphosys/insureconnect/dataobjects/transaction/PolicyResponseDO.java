/**
 * created by Anna
 */

package com.metamorphosys.insureconnect.dataobjects.transaction;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="POLICYRESPONSE")
public class PolicyResponseDO {
	
	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="policyResponse_seq") 
	@SequenceGenerator(name="policyResponse_seq",sequenceName="POLICYRESPONSE_SEQ",allocationSize=1) 
	Long id;
	
	@Column(name = "activityKey")
	private String activityKey;
	
	@Lob
	@Column(name = "activityValue")
	private String activityValue;
	
	@Column(name = "activityStatus")
	private String activityStatus;
	
	@Column(name="SYSTEMCREATEDDT")
	private Timestamp SystemCreatedDt;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getActivityKey() {
		return activityKey;
	}

	public void setActivityKey(String activityKey) {
		this.activityKey = activityKey;
	}

	public String getActivityValue() {
		return activityValue;
	}

	public void setActivityValue(String activityValue) {
		this.activityValue = activityValue;
	}

	public String getActivityStatus() {
		return activityStatus;
	}

	public void setActivityStatus(String activityStatus) {
		this.activityStatus = activityStatus;
	}

	public Timestamp getSystemCreatedDt() {
		return SystemCreatedDt;
	}

	public void setSystemCreatedDt(Timestamp systemCreatedDt) {
		SystemCreatedDt = systemCreatedDt;
	}

}
