package com.metamorphosys.insureconnect.dataobjects.transaction;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.TransactionDO;

@Entity
@Table(name="ENTITYSIGNATURE")
public class EntitySignatureDO extends TransactionDO {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="signatureId_seq")
	@SequenceGenerator(name="signatureId_seq",sequenceName="SIGNATUREID_SEQ",allocationSize=1)
	private Long id;
	
	@Column(name = "baseAgentCd")
	private String baseAgentCd;
	
	@Column(name = "entitySignatureID")
	private String entitySignatureID;
	
	@Column(name = "entityServiceID")
	private String entityServiceID;
	
	@Column(name = "entityID")
	private String entityID;
	
	@Column(name = "referenceID")
	private String referenceID;
	
	@Column(name = "referenceTypeCd")
	private String referenceTypeCd;
	
	@Lob
	@Column(name = "signature")
	private String signature;
	
	@Column(name = "entityImageName")
	private String entityImageName;
	
	@Column(name = "entityImageMimeType")
	private String entityImageMimeType;
	
	@Column(name = "signedPlace")
	private String signedPlace;
	
	@Column(name = "signedDtTime")
	private Date signedDtTime;
	
	@Column(name = "entityGuid")
	private String entityGuid;
	
	@Column(name = "imageURL")
	private String imageURL;
	
	@Column(name = "uploadStatus")
	private String uploadStatus;

	public String getBaseAgentCd() {
		return baseAgentCd;
	}

	public void setBaseAgentCd(String baseAgentCd) {
		this.baseAgentCd = baseAgentCd;
	}

	public String getEntitySignatureID() {
		return entitySignatureID;
	}

	public void setEntitySignatureID(String entitySignatureID) {
		this.entitySignatureID = entitySignatureID;
	}

	public String getEntityServiceID() {
		return entityServiceID;
	}

	public void setEntityServiceID(String entityServiceID) {
		this.entityServiceID = entityServiceID;
	}

	public String getEntityID() {
		return entityID;
	}

	public void setEntityID(String entityID) {
		this.entityID = entityID;
	}

	public String getReferenceID() {
		return referenceID;
	}

	public void setReferenceID(String referenceID) {
		this.referenceID = referenceID;
	}

	public String getReferenceTypeCd() {
		return referenceTypeCd;
	}

	public void setReferenceTypeCd(String referenceTypeCd) {
		this.referenceTypeCd = referenceTypeCd;
	}

	public String getSignature() {
		return signature;
	}

	public String getEntityImageName() {
		return entityImageName;
	}

	public void setEntityImageName(String entityImageName) {
		this.entityImageName = entityImageName;
	}

	public String getEntityImageMimeType() {
		return entityImageMimeType;
	}

	public void setEntityImageMimeType(String entityImageMimeType) {
		this.entityImageMimeType = entityImageMimeType;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getSignedPlace() {
		return signedPlace;
	}

	public void setSignedPlace(String signedPlace) {
		this.signedPlace = signedPlace;
	}

	public Date getSignedDtTime() {
		return signedDtTime;
	}

	public void setSignedDtTime(Date signedDtTime) {
		this.signedDtTime = signedDtTime;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEntityGuid() {
		return entityGuid;
	}

	public void setEntityGuid(String entityGuid) {
		this.entityGuid = entityGuid;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}
	

	public String getUploadStatus() {
		return uploadStatus;
	}

	public void setUploadStatus(String uploadStatus) {
		this.uploadStatus = uploadStatus;
	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return EntitySignatureDO.class.getSimpleName();
	}
}
