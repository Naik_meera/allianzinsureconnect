package com.metamorphosys.insureconnect.dataobjects.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "POLICYCOVERAGEINSUREDLDG")
public class PolicyCoverageInsuredLoadingDO extends BaseDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="polCvrgeInsurdLdgId_seq") 
	@SequenceGenerator(name="polCvrgeInsurdLdgId_seq",sequenceName="POLCVRGEINSURDLDGID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "loadingType")
	private String loadingType;
	
	@Column(name = "loadingValue")
	private Double loadingValue;
	
	@Column(name = "loadingReason")
	private String loadingReason;
	
	@Column(name = "loadingPremium")
	private Double loadingPremium;
	
	public String getLoadingType() {
		return loadingType;
	}
	public void setLoadingType(String loadingType) {
		this.loadingType = loadingType;
	}
	public Double getLoadingValue() {
		return loadingValue;
	}
	public void setLoadingValue(Double loadingValue) {
		this.loadingValue = loadingValue;
	}
	public String getLoadingReason() {
		return loadingReason;
	}
	public void setLoadingReason(String loadingReason) {
		this.loadingReason = loadingReason;
	}
	public Double getLoadingPremium() {
		return loadingPremium;
	}
	public void setLoadingPremium(Double loadingPremium) {
		this.loadingPremium = loadingPremium;
	}
	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return id;
	}
	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return PolicyCoverageInsuredLoadingDO.class.getSimpleName();
	}

}
