package com.metamorphosys.insureconnect.dataobjects.transaction;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.TransactionDO;

@Entity
@Table(name = "QUOTAIONCONTAINER")
public class QuotationContainerDO extends TransactionDO {

	
	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="QuotationContainerId_seq") 
	@SequenceGenerator(name="QuotationContainerId_seq",sequenceName="QUOTAIONCONTAINERID_SEQ",allocationSize=1) 
	Long id;
	
	@Column(name = "agentId")
	private String agentId;
	
	@Column(name = "baseAgentCd")
	private String baseAgentCd;

	@Column(name = "leadID")
	private String leadID;
	
	@Column(name = "quotationContainerID")
	private String quotationContainerID;
	
	@Column(name = "quotationContainerRefNum")
	private String quotationContainerRefNum;
	
	@Column(name = "quotationType")
	private String quotationType;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "coverType")
	private String coverType;
	
	@Column(name = "selectedPlan")
	private String selectedPlan;
	
	@Column(name = "fullName")
	private String fullName;
	
	@Column(name = "registrationNum")
	private String registrationNum;
	
	@Column(name = "registrationDt")
	private String registrationDt;
	
	@Column(name = "registrationYear")
	private String registrationYear;
	
	@Column(name = "seatingCapacity")
	private String seatingCapacity;
	
	@Column(name = "sumInsured")
	private String sumInsured;
	
	@Column(name = "quotationRefNum")
	private String quotationRefNum;
	
	@Column(name = "vehicleTypeCd")
	private String vehicleTypeCd;
	
	@Column(name = "vehicleMakeCd")
	private String vehicleMakeCd;
	
	@Column(name = "vehicleModelCd")
	private String vehicleModelCd;
	
	@Column(name = "vehicleVariant")
	private String vehicleVariant;
	
	@Column(name = "zoneCd")
	private String zoneCd;
	
	@Column(name = "selectionZoneMethod")
	private String selectionZoneMethod;
	
	// Sync properties
	
	@Column(name = "lastSyncDt")
	private String lastSyncDt;
	
	@Column(name = "lastUploadDt")
	private String lastUploadDt;
	
	@Column(name = "lastDownloadDt")
	private String lastDownloadDt;
	
	@Column(name = "state")
	private String state;
	
	@Column(name="defaultSumInsured")
	private Double defaultSumInsured;
	
	@Column(name="basePlanRate")
	private Integer basePlanRate;
	
	@Column(name="quotationDt")
	private Timestamp quotationDt;
	
	@Column(name = "netPremium")
	private Double netPremium;
	
	@Column(name = "commissionRate")
	private float commissionRate;
	
	@Column(name = "discountRate")
	private float discountRate;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<QuotationDO> quotationList;

	
	public float getCommissionRate() {
		return commissionRate;
	}

	public void setCommissionRate(float commissionRate) {
		this.commissionRate = commissionRate;
	}

	public float getDiscountRate() {
		return discountRate;
	}

	public void setDiscountRate(float discountRate) {
		this.discountRate = discountRate;
	}

	public Double getNetPremium() {
		return netPremium;
	}

	public void setNetPremium(Double netPremium) {
		this.netPremium = netPremium;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getBaseAgentCd() {
		return baseAgentCd;
	}

	public void setBaseAgentCd(String baseAgentCd) {
		this.baseAgentCd = baseAgentCd;
	}

	public String getLeadID() {
		return leadID;
	}

	public void setLeadID(String leadID) {
		this.leadID = leadID;
	}

	public String getQuotationContainerID() {
		return quotationContainerID;
	}

	public void setQuotationContainerID(String quotationContainerID) {
		this.quotationContainerID = quotationContainerID;
	}

	public String getQuotationContainerRefNum() {
		return quotationContainerRefNum;
	}

	public void setQuotationContainerRefNum(String quotationContainerRefNum) {
		this.quotationContainerRefNum = quotationContainerRefNum;
	}

	public String getQuotationType() {
		return quotationType;
	}

	public void setQuotationType(String quotationType) {
		this.quotationType = quotationType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCoverType() {
		return coverType;
	}

	public void setCoverType(String coverType) {
		this.coverType = coverType;
	}

	public String getSelectedPlan() {
		return selectedPlan;
	}

	public void setSelectedPlan(String selectedPlan) {
		this.selectedPlan = selectedPlan;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getRegistrationNum() {
		return registrationNum;
	}

	public void setRegistrationNum(String registrationNum) {
		this.registrationNum = registrationNum;
	}

	public String getRegistrationDt() {
		return registrationDt;
	}

	public void setRegistrationDt(String registrationDt) {
		this.registrationDt = registrationDt;
	}

	public String getRegistrationYear() {
		return registrationYear;
	}

	public void setRegistrationYear(String registrationYear) {
		this.registrationYear = registrationYear;
	}

	public String getSeatingCapacity() {
		return seatingCapacity;
	}

	public void setSeatingCapacity(String seatingCapacity) {
		this.seatingCapacity = seatingCapacity;
	}

	public String getSumInsured() {
		return sumInsured;
	}

	public void setSumInsured(String sumInsured) {
		this.sumInsured = sumInsured;
	}

	public String getQuotationRefNum() {
		return quotationRefNum;
	}

	public void setQuotationRefNum(String quotationRefNum) {
		this.quotationRefNum = quotationRefNum;
	}

	public String getVehicleTypeCd() {
		return vehicleTypeCd;
	}

	public void setVehicleTypeCd(String vehicleTypeCd) {
		this.vehicleTypeCd = vehicleTypeCd;
	}

	public String getVehicleMakeCd() {
		return vehicleMakeCd;
	}

	public void setVehicleMakeCd(String vehicleMakeCd) {
		this.vehicleMakeCd = vehicleMakeCd;
	}

	public String getVehicleModelCd() {
		return vehicleModelCd;
	}

	public void setVehicleModelCd(String vehicleModelCd) {
		this.vehicleModelCd = vehicleModelCd;
	}

	public String getVehicleVariant() {
		return vehicleVariant;
	}

	public void setVehicleVariant(String vehicleVariant) {
		this.vehicleVariant = vehicleVariant;
	}

	public String getZoneCd() {
		return zoneCd;
	}

	public void setZoneCd(String zoneCd) {
		this.zoneCd = zoneCd;
	}

	public String getSelectionZoneMethod() {
		return selectionZoneMethod;
	}

	public void setSelectionZoneMethod(String selectionZoneMethod) {
		this.selectionZoneMethod = selectionZoneMethod;
	}

	public String getLastSyncDt() {
		return lastSyncDt;
	}

	public void setLastSyncDt(String lastSyncDt) {
		this.lastSyncDt = lastSyncDt;
	}

	public String getLastUploadDt() {
		return lastUploadDt;
	}

	public void setLastUploadDt(String lastUploadDt) {
		this.lastUploadDt = lastUploadDt;
	}

	public String getLastDownloadDt() {
		return lastDownloadDt;
	}

	public void setLastDownloadDt(String lastDownloadDt) {
		this.lastDownloadDt = lastDownloadDt;
	}


	public List<QuotationDO> getQuotationList() {
		return quotationList;
	}

	public void setQuotationList(List<QuotationDO> quotationList) {
		this.quotationList = quotationList;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public String getObjectName() {
		return QuotationContainerDO.class.getSimpleName();
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Double getDefaultSumInsured() {
		return defaultSumInsured;
	}

	public void setDefaultSumInsured(Double defaultSumInsured) {
		this.defaultSumInsured = defaultSumInsured;
	}

	public Integer getBasePlanRate() {
		return basePlanRate;
	}

	public void setBasePlanRate(Integer basePlanRate) {
		this.basePlanRate = basePlanRate;
	}

	public Timestamp getQuotationDt() {
		return quotationDt;
	}

	public void setQuotationDt(Timestamp quotationDt) {
		this.quotationDt = quotationDt;
	}

}
