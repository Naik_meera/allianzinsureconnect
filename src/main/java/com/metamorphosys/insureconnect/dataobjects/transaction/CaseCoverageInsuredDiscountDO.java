package com.metamorphosys.insureconnect.dataobjects.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "CASECOVERAGEINSUREDDSC")
public class CaseCoverageInsuredDiscountDO extends BaseDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="caseCoverageInsuredDscId_seq") 
	@SequenceGenerator(name="caseCoverageInsuredDscId_seq",sequenceName="CASECOVERAGEINSUREDDSCID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "discountReason")
	private String discountReason;
	
	@Column(name = "discountValue")
	private Double discountValue;
	
	@Column(name = "discoutPremium")
	private Double discoutPremium;
	
	@Column(name = "discoutType")
	private String discoutType;
	
	@Override
	public Long getId() {
		return id;
	}
	public String getDiscountReason() {
		return discountReason;
	}
	public void setDiscountReason(String discountReason) {
		this.discountReason = discountReason;
	}
	public Double getDiscountValue() {
		return discountValue;
	}
	public void setDiscountValue(Double discountValue) {
		this.discountValue = discountValue;
	}
	public Double getDiscoutPremium() {
		return discoutPremium;
	}
	public void setDiscoutPremium(Double discoutPremium) {
		this.discoutPremium = discoutPremium;
	}
	public String getDiscoutType() {
		return discoutType;
	}
	public void setDiscoutType(String discoutType) {
		this.discoutType = discoutType;
	}
	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return CaseCoverageInsuredDiscountDO.class.getSimpleName();
	}

}
