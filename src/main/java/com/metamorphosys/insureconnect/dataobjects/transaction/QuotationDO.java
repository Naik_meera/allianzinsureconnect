package com.metamorphosys.insureconnect.dataobjects.transaction;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.TransactionDO;

@Entity
@Table(name = "QUOTATION")
public class QuotationDO extends TransactionDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="quotationId_seq") 
	@SequenceGenerator(name="quotationId_seq",sequenceName="QUOTATIONID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "quotationNum")
	private Integer quotationNum;
	
	@Column(name = "quotationDt")
	private Timestamp quotationDt;
	
	@Column(name = "proposalNo")
	private	String	proposalNo;
	
	@Column(name = "quotationRefNum")
	private	String	quotationRefNum;
	
	@Column(name = "baseAgentCd")
	private	String	baseAgentCd;
	
	@Column(name = "baseProductFamilyCd")
	private	String	baseProductFamilyCd;
	
	@Column(name = "baseProductCd")
	private	String	baseProductCd;
	
	@Column(name = "baseProductTypeCd")
	private	String	baseProductTypeCd;
	
	@Column(name = "productPlanOptionCd")
	private	String	productPlanOptionCd;
	
	@Column(name = "baseProductVersion")
	private String	baseProductVersion;
	
	@Column(name = "branchCd")
	private	String	branchCd;
	
	@Column(name = "channelCd")
	private	String	channelCd;
	
	@Column(name = "papSumInsured")
	private Double papSumInsured;
	
	@Column(name = "padSumInsured")
	private Double padSumInsured;
	
	@Column(name = "parent_key")
	private String parent_key;
	
	@Column(name = "quotationSeq")
	private Integer quotationSeq;
	
	@Column(name = "leadID")
	private Integer leadID;
	
	@Column(name = "quotationID")
	private Integer quotationID;
	
	@Column(name = "agentId")
	private String agentId;
	
	@Column(name = "quotationType")
	private String quotationType;
	
	@Column(name = "productPeriod")
	private String productPeriod;
	
	@Column(name = "finalPremium")
	private Integer finalPremium;
	
	@Column(name = "finalSumInsured")
	private Double finalSumInsured;
	
	@Column(name = "selected")
	private String selected;
	
	@Column(name = "pdfOutput")
	private String pdfOutput;
	
	@Column(name = "paymentFrequencyCd")
	private String paymentFrequencyCd;
	
	@Column(name = "netPremium")
	private Double netPremium;
	
	@Column(name = "commissionRate")
	private float commissionRate;
	
	@Column(name = "discountRate")
	private float discountRate;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<QuotationAdditionalFieldDO> quotationAdditionalFieldList;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval=true)
	@JoinColumn(name = "parentObject_id")
	private List<QuotationClientDO> quotationClientList;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<QuotationCoverageDO> quotationCoverageList;

	@Override
	public String getObjectName() {
		return QuotationDO.class.getSimpleName();
	}

	public float getCommissionRate() {
		return commissionRate;
	}

	public void setCommissionRate(float commissionRate) {
		this.commissionRate = commissionRate;
	}

	public float getDiscountRate() {
		return discountRate;
	}

	public void setDiscountRate(float discountRate) {
		this.discountRate = discountRate;
	}

	@Override
	public Long getId() {
		return id;
	}

	public Integer getQuotationNum() {
		return quotationNum;
	}

	public void setQuotationNum(Integer quotationNum) {
		this.quotationNum = quotationNum;
	}

	public Double getNetPremium() {
		return netPremium;
	}

	public void setNetPremium(Double netPremium) {
		this.netPremium = netPremium;
	}

	public Timestamp getQuotationDt() {
		return quotationDt;
	}

	public void setQuotationDt(Timestamp quotationDt) {
		this.quotationDt = quotationDt;
	}

	public String getProposalNo() {
		return proposalNo;
	}

	public void setProposalNo(String proposalNo) {
		this.proposalNo = proposalNo;
	}

	public String getQuotationRefNum() {
		return quotationRefNum;
	}

	public void setQuotationRefNum(String quotationRefNum) {
		this.quotationRefNum = quotationRefNum;
	}

	public String getBaseAgentCd() {
		return baseAgentCd;
	}

	public void setBaseAgentCd(String baseAgentCd) {
		this.baseAgentCd = baseAgentCd;
	}

	public String getBaseProductFamilyCd() {
		return baseProductFamilyCd;
	}

	public void setBaseProductFamilyCd(String baseProductFamilyCd) {
		this.baseProductFamilyCd = baseProductFamilyCd;
	}

	public String getBaseProductCd() {
		return baseProductCd;
	}

	public void setBaseProductCd(String baseProductCd) {
		this.baseProductCd = baseProductCd;
	}

	public String getBaseProductTypeCd() {
		return baseProductTypeCd;
	}

	public void setBaseProductTypeCd(String baseProductTypeCd) {
		this.baseProductTypeCd = baseProductTypeCd;
	}

	public String getProductPlanOptionCd() {
		return productPlanOptionCd;
	}

	public void setProductPlanOptionCd(String productPlanOptionCd) {
		this.productPlanOptionCd = productPlanOptionCd;
	}

	public String getBaseProductVersion() {
		return baseProductVersion;
	}

	public void setBaseProductVersion(String baseProductVersion) {
		this.baseProductVersion = baseProductVersion;
	}

	public String getBranchCd() {
		return branchCd;
	}

	public void setBranchCd(String branchCd) {
		this.branchCd = branchCd;
	}

	public String getChannelCd() {
		return channelCd;
	}

	public void setChannelCd(String channelCd) {
		this.channelCd = channelCd;
	}

	public List<QuotationAdditionalFieldDO> getQuotationAdditionalFieldList() {
		return quotationAdditionalFieldList;
	}

	public void setQuotationAdditionalFieldList(List<QuotationAdditionalFieldDO> quotationAdditionalFieldList) {
		this.quotationAdditionalFieldList = quotationAdditionalFieldList;
	}

	public List<QuotationClientDO> getQuotationClientList() {
		return quotationClientList;
	}

	public void setQuotationClientList(List<QuotationClientDO> quotationClientList) {
		this.quotationClientList = quotationClientList;
	}

	public List<QuotationCoverageDO> getQuotationCoverageList() {
		return quotationCoverageList;
	}

	public void setQuotationCoverageList(List<QuotationCoverageDO> quotationCoverageList) {
		this.quotationCoverageList = quotationCoverageList;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Double getPapSumInsured() {
		return papSumInsured;
	}

	public void setPapSumInsured(Double papSumInsured) {
		this.papSumInsured = papSumInsured;
	}

	public Double getPadSumInsured() {
		return padSumInsured;
	}

	public void setPadSumInsured(Double padSumInsured) {
		this.padSumInsured = padSumInsured;
	}

	public String getParent_key() {
		return parent_key;
	}

	public void setParent_key(String parent_key) {
		this.parent_key = parent_key;
	}

	public Integer getQuotationSeq() {
		return quotationSeq;
	}

	public void setQuotationSeq(Integer quotationSeq) {
		this.quotationSeq = quotationSeq;
	}

	public Integer getLeadID() {
		return leadID;
	}

	public void setLeadID(Integer leadID) {
		this.leadID = leadID;
	}

	public Integer getQuotationID() {
		return quotationID;
	}

	public void setQuotationID(Integer quotationID) {
		this.quotationID = quotationID;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getQuotationType() {
		return quotationType;
	}

	public void setQuotationType(String quotationType) {
		this.quotationType = quotationType;
	}

	public String getProductPeriod() {
		return productPeriod;
	}

	public void setProductPeriod(String productPeriod) {
		this.productPeriod = productPeriod;
	}

	public Integer getFinalPremium() {
		return finalPremium;
	}

	public void setFinalPremium(Integer finalPremium) {
		this.finalPremium = finalPremium;
	}

	public Double getFinalSumInsured() {
		return finalSumInsured;
	}

	public void setFinalSumInsured(Double finalSumInsured) {
		this.finalSumInsured = finalSumInsured;
	}

	public String getSelected() {
		return selected;
	}

	public void setSelected(String selected) {
		this.selected = selected;
	}

	public String getPdfOutput() {
		return pdfOutput;
	}

	public void setPdfOutput(String pdfOutput) {
		this.pdfOutput = pdfOutput;
	}

	public String getPaymentFrequencyCd() {
		return paymentFrequencyCd;
	}

	public void setPaymentFrequencyCd(String paymentFrequencyCd) {
		this.paymentFrequencyCd = paymentFrequencyCd;
	}

	public QuotationCoverageDO getBasePlan() {
		QuotationCoverageDO coverageDO = new QuotationCoverageDO();
		if(this.getQuotationCoverageList() != null && this.getQuotationCoverageList().size() > 0){
		for(QuotationCoverageDO tempCoverageDO : this.getQuotationCoverageList()){
				if(tempCoverageDO.getPlanTypeCd().toString().equals("BASEPLAN")){
					coverageDO = tempCoverageDO;
					return coverageDO;
				}
			}
		}
		
		return null;
	
	}

}
