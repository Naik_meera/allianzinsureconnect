package com.metamorphosys.insureconnect.dataobjects.transaction;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "QUOTATIONCOVERAGE")
public class QuotationCoverageDO extends BaseDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="quotationCoverageId_seq") 
	@SequenceGenerator(name="quotationCoverageId_seq",sequenceName="QUOTATIONCOVERAGEID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "productTypeCd")
	private String productTypeCd;
	
	@Column(name = "productFamilyCd")
	private String productFamilyCd;
	
	@Column(name = "productCd")
	private String productCd;
	
	@Column(name = "planCd")
	private String planCd;
	
	@Column(name = "planTypeCd")
	private String planTypeCd;
	
	@Column(name = "baseSumAssured")
	private Double baseSumAssured;
	
	@Column(name = "baseAnnualPremium")
	private Double baseAnnualPremium;
	
	@Column(name = "extraPremium")
	private Integer extraPremium;
	
	@Column(name = "modalPremium")
	private Double modalPremium;
	
	@Column(name = "productTerm")
	private Integer productTerm;
	
	@Column(name = "premiumPayingTerm")
	private Integer premiumPayingTerm;
	
	@Column(name = "paymentFrequencyCd")
	private String paymentFrequencyCd;
	
	@Column(name = "planOptionCd")
	private String planOptionCd;
	
	@Column(name = "parent_key")
	private String parent_key;
	
	@Column(name = "agentId")
	private String agentId;
	
	@Column(name = "baseAgentCd")
	private String baseAgentCd;
	
	@Column(name="quotationNum")
	private Integer quotationNum;
	
	@Column(name="quotationSeq")
	private Integer quotationSeq;

	@Column(name="quotationCoverageID")
	private Integer quotationCoverageID;
	
	@Column(name="productVersion")
	private String productVersion;
	
	@Column(name="coverType")
	private String coverType;
	
	@Column(name="totalDiscount")
	private Integer totalDiscount;
	
	@Column(name="totalRiderPremium")
	private Integer totalRiderPremium;
	
	@Column(name="totalPremium")
	private Integer totalPremium;
	
	@Column(name="policyterm")
	private Integer policyterm;
	
	@Column(name="coverageType")
	private String coverageType;
	
	@Column(name="sumInsured")
	private Double sumInsured;
	
	@Column(name="premiumRate")
	private Double premiumRate;
	
	@Column(name="coverageStartDt")
	private Timestamp coverageStartDt;
	
	@Column(name="planType")
	private String planType;
	
	@Column(name="selected")
	private String selected;
	
	@Column(name="associationType")
	private String associationType;
	
	@Column(name="productName")
	private String productName;
	
	@Column(name="coverageUIProperty")
	private String coverageUIProperty;
	
	@Column(name="coverageUIPropertyValue")
	private String coverageUIPropertyValue;
	
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<QuotationCoverageAdditionalFieldDO> quotationCoverageAdditionalFieldList;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<QuotationCoverageInsuredDO> quotationCoverageInsuredList;

	@Override
	public String getObjectName() {
		if (planTypeCd.equals("BASEPLAN")) {
			return planTypeCd;
		} else {
			return QuotationCoverageDO.class.getSimpleName();
		}
	}

	@Override
	public Long getId() {
		return id;
	}

	public String getProductTypeCd() {
		return productTypeCd;
	}

	public void setProductTypeCd(String productTypeCd) {
		this.productTypeCd = productTypeCd;
	}

	public String getProductFamilyCd() {
		return productFamilyCd;
	}

	public void setProductFamilyCd(String productFamilyCd) {
		this.productFamilyCd = productFamilyCd;
	}

	public String getProductCd() {
		return productCd;
	}

	public String getPlanTypeCd() {
		return planTypeCd;
	}

	public void setPlanTypeCd(String planTypeCd) {
		this.planTypeCd = planTypeCd;
	}

	public void setProductCd(String productCd) {
		this.productCd = productCd;
	}

	public String getPlanCd() {
		return planCd;
	}

	public void setPlanCd(String planCd) {
		this.planCd = planCd;
	}

	public Double getBaseSumAssured() {
		return baseSumAssured;
	}

	public void setBaseSumAssured(Double baseSumAssured) {
		this.baseSumAssured = baseSumAssured;
	}

	public Double getBaseAnnualPremium() {
		return baseAnnualPremium;
	}

	public void setBaseAnnualPremium(Double baseAnnualPremium) {
		this.baseAnnualPremium = baseAnnualPremium;
	}

	public Integer getExtraPremium() {
		return extraPremium;
	}

	public void setExtraPremium(Integer extraPremium) {
		this.extraPremium = extraPremium;
	}

	public Double getModalPremium() {
		return modalPremium;
	}

	public void setModalPremium(Double modalPremium) {
		this.modalPremium = modalPremium;
	}

	public Integer getProductTerm() {
		return productTerm;
	}

	public void setProductTerm(Integer productTerm) {
		this.productTerm = productTerm;
	}

	public Integer getPremiumPayingTerm() {
		return premiumPayingTerm;
	}

	public void setPremiumPayingTerm(Integer premiumPayingTerm) {
		this.premiumPayingTerm = premiumPayingTerm;
	}

	public String getPaymentFrequencyCd() {
		return paymentFrequencyCd;
	}

	public void setPaymentFrequencyCd(String paymentFrequencyCd) {
		this.paymentFrequencyCd = paymentFrequencyCd;
	}

	public String getPlanOptionCd() {
		return planOptionCd;
	}

	public void setPlanOptionCd(String planOptionCd) {
		this.planOptionCd = planOptionCd;
	}

	public List<QuotationCoverageAdditionalFieldDO> getQuotationCoverageAdditionalFieldList() {
		return quotationCoverageAdditionalFieldList;
	}

	public void setQuotationCoverageAdditionalFieldList(
			List<QuotationCoverageAdditionalFieldDO> quotationCoverageAdditionalFieldList) {
		this.quotationCoverageAdditionalFieldList = quotationCoverageAdditionalFieldList;
	}

	public List<QuotationCoverageInsuredDO> getQuotationCoverageInsuredList() {
		return quotationCoverageInsuredList;
	}

	public void setQuotationCoverageInsuredList(List<QuotationCoverageInsuredDO> quotationCoverageInsuredList) {
		this.quotationCoverageInsuredList = quotationCoverageInsuredList;
	}

	public String getParent_key() {
		return parent_key;
	}

	public void setParent_key(String parent_key) {
		this.parent_key = parent_key;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getBaseAgentCd() {
		return baseAgentCd;
	}

	public void setBaseAgentCd(String baseAgentCd) {
		this.baseAgentCd = baseAgentCd;
	}

	public Integer getQuotationNum() {
		return quotationNum;
	}

	public void setQuotationNum(Integer quotationNum) {
		this.quotationNum = quotationNum;
	}

	public Integer getQuotationSeq() {
		return quotationSeq;
	}

	public void setQuotationSeq(Integer quotationSeq) {
		this.quotationSeq = quotationSeq;
	}

	public Integer getQuotationCoverageID() {
		return quotationCoverageID;
	}

	public void setQuotationCoverageID(Integer quotationCoverageID) {
		this.quotationCoverageID = quotationCoverageID;
	}

	public String getProductVersion() {
		return productVersion;
	}

	public void setProductVersion(String productVersion) {
		this.productVersion = productVersion;
	}

	public String getCoverType() {
		return coverType;
	}

	public void setCoverType(String coverType) {
		this.coverType = coverType;
	}

	public Integer getTotalDiscount() {
		return totalDiscount;
	}

	public void setTotalDiscount(Integer totalDiscount) {
		this.totalDiscount = totalDiscount;
	}

	public Integer getTotalRiderPremium() {
		return totalRiderPremium;
	}

	public void setTotalRiderPremium(Integer totalRiderPremium) {
		this.totalRiderPremium = totalRiderPremium;
	}

	public Integer getTotalPremium() {
		return totalPremium;
	}

	public void setTotalPremium(Integer totalPremium) {
		this.totalPremium = totalPremium;
	}

	public Integer getPolicyterm() {
		return policyterm;
	}

	public void setPolicyterm(Integer policyterm) {
		this.policyterm = policyterm;
	}

	public String getCoverageType() {
		return coverageType;
	}

	public void setCoverageType(String coverageType) {
		this.coverageType = coverageType;
	}

	public Double getSumInsured() {
		return sumInsured;
	}

	public void setSumInsured(Double sumInsured) {
		this.sumInsured = sumInsured;
	}

	public Double getPremiumRate() {
		return premiumRate;
	}

	public void setPremiumRate(Double premiumRate) {
		this.premiumRate = premiumRate;
	}

	public Timestamp getCoverageStartDt() {
		return coverageStartDt;
	}

	public void setCoverageStartDt(Timestamp coverageStartDt) {
		this.coverageStartDt = coverageStartDt;
	}

	public String getPlanType() {
		return planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType;
	}

	public String getSelected() {
		return selected;
	}

	public void setSelected(String selected) {
		this.selected = selected;
	}

	public String getAssociationType() {
		return associationType;
	}

	public void setAssociationType(String associationType) {
		this.associationType = associationType;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getCoverageUIProperty() {
		return coverageUIProperty;
	}

	public void setCoverageUIProperty(String coverageUIProperty) {
		this.coverageUIProperty = coverageUIProperty;
	}

	public String getCoverageUIPropertyValue() {
		return coverageUIPropertyValue;
	}

	public void setCoverageUIPropertyValue(String coverageUIPropertyValue) {
		this.coverageUIPropertyValue = coverageUIPropertyValue;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
