package com.metamorphosys.insureconnect.dataobjects.transaction;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.TransactionDO;

@Entity
@Table(name = "CASE_M")
public class CaseDO extends TransactionDO {

private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="caseId_seq") 
@SequenceGenerator(name="caseId_seq",sequenceName="CASEID_SEQ",allocationSize=1) Long id;
	
	@Column(name = "policyNum")
	private String policyNum;
	
	@Column(name = "policyIssueDt")
	private Date  policyIssueDt;
	
	@Column(name = "proposalEntryDt")
	private Date proposalEntryDt;
	
	@Column(name = "proposalNum")
	private String proposalNum;
	
	@Column(name = "proposalReceivedDt")
	private Date proposalReceivedDt;

	@Column(name = "proposalSignedDt")
	private Date proposalSignedDt;
	
	@Column(name = "policyCommencementDt")
	private Date policyCommencementDt;

	@Column(name = "policyExpiryDt")
	private Date policyExpiryDt;

	@Column(name = "purposeOfInsuranceCd")
	private String purposeOfInsuranceCd;
	
	@Column(name = "quotationRefNum")
	private String quotationRefNum;
	
	@Column(name = "submissionModeCd")
	private String submissionModeCd;
	
	@Column(name = "baseAgentCd")
	private String baseAgentCd;
	
	@Column(name = "baseProductFamilyCd")
	private String baseProductFamilyCd;

	@Column(name = "baseProductCd")
	private String baseProductCd;

	@Column(name = "baseProductTypeCd")
	private String baseProductTypeCd;

	@Column(name = "productPlanOptionCd")
	private String productPlanOptionCd;
	
	@Column(name = "baseProductVersion")
	private Integer baseProductVersion;

	@Column(name = "policyTypeCd")
	private String policyTypeCd;
	
	@Column(name = "branchCd")
	private String branchCd;

	@Column(name = "channelCd")
	private String channelCd;
	
	@Column(name = "policyStatusCd")
	private String policyStatusCd;
	
	@Column(name = "processStatusCd")
	private String processStatusCd;
	
	@Column(name = "stageCd")
	private String stageCd;

	@Column(name = "uwDecisionCd")
	private String uwDecisionCd;

	@Column(name = "uwDecisionValue")
	private String uwDecisionValue;
	
	@Column(name = "uwDecisionDt")
	private Date uwDecisionDt;
	
	@Column(name = "workFlowDecisionCd")
	private String workFlowDecisionCd;

	@Column(name = "premiumStatusCd")
	private String premiumStatusCd;
	
	@Column(name = "validationStatusCd")
	private String validationStatusCd;
	
	@Column(name = "entityGuid")
	private String entityGuid;
	
	@Column(name="ruleEffectiveDate")
	private Date ruleEffectiveDate;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<CaseCoverageDO> caseCoverageList;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<CasePremiumSummaryDO> casePremiumSummaryList;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<CaseAgentDO> caseAgentList;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<CasePaymentDO> casePaymentList;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<CaseExclusionDO> caseExclusionList;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<CaseClientDO> caseClientList;
	
	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		id = id;
	}

	public String getPolicyNum() {
		return policyNum;
	}

	public void setPolicyNum(String policyNum) {
		this.policyNum = policyNum;
	}

	public Date getPolicyIssueDt() {
		return policyIssueDt;
	}

	public void setPolicyIssueDt(Date policyIssueDt) {
		this.policyIssueDt = policyIssueDt;
	}

	public Date getProposalEntryDt() {
		return proposalEntryDt;
	}

	public void setProposalEntryDt(Date proposalEntryDt) {
		this.proposalEntryDt = proposalEntryDt;
	}

	public String getProposalNum() {
		return proposalNum;
	}

	public void setProposalNum(String proposalNum) {
		this.proposalNum = proposalNum;
	}

	public Date getProposalReceivedDt() {
		return proposalReceivedDt;
	}

	public void setProposalReceivedDt(Date proposalReceivedDt) {
		this.proposalReceivedDt = proposalReceivedDt;
	}

	public Date getProposalSignedDt() {
		return proposalSignedDt;
	}

	public void setProposalSignedDt(Date proposalSignedDt) {
		this.proposalSignedDt = proposalSignedDt;
	}

	public Date getPolicyCommencementDt() {
		return policyCommencementDt;
	}

	public void setPolicyCommencementDt(Date policyCommencementDt) {
		this.policyCommencementDt = policyCommencementDt;
	}

	public Date getPolicyExpiryDt() {
		return policyExpiryDt;
	}

	public void setPolicyExpiryDt(Date policyExpiryDt) {
		this.policyExpiryDt = policyExpiryDt;
	}

	public String getPurposeOfInsuranceCd() {
		return purposeOfInsuranceCd;
	}

	public void setPurposeOfInsuranceCd(String purposeOfInsuranceCd) {
		this.purposeOfInsuranceCd = purposeOfInsuranceCd;
	}

	public String getQuotationRefNum() {
		return quotationRefNum;
	}

	public void setQuotationRefNum(String quotationRefNum) {
		this.quotationRefNum = quotationRefNum;
	}

	public String getSubmissionModeCd() {
		return submissionModeCd;
	}

	public void setSubmissionModeCd(String submissionModeCd) {
		this.submissionModeCd = submissionModeCd;
	}

	public String getBaseAgentCd() {
		return baseAgentCd;
	}

	public void setBaseAgentCd(String baseAgentCd) {
		this.baseAgentCd = baseAgentCd;
	}

	public String getBaseProductFamilyCd() {
		return baseProductFamilyCd;
	}

	public void setBaseProductFamilyCd(String baseProductFamilyCd) {
		this.baseProductFamilyCd = baseProductFamilyCd;
	}

	public String getBaseProductCd() {
		return baseProductCd;
	}

	public void setBaseProductCd(String baseProductCd) {
		this.baseProductCd = baseProductCd;
	}

	public String getBaseProductTypeCd() {
		return baseProductTypeCd;
	}

	public void setBaseProductTypeCd(String baseProductTypeCd) {
		this.baseProductTypeCd = baseProductTypeCd;
	}

	public String getProductPlanOptionCd() {
		return productPlanOptionCd;
	}

	public void setProductPlanOptionCd(String productPlanOptionCd) {
		this.productPlanOptionCd = productPlanOptionCd;
	}

	public Integer getBaseProductVersion() {
		return baseProductVersion;
	}

	public void setBaseProductVersion(Integer baseProductVersion) {
		this.baseProductVersion = baseProductVersion;
	}

	public String getPolicyTypeCd() {
		return policyTypeCd;
	}

	public void setPolicyTypeCd(String policyTypeCd) {
		this.policyTypeCd = policyTypeCd;
	}

	public String getBranchCd() {
		return branchCd;
	}

	public void setBranchCd(String branchCd) {
		this.branchCd = branchCd;
	}

	public String getChannelCd() {
		return channelCd;
	}

	public void setChannelCd(String channelCd) {
		this.channelCd = channelCd;
	}

	public String getPolicyStatusCd() {
		return policyStatusCd;
	}

	public void setPolicyStatusCd(String policyStatusCd) {
		this.policyStatusCd = policyStatusCd;
	}

	public String getProcessStatusCd() {
		return processStatusCd;
	}

	public void setProcessStatusCd(String processStatusCd) {
		this.processStatusCd = processStatusCd;
	}

	public String getStageCd() {
		return stageCd;
	}

	public void setStageCd(String stageCd) {
		this.stageCd = stageCd;
	}

	public String getUwDecisionCd() {
		return uwDecisionCd;
	}

	public void setUwDecisionCd(String uwDecisionCd) {
		this.uwDecisionCd = uwDecisionCd;
	}

	public String getUwDecisionValue() {
		return uwDecisionValue;
	}

	public void setUwDecisionValue(String uwDecisionValue) {
		this.uwDecisionValue = uwDecisionValue;
	}

	public Date getUwDecisionDt() {
		return uwDecisionDt;
	}

	public void setUwDecisionDat(Date uwDecisionDt) {
		this.uwDecisionDt = uwDecisionDt;
	}

	public String getWorkFlowDecisionCd() {
		return workFlowDecisionCd;
	}

	public void setWorkFlowDecisionCd(String workFlowDecisionCd) {
		this.workFlowDecisionCd = workFlowDecisionCd;
	}

	public String getPremiumStatusCd() {
		return premiumStatusCd;
	}

	public void setPremiumStatusCd(String premiumStatusCd) {
		this.premiumStatusCd = premiumStatusCd;
	}

	public String getValidationStatusCd() {
		return validationStatusCd;
	}

	public void setValidationStatusCd(String validationStatusCd) {
		this.validationStatusCd = validationStatusCd;
	}

	public List<CaseCoverageDO> getCaseCoverageList() {
		return caseCoverageList;
	}

	public void setCaseCoverageList(List<CaseCoverageDO> caseCoverageList) {
		this.caseCoverageList = caseCoverageList;
	}

	public List<CasePremiumSummaryDO> getCasePremiumSummaryList() {
		return casePremiumSummaryList;
	}

	public void setCasePremiumSummaryList(List<CasePremiumSummaryDO> casePremiumSummaryList) {
		this.casePremiumSummaryList = casePremiumSummaryList;
	}

	public List<CaseAgentDO> getCaseAgentList() {
		return caseAgentList;
	}

	public void setCaseAgentList(List<CaseAgentDO> caseAgentList) {
		this.caseAgentList = caseAgentList;
	}

	public List<CasePaymentDO> getCasePaymentList() {
		return casePaymentList;
	}

	public void setCasePaymentList(List<CasePaymentDO> casePaymentList) {
		this.casePaymentList = casePaymentList;
	}

	public List<CaseClientDO> getCaseClientList() {
		return caseClientList;
	}

	public void setCaseClientList(List<CaseClientDO> caseClientList) {
		this.caseClientList = caseClientList;
	}

	public List<CaseExclusionDO> getCaseExclusionList() {
		return caseExclusionList;
	}

	public void setCaseExclusionList(List<CaseExclusionDO> caseExclusionList) {
		this.caseExclusionList = caseExclusionList;
	}

	public void setUwDecisionDt(Date uwDecisionDt) {
		this.uwDecisionDt = uwDecisionDt;
	}

	public String getEntityGuid() {
		return entityGuid;
	}

	public void setEntityGuid(String entityGuid) {
		this.entityGuid = entityGuid;
	}

	@Override
	public String getObjectName() {
		return CaseDO.class.getSimpleName();
	}

	public Date getRuleEffectiveDate() {
		return ruleEffectiveDate;
	}

	public void setRuleEffectiveDate(Date ruleEffectiveDate) {
		this.ruleEffectiveDate = ruleEffectiveDate;
	}
	
}
