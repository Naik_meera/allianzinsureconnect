package com.metamorphosys.insureconnect.dataobjects.transaction;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

@Entity
@Table(name = "POLICYCLIENT")
public class PolicyClientDO extends BaseDO {
	
	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="policyClientId_seq") 
	@SequenceGenerator(name="policyClientId_seq",sequenceName="POLICYCLIENTID_SEQ",allocationSize=1) Long id;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "customerId")
	private String customerId;

	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "clientTypeCd")
	private String clientTypeCd;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "titleCd")
	private String titleCd;
	
	//@Pattern(regexp = "^[A-Za-z0-9\\s]*$")
	@Column(name = "firstName")
	private String firstName;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "middleName")
	private String middleName;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "lastName")
	private String lastName;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "firstName1")
	private String firstName1;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "middleName1")
	private String middleName1;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "lastName1")
	private String lastName1;
	
	//@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "companyName")
	private String companyName;
	
	//@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "companyName1")
	private String companyName1;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "genderCd")
	private String genderCd;
	

	@Column(name = "dateOfBirth")
	private Date dateOfBirth;
	
	@Column(name = "age")
	private short age;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "maritalStatusCd")
	private String maritalStatusCd;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "occupationCd")
	private String occupationCd;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "industryCd")
	private String industryCd;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "natureOfDutyCd")
	private String natureOfDutyCd;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "occupationClassCd")
	private String occupationClassCd;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "medicalClassCd")
	private String medicalClassCd;
	
	@Column(name = "montlyIncome")
	private Double montlyIncome;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "mothersMaidenName")
	private String mothersMaidenName;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "fathersName")
	private String fathersName;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "givenName")
	private String givenName;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "legalName")
	private String legalName;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "otherName")
	private String otherName;
	
	//@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "identityTypeCD")
	private String identityTypeCD;
	
	//@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "identityNum")
	private String identityNum;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "countryOfBirthCd")
	private String countryOfBirthCd;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "citizenshipCd")
	private String citizenshipCd;
	
	@Column(name = "income")
	private Double income;
	
	@Column(name = "isPoliticalFlag")
	private boolean isPoliticalFlag;
	
	@Column(name = "isConvictedFlag")
	private boolean isConvictedFlag;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "clientStatusCd")
	private String clientStatusCd;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "deDupStatusCd")
	private String deDupStatusCd;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "roleCd")
	private String roleCd;
	
	@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "relationCd")
	private String relationCd;

	//@Pattern(regexp = "^[A-Za-z0-9]*$")
	@Column(name = "contactPersonName")
	private String contactPersonName;

	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<PolicyClientAddressDO> policyClientAddressList;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<PolicyClientContactDetailsDO> policyClientContactDetailsList;
	
	@Column(name = "idNameChk")
	private String idNameChk;
	/*@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentObject_id")
	private List<PolicyClientHistoryDO> policyClientHistoryList;*/
	
	public Long getId() {
		return id;
	}
	public String getIdNameChk() {
		return idNameChk;
	}
	public void setIdNameChk(String idNameChk) {
		this.idNameChk = idNameChk;
	}
	public Double getMontlyIncome() {
		return montlyIncome;
	}
	public void setMontlyIncome(Double montlyIncome) {
		this.montlyIncome = montlyIncome;
	}
	public String getIdentityNum() {
		return identityNum;
	}
	public void setIdentityNum(String identityNum) {
		this.identityNum = identityNum;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getClientTypeCd() {
		return clientTypeCd;
	}
	public void setClientTypeCd(String clientTypeCd) {
		this.clientTypeCd = clientTypeCd;
	}
	public String getTitleCd() {
		return titleCd;
	}
	public void setTitleCd(String titleCd) {
		this.titleCd = titleCd;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName1() {
		return firstName1;
	}
	public void setFirstName1(String firstName1) {
		this.firstName1 = firstName1;
	}
	public String getMiddleName1() {
		return middleName1;
	}
	public void setMiddleName1(String middleName1) {
		this.middleName1 = middleName1;
	}
	public String getLastName1() {
		return lastName1;
	}
	public void setLastName1(String lastName1) {
		this.lastName1 = lastName1;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCompanyName1() {
		return companyName1;
	}
	public void setCompanyName1(String companyName1) {
		this.companyName1 = companyName1;
	}
	public String getGenderCd() {
		return genderCd;
	}
	public void setGenderCd(String genderCd) {
		this.genderCd = genderCd;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public short getAge() {
		return age;
	}
	public void setAge(short age) {
		this.age = age;
	}
	public String getMaritalStatusCd() {
		return maritalStatusCd;
	}
	public void setMaritalStatusCd(String maritalStatusCd) {
		this.maritalStatusCd = maritalStatusCd;
	}
	public String getOccupationCd() {
		return occupationCd;
	}
	public void setOccupationCd(String occupationCd) {
		this.occupationCd = occupationCd;
	}
	public String getIndustryCd() {
		return industryCd;
	}
	public void setIndustryCd(String industryCd) {
		this.industryCd = industryCd;
	}
	public String getNatureOfDutyCd() {
		return natureOfDutyCd;
	}
	public void setNatureOfDutyCd(String natureOfDutyCd) {
		this.natureOfDutyCd = natureOfDutyCd;
	}
	public String getOccupationClassCd() {
		return occupationClassCd;
	}
	public void setOccupationClassCd(String occupationClassCd) {
		this.occupationClassCd = occupationClassCd;
	}
	public String getMedicalClassCd() {
		return medicalClassCd;
	}
	public void setMedicalClassCd(String medicalClassCd) {
		this.medicalClassCd = medicalClassCd;
	}
	public String getMothersMaidenName() {
		return mothersMaidenName;
	}
	public void setMothersMaidenName(String mothersMaidenName) {
		this.mothersMaidenName = mothersMaidenName;
	}
	public String getFathersName() {
		return fathersName;
	}
	public void setFathersName(String fathersName) {
		this.fathersName = fathersName;
	}
	public String getGivenName() {
		return givenName;
	}
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	public String getLegalName() {
		return legalName;
	}
	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}
	public String getOtherName() {
		return otherName;
	}
	public void setOtherName(String otherName) {
		this.otherName = otherName;
	}
	public String getIdentityTypeCD() {
		return identityTypeCD;
	}
	public void setIdentityTypeCD(String identityTypeCD) {
		this.identityTypeCD = identityTypeCD;
	}
	public String getCountryOfBirthCd() {
		return countryOfBirthCd;
	}
	public void setCountryOfBirthCd(String countryOfBirthCd) {
		this.countryOfBirthCd = countryOfBirthCd;
	}
	public String getCitizenshipCd() {
		return citizenshipCd;
	}
	public void setCitizenshipCd(String citizenshipCd) {
		this.citizenshipCd = citizenshipCd;
	}
	public Double getIncome() {
		return income;
	}
	public void setIncome(Double income) {
		this.income = income;
	}
	public boolean isPoliticalFlag() {
		return isPoliticalFlag;
	}
	public void setPoliticalFlag(boolean isPoliticalFlag) {
		this.isPoliticalFlag = isPoliticalFlag;
	}
	public boolean isConvictedFlag() {
		return isConvictedFlag;
	}
	public void setConvictedFlag(boolean isConvictedFlag) {
		this.isConvictedFlag = isConvictedFlag;
	}
	public String getClientStatusCd() {
		return clientStatusCd;
	}
	public void setClientStatusCd(String clientStatusCd) {
		this.clientStatusCd = clientStatusCd;
	}
	public String getDeDupStatusCd() {
		return deDupStatusCd;
	}
	public String getRelationCd() {
		return relationCd;
	}
	public void setRelationCd(String relationCd) {
		this.relationCd = relationCd;
	}
	public void setDeDupStatusCd(String deDupStatusCd) {
		this.deDupStatusCd = deDupStatusCd;
	}
	public List<PolicyClientAddressDO> getPolicyClientAddressList() {
		return policyClientAddressList;
	}
	public void setPolicyClientAddressList(List<PolicyClientAddressDO> policyClientAddressList) {
		this.policyClientAddressList = policyClientAddressList;
	}
	public List<PolicyClientContactDetailsDO> getPolicyClientContactDetailsList() {
		return policyClientContactDetailsList;
	}
	public void setPolicyClientContactDetailsList(List<PolicyClientContactDetailsDO> policyClientContactDetailsList) {
		this.policyClientContactDetailsList = policyClientContactDetailsList;
	}
	/*public List<PolicyClientHistoryDO> getPolicyClientHistoryList() {
		return policyClientHistoryList;
	}
	public void setPolicyClientHistoryList(List<PolicyClientHistoryDO> policyClientHistoryList) {
		this.policyClientHistoryList = policyClientHistoryList;
	}*/

	@Override
	public String getObjectName() {
		// TODO Auto-generated method stub
		return PolicyClientDO.class.getSimpleName();
	}

	public String getContactPersonName() {
		return contactPersonName;
	}
	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}
	public String getRoleCd() {
		return roleCd;
	}

	public void setRoleCd(String roleCd) {
		this.roleCd = roleCd;
	}
}
