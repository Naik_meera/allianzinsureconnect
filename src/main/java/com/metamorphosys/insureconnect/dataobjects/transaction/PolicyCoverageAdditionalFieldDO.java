package com.metamorphosys.insureconnect.dataobjects.transaction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.metamorphosys.insureconnect.dataobjects.common.AdditionalFieldDO;

@Entity
@Table(name = "POLICYCOVERAGEADDFLD")
public class PolicyCoverageAdditionalFieldDO extends AdditionalFieldDO {

	private @Id @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="policyCoverageAddFldId_seq") 
	@SequenceGenerator(name="policyCoverageAddFldId_seq",sequenceName="POLICYCOVERAGEADDFLDID_SEQ",allocationSize=1) Long id;
	
	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return id;
	}

}
