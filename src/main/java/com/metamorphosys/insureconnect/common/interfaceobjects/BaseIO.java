package com.metamorphosys.insureconnect.common.interfaceobjects;

import java.util.List;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;

public class BaseIO {

	public BaseDO baseDO;
	
	public List<BaseDO> baseDOList;

	public BaseDO serviceDO;
	private BaseDO preferenceDO;

	public BaseDO getBaseDO() {
		return baseDO;
	}

	public void setBaseDO(BaseDO baseDO) {
		this.baseDO = baseDO;
	}

	public List<BaseDO> getBaseDOList() {
		return baseDOList;
	}

	public void setBaseDOList(List<BaseDO> baseDOList) {
		this.baseDOList = baseDOList;
	}

	public BaseDO getServiceDO() {
		return serviceDO;
	}

	public void setServiceDO(BaseDO serviceDO) {
		this.serviceDO = serviceDO;
	}
	
	public BaseDO getPreferenceDO() {
		return preferenceDO;
	}
	
	public void setPreferenceDO(BaseDO preferenceDO) {
		this.preferenceDO = preferenceDO;
	}
		
}
