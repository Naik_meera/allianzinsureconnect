package com.metamorphosys.insureconnect.common.interfaceobjects;

import java.util.List;

import com.metamorphosys.insureconnect.dataobjects.common.BaseDO;
import com.metamorphosys.insureconnect.dataobjects.common.ResultDO;
import com.metamorphosys.insureconnect.dataobjects.master.RuleDO;

public class RuleIO {

	private List<BaseDO> transactionDOList;
	private List<RuleDO> ruleDOList;
	private List<ResultDO> resultDOList;
	private BaseDO serviceDO;
	private BaseDO preferenceDO;
	
	public List<BaseDO> getTransactionDOList() {
		return transactionDOList;
	}
	public void setTransactionDOList(List<BaseDO> transactionDOList) {
		this.transactionDOList = transactionDOList;
	}
	public List<RuleDO> getRuleDOList() {
		return ruleDOList;
	}
	public void setRuleDOList(List<RuleDO> ruleDOList) {
		this.ruleDOList = ruleDOList;
	}
	public List<ResultDO> getResultDOList() {
		return resultDOList;
	}
	public void setResultDOList(List<ResultDO> resultDOList) {
		this.resultDOList = resultDOList;
	}
	public BaseDO getServiceDO() {
		return serviceDO;
	}
	public void setServiceDO(BaseDO serviceDO) {
		this.serviceDO = serviceDO;
	}
	public BaseDO getPreferenceDO() {
		return preferenceDO;
	}
	public void setPreferenceDO(BaseDO preferenceDO) {
		this.preferenceDO = preferenceDO;
	}
}
