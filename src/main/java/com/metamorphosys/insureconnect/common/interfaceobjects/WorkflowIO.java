package com.metamorphosys.insureconnect.common.interfaceobjects;

import java.util.List;

import com.metamorphosys.insureconnect.dataobjects.master.RuleDO;

public class WorkflowIO extends BaseIO{

	List<RuleDO> nextTaskList;

	public List<RuleDO> getNextTaskList() {
		return nextTaskList;
	}

	public void setNextTaskList(List<RuleDO> nextTaskList) {
		this.nextTaskList = nextTaskList;
	}
	
	
}
