package com.metamorphosys.insureconnect.common.interfaceobjects;

import java.util.HashMap;

import com.metamorphosys.insureconnect.dataobjects.master.VocabularyDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.ServiceDO;

public class VocabularyIO {
	
	public VocabularyDO vocabulary;
	
	public HashMap<String, HashMap<String, String>> uIMetadataMap;
	
	public ServiceDO serviceDO;

	public HashMap<String, HashMap<String, String>> getuIMetadataMap() {
		return uIMetadataMap;
	}

	public VocabularyDO getVocabulary() {
		return vocabulary;
	}

	public void setVocabulary(VocabularyDO vocabulary) {
		this.vocabulary = vocabulary;
	}

	public void setuIMetadataMap(HashMap<String, HashMap<String, String>> uIMetadataMap) {
		this.uIMetadataMap = uIMetadataMap;
	}

	public ServiceDO getServiceDO() {
		return serviceDO;
	}

	public void setServiceDO(ServiceDO serviceDO) {
		this.serviceDO = serviceDO;
	}
	
}
