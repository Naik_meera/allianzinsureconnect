package com.metamorphosys.insureconnect.common.interfaceobjects;

public class RequestDataIO {

	private String objectName;
	
	private String object;

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}
	
	
}
