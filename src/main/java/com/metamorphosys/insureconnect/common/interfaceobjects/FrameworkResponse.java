package com.metamorphosys.insureconnect.common.interfaceobjects;

import java.util.HashMap;

public class FrameworkResponse extends HashMap<String, Object> {
	
	public static final String DATA_KEY = "data";
	public static final String ERRORCD_KEY = "errorCd";
	public static final String ERRORMSG_KEY = "errorMsg";

}
