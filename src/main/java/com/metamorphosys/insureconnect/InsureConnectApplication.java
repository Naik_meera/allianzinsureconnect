package com.metamorphosys.insureconnect;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
@EnableAsync
@ImportResource({"classpath*:config/*.xml"})
public class InsureConnectApplication {

	private static final Logger log = LoggerFactory.getLogger(InsureConnectApplication.class);

	private static ApplicationContext context = null;
	
	@RequestMapping("/")
	String home() {
		return "Welcome to MetaMorphoSys :: Insure Connect!!";
	}

	public static ApplicationContext getApplicationContext()
	{
		return InsureConnectApplication.context;
	}
	
	public static void main(String[] args) {
		InsureConnectApplication.context = SpringApplication.run(InsureConnectApplication.class, args);
		Object object = context.getBean("webCrudController");
		log.info("webCrudController: "+object);
	}
	
}
